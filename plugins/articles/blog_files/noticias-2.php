<?php include('header.php')?>
<style type="text/css">
#full-container{
width:100%;
height:auto;
display:table;
background:url(/blog_files/blog-bg.png);
}
#post{
	background:#fff;
	width:auto;
	display:Table;
	height:auto;
	padding-bottom:10px;
	padding-left: 2%;
	padding-right: 2%;
	margin-right: 2%;
	margin-top:5px;
	margin-bottom:20px;
}
@font-face {
    font-family: 'serif_black';
    src: url('/blog_files/font/seribl_-webfont.eot');
    src: url('/blog_files/font/seribl_-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blog_files/font/seribl_-webfont.woff') format('woff'),
         url('/blog_files/font/seribl_-webfont.ttf') format('truetype'),
         url('/blog_files/font/seribl_-webfont.svg#serif_blackserif_black') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'fjordone';
    src: url('/blogfiles/font/fjordone-regular-webfont.eot');
    src: url('/blogfiles/font/fjordone-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blogfiles/font/fjordone-regular-webfont.woff') format('woff'),
         url('/blogfiles/font/fjordone-regular-webfont.ttf') format('truetype'),
         url('/blogfiles/font/fjordone-regular-webfont.svg#fjordone') format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'bebas';
    src: url('/blog_files/font/bebasneue-webfont.eot');
    src: url('/blog_files/font/bebasneue-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blog_files/font/bebasneue-webfont.woff') format('woff'),
         url('/blog_files/font/bebasneue-webfont.ttf') format('truetype'),
         url('/blog_files/font/bebasneue-webfont.svg#bebas_neueregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'dinpro';
    src: url('/blog_files/font/dinpro-webfont.eot');
    src: url('/blog_files/font/dinpro-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blog_files/font/dinpro-webfont.woff') format('woff'),
         url('/blog_files/font/dinpro-webfont.ttf') format('truetype'),
         url('/blog_files/font/dinpro-webfont.svg#dinpro-regularregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
#blog{
	position:relative;
	width:100%;
}
#blog hr{
	border:0;
	height:1px;
	width:100%;
	background:url(/blog_files/dotted.png) repeat-x top left;
}
.contenidointerno .blogtitle{
	width:100%;
	height:auto;
}
.contenidointerno .blogtitle h1{
font-family: fjordone;
font-size:32px;
color: #444;
margin-top: 10px;
margin-bottom: 0;
padding: 0;
}
.contenidointerno .blogtitle a{
	color:#444;
	text-decoration:none;
}
.contenidointerno .blogtitle a:hover{
	color:#033d62;
	text-decoration:underline;
}
.contenidointerno .blogtitle p{
	font-family:dinpro;
	color:#444;
	width:98%;
	padding-left:0;
	text-align: justify;
	margin-top:5px;
	padding:0;
	width:50%;
	float:left;
}
.contenidointerno .blogtitle p.comments{
	float:right;
	width50%;
	text-align:right;
}
.readmore{
display: block;
width: 80px;
height: 24px;
/* background: #033d62; */
text-align: center;
border: 3px solid rgb(115, 115, 115);
text-decoration: none;
padding-top: 7px;
color: #999;
border-radius: 7px;
margin-bottom: 10px;
font-family: dinpro;
margin-left:31px;
font-family: dinpro;
}
.readmore:hover{
	color:#073984;
	border: 3px solid rgb(10, 70, 170);
}
.blogcontainer{
	position:relative;
	  -webkit-transition: all 1s ease;
     -moz-transition: all 0.5s ease;
       -o-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;
          transition: all 0.5s ease;
}
.blogcontainer iframe#youtube{
	width: 678px;
	height: 400px;
	float: left;
	left: 34px;
	position: relative;
}
.blogcontainer img{
	position:relative;float:right;
	-webkit-transition: all 0.5s ease;
     -moz-transition: all 0.5s ease;
       -o-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;
          transition: all 0.5s ease;
}
.blogcontainer img:hover{
	-webkit-filter: grayscale(70%);
	-webkit-box-shadow: inset 0px 0px 15px 6px rgba(50, 50, 50, 0.75);
    -moz-box-shadow:    inset 0px 0px 15px 6px rgba(50, 50, 50, 0.75);	
    box-shadow:         inset 0px 0px 15px 6px rgba(50, 50, 50, 0.75);
}
.contenidointerno{
	margin-right:20px;
}
.contenidointerno .blogdate{
	position:absolute;
	top:5px;
	left:-8px;
	width:42px;
	padding-left:10px;
	height:83px;
	display:block;
	margin:0;
	padding:0;
	color:rgb(200, 200, 200);
	text-align:center;
	margin:0;
	padding:0;
}
.contenidointerno .bloglikes{
	position: relative;
	background: url(blog_files/likes.png) no-repeat top center;
	padding-top: 23px;
	top: 143px;
	left: -13px;
	width: 42px;
	padding-left: 10px;
	height: 83px;
	display: block;
	margin: 0;
	text-align: center;
	  -webkit-transition: all 1s ease;
     -moz-transition: all 0.5s ease;
       -o-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;
          transition: all 0.5s ease;
}
.contenidointerno .bloglikes:hover{
	background: url(blog_files/likesH.png) no-repeat top center;
}
.contenidointerno .bloglikes a{
	text-decoration:none;
}
.contenidointerno .bloglikes p{
	font-family: serif_black;
	text-align: center;
	color: #999;
	display: block;
	width: 30px;
	height: 30px;
	margin: 0;
	padding: 0;
	margin-top:5px;
}
.contenidointerno .blogdate h1{
	font-family:serif_black;
	font-size:46px;
	border-bottom:1px solid #ccc;
	color:#ff9900;
	margin:0;
	padding:0;
}
.contenidointerno ul{
	list-style:none;
	position: relative;
	width:69%;
	padding:0;
	margin:0;
	display:table;
	height:35px;
	padding-left:37px;
	float:left;
}
.contenidointerno ul li{
	float:left;
	padding-left:25px;
	height:22px;
	margin-right:30px;
	font-family:dinpro;
	color:#444;
}
.contenidointerno ul li.user{
	background:url(blog_files/user.png) no-repeat left;
}
.contenidointerno ul li.chat{
	background:url(blog_files/chat.png) no-repeat left;
}
.contenidointerno ul li.folder{
	background:url(blog_files/folder.png) no-repeat left;
}
.contenidointerno ul li.tags{
	background:url(blog_files/tags.png) no-repeat left;
}
/*2ND STYLE*/
.contenidointerno ul li.chat2{
	background:url(blog_files/chat-ico2.png) no-repeat left;
	padding-left: 35px;
	height: 35px;
	line-height: 35px;
}
.contenidointerno ul li.folder2{
	background:url(blog_files/folder-ico2.png) no-repeat left;
	padding-left: 35px;
	height: 35px;
	line-height: 35px;
}
.contenidointerno ul li.tags2{
	background:url(blog_files/tag-ico2.png) no-repeat left;
	padding-left: 35px;
	height: 35px;
	line-height: 35px;
}
.contenidointerno ul li.share{
	float:right;
	margin-left:10px;
	margin-right:0;
	padding-left:0;
}
.contenidointerno ul.social{
	float:right;
	margin:0;
	padding:0;
	list-style:none;
	clear:none;
	width:25%;
	height:auto;
	top:0;
	display:table;
}
.contenidointerno ul.social li{
	position:relative;
	float:right;
	margin-left:10px;
	padding:0;
	top:0;
	margin:0;
	  -webkit-transition: all 1s ease;
     -moz-transition: all 0.5s ease;
       -o-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;
          transition: all 0.5s ease;
}
.contenidointerno ul.social li a{
	margin-left:5px;
}
.contenidointerno p{
	 font-family:dinpro;
	 color:#444;
	 width:98%;
	padding-left: 31px;
	width: 681px;
	text-align: justify;
}
.contenidointerno .categories{
	clear:both;
	display:table;	
}
.contenidointerno .categories ul{
	list-style:none;
	margin:0;
	margin-bottom:10px;
	width:100%;
}
.contenidointerno .categories ul li{
	float:left;
	background:#ccc;
	color:#999;
	min-width:90px;
	height:30px;
	display:block;
	line-height:30px;
	margin-right:5px;
	border-radius:5px;
	padding:0;
}
.contenidointerno .categories ul li a{
	text-decoration:none;
	color:#fff;
	min-width:90px;
	height:30px;
	display:block;
	line-height:30px;
	border-radius:5px;
	text-align:center;
}
.contenidointerno .categories ul li a:hover{
	background:#ff9900;
	color:#fff;
}
.contenidointerno blockquote{
	background: #efefef;
	font-family: dinpro;
	font-style: italic;
	font-size: 18px;
	color: #666;
	width: 98%;
	padding-right: 12px;
	width: 663px;
	text-align: justify;
	padding-top: 5px;
	padding-bottom: 5px;
	margin-right:0;
}
.contenidointerno .pagination{
	position:relative;
	margin:auto;
	display:table;
	margin-bottom:18px;	
}
.contenidointerno .pagination ul{
	list-style:none;
	float:left;
	margin:0;
	padding:0;
	width:auto;
}
.contenidointerno .pagination ul li{
	margin: 0;
	padding: 0;
	height: 25px;
	background: #f2f6f8; /* Old browsers */
	background: -moz-linear-gradient(top,  #f2f6f8 0%, #d8e1e7 50%, #b5c6d0 51%, #e0eff9 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f6f8), color-stop(50%,#d8e1e7), color-stop(51%,#b5c6d0), color-stop(100%,#e0eff9)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* IE10+ */
	background: linear-gradient(to bottom,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=0 ); /* IE6-9 */
	color:#666;
	height: 35px;
	width: 25px;
	text-align: center;
}
.contenidointerno .pagination ul li a{
	text-decoration:none;
	font-family:dinpro;
	color:#666;
	height: 35px;
	width: 25px;
	line-height:35px;
	display:block;
}
.contenidointerno .pagination ul li a:hover{
	text-decoration:none;
	font-family:dinpro;
	background: #3b679e; /* Old browsers */
	background: -moz-linear-gradient(top,  #3b679e 0%, #2b88d9 50%, #207cca 51%, #7db9e8 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3b679e), color-stop(50%,#2b88d9), color-stop(51%,#207cca), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #3b679e 0%,#2b88d9 50%,#207cca 51%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #3b679e 0%,#2b88d9 50%,#207cca 51%,#7db9e8 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #3b679e 0%,#2b88d9 50%,#207cca 51%,#7db9e8 100%); /* IE10+ */
	background: linear-gradient(to bottom,  #3b679e 0%,#2b88d9 50%,#207cca 51%,#7db9e8 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3b679e', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
	color:#fff;
	height: 35px;
	width: 25px;
	line-height:35px;
}
.contenidointerno .pagination ul li.first{
	-webkit-border-top-left-radius: 8px;
-webkit-border-bottom-left-radius: 8px;
-moz-border-radius-topleft: 8px;
-moz-border-radius-bottomleft: 8px;
border-top-left-radius: 8px;
border-bottom-left-radius: 8px;
border-right:1px solid #ccc;
display:block;
}
.contenidointerno .pagination ul li.first a:hover{
-webkit-border-top-left-radius: 8px;
-webkit-border-bottom-left-radius: 8px;
-moz-border-radius-topleft: 8px;
-moz-border-radius-bottomleft: 8px;
border-top-left-radius: 8px;
border-bottom-left-radius: 8px;
}
.contenidointerno .pagination ul li.last{
	-webkit-border-top-right-radius: 8px;
-webkit-border-bottom-right-radius: 8px;
-moz-border-radius-topright: 8px;
-moz-border-radius-bottomright: 8px;
border-top-right-radius: 8px;
border-bottom-right-radius: 8px;
order-left:1px solid #ccc;
}
.contenidointerno .pagination ul li.last a:hover{
-webkit-border-top-right-radius: 8px;
-webkit-border-bottom-right-radius: 8px;
-moz-border-radius-topright: 8px;
-moz-border-radius-bottomright: 8px;
border-top-right-radius: 8px;
border-bottom-right-radius: 8px;
}
.contenidointerno .pagination ul li.active{
background: #3b679e; /* Old browsers */
background: #1e5799; /* Old browsers */
background: -moz-linear-gradient(top,  #1e5799 0%, #207cca 48%, #207cca 48%, #2989d8 51%, #7db9e8 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(48%,#207cca), color-stop(48%,#207cca), color-stop(51%,#2989d8), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #1e5799 0%,#207cca 48%,#207cca 48%,#2989d8 51%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #1e5799 0%,#207cca 48%,#207cca 48%,#2989d8 51%,#7db9e8 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #1e5799 0%,#207cca 48%,#207cca 48%,#2989d8 51%,#7db9e8 100%); /* IE10+ */
background: linear-gradient(to bottom,  #1e5799 0%,#207cca 48%,#207cca 48%,#2989d8 51%,#7db9e8 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
}
.contenidointerno .pagination ul li.active a{
	color:#fff;
}
</style>
<div id="full-container">
<div class="contenedor_interno">
	<div id="blog">
		<div style="float:left;width:68%;">
		    <div class="contenidointerno">
<!--Blog content goes here-->
				<div id="post">
					<div class="blogtitle">
			    		<h1>Cities of the future? Indian PM pushes plan for 100 'smart cities'</h1>
			    		<p>By: <a href="#">Casey Tolan</a></p>
			    		<p class="comments"><a href="#">63 Comments</a></p>
			    	</div>
			    	<hr>
			    	<div style="clear:both;"></div>
			    	<div class="blogcontainer">
			    		<img src="blog_files/img2.jpg">
			    		<div class="blogdate">
				    		<img src="blog_files/date.png">
				    		<h1>18</h1>
				    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
			    		</div>
		    			<div class="bloglikes">
			    			<a href="#">
					    		<p>4</p>
					    	</a>
			    		</div>
			    	<p>Whether the proposal will be an empty slogan or the biggest city-building project in Indian history remains to be seen. Some doubt that India, where many people live without basic infrastructure, should be focused on sci-fi-esque designs.</p>
			    	<p>But several smart city projects are already in the works, including in the state of Gujarat, where Modi's record as chief minister suggests a focus on the country's urban middle class.</p>
			    	<p><strong>What makes a city smart?</strong></p>
			    	<p>While there's no single definition of a "smart" city, the term generally refers to cities using information technology to solve urban problems.</p>
			    	<p>Think of sensors monitoring water levels, energy usage, traffic flows, and security cameras, and sending that data directly to city administrators. Or apps that help residents navigate traffic, report potholes and vote. Or trash collection that's totally automated.</p>
			    	
			    	<p>For Modi, who took office in May, building new cities is a way to deal with the country's rapidly urbanizing population while also competing with China, which has made smart cities a centerpiece of its own policies.</p>
			    	<ul class="social">
			    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
						<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
			    	</ul>
			    	<a href="#" class="readmore">Read More</a>
			    	<div class="categories">
			    		<ul>
			    			<li><a href="#">World News</a></li>
			    			<li><a href="#">Smart City</a></li>
			    			<li><a href="#">India</a></li>
			    			<li><a href="#">Construction</a></li>
			    			<li><a href="#">Technology</a></li>
			    		</ul>
			    	</div>
			    </div>
<!--Blog content ends here-->
<!--Blog content goes here-->
				<div id="post">
					<div class="blogtitle">
			    		<h1>Susie Wolff makes her mark at German GP practice</h1>
			    		<p>By: <a href="#">Casey Tolan</a></p>
			    		<p class="comments"><a href="#">63 Comments</a></p>
			    	</div>
			    	<hr>
			    	<div style="clear:both;"></div>
			    	<div class="blogcontainer">
			    		<iframe id="youtube" src="//www.youtube.com/embed/72LjH3l9P44" frameborder="0"></iframe>
			    		<div class="blogdate">
				    		<img src="blog_files/date.png">
				    		<h1>17</h1>
				    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
			    		</div>
		    			<div class="bloglikes">
			    			<a href="#">
					    		<p>4</p>
					    	</a>
			    		</div>
					<p>(CNN) -- Susie Wolff put the disappointments of Silverstone behind her on Friday with an impressive run in the first free practice session ahead of Sunday's German Grand Prix.</p>
			    	<p>The Williams development driver only managed four laps during practice at the British Grand Prix a fortnight ago before engine problems curtailed her involvement.</p>
			    	<p>But it was a happier story at Hockenheim as the 31-year-old Scot completed 20 laps finishing a highly respectable 15th.</p>
			    	<p>Her best lap time of one minute 20.769 seconds was just 0.227 seconds behind Williams' driver Felipe Massa who finished the session in 11th place.</p>
			    	<blockquote>"I didn't race against many girls. Susie was one of the very few, if not the only one, I raced against. We shared a podium together a couple of times."</blockquote>
			    	<ul class="social">
			    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
	<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
			    	</ul>
			    	<a href="#" class="readmore">Read More</a>
			    	<div class="categories">
			    		<ul>
			    			<li><a href="#">World News</a></li>
			    			<li><a href="#">Sports</a></li>
			    			<li><a href="#">F1</a></li>
			    			<li><a href="#">Williams</a></li>
			    			<li><a href="#">Susie Wolff</a></li>
			    		</ul>
			    	</div>
			    </div>
<!--Blog content ends here-->
<!--Blog content goes here-->
				<div id="post">
					<div class="blogtitle">
			    	<h1>Susie Wolff makes her mark at German GP practice</h1>
			    		<p>By: Casey Tolan</p>
			    	</div>
			    	<hr>
			    	<div style="clear:both;"></div>
			    	<div class="blogcontainer">
			    		<iframe id="youtube" src="//www.youtube.com/embed/72LjH3l9P44" frameborder="0"></iframe>
			    		<div class="blogdate">
				    		<img src="blog_files/date.png">
				    		<h1>17</h1>
				    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
			    		</div>
		    			<div class="bloglikes">
			    			<a href="#">
					    		<p>0</p>
					    	</a>
			    		</div>
			    	<p>(CNN) -- Susie Wolff put the disappointments of Silverstone behind her on Friday with an impressive run in the first free practice session ahead of Sunday's German Grand Prix.</p>
			    	<p>The Williams development driver only managed four laps during practice at the British Grand Prix a fortnight ago before engine problems curtailed her involvement.</p>
			    	<p>But it was a happier story at Hockenheim as the 31-year-old Scot completed 20 laps finishing a highly respectable 15th.</p>
			    	<p>Her best lap time of one minute 20.769 seconds was just 0.227 seconds behind Williams' driver Felipe Massa who finished the session in 11th place.</p>
			    	<blockquote>"I didn't race against many girls. Susie was one of the very few, if not the only one, I raced against. We shared a podium together a couple of times."</blockquote>
			    	<ul class="social">
			    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
	<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
			    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
			    	</ul>
			    	<a href="#" class="readmore">Read More</a>
			    	<div class="categories">
			    		<ul>
			    			<li><a href="#">World News</a></li>
			    			<li><a href="#">Sports</a></li>
			    			<li><a href="#">F1</a></li>
			    			<li><a href="#">Williams</a></li>
			    			<li><a href="#">Susie Wolff</a></li>
			    		</ul>
			    	</div>
			    </div>
<!--Blog content ends here-->
<!--Blog content goes here-->
		    	<h1 class="blogtitle">Looking Back at the U.S. World Cup</h1>
		    	<div class="blogcontainer">
		    		<img src="blog_files/img4.jpg">
		    		<div class="blogdate">
			    		<img src="blog_files/date.png">
			    		<h1>8</h1>
			    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
		    		</div>
	    			<div class="bloglikes">
		    			<a href="#">
				    		<p>215</p>
				    	</a>
		    		</div>
		    	<ul>
		    		<li class="user">Eddie Gonzalez </li>
		    		<li class="chat">24 Comments</li>
		    		<li class="folder">Sports</li>
		    		<li class="tags">Sports, Soocer, USA, World Cup,</li>
		    	</ul>
		    	<p>The United States Men’s National Team captured the hearts and adoration of the entire nation during the 2014 World Cup in Brazil. While familiar faces such as goalie Tim Howard and striker Clint Dempsey came through with brilliant saves and heroic goals in clutch moments, talented young players DeAndre Yedlin and Julian Green burst onto the international scene as Americans to keep an eye on in the future.</p>
		    	<p>The first game of the group stage saw the U.S. side pull out a gutsy 2-1 victory over a tough Ghana team that knocked the Americans out of the 2010 World Cup in South Africa. Clint Dempsey scored less than a minute into the game and John Brooks Jr. sealed the deal with a beautiful finish in the 86th minute.</p>
		    	<ul class="social">
		    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
		    	</ul>
		    	<a href="#" class="readmore">Read More</a>
		    	<hr>
<!--Blog content ends here-->
<!--Blog content goes here-->
		    	<h1 class="blogtitle">As Israel warns of bigger Gaza assault, Obama cautions on 'civilian casualties'</h1>
		    	<div class="blogcontainer">
		    		<iframe id="youtube" src='http://edition.cnn.com/video/api/embed.html#/video/world/2014/07/18/newday-dnt-penhaul-israel-launches-gaza-ground-defensive.cnn' frameborder='0'></iframe>
		    		<div class="blogdate">
			    		<img src="blog_files/date.png">
			    		<h1>6</h1>
			    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
		    		</div>
	    			<div class="bloglikes">
		    			<a href="#">
				    		<p>4</p>
				    	</a>
		    		</div>
		    	<ul>
		    		<li class="user">Michael Martinez</li>
		    		<li class="chat">63 Comments</li>
		    		<li class="folder">World News</li>
		    		<li class="tags">Gaza, Israel, Middle East,Palestina</li>
		    	</ul>
		    	<p>Gaza City (CNN) -- Gaza's night skies were illuminated Friday by clashes between Israeli forces and Hamas militants throughout the 27-mile-long Palestinian territory.</p>
		    	<p>With fighting reported all along Gaza, casualties poured into Gaza City's Shifaa Hospital, including children, after Israeli artillery shelled east of the city, physicians told the Hamas-run Al-Aqsa TV.</p>
		    	<ul class="social">
		    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
		    	</ul>
		    	<a href="#" class="readmore">Read More</a>
		    	<hr>
<!--Blog content ends here-->
<!--Blog content goes here-->
		    	<h1 class="blogtitle">Disease vs drugs: How microbes are winning the war</h1>
		    	<div class="blogcontainer">
		    		<img src="blog_files/img5.jpg">
		    		<div class="blogdate">
			    		<img src="blog_files/date.png">
			    		<h1>3</h1>
			    		<span style="color:#666;font-size:16px;font-family:serif_black">JUL<br>2014</div>
		    		</div>
	    			<div class="bloglikes">
		    			<a href="#">
				    		<p>4</p>
				    	</a>
		    		</div>
		    	<ul>
		    		<li class="user">Meera Senthilingam</li>
		    		<li class="chat">0 Comments</li>
		    		<li class="folder">Health</li>
		    		<li class="tags">Drugs,microbes, Desease</li>
		    	</ul>
		    	<p>Experts warn that a global health crisis is on the horizon as drugs available for once treatable diseases are losing their edge. The bacteria, viruses and parasites behind diseases such as tuberculosis, malaria and gonorrhea are fighting back by developing resistance to our arsenal of pharmaceuticals. </p>
		    	<p>With the World Health Organization saying that we could be heading to a "post-antibiotic age," what infections are becoming a concern? </p>
		    	<ul class="social">
		    		<li><a href="#"><img src="blog_files/btn-gpH.png" onmouseover="this.src='blog_files/btn-gp.png'" onmouseout="this.src='blog_files/btn-gpH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-pnH.png" onmouseover="this.src='blog_files/btn-pn.png'" onmouseout="this.src='blog_files/btn-pnH.png'"></a></li>
<li><a href="#"><img src="blog_files/btn-fbH.png" onmouseover="this.src='blog_files/btn-fb.png'" onmouseout="this.src='blog_files/btn-fbH.png'"></a></li>
		    		<li><a href="#"><img src="blog_files/btn-twH.png" onmouseover="this.src='blog_files/btn-tw.png'" onmouseout="this.src='blog_files/btn-twH.png'"></a></li>
		    	</ul>
		    	<a href="#" class="readmore">Read More</a>
		    	<hr>
<!--Blog content ends here-->
<!--Pagination-->
<div class="pagination">
	<ul>
		<li class="first"><a href="#"><<</a></li>
		<li class="active"><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li><a href="#">5</a></li>
		<li><a href="#">6</a></li>
		<li><a href="#">7</a></li>
		<li><a href="#">8</a></li>
		<li><a href="#">9</a></li>
		<li class="last"><a href="#">>></a></li>
	</ul>
</div>
		    </div>
		</div>
	<div style="float:right;width:25%;">
   		<?php include('lateral.php');?>		
	</div>
</div>
</div>
</div>
<?php include('footer.php')?>