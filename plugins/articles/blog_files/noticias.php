<?php include('header.php')?>
<style type="text/css">
@font-face {
    font-family: 'bebas';
    src: url('/blog_files/font/bebasneue-webfont.eot');
    src: url('/blog_files/font/bebasneue-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blog_files/font/bebasneue-webfont.woff') format('woff'),
         url('/blog_files/font/bebasneue-webfont.ttf') format('truetype'),
         url('/blog_files/font/bebasneue-webfont.svg#bebas_neueregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'dinpro';
    src: url('/blog_files/font/dinpro-webfont.eot');
    src: url('/blog_files/font/dinpro-webfont.eot?#iefix') format('embedded-opentype'),
         url('/blog_files/font/dinpro-webfont.woff') format('woff'),
         url('/blog_files/font/dinpro-webfont.ttf') format('truetype'),
         url('/blog_files/font/dinpro-webfont.svg#dinpro-regularregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
#blog{
	position:relative;
	width:100%;
}
#blog hr{
	border:1px dotted #ccc;
	height:0;
	width:100%;
}
.blogtitle{
font-family:dinpro;
font-size:28px;
color:#444;
font-weight:normal;	
}
.readmore{
display: block;
width: 80px;
height: 24px;
background:#033d62;
text-align: center;
border: 3px solid rgb(2, 49, 78);
text-decoration: none;padding-top: 7px;
color: #fff;
border-radius: 7px;
margin-bottom: 10px;
font-family: dinpro;
}
.blogcontainer{position:relative;}
.blogcontainer img{position:relative;}
p.blogdate{
	position:absolute;
	top:5px;
	left:-8px;
	width:42px;
	padding-left:10px;
	height:83px;
	display:block;
	background:url(blog_files/date-bg.png);
	font-family:bebas;
	font-size:27px;
	margin:0;
	padding:0;
	color:rgb(139, 139, 139);
}
.contenidointerno ul{
	list-style:none;
	margin-top:10px;
	width:715px;
	padding:0;
	clear:both;
	display:table;
	height:25px;
}
.contenidointerno ul li{
	float:left;
	padding-left:25px;
	height:22px;
	margin-right:30px;
	font-family:dinpro;
	color:#444;
}
.contenidointerno ul li.user{
	background:url(blog_files/user.png) no-repeat left;
}
.contenidointerno ul li.chat{
	background:url(blog_files/folder.png) no-repeat left;
}
.contenidointerno ul li.folder{
	background:url(blog_files/chat.png) no-repeat left;
}
.contenidointerno ul li.share{
	float:right;
	margin-left:10px;
	margin-right:0;
	padding-left:0;
}
.contenidointerno p{
	 font-family:dinpro;
	 color:#444;
}
</style>
<div class="contenedor_interno">
	<div id="blog">
		<div style="float:left;width:75%;">
		    <div class="contenidointerno">
<!--Blog content goes here-->
		    	<h1 class="blogtitle">Proin ultricies malesuada diam id gravida.</h1>
		    	<div class="blogcontainer">
		    		<img src="blog_files/img1.jpg">
		    		<p class="blogdate">SEP<br><span style="color:#666;font-size:16px;">23</p>
		    	</div>
		    	<ul>
		    		<li class="user">Novaro</li>
		    		<li class="chat">Blog, Image</li>
		    		<li class="folder">0 Comment</li>
		    		<li class="share"><img src="blog_files/twitter.png"></li>
		    		<li class="share"><img src="blog_files/fb.png"></li>
		    		<li class="share"><img src="blog_files/gplus.png"></li>
		    	</ul>
		    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas euismod diam at commodo sagittis. Nam id molestie velit. Nunc id nisl tristique, dapibus tellus quis, dictum metus. Pellentesque id imperdiet est. Maecenas adipiscing dui at enim placerat hendrerit. Curabitur pulvinar fermentum ante id bibendum.</p>
		    	<a href="#" class="readmore">Read More</a>
		    	<hr>
<!--Blog content ends here-->
<!--Blog content goes here-->
		    	<h1 class="blogtitle">Proin ultricies malesuada diam id gravida.</h1>
		    	<div class="blogcontainer">
		    		<img src="blog_files/img1.jpg">
		    		<p class="blogdate">SEP<br><span style="color:#666;font-size:16px;">23</p>
		    	</div>
		    	<ul>
		    		<li class="user">Novaro</li>
		    		<li class="chat">Blog, Image</li>
		    		<li class="folder">0 Comment</li>
		    		<li class="share"><img src="blog_files/twitter.png"></li>
		    		<li class="share"><img src="blog_files/fb.png"></li>
		    		<li class="share"><img src="blog_files/gplus.png"></li>
		    	</ul>
		    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas euismod diam at commodo sagittis. Nam id molestie velit. Nunc id nisl tristique, dapibus tellus quis, dictum metus. Pellentesque id imperdiet est. Maecenas adipiscing dui at enim placerat hendrerit. Curabitur pulvinar fermentum ante id bibendum.</p>
		    	<a href="#" class="readmore">Read More</a>
		    	<hr>
<!--Blog content ends here-->
		    </div>
		</div>
	<div style="float:right;width:25%;">
   		<?php include('lateral.php');?>		
	</div>
</div>
</div>
<?php include('footer.php')?>