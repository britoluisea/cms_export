<?php include dirname(__FILE__)."/../".$_SESSION['THEME']."head.php"; ?>
<?php header('location:'.SERVER.'index'); ?>
	<style type="text/css">
		#error-page {
			margin: 10em auto;
			padding: 2em;
			width: 100%;
			max-width: 700px;
			background: #eee;
			-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.13);
			box-shadow: 0 1px 3px rgba(0,0,0,0.13);
		}
		ul li {
			margin-bottom: 10px;
			font-size: 14px ;
		}
		a {
			color: #21759B;
			text-decoration: none;
		}
		a:hover {
			color: #D54E21;
		}
		.button {
			background: #f7f7f7;
			border: 1px solid #cccccc;
			color: #555;
			display: inline-block;
			text-decoration: none;
			font-size: 13px;
			line-height: 26px;
			height: 28px;
			margin: 0;
			padding: 0 10px 1px;
			cursor: pointer;
			-webkit-border-radius: 3px;
			-webkit-appearance: none;
			border-radius: 3px;
			white-space: nowrap;
			-webkit-box-sizing: border-box;
			-moz-box-sizing:    border-box;
			box-sizing:         border-box;
			-webkit-box-shadow: inset 0 1px 0 #fff, 0 1px 0 rgba(0,0,0,.08);
			box-shadow: inset 0 1px 0 #fff, 0 1px 0 rgba(0,0,0,.08);
		 	vertical-align: top;
		}
		.button.button-large {
			height: 29px;
			line-height: 28px;
			padding: 0 12px;
		}
		.button:hover,
		.button:focus {
			background: #fafafa;
			border-color: #999;
			color: #222;
		}
		.button:focus  {
			-webkit-box-shadow: 1px 1px 1px rgba(0,0,0,.2);
			box-shadow: 1px 1px 1px rgba(0,0,0,.2);
		}
		.button:active {
			background: #eee;
			border-color: #999;
			color: #333;
			-webkit-box-shadow: inset 0 2px 5px -3px rgba( 0, 0, 0, 0.5 );
		 	box-shadow: inset 0 2px 5px -3px rgba( 0, 0, 0, 0.5 );
		}
	</style>
	<div id="error-page" class="text-center">
		<h1><strong>Error 
			<?php 
				if((isset($errorDocument) && $errorDocument==403) || $url_request['request_url']=='error-403'){?> 403 Forbidden  <?php } 
				if((isset($errorDocument) && $errorDocument==500) || $url_request['request_url']=='error-500'){?> 500 Server<?php } 
				if((isset($errorDocument) && $errorDocument==503) || $url_request['request_url']=='error-503'){?> 503 Server Unavailable <?php } 
				else{ ?> 404  Page not found <?php }
				?></strong></h1>
		<a href="<?=SERVER?>index">[Back to home]</a>
	</div>
<?php include dirname(__FILE__)."/../".$_SESSION['THEME']."foot.php"; ?>