<?php
require_once("header_solo.php");
?>
<?php
	if (empty($opc))	
	{
		$opc = 1;
	}
?>
<!-- Bootstrap CSS Toolkit styles -->
<link rel="stylesheet" href="filemultiupload/css/bootstrap.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="filemultiupload/css/style.css">
<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
<link rel="stylesheet" href="filemultiupload/css/bootstrap-responsive.css">
<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="filemultiupload/css/bootstrap-ie6.min.css"><![endif]-->
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="filemultiupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="filemultiupload/css/jquery.fileupload-ui-noscript.css"></noscript>
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]><script src="filemultiupload/js/html5.js"></script><![endif]-->
<table border="0" align="center" cellpadding="0" cellspacing="0" id="Tabla_01">
	<tr>
		<td>
		  <img src="img/box_01.png" width="20" height="60" alt=""></td>
		<td background="img/box_02_blanco.png"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td><div class="cat_titulo_box">Add Photos</div></td>
		    <td width="25"><a href="material-list-img.php?i=<?=$i?>" target="_top"><img src="img/eliminar.png" width="24" height="24" /></a></td>
	      </tr>
	    </table></td>
		<td>
			<img src="img/box_03.png" width="20" height="60" alt=""></td>
	</tr>
	<tr>
		<td background="img/box_04.png">
			<img src="img/box_04.png" width="20" height="101" alt=""></td>
		<td bgcolor="#FFFFFF">
        
        <div>
        
            <!-- The file upload form used as target for the file upload widget -->
		    <form id="fileupload" action="filemultiupload/server/php_materials/index.php?i=<?=$_GET['i']?>" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="span7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>Add files...</span>                     
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancel upload</span>
                </button>
                <button name="button2" type="button" class="btn" id="button2" onclick="self.parent.tb_remove();self.parent.location.reload();">
                <i class="icon-share-alt"></i> Cancel
                </button>
            </div>
            <!-- The global progress information -->
            <div class="span5 fileupload-progress fade">
                <!-- The global progress bar --><!-- The extended global progress information -->
              <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The loading indicator is shown during file processing -->
        <div class="fileupload-loading"></div>
        <br>
        <!-- The table listing the files available for upload/download -->        

        <div style="overflow-x: hidden;	overflow-y: scroll;  height: 235px;    white-space:nowrap">
        <table class="table table-striped"><tbody class="files" ></tbody></table>
        </div>
    </form>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview" width = "50"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td width = "20">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td width = "20">{% if (!i) { %}
            <button class="btn btn-warning cancel">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancel</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">        
    </tr>
{% } %}
</script>


<script src="filemultiupload/js/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="filemultiupload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="filemultiupload/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="filemultiupload/js/load-image.min.js"></script>
<!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
<script src="filemultiupload/js/bootstrap.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="filemultiupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="filemultiupload/js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="filemultiupload/js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="filemultiupload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!--<script src="filemultiupload/js/main.js"></script>-->
<script>
$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'filemultiupload/server/php_materials/index.php?i=<?=$_GET['i']?>',
    });
	
    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
	
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/filemultiupload/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.com' ||
            window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            process: [
                {
                    action: 'load',
                    fileTypes: /^image\/(gif|jpeg|png)$/,
                    maxFileSize: 20000000 // 20MB
                },
                {
                    action: 'resize',
                    maxWidth: 1440,
                    maxHeight: 900
                },
                {
                    action: 'save'
                }
            ]
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<span class="alert alert-error"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
		
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function (result) {
            $(this).removeClass('fileupload-processing');
			
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, null, {result: result});
        });
    }

});

</script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="filemultiupload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
		<!--uploadUrl: "inventory_projects_lista.php?i=<?=$i?>"-->
        <table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td></td>
  </tr>
</table>

          </div>
        
        </td>
		<td background="img/box_06.png">
			<img src="img/box_06.png" width="20" height="101" alt=""></td>
	</tr>
	<tr>
		<td>
    <img src="img/box_07.png" width="20" height="15" alt=""></td>
		<td background="img/box_08.png">
			<img src="img/box_08.png" width="181" height="15" alt=""></td>
		<td>
			<img src="img/box_09.png" width="20" height="15" alt=""></td>
	</tr>
</table>