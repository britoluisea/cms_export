<?php
require_once("header.php");
@import_request_variables("gpc");
?>
<?php
	if (empty($opc))	
	{
		$opc = 1;
		$wfecha = date("m/d/Y");
	}
?>
<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="ckfinder/ckfinder.js" type="text/javascript"></script>
<script src="inc_ajax.js" language="JavaScript"></script>
	<link type="text/css" href="arc_calendar/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="arc_calendar/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="arc_calendar/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="arc_calendar/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="arc_calendar/ui/jquery.ui.datepicker.js"></script>
<!--	<link type="text/css" href="arc_calendar/demos.css" rel="stylesheet" />-->
	<script type="text/javascript">
	$(function() {
		$( "#fec1" ).datepicker({
			dateFormat: 'mm/dd/yy',
			showOn: "button",
			buttonImage: "img/calendar.gif",
			buttonImageOnly: true
		});
	});
	
	</script>

<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />

<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<table class="height100" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%" valign="top" class="">
    
 <?php include_once 'preferences-material.php'; ?>

</td>
    <td width="100%" valign="top"><section>
    <div id="contenido">
    

      
   <form action="gampro.php" method="post" enctype="multipart/form-data" name="form1">
  <?php
  
	$sql_1="select * from material_list where idx='$i' "; //Devuelve todos los Idiomas
	$consulta_1=mysql_query($sql_1);
	$atabla=array();
	while($fila_1=mysql_fetch_assoc($consulta_1))
	{
		$atabla['en']=$fila_1;
		//	print_r($atabla)."<br>";
		$opc = 2;
	//	$wfecha = strftime('%m/%d/%Y', strtotime($atabla['en']["fecreg"]));
		$i = $atabla['en']["idx"];
		$g = $atabla['en']["idxmaterial"];
	}

?>
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td>
    <div class="navipage"><?php
    if($opc == 1)
	{
		echo "[ New Material ]";
	}else{
		echo "[ Edit Material ]";
	}
	?></div>
    </td>
    <td>
      
    
    <table border="0" align="right" cellpadding="5" cellspacing="0">
      <tr>
        <td><input name="button" type="submit" class="clase_btn save_btn" id="button2" tabindex="35" value="Save" /></td>
        <td><a class="clase_btn cancel_btn" href="material-list.php?g=<?=$g?>">Cancel</a></td>
        </tr>
    </table>
    
    
    
       </td>
  </tr>
</table>
  
<input type="hidden" name="ope" value="material-list"/>
<input type="hidden" name="opc" value="<?=$opc?>" />
<input type="hidden" name="i" value="<?=$i?>" />

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <div class="boxderecha curva10">
    <table width="96%" border="0" align="center">
    <tr>
      <td colspan="3">&nbsp;</td>
      </tr>
    <?php
    if($opc == 1)
	{
	?>
    <?php
	}
	else
	{
	?>
    <?php
	}
	?>
    <tr>
      <td> Orginal Name</td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="nombre" type="text" class="caja_texto" id="nombre" value="<?=$atabla['en']["nombre"]?>" size="80" />
        </span></td>
    </tr>
    <tr>
      <td> Material</td>
      <td><span class="asterisco">*</span></td>
      <td>
      <span id="spryselect1">
      <select name="material" class="caja_texto" id="material" style="width:250px;" onchange='cargarContenido("inc_category.php","div_category","valorget=1","idx="+document.getElementById("group").value+"");'>
            <option value=""> &#8212; Select &#8212; </option>
    <?php
	$sql="select * from material_category order by nombre";
	//$sql="select * from subcategoria where lg='$lg' order by nombre";
	$consulta=mysql_query($sql);
  	while($fila=mysql_fetch_array($consulta))
	{
	?>
   	 <option value="<?=$fila["idx"]?>"
    <?php
    if($fila["idx"]==$g)
    {
   	 echo "selected='selected'";
    }
    ?>>
    <?=$fila["nombre"]?>
   	 </option>
    <?php
    }
	?>
      </select>
      </span>
      </td>
    </tr>
    <tr>
      <td> Country of origin</td>
      <td>&nbsp;</td>
      <td><input name="country" type="text" class="caja_texto" id="country" value="<?=$atabla['en']["country"]?>" size="80" /></td>
    </tr>
    <tr>
      <td> Pattern</td>
      <td>&nbsp;</td>
      <td>
      <div id="div_category">
      <span id="spryselect2">
      <select name="pattern" class="caja_texto" id="pattern" style="width:250px;">
            <option value=""> &#8212; Choose Pattern &#8212; </option>
    <?php
	$sql="select * from material_pattern order by nombre";
	//$sql="select * from subcategoria where lg='$lg' order by nombre";
	$consulta=mysql_query($sql);
  	while($fila=mysql_fetch_array($consulta))
	{
	?>
   	 <option value="<?=$fila["idx"]?>"
    <?php
    if($fila["idx"]==$atabla['en']["idxpattern"])
    {
   	 echo "selected='selected'";
    }
    ?>>
    <?=$fila["nombre"]?>
   	 </option>
    <?php
    }
	?>
      </select>
      </span>
      </div>
      </td>
    </tr>
    <tr>
      <td> Colors</td>
      <td>&nbsp;</td>
      <td><input name="colors" type="text" class="caja_texto" id="colors" value="<?=$atabla['en']["colores"]?>" size="80" /></td>
    </tr>
    <tr>
      <td width="110"> Application</td>
      <td width="10">&nbsp;</td>
      <td><input name="application" type="text" class="caja_texto" id="application" value="<?=$atabla['en']["application"]?>" size="80" />
        </td>
    </tr>
    <tr>
      <td>Cover<br />
        <em><strong>Minimum size: 446px - 302px </strong></em></td>
      <td>&nbsp;</td>
      <td>
      
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
		   <?php
				$img_width = 'width="50"'; // default
				$img_height = 'height="50"'; // default
				if($atabla['en']["imagen"]=="")
				{
					$wimg="../img/img_blanco.png";
				}
				else
				{
					$wimg="../".$_SESSION['value_admin_nomcar']."/material/".$atabla['en']["nomcar"]."/small/".$atabla['en']["imagen"];
					$img = @imagecreatefromjpeg($wimg);

					if ($img)
					{
						$img_width = imagesx($img);
						$img_height = imagesy($img);
						if ($img_width == $img_height)
						{
						$img_width = 'width="50"';
						$img_height = '';									
						}
						elseif ($img_width > $img_height)
						{
						$img_width = 'width="50"';
						$img_height = '';																
						}
						else
						{
						$img_width = '';
						$img_height = 'height="50"';																
						}						
					}								
				}			
				?>   
              	<div><img src="<?php echo $wimg ?>" <?=$img_width?> <?=$img_height?> style="border:1px solid #999999; padding:5px;" />				</div>
         <input type="hidden" name="img[en]" id="img[en]" value="<?=$atabla['en']["imagen"]?>"/>
         </td>
          <td>&nbsp;</td>
          <td><input type="file" name="foto[en]" id="foto[en]" size="7" /></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td>Status</td>
      <td>&nbsp;</td>
      <td>
        <select name="activo" class="caja_texto" style="width:150px">
          <option value="1"
         <?php
         if($atabla['en']["activo"]=="1")
		 {
		 	echo "selected='selected'";
		 }
         ?>>Online</option>
          <option value="0" <?php
         if($atabla['en']["activo"]=="0")
		 {
		 	echo "selected='selected'";
		 }
         ?>>Offline</option>  
          </select>      </td>
    </tr>
    <tr>
      <td> Comments</td>
      <td>&nbsp;</td>
      <td></td>
    </tr>
	 <tr>
	   <td colspan="3">
       <textarea name="FCKeditor1" id="FCKeditor1" ><?=$atabla['en']["comentarios"]?></textarea>
        <script type="text/javascript">
        var newCKEdit = CKEDITOR.replace('FCKeditor1',{height:'300',allowedContent:true});
        CKFinder.setupCKEditor(newCKEdit, 'ckfinder/');
        </script>
       <?php
		//$oFCKeditor = new FCKeditor('FCKeditor1');
		//$oFCKeditor->BasePath = 'FCKeditor/';
		//$oFCKeditor->Height = '300';
		//$oFCKeditor->Width = '100%';
//		//										$oFCKeditor->ToolbarSet	= 'Basic' ;
		//$oFCKeditor->Value = $atabla['en']["comentarios"];
		//$oFCKeditor->Create();
	 ?>
       </td>
	   </tr>
	 <tr>
	   <td colspan="3"><hr></td>
	   </tr>
	 <tr>
	   <td colspan="3"><div style="font-size:24px; font-weight:bold;">SEO</div></td>
	   </tr>
	 <tr>
	   <td colspan="3">Title</td>
	   </tr>
	 <tr>
	   <td colspan="3"><input name="title" type="text" id="title" class="caja_texto" value="<?=$atabla['en']["title"]?>" size="90"  style="width:100%"/></td>
	   </tr>
	 <tr>
	   <td colspan="3">
       Keywords</td>
	   </tr>
	 <tr>
	   <td colspan="3">
<textarea name="keywords" cols="69" rows="6" class="caja_texto" id="keywords" style="width:100%"><?=$atabla['en']["keywords"]?></textarea></td>
	   </tr>
	 <tr>
	   <td colspan="3">Description</td>
	   </tr>
	 <tr>
	   <td colspan="3">
       <textarea name="description" cols="69" rows="6" class="caja_texto" id="description" style="width:100%"><?=$atabla['en']["description"]?></textarea></td>
	   </tr>
    </table>
  </div>
    </td>
  </tr>
</table>

<table border="0" align="center" cellpadding="5" cellspacing="0">
      <tr>
        <td><input name="button" type="submit" class="clase_btn save_btn" id="button2" tabindex="35" value="Save" /></td>
        <td><a class="clase_btn cancel_btn" href="material-list.php?g=<?=$g?>">Cancel</a></td>
        </tr>
    </table>

<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
//-->
</script>

</form>
    
    
    </div>


  </section>
</td>

  </tr>
</table>

<?php
include("footer.php");
?>