<?php
session_start();
include(dirname(__FILE__)."/../varRutas.php");
include dirname(__FILE__)."/../cn/cnx.php";
include dirname(__FILE__)."/system/languages/languages.php";
date_default_timezone_set('America/New_York');
$_SESSION['lang']=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en';
$widxamusuario = isset($_SESSION['value_admin_idx']) ?$_SESSION['value_admin_idx'] : '';
$widxamusuario_pri = isset($_SESSION['value_admin_idxamusuario']) ? $_SESSION['value_admin_idxamusuario'] : '';

if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin']))
{
    header("Location: ".SERVER_P."index.php");
    exit();
}
$cookie_name = 'PHPSESSID';
$valor = $_COOKIE[$cookie_name];
//setcookie($cookie_name, $valor, strtotime("+6 year"), "/");
setcookie($cookie_name, $valor, strtotime('today 23:59'), '/'); //24 horas activo
include('paginator.class.php');
$f = explode("/", $_SERVER['PHP_SELF']);
$f_pagina = $f[count($f) - 1];
$userlevel=0;
$sql2 = "SELECT userlevel FROM usuarios where activo=1 and idx = '$widxamusuario'  limit 0,1";
$res2=ejecutar($sql2);
if ($col2=fetchArray($res2))
{   
    $userlevel  = $col2['userlevel'];

}

$wvar_permiso_si_o_no = 0;          
$acl2=array();
$acl2[]='home.php';
$acl2[]='access-denied.php';
$acl2[]='cerrar.php';
$acl2[]='index.php';
$acl2[]='form-c.php';
$acl2[]='form_c.php';
$acl2[]='form-f.php';
$acl2[]='form_f.php';
$acl2[]='form-m.php';
$acl2[]='form-d.php';
$acl2[]='form-e.php';
$acl2[]='form-v.php';
$acl2[]='banner/index.php';
$acl2[]='gallery-f.php';
$acl2[]='gallery-error.php';
$acl2[]='img.php';
$acl2[]='form/index.php';
$acl2[]='review/index.php';
$acl2[]='edit.php';
$acl2[]='widget-scroll.php';
$acl2[]='widget-c.php';
$acl2[]='options-form.php';
$acl2[]='project-f.php';
$acl2[]='project-img.php';
$acl2[]='error.php';
$acl2[]='status.php';
$acl2[]='status-f.php';
$acl2[]='category.php';
$acl2[]='category-f.php';
$acl2[]='group.php';
$acl2[]='group-f.php';
$acl2[]='widget.php';
$acl2[]='options.php';
$acl2[]='articles-setting.php';
$acl2[]='gallery-list-img-n.php';
////webmaster
$acl2[]='f.php';
$acl2[]='info.php';
$acl2[]='service.php';
$acl2[]='link.php';
$acl2[]='label.php';
$acl2[]='privileges.php';
$acl2[]='gallery.php';
$acl2[]='gallery-list-imagen.php';
$acl2[]='project-list-imagen.php';
$acl2[]='e.php';
$acl2[]='email.php';
$acl2[]='information.php';
$acl2[]='inactive.php';
$acl2[]='box.php';
$acl2[]='b.php';
$acl2[]='save-article.php';
$acl2[]='articles-category-n.php';
$acl2[]='ifra_decrip.php';
$acl2[]='pages.php';
$acl2[]='boxes.php';
$acl2[]='delete.php';
$acl2[]='form-add.php';
$acl2[]='form-add-pages.php';
$acl2[]='addVideo.php';
$acl2[]='install.php';
$acl2[]='infoTheme.php';
$acl2[]='menu-website.php';
$acl2[]='menu-n.php';
$acl2[]='menu-list.php';
$acl2[]='menu-list-n.php';
$acl2[]='_content-ed.php';
$acl2[]='notification.php';
$acl2[]='users-privileges.php';
////webmaster
$acl2[]='page-builder-group-n.php';
$acl2[]='language.php';
if ($userlevel == '998' or $userlevel == '997') { // Webmaster - support
   // $sql_perm_menu = "select *  from usuariomenu_permiso  where idxusuariosec = '1' "; //Devuelve todos los Idiomas           
    $sql_perm_menu = "
    select usuariomenu_permiso_category.*, usuariomenu_permiso.idxpermisocategory as idxpermisocategory
    from
    usuariomenu_permiso_category,usuariomenu_permiso 
    where 
    usuariomenu_permiso_category.idx = usuariomenu_permiso.idxpermisocategory and
    usuariomenu_permiso_category.activousersec=1 and
    usuariomenu_permiso.idxusuariosec = '1'";
    $acl2[]='preferences-gen-wm-web-content.php';
    $acl2[]='preferences-gen-wm-webmaster.php';
    $acl2[]='preferences-gen-wm-webmaster-n.php';
    $acl2[]='preferences-gen-wm-menu-privileges.php';
    $acl2[]='preferences-gen-wm-scroll.php';
    $acl2[]='preferences-gen-wm-support-gallery-options.php';
    $acl2[]='my-account.php';
    $acl2[]='change-password.php';
    $acl2[]='activity-log-updates.php';
}

if ($userlevel == '999') { // Account
   // $sql_perm_menu = "select *  from usuariomenu_permiso  where idxusuariosec = '1' "; //Devuelve todos los Idiomas           
    $sql_perm_menu = "
    select usuariomenu_permiso_category.*, usuariomenu_permiso.idxpermisocategory as idxpermisocategory
    from
    usuariomenu_permiso_category,usuariomenu_permiso 
    where 
    usuariomenu_permiso_category.idx = usuariomenu_permiso.idxpermisocategory and
    usuariomenu_permiso_category.activousersec=1 and
    usuariomenu_permiso.idxusuariosec = '1'";
    $acl2[]='my-account.php';
    $acl2[]='change-password.php';
}

if ($userlevel == '1000') { // Users secundary
    $sql_perm_menu = "
	select usuariomenu_permiso_category.*, usuariosec_permiso.idxpermisocategory as idxpermisocategory,usuariomenu_permiso.*
	from
	usuariomenu_permiso_category,usuariosec_permiso,usuariomenu_permiso
	where 
	usuariomenu_permiso_category.idx = usuariosec_permiso.idxpermisocategory and
	usuariomenu_permiso_category.activousersec=1 and
    usuariomenu_permiso.idxpermisocategory= usuariosec_permiso.idxpermisocategory and
    usuariosec_permiso.idxusuariosec = $widxamusuario ";
  
 }
if ($userlevel == '1') {
    //$sql_perm_menu = "select idxpermisocategory  from usuariosec_permiso  where idxusuariosec = $widxamusuario_pri "; //Devuelve todos los Idiomas            
    $sql_perm_menu = "
    select usuariomenu_permiso_category.*, usuariosec_permiso.idxpermisocategory as idxpermisocategory,usuariomenu_permiso.*
    from
    usuariomenu_permiso_category,usuariosec_permiso,usuariomenu_permiso
    where 
    usuariomenu_permiso_category.idx = usuariosec_permiso.idxpermisocategory and
    usuariomenu_permiso_category.activousersec=1 and
    usuariomenu_permiso.idxpermisocategory= usuariosec_permiso.idxpermisocategory and
    usuariosec_permiso.idxusuariosec = $widxamusuario_pri ";
}
if ($userlevel == '2') {
    $sql_perm_menu = "
    select usuariomenu_permiso_category.*, usuariosec_permiso.idxpermisocategory as idxpermisocategory,usuariomenu_permiso.*
    from
    usuariomenu_permiso_category,usuariosec_permiso,usuariomenu_permiso
    where 
    usuariomenu_permiso_category.idx = usuariosec_permiso.idxpermisocategory and
    usuariomenu_permiso_category.activousersec_tipo2=1 and
    usuariomenu_permiso.idxpermisocategory= usuariosec_permiso.idxpermisocategory and
    usuariosec_permiso.idxusuariosec = $widxamusuario_pri ";        
}

$consulta_perm_menu = ejecutar($sql_perm_menu);
//echo'SQL <br>';
//echo 'level = '.$userlevel.'<br>';
//echo $sql_perm_menu.'<br>';

$atabla_perm_menu = array();
while ($fila_perm_menu = fetchAssoc($consulta_perm_menu)) {
    $atabla_perm_menu[$fila_perm_menu['idxpermisocategory']] = $fila_perm_menu;
    //echo "--->".$fila_perm_menu['url'].'<br>';
}
$acl=array();
$acl[]='home.php';
$acl[]='index.php';
foreach ($atabla_perm_menu as $key => $value) {
            $url=$atabla_perm_menu[$key]['url'];
            $files=explode(',',$url);
            if(count($files) > 0){
                foreach ($files as $f) {
                    $acl[]=$f;
                    $acl2[]=$f;
                }
            }
            else{
                $acl[]=$url;
                $acl2[]=$url;
            }
        }

$uri = explode('/', $_SERVER["REQUEST_URI"]);
$buscarGet = explode('?', $uri[(count($uri)-1)] );
if(count($buscarGet) > 0){ $file = $buscarGet[0]; }
else{ $file = $uri[(count($uri)-1)];   }        
//echo'file = '.$file.'<br>';

if ( $userlevel == '1000' ){    
    //echo'<pre>';print_r($acl);echo'</pre>';    
    if (!in_array($file, $acl)) {
        header("Location: access-denied.php");
        //echo'mal= '.$file;
        exit();
    }
  
}

if ($userlevel != '999' && $userlevel == '998' ){
    if (!in_array($file, $acl2)) {
        header("Location: access-denied.php");
        //echo'mal= '.$file.'<br>';
        //echo'<pre>';print_r($acl2);echo'</pre><br>';  
        exit();
    }
}

        $wwhite_label_image = "";
        $wwhite_label_footer = "";
        $usarlogopropio=0;
        $sql_1 = "select white_label_image, white_label_footer, usarlogopropio, logopropio, colorheader, accountname from usuarioscompany where idx = '1' "; //Devuelve todos los Idiomas
        $consulta_1 = ejecutar($sql_1);
        while ($fila_1 = fetchAssoc($consulta_1)) {
            $atabla = $fila_1;
            $wwhite_label_image = $atabla["white_label_image"];
            $wwhite_label_footer = $atabla["white_label_footer"];
            $usarlogopropio=$atabla["usarlogopropio"];
        }
        $logo_cms=SERVER."imgcms/system/logo-cms.png";
        $logo_principal='';
        if ($usarlogopropio == 0) {  $logo_principal = $logo_cms; } 
        else 
        {
            if ($atabla["logopropio"] == "") { $logo_principal = $logo_cms; } 
            else { $logo_principal = SERVER."imgcms/" . $_SESSION['value_admin_nomcar'] . '/' . $atabla["logopropio"];}
        }
        include dirname(__FILE__)."/themes/themes.php";
?>        
