<?php include("header.php");?>
<br><br>
<table border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
    <td>
      <h1 align="center"><?=isset($dataGlobal['access_denied']) ? $dataGlobal['access_denied'] : 'Access Denied'?></h1>
      <div>
        <div align="center">
          <?=isset($dataGlobal['access_denied_MSJ']) ? $dataGlobal['access_denied_MSJ'] : 'Your account is with some restrictions. '?><br>
          <?=isset($dataGlobal['access_denied_MSJ2']) ? $dataGlobal['access_denied_MSJ2'] : 'Please contact the Administrator for more information.'?>
        </div>
      </div>
    </td>
  </tr>
</table>
<?php include("footer.php");?>