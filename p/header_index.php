<?php
session_start();
include dirname(__FILE__)."/../varRutas.php";
include dirname(__FILE__)."/../cn/cnx.php";
include dirname(__FILE__)."/system/languages/languages.php";
if(isset($_SESSION['value_admin_idx']))
{
    //echo'si exite la session'; exit;
    $widxamusuario      = $_SESSION['value_admin_idx'];          
    $widxamusuario_pri  = $_SESSION['value_admin_idxamusuario'];
    if(!empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] == true or !empty($_SESSION['var_sec_admin']))
    {
        header("Location: home.php");
        exit(); 
    }
}
else
{
    include 'authSession.php';
    if(getAuthSession()){header("Location: home.php");}
}
        $wwhite_label_image = "";
        $wwhite_label_footer = "";
        $usarlogopropio=0;
        $sql_1 = "select white_label_image, white_label_footer, usarlogopropio, logopropio, colorheader, accountname from usuarioscompany where idx = '1' "; //Devuelve todos los Idiomas
        $consulta_1 = ejecutar($sql_1);
        while ($fila_1 = fetchAssoc($consulta_1)) 
        {
            $atabla = $fila_1;
            $wwhite_label_image = $atabla["white_label_image"];
            $wwhite_label_footer = $atabla["white_label_footer"];
            $usarlogopropio=$atabla["usarlogopropio"];
        }
        $logo_cms=IMG_CMS."system/logo-cms.png";
        $logo_principal='';
        if ($usarlogopropio == 0) {  $logo_principal = $logo_cms; } 
        else 
        {
            if ($atabla["logopropio"] == "") { $logo_principal = $logo_cms; } 
            else { $logo_principal = IMG_CMS. $_SESSION['value_admin_nomcar'] . '/' . $atabla["logopropio"];}
        }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?=IMG_CMS?>system/icon-cms.png" type="image/png" sizes="16x16">
        <title>CMS</title>
        <meta name="author" content="marketea.com">
        <meta name = "robots" content = "noindex, nofollow">
        <base href="<?=SERVER_P?>" />
        <link href="<?=CSS_P?>bootstrap.min.css" rel="stylesheet">
        <link href="<?=CSS_P?>font-awesome.min.css" rel="stylesheet">
        <link href="<?=CSS_P?>simple-sidebar.css" rel="stylesheet">
        <link href="<?=CSS_P?>customize.css" rel="stylesheet">
    </head>
    <body class="bg-dark">
        <nav class="navbar navbar-default no-margin navbar-fixed-top" id="navTop">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand " style="width: 170px;">
                    <a class="navbar-brand" href="<?=URL_INDEX_P?>" style="padding: 5px 15px;"><img src="<?=$logo_cms?>" alt="logo" class="logo" style="width:160px;"></a>
                </div>
                <!-- navbar-header-->
            </div>
        </nav>