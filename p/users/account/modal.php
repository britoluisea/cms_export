<div class="modal fade" id="ventana1" tabindex="-1" role="dialog" aria-labelledby="formAddBannerLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></h4>
      </div>
      <div class="modal-body"> 
		<?=isset($dataGlobal['label_del_record']) ? $dataGlobal['label_del_record'] : 'Delete Record'?>: <input name="bookId" type="text" style="border:none; width:70%" value=""  />
      </div>
      <div class="modal-footer">
      	<a href="#" class="btn btn-small btn-danger danger"><i class="fa fa-trash-o bigger-120"></i> <?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></a>
      	<button tyle="button" class="btn btn-small" data-dismiss="modal"><i class="fa fa-times"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?></button>
      </div>
    </div>
  </div>
</div>
 <script>
$('#ventana1').on('show.bs.modal', function(e) {
    var bookId = $(e.relatedTarget).data('book-id');
    $(e.currentTarget).find('input[name="bookId"]').val(bookId);
	$(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
    $('.debug-url').html('Delete URL: <strong>' + $(this).find('.danger').attr('href') + '</strong>');
});
</script>
