<?php 
include("project_f.php");
$error_exten=isset($dataGlobal['error_exten']) ? $dataGlobal['error_exten'] : 'Extensions, PNG, GIF are not allowed.';
  $label_error=isset($dataGlobal['label_error']) ? $dataGlobal['label_error'] : 'Error';
  $label_success=isset($dataGlobal['label_success']) ? $dataGlobal['label_success'] : 'Success';
  $msj_success_js=isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
if($msj == 1) {
    echo'<script>$(function() {
    alerError("'.$label_error.'", "'.$error_exten.'", "6000");});</script>';
}else{   
  if($Result1 == true){
    //echo $updateSQL; exit;
    echo'<script>$(function() {
    alerValid("'.$label_success.'", "'.$msj_success_js.'", "6000");
    window.location="index.php";});</script>';
  }
}
?>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link href="<?=PLUGINS_P?>SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" >
<script src="<?=PLUGINS_P?>SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
<script>
function marketing(opc){
  if (opc == '1'){
    document.getElementById("div_marketing1").style.display="block";
    document.getElementById("div_marketing2").style.display="none";
  }else{
    document.getElementById("div_marketing1").style.display="none";
    document.getElementById("div_marketing2").style.display="block";
  } 
}
</script>
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td width="1%" valign="top">
 <?php include_once ('menu.php'); ?>
    </td>
    <td style="padding: 10px;">
<form method="post" name="form1" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data">
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td>
<div class="navipage">
<?php if(empty($_GET['i'])){  ?>
<?=isset($dataGlobal['new_projects']) ? '[ '.$dataGlobal['new_projects'].' ]' : '[ New Projects ]'?>
<input type="hidden" name="MM_insert" value="form1">
<input type="hidden" name="validar" value="index">
<?php }else{ ?>
<?=isset($dataGlobal['edit_projects']) ? '[ '.$dataGlobal['edit_projects'].' ]' : '[ Edit Projects ]'?>
  <input type="hidden" name="MM_update" value="form1">
<?php }?>
    </div>
  </td>
    <td>
    <table border="0" align="right" cellpadding="5" cellspacing="0">
      <tr>
        <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>" /></td>
        <td><a class="btn btn-small btn-default" href="index.php"><?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancle'?></a></td>
        </tr>
    </table>
       </td>
  </tr>
</table>
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="96%" border="0" align="center">
    <tr>
      <td><?=isset($dataGlobal['label_group']) ? $dataGlobal['label_group'] : 'Group'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="spryselect1">
        <select name="group" style="width:250px">
          <?php if(empty($_GET['i'])){  ?>
            <option><?=isset($dataGlobal['_select_']) ? $dataGlobal['_select_'] : '- Select -'?></option>
          <?php }
          $c = ejecutar($query_c);
          while ($row_g = fetchAssoc($c)){  ?>
            <option value="<?php echo $row_g['idx']?>"  <?php if(isset($row_u['idxgrupo']) && $row_g['idx']==$row_u['idxgrupo']){echo'selected';}?>><?=$row_g['nombre']?></option>
            <?php
          } ?>
        </select></span>
    </td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['label_category']) ? $dataGlobal['label_category'] : 'Category'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="spryselect2">
        <select name="category" style="width:250px">
         <?php  
         if(empty($_GET['i']))
          {  ?>
            <option value="<?php echo $row_e['idx']?>"><?php echo $row_e['nombre']?> </option>
            <option value="0"><?=isset($dataGlobal['uncategorized']) ? $dataGlobal['uncategorized'] : 'Uncategorized'?></option>
            <?php 
          }
          else
          {  ?>
            <option value="0"><?=isset($dataGlobal['uncategorized']) ? $dataGlobal['uncategorized'] : 'Uncategorized'?> </option>
            <?php 
          }
          $f = ejecutar($query_f);
          while ($row_ca = fetchAssoc($f))
          {  ?>
            <option value="<?php echo $row_ca['idx']?>" <?php if(isset($row_u['idcategorias']) && $row_ca['idx']==$row_u['idcategorias']){echo'selected';}?>>
              <?php echo $row_ca['nombre']?>                
            </option>
            <?php
          } ?>
        </select>
    </span>
    </td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="nombre" type="text" id="nombre" class="caja_texto" value="<?php echo $row_u['nombre']; ?>" size="80" />
      </span>
</td>
    </tr>
      <td>
      <?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover'?><br />
        <i><strong><?=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width'?>: <?= $w ?>px<br />
          <?=isset($dataGlobal['label_height']) ? $dataGlobal['label_height'] : 'Height'?>: <?= $h ?>px</strong>
          </i></td>
      <td>&nbsp;</td>
      <td>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <div><i class="fa fa-file-code"></i>
<?php if(empty($_GET['i'])){ ?>
<i class="fa fa-file-image-o" style="font-size:48px;color:blue"></i>
<?php }else{ ?>
<?php if(empty($row_u['imagen'])){ ?>
<i class="fa fa-file-image-o" style="font-size:48px;color:blue"></i>
<?php }else{ ?>
<img src="../../imgcms/product_d/<?php echo $row_u['nomcar']; ?>/small/<?php echo $row_u['imagen']; ?>" style="border:1px solid #999999; padding:5px;" width="50px" height="50px" />
<?php } ?>
<?php } ?>
      </div>
            </td>
          <td>&nbsp;</td>
          <td><input type="file" name="imagen" id="imagen" /></td>
          </tr>
        </table>
    </td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['label_location']) ? $dataGlobal['label_location'] : 'Location'?></td>
      <td></td>
      <td>
        <input name="location" type="text" id="location" class="caja_texto" value="<?php echo $row_u['location']; ?>" size="80" />
</td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date'?></td>
      <td></td>
      <td>
        <input name="fecha" type="text" id="fecha" class="caja_texto" value="<?php echo $row_u['fecha']; ?>" size="80" />
</td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['project_is']) ? $dataGlobal['project_is'] : 'Project is'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="spryselect3">
<select name="projectstatus" style="width:250px">
 <?php if(empty($_GET['i'])){  ?>
 <option><?=isset($dataGlobal['_select_']) ? $dataGlobal['_select_'] : '- Select -'?></option>
 <?php }else{  ?>
  <option value="<?php echo $row_a['idx']?>"><?php echo $row_a['nombre']?></option>
 <?php } do {  ?>
  <option value="<?php echo $row_b['idx']?>"><?php echo $row_b['nombre']?></option>
  <?php
} while ($row_b = fetchAssoc($b));
  $rows = numRows($b);
  if($rows > 0) {
      mysqli_data_seek($b, 0);
    $row_b = fetchAssoc($b); } ?>
</select></span>
    </td>
    </tr>
    <tr>
      <td><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></td>
      <td>&nbsp;</td>
      <td>
      <select name="activo" style="width:150px">
<option value="1" <?php if($row_u['activo'] == 1){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active'?></option>
<?php if(empty($_GET['i'])){ ?>
<option value="0"><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php }else{?>
<option value="0" <?php if($row_u['activo'] == 0){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php } ?>
    </select>
    </td>
    </tr>
    <tr>
      <td valign="top" colspan="3"><?=isset($dataGlobal['small_descrip']) ? $dataGlobal['small_descrip'] : 'Small Description'?>
        <textarea name="descripcion" id="descripcion" rows="3" style="width:100%" class="caja_texto"><?php echo $row_u['descripcion']; ?></textarea>
    </td>
    </tr>
    <tr>
    <tr>
      <td valign="top" colspan="3"><?=isset($dataGlobal['full_descrip']) ? $dataGlobal['full_descrip'] : 'Full Description'?>
        <textarea name="fulldescripcion" id="fulldescripcion" cols="45" rows="4" class="caja_texto"><?php echo $row_u['fulldescripcion']; ?></textarea>
        <script type="text/javascript">
        var newCKEdit = CKEDITOR.replace('fulldescripcion',{height:'250',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
        CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
        </script>
    </td>
    </tr>
    <tr>
    <tr>
      <td colspan="3"><hr /></td>
      </tr>
    <tr>
      <td colspan="3">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="50"><div style="font-size:22px;"><strong><?=isset($dataGlobal['marketing']) ? $dataGlobal['marketing'] : 'Marketing'?></strong></div></td>
          <td>
<label><input type="radio" name="usamarketing" value="1" <?php if($row_u['usamarketing'] == 1){echo "checked='checked'";} ?>  onclick="marketing(1)"><?=isset($dataGlobal['custom_metas']) ? $dataGlobal['custom_metas'] : 'Custom Metas'?></label>
<label><input type="radio" name="usamarketing" value="0" <?php if($row_u['usamarketing'] == 0){echo "checked='checked'";} ?>  onclick="marketing(0)"><?=isset($dataGlobal['universal_metas']) ? $dataGlobal['universal_metas'] : 'Universal Metas'?></label>
            </td>
          </tr>
  </table>
  <div id="div_marketing1">
  <div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_keywords']) ? $dataGlobal['label_keywords'] : 'Keywords'?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Desciption'?></li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">
      <div style="padding:8px;">
     <input name="title" type="text" id="title" class="caja_texto" value="<?=$row_u["title"]?>" size="90" /> 
     </div>
    </div>
    <div class="TabbedPanelsContent">
    <div style="padding:8px;">
    <textarea name="keywords" cols="90" rows="5" class="caja_texto" id="keywords"><?=$row_u["keywords"]?></textarea>
    </div>
    </div>
    <div class="TabbedPanelsContent">
    <div style="padding:8px;">
    <textarea name="descriptiona" cols="90" rows="5" class="caja_texto" id="descriptiona"><?=$row_u["descriptiona"]?></textarea>
    </div>
    </div>
  </div>
</div>
    </div>
        <div id="div_marketing2">                
<div id="TabbedPanels2" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_keywords']) ? $dataGlobal['label_keywords'] : 'Keywords'?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Desciption'?></li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">
     <div style="font-size:14px; padding:8px;"><?=$row_u1['title']?></div>
    </div>
    <div class="TabbedPanelsContent">
    <div style="font-size:14px; padding:8px;"><?=nl2br($row_u1['keywords'])?></div>
    </div>
    <div class="TabbedPanelsContent">
      <div style="font-size:14px; padding:8px;"><?=nl2br($row_u1['description'])?></div>
    </div>
  </div>
</div>
          <br />
          </div>      
        <script>
    marketing("<?=$row_u["usamarketing"]?>");
    </script>
  </td>
    </tr>
    <tr>
      <td colspan="3"><hr></td>
    </tr>
    <tr>
      <td colspan="3">
       <?php if(!empty($_GET['i'])){?>
      <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr>
          <td><div style="font-size:22px;"><strong><?=isset($dataGlobal['label_code']) ? $dataGlobal['label_code'] : 'Code'?>:</strong></div></td>
          </tr>
        <tr>
          <td><textarea cols="5" rows="3" readonly onClick="this.select()" style="width:100%"
            ><?="<?php"?> $id=<?=$_GET['i']?>;include _INC."_projects-code.php";<?="?>"?></textarea></td>
          </tr>
        </table>
       <?php } ?>
        </td>
    </tr>
    </table>
    </td>
  </tr>
</table>
  <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
</form>
    </td>
  </tr>
</table>
<script type="text/javascript">
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect2");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect3");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var TabbedPanels2 = new Spry.Widget.TabbedPanels("TabbedPanels2");
</script>  
<?php include(dirname(__FILE__)."/../footer.php"); ?>