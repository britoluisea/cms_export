<?php
include(dirname(__FILE__)."/../header.php");
include("name_table_bd.php");
require_once('../system/function.php');
$_GET['i'] = 1;
$msj=" ";
$colname_widget = isset($_GET['i']) ? $_GET['i'] : 1;
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= " " . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update_widget"])) && ($_POST["MM_update_widget"] == "form1")) {
  $title=addslashes($_POST['title']);
  $updateSQL = "UPDATE $tb_e SET num_reg_widg='".$_POST['num_reg_widg']."', tipoprojects='".$_POST['tipoprojects']."', title='".$title."' WHERE idx='".$_POST['idx']."' ";
  $Result1 = ejecutar($updateSQL);
if($Result1){
  $data_saved=isset($dataGlobal['data_saved']) ? $dataGlobal['data_saved'] : 'Data Saved';
  $msj="<table width='400' border='0' align='center' cellpadding='0' cellspacing='5' class='Estilo_data_guardado1'><tr><td><div align='center'><strong>".$data_saved."</strong></div></td>  </tr></table>";
}
}
  $query_u = sprintf("SELECT * FROM $tb_e WHERE idx = %s", $colname_widget);
  $u = ejecutar($query_u);
  $row_u = fetchAssoc($u);
  $totalRows_u = numRows($u);
?>
<table width="100%" border="0" align="center">
  <tr>
    <td width="10%" style="vertical-align: top; padding-top: 10px;">
      <?php include_once ('menu.php'); ?>
      </td>
      <td style="vertical-align: top; padding-top: 10px;">
        <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
          <table width="100%" border="0"  cellpadding="8" cellspacing="0">
            <tr>
              <td style="padding-left: 20px;">
                <div class="navipage"><strong><?=isset($dataGlobal['project_widget']) ? $dataGlobal['project_widget'] : 'Project Widget'?></strong></div>
              </td>
              <td>
                <table border="0"  align="right" style="border-spacing: 3px; border-collapse: initial;">
                  <tr>
                    <td><input type="submit" class='btn btn-success' value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>" /></td>
                    <td><a class='btn btn-small btn-default' href="<?=$_SERVER["PHP_SELF"]?>"><?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancle'?></a></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <br><?php echo $msj; ?><br>  
          <div class="col-sm-12">
            <table  width="80%" align="left">
              <tr valign="baseline">
                <td style="padding-bottom: 10px;" align="left" width="20%"><?=isset($dataGlobal['number_records']) ? $dataGlobal['number_records'] : 'Number of records'?>:</td>
                <td style="padding-bottom: 10px;" width="60%">
                  <select class="form-control" name="num_reg_widg">
                    <?php for($n = 1; $n <= 10; $n++){ ?>
                      <option value="<?=$n?>" <?php if($row_u["num_reg_widg"] == $n){?>selected <?php } ?>>
                        <?=$n?>
                      </option>
                    <?php } ?>
                  </select>
                </td>
                <td  align="lef" style="padding-left: 10px;padding-bottom: 10px"><?=isset($dataGlobal['label_records']) ? $dataGlobal['label_records'] : 'records'?></td>    
              </tr>
              <tr>
                <td style="padding-bottom: 10px;"><?=isset($dataGlobal['type_project']) ? $dataGlobal['type_project'] : 'Type of Project'?></td>  
                <td style="padding-bottom: 10px;">
                  <select name="tipoprojects" id="template_inner" class="form-control">
                    <?php
                    $dir =  dirname(__FILE__)."/../../".$_SESSION['THEME']."/widgets/projects/"; 
                    if(file_exists($dir))
                    {
                      chdir ($dir);  ///vaciar carpeta de inner
                      $dh=opendir('.');
                      while ($file = readdir ($dh)) 
                      {
                        if ($file != "." && $file != "..") { ?>
                          <option value="<?=$file?>" <?php if($file==$row_u['tipoprojects']) { echo "selected='selected'"; } ?>><?=strtoupper(substr($file, 0,-4))?></option>
                          <?php 
                        }
                      }
                      closedir($dh);
                    }
                    ?>
                  </select>
                </td>
                <td style="padding-bottom: 10px;"></td>
              </tr>
              <tr valign="baseline">
                <td ><?=isset($dataGlobal['label_code']) ? $dataGlobal['label_code'] : 'Code'?>:</td>
                <td  colspan="2" style="width:100%">
                  <textarea class="form-control"  style="width:100%" readonly onClick="this.select()"  ><?="<?php"?> include(_INC."_projects-widget.php");<?="?>"?></textarea>
                </td>
              </tr>
            </table>
</div>                    
<div class="col-sm-12">
    <div class="col-sm-12">      
      <script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
      <script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
      <style>select#align-icon {margin-bottom: 0px !important;}</style>
      <script type="text/javascript">
        $(document).ready(function() {
          var window_height = 350; 
          var newCKEdit = CKEDITOR.replace('title',{height:window_height,allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
          CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
        });
      </script>
      <div style="font-size:20px; color:#2679b5">
        <strong><?=isset($dataGlobal['box_project']) ? $dataGlobal['box_project'] : 'Box Project'?></strong>
      </div>      
      <div class="col-sm-12">
        <?php 
          include dirname(__FILE__). "/../system/btn-icons.php";
          addBtnIcons($idEditor='CKEDITOR.instances.title');
        ?>
      </div>
      <textarea name="title" id="title" ><?=stripslashes($row_u['title'])?></textarea>      
    </div>
</div>                    
  <input type="hidden" name="MM_update_widget" value="form1">
  <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
</form>
</td>
</tr>
</table>
<div style="margin-top:20%;"></div>
<?php include(dirname(__FILE__)."/../footer.php");?>