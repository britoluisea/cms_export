<aside>
  <div id="resize">&laquo;</div>
  <div id="resize2">&raquo;</div>
  <div id="lateral">
    <div id="navl1">
      <h2><?=isset($dataGlobal['custom_tools']) ? $dataGlobal['custom_tools'] : 'Custom Tools'?></h2>
      <ul id="menua" class="genial_menu">
        <?php 
          $query_vq = "SELECT * FROM $tb_h where activo = 1 ORDER BY item ASC";
          $vq = ejecutar($query_vq);
          $row_vq = fetchAssoc($vq);
          $totalRows_vq = numRows($vq);
          do 
          { ?>
              <li id="menucat2" class="menucat1">
                <a class="expanded"><?php echo $row_vq['nombre']; ?></a>
                <ul>
                  <?php 
                    $filax = $row_vq['idx']; //sustitucion 
                    $sql = "SELECT * FROM $tb_g WHERE idx_group = '".$filax."' AND activo = 1 ORDER BY item ASC";
                    //echo $sql;
                    $resultx = ejecutar($sql);     
                    $numx = numRows($resultx);  
                    if($rowx = fetchArray($resultx))
                    { 
                      if(!empty($rowx['nombre']))
                      {
                        do
                        { ?>
                          <li>
                            <a href="index.php?g=<?php echo $row_vq['idx']; ?>&c=<?php echo $rowx['idx']; ?>">
                              <?php echo $rowx['nombre']; ?>
                            </a>
                          </li>
                          <?php 
                        } while ($rowx = fetchArray($resultx));
                      }
                    }
                  ?>
                </ul>
              </li>
              <?php 
          }while ($row_vq = fetchAssoc($vq));
        ?>
        <li id="menucat1" class="menucat1">
          <a class="expanded"><?=isset($dataGlobal['projects']) ? $dataGlobal['projects'] : 'Projects'?> </a>
          <ul>
            <li>
              <a href="index.php">
                <?=isset($dataGlobal['project_list']) ? $dataGlobal['project_list'] : 'Project List'?> 
              </a>
            </li> 
            <li>
              <a href="status.php">
                <?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?>
              </a>
            </li> 
            <li>
              <a href="group.php">
                <?=isset($dataGlobal['label_group']) ? $dataGlobal['label_group'] : 'Group'?>
              </a>
            </li> 
            <li>
              <a href="category.php">
                <?=isset($dataGlobal['label_category']) ? $dataGlobal['label_category'] : 'Category'?>
              </a>
            </li> 
            <li>
              <a href="widget.php">
                <?=isset($dataGlobal['label_widget']) ? $dataGlobal['label_widget'] : 'Widget'?>
              </a>
            </li>            
            <li>
              <a href="options.php">
                <?=isset($dataGlobal['label_options']) ? $dataGlobal['label_options'] : 'Options'?>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</aside>
