<?php 
require_once(dirname(__FILE__)."/../header.php");
include("project_category_c.php");
?>
<!-- modal confirmar eliminar registro -->
<?php include("modal.php");?>
<!-- modal confirmar eliminar registro-->
<table class="height100" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%" valign="top" class="">
 <?php include_once ('menu.php'); ?>
</td>
    <td width="100%" valign="top"><section>
    <div id="contenido">
      <table style="margin-bottom:12px" width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td width="50%" bgcolor="">
<a href="category-f.php"  class='btn btn-small btn-success'><strong><span>+</span> <?=isset($dataGlobal['add_category']) ? $dataGlobal['add_category'] : 'Add Category'?></strong></a>
   </td>
    <td width="50%" >
    </td>
  </tr>
</table>
    <div class="estilotabla">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th width="80" ><?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover'?></th>
          <th  width="35%" ><div align="left"><?=isset($dataGlobal['label_group']) ? $dataGlobal['label_group'] : 'Group'?></div></th>
          <th  width="35%" ><div align="left"><?=isset($dataGlobal['label_category']) ? $dataGlobal['label_category'] : 'Category'?></div></th>
          <th width="10"><div align="left"><?=isset($dataGlobal['label_link']) ? $dataGlobal['label_link'] : 'Link'?></div></th>
          <th width="45" ><?=isset($dataGlobal['label_up']) ? $dataGlobal['label_up'] : 'Up'?></th>
          <th width="45" ><?=isset($dataGlobal['label_dw']) ? $dataGlobal['label_dw'] : 'Dw'?></th>
          <th width="60" ><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?> </th>
          <th colspan="2" ><?=isset($dataGlobal['label_action']) ? $dataGlobal['label_action'] : 'Action'?></th>
          </tr>
<?php if ($totalRows_c == 0) { // Show if recordset empty ?>
    <tr>
      <th colspan="100" align="center" class="text-uppercase" style="color:#FF0000"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record'?></th>
    </tr>
<?php } // Show if recordset empty ?>
<?php if ($totalRows_c > 0) { // Show if recordset not empty ?>
  <?php 
    $i = 1;
do { 
      $img = "../../imgcms/product_d/".$row_c['nomcar']."/small/".$row_c['imagen'];
      ?>
      <tr>
        <td>
          <div align="center" style="margin:3px;">
            <?php if(empty($row_c['imagen'])){ ?>
              <i class="fa fa-file-image-o" style="font-size:48px;color:blue"></i>
            <?php }else{ ?>
              <img src="<?= $img ?>"  width="50px" height="50px" style="border:1px solid #999999; padding:5px;" />  
            <?php } ?>
          </div>
        </td>
        <td>
          <div align="left">
            <?php 
            $fila = $row_c['idx_group']; //sustitucion 
            $result = ejecutar("SELECT * FROM $tb_h WHERE idx = '".$fila."' AND activo = 1");     
            $num = numRows($result);  
            if($row = fetchArray($result)){ 
            if(!empty($row['nombre'])){
            do { ?>
            <?php echo $row['nombre']; ?> 
            <?php } while ($row = fetchArray($result));
            } }
            ?>
          </div>
        </td>
        <td><div align="left"><?php echo $row_c['nombre']; ?></div></td>
        <td>
          <div class="cat_nombre">
            <div align="left">
              <a class='btn btn-small btn-default' href="<?=SERVER?>projects/<?php echo $row_c['slug']; ?>" target="_blank"><i class="fa fa-link"></i></a>
            </div>
          </div>
        </td>
        <td> 
          <div align="center">
            <?php if($i != 1){ ?>
                <span class="Estilo6">
                  <a class='btn btn-small btn-info' onClick="up<?php echo $row_c['item']; ?>();"><i class="fa fa-arrow-up"></i></a>
                  <input type="hidden" name="i<?= $row_c['item']; ?>" id="i<?= $row_c['item']; ?>" value="<?= $row_c['item']; ?>" >
                  <script>
                    function up<?php echo $row_c['item']; ?>(){
                      var parametros = {
                        "i": $("#i<?php echo $row_c['item']; ?>").val(),
                      };
                      $.ajax({
                        data: parametros,
                        url: "orden-upc.php",
                        type: "POST",
                        success: function(vista){
                          window.location.href = "category.php";
                        }
                      });
                    }
                    </script>
                  </span>
            <?php } ?>
          </div>
        </td>
        <td>    
          <div align="center">
            <?php if($i != $totalRows_c){ ?>
              <span class="Estilo6">
                <a class='btn btn-small btn-info' onClick="dw<?php echo $row_c['item']; ?>();"><i class="fa fa-arrow-down"></i></a>
                <input type="hidden" name="i<?= $row_c['item']; ?>" id="i<?= $row_c['item']; ?>" value="<?= $row_c['item']; ?>" >
                <script>
                function dw<?php echo $row_c['item']; ?>(){
                  var parametros = {
                    "i": $("#i<?php echo $row_c['item']; ?>").val(),
                  };
                  $.ajax({
                    data: parametros,
                    url: "orden-downc.php",
                    type: "POST",
                    success: function(vista){
                      window.location.href = "category.php";
                    }
                  });
                }
                </script> 
              </span>
            <?php } ?>
          </div>
        </td>
        <td>
          <div align="center" class="Estilo_tipoestado">
            <?php
              switch ($row_c['activo']) {
              case ($row_c['activo'] = 1):
                  $active=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active';
                  echo "<label class='btn btn-small btn-success'>".$active."</label>";
                  break;
              case ($row_c['activo'] = 0):
                  $inactive=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive';
                  echo "<label class='btn btn-small btn-danger'>".$inactive."</label>";
                  break;
            }?> 
          </div>
        </td>
        <td>
          <div align="center"> 
            <a href="category-f.php?i=<?php echo $row_c['idx']; ?>" title="Edit Gallery" class='btn btn-small btn-primary'><i class="fa fa-edit"></i></a>
          </div>
        </td>
        <td> 
          <a data-target="#ventana1" data-href="delete-category.php?i=<?php echo $row_c['idx']; ?>" class="btn btn-small btn-danger" href="#" data-toggle="modal" data-book-id="<?php echo $row_c['nombre']; ?>?" style=" padding: 5px 10px;">
            <i class="fa fa-trash-o bigger-120"></i>
          </a>
        </td>
      </tr>
      <?php
      $i++;
    } while ($row_c = fetchAssoc($c)); ?>
  <?php } // Show if recordset not empty ?>
      </table>
    </div> 
      <div id="pg">  
<!-- paginador-->
<?php include("paginador.php");?>
<!-- paginador-->   
              </div>
    </div>
  </section>
</td>
  </tr>
</table>
<?php require_once(dirname(__FILE__).'/../footer.php');?>
