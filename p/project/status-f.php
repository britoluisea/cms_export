<?php include("project_status_f.php"); ?>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<style>
  .form-control{
    margin-bottom: 10px;
  }
</style>

<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td width="1%" valign="top">
 <?php include_once ('menu.php'); ?>
    </td>
    <td style="vertical-align: top; padding: 20px;">
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td>
<div class="navipage">
<?php if(empty($_GET['i'])){  ?>
<?=isset($dataGlobal['new_status']) ? '[ '.$dataGlobal['new_status'].']' : '[ New Status ]'?>
<input type="hidden" name="MM_insert" value="form1">
<input type="hidden" name="validar" value="status">
<?php }else{ ?>
<?=isset($dataGlobal['edit_status']) ? '[ '.$dataGlobal['edit_status'].']' : '[ Edit Status ]'?>
  <input type="hidden" name="MM_update" value="form1">
<?php }?>
    </div>
	</td>
    <td>
    <table border="0" align="right" cellpadding="5" cellspacing="0" style="margin-bottom: 20px;">
      <tr>
        <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>" /></td>
        <td><a class="btn btn-small btn-default" href="status.php"><?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancle'?></a></td>
        </tr>
    </table>
       </td>
  </tr>
</table>

<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

    <table width="96%" border="0" align="center">
    <tr>
      <td><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="nombre" type="text" id="nombre" class="form-control" value="<?php echo $row_u['nombre']; ?>" size="80" />
      </span>
</td>
    </tr>


    <tr>
      <td><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></td>
      <td>&nbsp;</td>
      <td>
  	  <select name="activo" class="form-control" style="width:150px">
<option value="1" <?php if($row_u['activo'] == 1){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active'?></option>
<?php if(empty($_GET['i'])){ ?>
<option value="0"><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php }else{?>
<option value="0" <?php if($row_u['activo'] == 0){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php } ?>
	  </select>
	  </td>
    </tr>
   
  
    </table>

    </td>
  </tr>
</table>
  <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
</form>

    </td>
  </tr>
</table>


<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
</script>  

<?php include(dirname(__FILE__)."/../footer.php"); ?>