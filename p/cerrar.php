<?php
	session_start();
	include dirname(__FILE__)."/../varRutas.php";
	include dirname(__FILE__)."/../cn/cnx.php";
	if(empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin']))
	{		
		header("Location: index.php");
		exit();	
	}
	else
	{
    	include 'authSession.php';
    	editAuthSession($_SESSION['value_admin_idx']);
		unset($_SESSION['error_array']);
		unset($_SESSION['value_array']['usuarios']);
		unset($_SESSION['var_sec_admin']);
		unset($_SESSION['value_admin_idx']);
		unset($_SESSION['value_admin_idxamusuario']);
		unset($_SESSION['value_admin_idxusuariossp']);
		unset($_SESSION['value_admin_usuarios']);
		unset($_SESSION['value_admin_userlevel']);
		unset($_SESSION['value_admin_nomcar']);
		unset($_SESSION['value_temp_usuarios']);
		unset($_SESSION['value_temp_remember']);
		//session_destroy();
		header("Location: index.php");
	}
?>