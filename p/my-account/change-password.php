<?php require_once("../header.php"); ?>
<?php
  if (isset($_POST['ope']) && $_POST['ope']=="camclavenormal")
  {
    $error = 0;
    $ir = 'change-password-err.php'; // default
    $x1=$_POST['x1'];
    $x2=$_POST['x2'];
    $x3=$_POST['x3'];
    if ($x2 == $x3)
    {
      $x1 = md5(md5($x1));  
      $widx=$widxamusuario; // esta variabele es el id del user en session, viene dada desde el header
      $sql="select * from usuarios where idx = '".$widx."' and claves = '".$x1."' limit 0,1";
      $sql_query=ejecutar($sql);
      if($fila2 =  fetchAssoc($sql_query))
      {
        $x2 = md5(md5($x2));
        $sql_UPDATE = "UPDATE usuarios  set claves = '".$x2."' where idx = '".$widx."' ";
        $update_query= ejecutar($sql_UPDATE);
        $ir = 'change-password-exito.php';
      }
      else
      {
        $error = 1; $msj= isset($dataGlobal['change_pass_msj1']) ? $dataGlobal['change_pass_msj1'] : 'You have typed your old password wrong.';
      }   
    }
    else
    {
      $error = 2; $msj=isset($dataGlobal['change_pass_msj2']) ? $dataGlobal['change_pass_msj2'] : 'New password and confirm password do not match.';
    }
    if($error > 0)
    { 
?>
        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="15" style="border:1px solid #dd3c10; background-color:#ffebe8; margin:5px;" >
          <tr>
            <td align="left">
              <p style=" padding: 10px;">
                <strong><?=isset($dataGlobal['change_pass_msj3']) ? $dataGlobal['change_pass_msj3'] : 'Please correct the following errors before continuing'?>:</strong><br />
                - <?=isset($dataGlobal['change_pass_msj4']) ? $dataGlobal['change_pass_msj4'] : 'Wrong Password'?><br />
                - <?=isset($dataGlobal['change_pass_msj5']) ? $dataGlobal['change_pass_msj5'] : 'Two Passwords must match'?><br />
                <?=$msj?>
              </p>
            </td>
          </tr>
        </table>
        <?php
      }
      if($error == 0)
      { ?>
        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style=" margin:5px;" >
          <tr>
            <td>
              <p style=" padding: 10px;" align="center"><?=isset($dataGlobal['change_pass_msj6']) ? $dataGlobal['change_pass_msj6'] : 'Your password was changed successfully'?></p>
            </td>
          </tr>
        </table>
        <?php
      }
  } 
?>
<style>
  .bg-titulo{
  color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
  }
</style>
<section>
  <div id="contenido">
    <div class="col-sm-offset-4 col-sm-4 thumbnail">
      <div class="col-sm-12  bg-titulo">        
        <h1 class="h1 text-center">
          <?=isset($dataGlobal['changePass']) ? $dataGlobal['changePass'] : 'Cange Password'?>
        </h1>
      </div>
      <div class="col-sm-12">
        <br>
        <ul class="list-group">
          <li class="list-group-item"><b><?=isset($dataGlobal['current_pass']) ? $dataGlobal['current_pass'] : 'Current password'?>:</b> <input name="x1" type="password" id="x1" class="form-control" size="20" required="true" /></li>
          <li class="list-group-item"><b><?=isset($dataGlobal['new_pass']) ? $dataGlobal['new_pass'] : 'New Password'?>: </b> <input name="x2" type="password" id="x2" class="form-control" size="20" required="true" /></li>
          <li class="list-group-item"><b><?=isset($dataGlobal['retype_pass']) ? $dataGlobal['retype_pass'] : 'Retype Password'?>: </b> <input name="x3" type="password" id="x3" class="form-control" size="20" required="true" /></li>
          <li class="list-group-item text-center">
            <input name="button" type="submit" class="btn btn-primary btn-lg" id="button2" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>" />
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php include("../footer.php"); ?>
