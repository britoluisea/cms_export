<?php include dirname(__FILE__).'/../header.php'; ?> 
<?php 
  $opc='';
  if (isset($_GET['opc'])) { $opc = $_GET['opc']; }
  if (empty($opc))   { $opc = 1; }
  $mostrarservicio = '';
  $descripservicio = '';
  $accountname = '';
  $sql_1="select * from usuarioscompany where idx = '1' "; //Devuelve todos los Idiomas
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $atabla=$fila_1;    
    $mostrarservicio = $atabla["activeaccountinfoservice"];
    $descripservicio = $atabla["accountinfoservice"];
    $accountname = $atabla["accountname"];
  }
  $sql_1="select * from usuarios where idx = '$widxamusuario' "; //Devuelve todos los Idiomas
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $atabla=$fila_1;
    $opc = 2;
    $i = $atabla["idx"];
  }
?>
<style>
  .bg-titulo{
  color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
  }
  .list-group-item.sub-titulo{color:#000;}
  .list-group-item{color:#000;}
  .list-group-item b{color:#999;}
</style>
<section>
  <div class="container">
      <br>
      <br>
    <div class="col-sm-offset-2 col-sm-8 thumbnail">
      <div class="col-sm-12  bg-titulo">        
        <h1 class="h1 text-center">
          <?=isset($dataGlobal['my_account']) ? $dataGlobal['my_account'] : 'My Account'?>
        </h1>
      </div>
      <div class="text-center" cms-cols="col cs30 cx100">
        <p class="h1 btn-primary" style="padding: 10px 5px; "><span class="glyphicon glyphicon-user fa-4x"></span></p>
      </div>
      <div class="" cms-cols="col cs70 cx100">
        <br>
        <?php
          /*if ($mostrarservicio == 1 ) { ?>
            <ul class="list-group">
              <li class="list-group-item text-center text-uppercase "><strong><?=isset($dataGlobal['account_services']) ? $dataGlobal['account_services'] : 'Account Services'?></strong></li>
              <li class="list-group-item"><?=$descripservicio?></li>
            </ul>
        <?php } */ ?>
        <ul class="list-group">
          <li class="list-group-item text-center text-uppercase sub-titulo"><strong><?=isset($dataGlobal['account_login']) ? $dataGlobal['account_login'] : 'Login Information'?></strong></li>
          <li class="list-group-item"><b><?=isset($dataGlobal['email_user']) ? $dataGlobal['email_user'] : 'Email (User)'?>: </b> <?=$atabla["username"]?></li>
          <li class="list-group-item"><b><?=isset($dataGlobal['labelPassLogin']) ? $dataGlobal['labelPassLogin'] : 'Password'?>: ********</b></li>
        </ul>
      </div>
      <div class="col-sm-12">
        
        <ul class="list-group">          
          <li class="list-group-item text-center text-uppercase sub-titulo"><strong><?=isset($dataGlobal['account_info2']) ? $dataGlobal['account_info2'] : '"Account Information'?></strong></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['account_name']) ? $dataGlobal['account_name'] : 'Account Name'?>: </b> <?=$accountname?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['company_name']) ? $dataGlobal['company_name'] : 'Company Name'?>: </b> <?=$atabla["compania"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['first_name']) ? $dataGlobal['first_name'] : 'First Name'?>: </b> <?=$atabla["nombres"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['last_name']) ? $dataGlobal['last_name'] : 'Last Name'?>: </b> <?=$atabla["apellidos"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['address']) ? $dataGlobal['address'] : 'Address'?>: </b> <?=$atabla["direccion"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['address']) ? $dataGlobal['address'] : 'Address'?>: </b> <?=$atabla["direccion2"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?>: </b> <?=$atabla["ciudad"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?>: </b> <?=$atabla["estado"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['code_zip']) ? $dataGlobal['code_zip'] : 'Code Zip'?>: </b> <?=$atabla["zipcode"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['telephone']) ? $dataGlobal['telephone'] : 'Telephone'?>: </b> <?=$atabla["telefono"]?></li>
          <li class="list-group-item col-sm-6"><b><?=isset($dataGlobal['cell_tlf']) ? $dataGlobal['cell_tlf'] : 'Cell Phone'?>: </b> <?=$atabla["celular"]?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php include dirname(__FILE__).'/../footer.php'; ?> 
