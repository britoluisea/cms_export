<?php include(dirname(__FILE__)."/../header.php");?>
<div class="container">
  <br> <br>
  <form action="save-setting.php" method="post" enctype="multipart/form-data" name="form1">
    <?php
      $i = 1;
      $sql_1="select * from articles_options where idx='$i' "; //Devuelve todos los Idiomas
      $consulta_1=ejecutar($sql_1);
      $atabla=array();
      while($fila_1=fetchAssoc($consulta_1))
      {
        $atabla=$fila_1;
        $opc = 2;
        //$wfecha = strftime('%m/%d/%Y', strtotime($atabla["fecreg"]));
        $i = $atabla["idx"];
     }
    ?>
    <div>
      <table width="50%" border="0" align="center" cellpadding="8" cellspacing="0">
        <tr>
          <td>
            <div class="navipage h2"><?=isset($dataGlobal['blog_setting']) ? $dataGlobal['blog_setting'] : 'Blog Setting' ?>  / <strong><?=isset($dataGlobal['posts_list']) ? $dataGlobal['posts_list'] : 'Posts List' ?></strong></div>
          </td>
          <td>
            <table border="0" align="left" cellpadding="5" cellspacing="0">
              <tr>
                <td>
                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save' ?></button>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <?php
        if (isset($_SESSION['data_guarda_bien']) && $_SESSION['data_guarda_bien'] == 1 )
        { ?>
          <table width="400" border="0" align="center" cellpadding="0" cellspacing="5" class="Estilo_data_guardado1"  >
            <tr>
              <td>
                <div align="center">
                  <strong><?=isset($dataGlobal['data_saved']) ? $dataGlobal['data_saved'] : 'Data Saved' ?></strong><br />
                </div>
              </td>
            </tr>
          </table>
          <?php
          unset ($_SESSION['data_guarda_bien']);
        }
      ?>        
      <input type="hidden" name="ope" value="articles-options"/>
      <input type="hidden" name="opc" value="<?=$opc?>" />
      <input type="hidden" name="i" value="<?=$i?>" />
      <table width="50%" border="0" align="center">
      
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['post_pagination']) ? $dataGlobal['post_pagination'] : 'Post Pagination' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['number_records']) ? $dataGlobal['number_records'] : 'Number of records' ?></td>
                <td>:</td>
                <td>
                  <select name="num_reg_page_cms" id="num_reg_page_cms" class="form-control" style="width:105px; margin-bottom:10px;">
                  <?php
                    for($ifor = 10; $ifor <= 100; $ifor= $ifor+10)
                    { ?>
                      <option value="<?=$ifor?>" 
                       <?php if ($atabla["num_reg_page_cms"] == $ifor) {?>selected <?php } ?>>
                       <?=$ifor?> rows
                      </option> <?php
                    }
                  ?>
                  </select> 

                </td>
              </tr>
              <tr>
                <td width="200"><?=isset($dataGlobal['number_records_page']) ? $dataGlobal['number_records_page'] : 'Number of records' ?></td>
                <td width="10">:</td>
                <td>
                  <select name="num_reg_page" id="num_reg_page" class="form-control" style="width:105px; margin-bottom:10px;">
                    <?php
                      for($ifor = 10; $ifor <= 100; $ifor= $ifor+10)
                      {
                        ?>
                        <option value="<?=$ifor?>" 
                          <?php if ($atabla["num_reg_page"] == $ifor) {?>selected <?php } ?>>
                          <?=$ifor?> rows
                        </option>
                        <?php
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <hr style="background-color:#cccccc; border:0; height: 1px;"/>
                </td>
              </tr>
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['post_share']) ? $dataGlobal['post_share'] : 'Post Share' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['facebook_share']) ? $dataGlobal['facebook_share'] : 'Facebook Share' ?></td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="face_share" type="radio" id="face_share" value="1" <?php if($atabla["face_share"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="face_share" value="0" id="face_share" <?php if($atabla["face_share"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?> 
                  </label>
                </td>
              </tr>
              <tr >
                <td><?=isset($dataGlobal['twitter_share']) ? $dataGlobal['twitter_share'] : 'Twitter Share' ?></td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="twitter_share" type="radio" id="twitter_share" value="1" <?php if($atabla["twitter_share"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="twitter_share" value="0" id="twitter_share" <?php if($atabla["twitter_share"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['post_like']) ? $dataGlobal['post_like'] : 'Post Like' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['facebook_like']) ? $dataGlobal['facebook_like'] : 'Facebook Like' ?></td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="face_like" type="radio" id="face_like" value="1" <?php if($atabla["face_like"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="face_like" value="0" id="face_like" <?php if($atabla["face_like"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?> 
                  </label>
                </td>
              </tr>
              <tr >
                <td>Twitter Like</td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="twitter_like" type="radio" id="twitter_like" value="1" <?php if($atabla["twitter_like"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="twitter_like" value="0" id="twitter_like" <?php if($atabla["twitter_like"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3">
                  <hr style="background-color:#cccccc; border:0; height: 1px;"/>
                </td>
              </tr>
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['facebook_comments']) ? $dataGlobal['facebook_comments'] : 'Facebook Comments' ?></td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="face_comment" type="radio" id="face_comment" value="1" <?php if($atabla["face_comment"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="face_comment" value="0" id="face_comment" <?php if($atabla["face_comment"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?>
                  </label>
                </td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?></td>
                <td>:</td>
                <td valign="top">
                  <label>
                    <input name="comments_blog" type="radio" id="comments_blog" value="1" <?php if($atabla["comments_blog"]=="1") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_show']) ? $dataGlobal['label_show'] : 'Show' ?> &nbsp;
                  </label>
                  <label>
                    <input type="radio" name="comments_blog" value="0" id="comments_blog" <?php if($atabla["comments_blog"]=="0") : ?> checked="checked" <?php endif;?>>
                    <?=isset($dataGlobal['label_hide']) ? $dataGlobal['label_hide'] : 'Hide' ?>
                  </label>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <hr style="background-color:#cccccc; border:0; height: 1px;"/>
                </td>
              </tr>
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['label_colores']) ? $dataGlobal['label_colores'] : 'Colors' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="middle" ><?=isset($dataGlobal['btn_text_color']) ? $dataGlobal['btn_text_color'] : 'Button Text Color' ?></td>
                <td valign="middle">:</td>
                <td valign="middle">
                  <input type="text" name="color_text_btn" id="color_text_btn" class="form-control kolorPicker" style="width:120px;margin-bottom:20px;" value="<?=isset($atabla["color_text_btn"]) ? $atabla["color_text_btn"] : '#000000'?>">
                  <div id="color_text_btn_btn" style="background-color:<?=isset($atabla["color_text_btn"]) ? $atabla["color_text_btn"] : '#000000'?>;width: 33px;height: 33px;border: 1px solid #000;margin-left: 130px;"></div>
                </td>
              </tr>
              <tr>
                <td valign="middle" ><?=isset($dataGlobal['btn_back_color']) ? $dataGlobal['btn_back_color'] : 'Button Background  Color' ?></td>
                <td valign="middle">:</td>
                <td valign="middle">
                  <input type="text" name="fondo_btn" id="fondo_btn" class="form-control kolorPicker" style="width:120px;margin-bottom:20px;" value="<?=isset($atabla["fondo_btn"]) ? $atabla["fondo_btn"] : '#ffffff'?>">
                  <div id="fondo_btn_btn" style="background-color:<?=isset($atabla["fondo_btn"]) ? $atabla["fondo_btn"] : '#ffffff'?>;width: 33px;height: 33px;border: 1px solid #000;margin-left: 130px;"></div>
                </td>
              </tr>
              <tr>
                <td valign="middle" ><?=isset($dataGlobal['blog_text_color']) ? $dataGlobal['blog_text_color'] : 'Blog Text Color' ?></td>
                <td valign="middle">:</td>
                <td valign="middle">
                  <input type="text" name="color_text_descrip" id="color_text_descrip" class="form-control kolorPicker" style="width:120px;margin-bottom:20px;" value="<?=isset($atabla["color_text_descrip"]) ? $atabla["color_text_descrip"] : '#ffffff'?>">
                  <div id="color_text_descrip_btn" style="background-color:<?=isset($atabla["color_text_descrip"]) ? $atabla["color_text_descrip"] : '#ffffff'?>;width: 33px;height: 33px;border: 1px solid #000;margin-left: 130px;"></div>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <hr style="background-color:#cccccc; border:0; height: 1px;"/>
                </td>
              </tr>
              <tr>
                <td><div style="font-size:14px;font-weight:bold;">CMS: <?=isset($dataGlobal['post_image']) ? $dataGlobal['post_image'] : 'Post Image' ?></div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width' ?> (px) </td>
                <td>:</td>
                <td>
                  <input name="width" type="text"  class="form-control" style="width:105px; margin-bottom:10px;" id="width" value="<?=$atabla["width"]?>" />
                </td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['label_height']) ? $dataGlobal['label_height'] : 'Height' ?> (px)</td>
                <td>:</td>
                <td>
                  <input name="height" type="text" class="form-control" style="width:105px; margin-bottom:10px;" id="height" value="<?=$atabla["height"]?>" size="15" />
                </td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['label_quality']) ? $dataGlobal['label_quality'] : 'Quality' ?></td>
                <td>:</td>
                <td>
                  <select name="quality" id="quality" class="form-control" style="width:105px; margin-bottom:10px;">
                  <?php
                    for($ifor = 10; $ifor <= 100; $ifor= $ifor+5)
                    { ?>
                      <option value="<?=$ifor?>" 
                       <?php if ($atabla["quality"] == $ifor) {?>selected <?php } ?>>
                       <?=$ifor?>% 
                      </option> <?php
                    }
                  ?>
                  </select> 

                </td>
              </tr>
              <tr>
                <td><?=isset($dataGlobal['propor_width']) ? $dataGlobal['propor_width'] : 'Proportional Width' ?></td>
                <td>:</td>
                <td valign="top">
                  <div class="form-group">
                  <label class="radio-inline">
                    <input name="relativo" type="radio" id="relativo"  value="1" 
                    <?php if($atabla["relativo"]=="1") : ?> checked="checked"  <?php endif;?>>
                    <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes' ?>
                    &nbsp;
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="relativo" value="0" id="relativo2" 
                    <?php if($atabla["relativo"]=="0") : ?> checked="checked"  <?php endif;?>>
                    <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not' ?>
                  </label>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
      </table>
    </div>
  </form>
</div>
<link rel="stylesheet" href="<?=CSS_P?>colorpicker.css" type="text/css" />
<link rel="stylesheet" href="<?=PLUGINS_P?>jquery.kolorpicker/style/kolorpicker.css" type="text/css"  />
<script type="text/javascript" src="<?=JS_P?>colorpicker.js"></script>
<script>
$('#color_text_btn').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#'+hex);
    $('#color_text_btn_btn').css('background-color', '#'+hex);
    $(el).ColorPickerHide();
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
});
$('#fondo_btn').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#'+hex);
    $('#fondo_btn_btn').css('background-color', '#'+hex);
    $(el).ColorPickerHide();
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
});
$('#color_text_descrip').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#'+hex);
    $('#color_text_descrip_btn').css('background-color', '#'+hex);
    $(el).ColorPickerHide();
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
});
$('#color_text_btn_btn').click(function() {$('#color_text_btn').click(); });
$('#fondo_btn_btn').click(function() {$('#fondo_btn').click(); });
$('#color_text_descrip_btn').click(function() {$('#color_text_descrip').click(); });
$('#color_text_btn').change(function(event) {
  $('#color_text_btn_btn').css('background-color', $(this).val());
});
$('#fondo_btn').change(function(event) {
  $('#fondo_btn_btn').css('background-color', $(this).val());
});
$('#color_text_descrip').change(function(event) {
  $('#color_text_descrip_btn').css('background-color', $(this).val());
});
$('.colorpicker_submit').html('<i class="fa fa-check"></i>');
$('.colorpicker_submit').css('background', '#0ec739');
$('.colorpicker_submit').css('color', '#000');
$('.colorpicker_submit').css('border-radius', '50%');
$('.colorpicker_submit').css('padding-left', '5px');
$('.colorpicker_submit').css('padding-top', '2px');

</script>

<?php include(dirname(__FILE__)."/../footer.php"); ?>