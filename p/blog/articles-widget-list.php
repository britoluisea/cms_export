<link href="<?=PLUGINS_P?>multiple-select/multiple-select.css" rel="stylesheet"/>
<script src="<?=PLUGINS_P?>multiple-select/jquery.multiple.select.js"></script>
<form action="save-widget-list.php" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
  <table width="600px" border="0" align="center" cellpadding="8" cellspacing="0">
    <tr>
      <td>
        <div class="navipage"><?=isset($dataGlobal['blog']) ? $dataGlobal['blog'] : 'Blog' ?> / <strong> <?=isset($dataGlobal['widget_list']) ? $dataGlobal['widget_list'] : 'Widget List' ?></strong></div>
      </td>
      <td>
        <table border="0" align="right" cellpadding="2" cellspacing="2">
          <tr>

            <td>
              <button  name="save" id="save" type="submit" class="btn btn-small btn-primary"  value="save"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save' ?></button>    
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <?php
          if (isset($_SESSION['data_guarda_bien']) && $_SESSION['data_guarda_bien'] == 1 )
          {?>
            <div class="alert alert-success text-center">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong><?=isset($dataGlobal['data_saved']) ? $dataGlobal['data_saved'] : 'Data Saved' ?> </strong> 
            </div>
            <?php
            unset ($_SESSION['data_guarda_bien']);
          }
        ?>
      </td>
    </tr>
  </table>
  <?php
      $i = 1;
      $sql_1="select * from articles_widget_list where idx='$i' "; 
      $consulta_1=ejecutar($sql_1);
      $atabla=array();
      while($fila_1=fetchAssoc($consulta_1))
      {
        $atabla=$fila_1;
        $opc = 2;
        $i = $atabla["idx"];
        $num_col_page=$atabla["num_col_page"];
        $num_reg_limit=$atabla["num_reg_limit"];
        $box_name=$atabla["box_name"];
        $text_color=$atabla["text_color"];
        $background_color=$atabla["background_color"];
        $layout=$atabla["layout"];
      }
  ?>
  <table border="0" width="600px" align="center" cellpadding="2" cellspacing="2">
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" width="200">
        <div align="left">
          <?=isset($dataGlobal['categories']) ? $dataGlobal['categories'] : 'Categories' ?>
        </div>
      </td>
      <td valign="top" width="10">:</td>
      <td width="490">
        <?php
          $sql_prodrel="select * from articles_widget_list_category where news_widget_id='1'  "; 
          $consulta_prodrel=ejecutar($sql_prodrel);
          $aprodrel=array();
          $cont=numRows($consulta_prodrel);
          while($fila_prodrel=fetchAssoc($consulta_prodrel))
          {	
            $opc = 2;
            $aprodrel[$fila_prodrel["category_id"]] = $fila_prodrel["category_id"];
          }	
        ?>
        <select multiple="multiple" name="category_id[]" class="form-control" data-placeholder="- Select Category -">
          <?php
            $sql="select * from articles_category order by item desc";
            $consulta=ejecutar($sql);
            while($fila=fetchArray($consulta))
            {
              ?>
              <option value="<?=$fila['idx']?>" <?php if(isset($aprodrel[$fila["idx"]]) && $fila['idx']==$aprodrel[$fila["idx"]]) :?> selected="selected" <?php endif; ?>>
                <?=$fila['nombre']?>
              </option>
              <?php 
            } 
          ?>
        </select>
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" ><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <input type="text" name="box_name" class="form-control" style="width:250px;" value="<?=$box_name?>">
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><div align="left"><?=isset($dataGlobal['number_records_page']) ? $dataGlobal['number_records_page'] : 'Number of records' ?></div></td>
      <td valign="top">:</td>
      <td>
        <select name="num_reg_limit"   class="form-control">
          <?php
            for($ifor = 1; $ifor <= 20; $ifor++)
            { ?>
              <option value="<?=$ifor?>" <?php if ($num_reg_limit == $ifor) {?>selected <?php } ?>>
                <?=$ifor?>
              </option>
              <?php
			      }
          ?>
        </select>
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><?=isset($dataGlobal['label_sort']) ? $dataGlobal['label_sort'] : 'Sort' ?></td>
      <td valign="top">:</td>    
      <td valign="top">
        <label>
          <input name="sort" type="checkbox" id="sort"  <?php if($atabla["sort"]==1){?> checked="checked" <?php } ?>/> <?=isset($dataGlobal['newest_at_top']) ? $dataGlobal['newest_at_top'] : 'Newest at top' ?>
        </label>
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>    
    <tr>
      <td valign="top" ><?=isset($dataGlobal['text_color']) ? $dataGlobal['text_color'] : 'Text Color' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <input type="text" name="text_color" id="text_color" class="form-control kolorPicker" style="width:120px;" value="<?=$text_color?>">
        <div id="text_col_btn" style="background-color:<?=$text_color?>;width: 33px;height: 33px;border: 1px solid #000;margin-left: 130px;"></div>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" ><?=isset($dataGlobal['background_color']) ? $dataGlobal['background_color'] : 'Background Color' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <input type="text" name="back_col" id="back_col" class="form-control kolorPicker" style="width:120px;" value="<?=$background_color?>">
        <div id="back_col_btn" style="background-color:<?=$background_color?>;width: 33px;height: 33px;border: 1px solid #000;margin-left: 130px;"></div>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><?=isset($dataGlobal['show_cover']) ? $dataGlobal['show_cover'] : 'Show cover' ?></td>
      <td valign="top">&nbsp;</td>
      <td valign="top">
        <select name="show_cover" class="form-control" id="show_cover" style="width:150px">
          <option value="1" <?php if($atabla["show_cover"]=="1") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes' ?>
          </option>
          <option value="0" <?php if($atabla["show_cover"]=="0") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not' ?>
          </option>
        </select>
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><?=isset($dataGlobal['show_date_time']) ? $dataGlobal['show_date_time'] : 'Show Date / Time' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <select name="show_date" class="form-control" id="show_date" style="width:150px">
          <option value="1" <?php if($atabla["show_date"]=="1") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes' ?>
          </option>
          <option value="0" <?php if($atabla["show_date"]=="0") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not' ?>
          </option>
        </select>
      </td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td valign="top">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><?=isset($dataGlobal['show_descrip']) ? $dataGlobal['show_descrip'] : 'Show Description' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <select name="show_descrip" class="form-control" id="show_descrip" style="width:150px">
          <option value="1" <?php if($atabla["show_descrip"]=="1") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes' ?>
          </option>
          <option value="0" <?php if($atabla["show_descrip"]=="0") { echo "selected='selected'"; } ?>>
            <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not' ?>
          </option>
        </select>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><?=isset($dataGlobal['label_layout']) ? $dataGlobal['label_layout'] : 'Layout' ?></td>
      <td valign="top">:</td>
      <td valign="top">
        <select name="layout" id="layout" class="form-control">
          <?php
            $dir =  dirname(__FILE__)."/../../".$_SESSION['THEME']."/widgets/blog/"; 
            if(file_exists($dir))
            {
              chdir ($dir);  ///vaciar carpeta de inner
              $dh=opendir('.');
              while ($file = readdir ($dh)) {
                    if ($file != "." && $file != "..") { ?>
                    <option value="<?=$file?>" <?php if($file==$layout) { echo "selected='selected'"; } ?>><?=strtoupper(substr($file, 0,-4))?></option>
                    <?php }
              }
              closedir($dh);
            }
          ?>
        </select>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><div align="left"><?=isset($dataGlobal['label_code']) ? $dataGlobal['label_code'] : 'Code' ?></div></td>
      <td valign="top">:</td>
      <td rowspan="2">
        <textarea class="form-control" readonly onClick="this.select()" ><?="<?php"?> include _INC."_blog_widget_list.php"; <?="?>"?></textarea>
      </td>
    </tr>
    <tr>
      <td valign="top"><br></td>
      <td valign="top"><br></td>
      <td><br></td>
    </tr>
    <tr>
      <td valign="top"><br></td>
      <td valign="top"><br></td>
      <td><br></td>
    </tr>
  </table>
</form>
<link rel="stylesheet" href="<?=CSS_P?>colorpicker.css" type="text/css" />
<link rel="stylesheet" href="<?=PLUGINS_P?>jquery.kolorpicker/style/kolorpicker.css" type="text/css"  />
<script type="text/javascript" src="<?=JS_P?>colorpicker.js"></script>
<script>
$('#back_col').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#'+hex);
    $('#back_col_btn').css('background-color', '#'+hex);
    $(el).ColorPickerHide();
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
});
$('#text_color').ColorPicker({
  onSubmit: function(hsb, hex, rgb, el) {
    $(el).val('#'+hex);
    $('#text_col_btn').css('background-color', '#'+hex);
    $(el).ColorPickerHide();
  },
  onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
  }
})
.bind('keyup', function(){
  $(this).ColorPickerSetColor(this.value);
});
$('#back_col_btn').click(function() {$('#back_col').click(); });
$('#text_col_btn').click(function() {$('#text_color').click(); });
$('.colorpicker_submit').html('<i class="fa fa-check"></i>');
$('.colorpicker_submit').css('background', '#0ec739');
$('.colorpicker_submit').css('color', '#000');
$('.colorpicker_submit').css('border-radius', '50%');
$('.colorpicker_submit').css('padding-left', '5px');
$('.colorpicker_submit').css('padding-top', '2px');
$('#back_col').change(function(event) {
  $('#back_col_btn').css('background-color', $(this).val());
});
$('#text_color').change(function(event) {
  $('#text_col_btn').css('background-color', $(this).val());
});
</script>
