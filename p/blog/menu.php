<?php $tab = isset($_GET['tab']) ? $_GET['tab'] : '1';?>
<ul class="nav nav-tabs" role="tablist">
    <li role="Post" class="<?php if($tab=='1'){echo'active';}?>">
        <a href="#tabs-1" aria-controls="tabs-1" role="Post" data-toggle="tab"><?=isset($dataGlobal['post']) ? $dataGlobal['post'] : 'Post' ?></a>
    </li>
    <li role="Categories" class="<?php if($tab=='2'){echo'active';}?>">
        <a href="#tabs-2" aria-controls="tabs-2" role="Categories" data-toggle="tab"><?=isset($dataGlobal['categories']) ? $dataGlobal['categories'] : 'Categories' ?></a>
    </li>
    <li role="Tags" class="<?php if($tab=='3'){echo'active';}?>">
        <a href="#tabs-3" aria-controls="tabs-3" role="Tags" data-toggle="tab"><?=isset($dataGlobal['label_tags']) ? $dataGlobal['label_tags'] : 'Tags' ?></a>
    </li>
    <li role="Widget List" class="<?php if($tab=='4'){echo'active';}?>">
        <a href="#tabs-4" aria-controls="tabs-4" role="Widget List" data-toggle="tab"><?=isset($dataGlobal['widget_list']) ? $dataGlobal['widget_list'] : 'Widget List' ?></a>
    </li>
</ul>