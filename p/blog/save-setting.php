<?php 
session_start();
include dirname(__FILE__)."/../../cn/cnx.php";
include dirname(__FILE__)."/../system/functions.php";
$widxamusuario = $_SESSION['value_admin_idx'];
$widxamusuario_pri = $_SESSION['value_admin_idxamusuario'];
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
    exit();
}
else
{	
    // variables 
    $ope = isset($_POST['ope']) ? $_POST['ope'] : '';
    $wwidth = isset($_POST['width']) ? trim($_POST['width']) : '';
    $wheight = isset($_POST['height']) ? trim($_POST['height']) : '';
    $quality = isset($_POST['quality']) ? $_POST['quality'] : '50';
    $relativo = isset($_POST['relativo']) ? $_POST['relativo'] : '1';
    $num_reg_page = isset($_POST['num_reg_page']) ? $_POST['num_reg_page'] : '10';
    $num_reg_page_cms = isset($_POST['num_reg_page_cms']) ? $_POST['num_reg_page_cms'] : '100';
    $face_like = isset($_POST['face_like']) ? $_POST['face_like'] : '0';
    $twitter_like = isset($_POST['twitter_like']) ? $_POST['twitter_like'] : '0';
    $face_share = isset($_POST['face_share']) ? $_POST['face_share'] : '0';
    $twitter_share = isset($_POST['twitter_share']) ? $_POST['twitter_share'] : '0';
    $face_comment = isset($_POST['face_comment']) ? $_POST['face_comment'] : '0';
    $comments_blog = isset($_POST['comments_blog']) ? $_POST['comments_blog'] : '0';
    $color_text_btn = isset($_POST['color_text_btn']) ? $_POST['color_text_btn'] : '#000000';
	$fondo_btn = isset($_POST['fondo_btn']) ? $_POST['fondo_btn'] : '#ffffff';
	$color_text_descrip = isset($_POST['color_text_descrip']) ? $_POST['color_text_descrip'] : '#555555';
	if ($ope == "articles-options") {
	    $sql_0 = "update articles_options 
				set
				width = '$wwidth',
				height = '$wheight',
				quality = '$quality',
				relativo = '$relativo',
				num_reg_page = '$num_reg_page',
				num_reg_page_cms = '$num_reg_page_cms',
				face_like ='$face_like',
				face_share ='$face_share',
				twitter_like ='$twitter_like',
				twitter_share = '$twitter_share',
				twitter_share = '$twitter_share',
				face_comment = '$face_comment',
				comments_blog = '$comments_blog',
				color_text_btn = '$color_text_btn',
				fondo_btn = '$fondo_btn',
				color_text_descrip = '$color_text_descrip'
				where
				idx = 1
				";
	    if (!$res_0 = ejecutar($sql_0)) {
	        echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	        exit();
	    }
	    $_SESSION['data_guarda_bien'] = 1;
	    header("Location: articles-setting.php");
	    //exit();
	}
}