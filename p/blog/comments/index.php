<?php 
	include(dirname(__FILE__)."/../../header.php");
	$action = isset($_GET['action']) ? $_GET['action'] : '';
	$status = isset($_GET['status']) ? $_GET['status'] : '';
	$search = isset($_GET['search']) ? $_GET['search'] : '';
	$p = isset($_GET['p']) ? $_GET['p'] : '';	
	$i = isset($_GET['i']) ? $_GET['i'] : '';	
	if($action=='approve')
	{
		$sql_0 ="update comments set comment_status_id = '2', comment_status_id_salvar = '2' where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{
			echo "Error al Eliminar "; 		
		}		
	}
	if($action=='unapprove')
	{
		$sql_0 ="update comments set comment_status_id = '1', comment_status_id_salvar = '1' where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{
			echo "Error al Eliminar "; 		
		}
	}
	if($action=='spam')
	{
		$sql_0 ="update comments set comment_status_id = '3' where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{
			echo "Error al Eliminar "; 		
		}
	}
	if($action=='trash')
	{
		$sql_0 ="update comments set comment_status_id = '4' where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{				
			echo "Error al Eliminar "; 		
		}
	}
	if($action=='notspam')
	{
		$sql_0 ="update comments set comment_status_id = comment_status_id_salvar where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{				
			echo "Error al Eliminar "; 		
		}
	}
	if($action=='restore')
	{
		$sql_0 ="update comments set comment_status_id = comment_status_id_salvar where idx  = '$i'  ";
		if (!$res_0=ejecutar($sql_0))	
		{				
			echo "Error al Eliminar "; 		
		}	
	}
 	$sqlAll="select * from comments "; 
	$consultaAll=ejecutar($sqlAll);
	$numAll=numRows($consultaAll);
 	$sqlPen="select * from comments  where comment_status_id='1'"; 
	$consultaPen=ejecutar($sqlPen);
	$numPen=numRows($consultaPen);
 	$sqlApp="select * from comments  where comment_status_id='2'"; 
	$consultaApp=ejecutar($sqlApp);
	$numApp=numRows($consultaApp);
 	$sqlTra="select * from comments  where comment_status_id='3'"; 
	$consultaTra=ejecutar($sqlTra);
	$numTra=numRows($consultaTra);
 	$sqlSpa="select * from comments  where comment_status_id='4'"; 
	$consultaSpa=ejecutar($sqlSpa);
	$numSpa=numRows($consultaSpa);
?>
<div class="container-fluid" id="main-container">
	<br><br>
	<div id="tabs">
		<table width="100%" border="0" cellspacing="2" cellpadding="2">
			<tbody>
				<tr>
					<td>
						<?php include('menu.php'); ?>
					</td>
					<td>
						<div>
							<form id="form_busqueda" name="form_busqueda" class="form-inline" method="get" action="">
								<table align="right" border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td>
											<div class="form-group">
												<label class="sr-only" for="search"><?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?></label>
												<div class="input-group">
													<div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
													<input type="text" name="search" id="search" value="<?=$search?>" placeholder="<?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?>..." autocomplete="off" class="form-control">
												</div>
											</div>
											<button type="submit" class="btn btn-dark"><?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?></button>
											<a href="<?=$_SERVER["PHP_SELF"]?>?tab=2" class="btn btn-small btn-default"> <?=isset($dataGlobal['label_all']) ? $dataGlobal['label_all'] : 'All' ?> </a>
										</td>
									</tr>
								</table>
							</form>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="tab-content">
			<?php if(isset($_SESSION["error"])): ?>
				<div align="center"  class="alert alert-error"><?=$_SESSION["error"]?></div>
				<?php unset($_SESSION['error']);	?>
			<?php endif;?>
			<table border="0" align="center" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="150" ><div align="left"><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User' ?></div></th>
						<th ><div align="left"><?=isset($dataGlobal['label_comment']) ? $dataGlobal['label_comment'] : 'Comment' ?></div></th>
						<th width="250" class="grilla_encab"><div align="left"><?=isset($dataGlobal['in_response_to']) ? $dataGlobal['in_response_to'] : 'In Response to' ?></div></th>
					</tr>
				</thead>
				<?php
					$wfiltro="and (comment_status_id='1' OR comment_status_id='2')";
					if($status=='pending')
					{
						$wfiltro=" and comment_status_id='1' ";
					}
					if($status=='approved')
					{
						$wfiltro=" and comment_status_id='2' ";
					}
					if($status=='spam')
					{
						$wfiltro=" and comment_status_id='3' ";
					}
					if($status=='trash')
					{
						$wfiltro=" and comment_status_id='4' ";
					}
					if(!empty($p))
					{
						$wfiltro = $wfiltro." and article_id='$p' ";
					}
					$t1="comments";
					$t2="usuarios";
					$t3="comments_status";
					$t4="articles";
					$sql="select 
					$t1.*,
					$t2.idx as usu_idx, 
					$t2.username as usu_username,
					$t2.nombres as usu_nombres,
					$t2.apellidos as usu_apellidos,
					$t2.userlevel as usu_userlevel,
					$t3.idx as comm_stat_idx,
					$t3.nombre as comm_stat_nombre,
					$t4.idx as art_idx,
					$t4.nombre as art_nombre,
					$t4.slug as art_slug
					from
					$t1,$t2,$t3,$t4
					where
					$t1.user_id = $t2.idx and 
					$t1.comment_status_id = $t3.idx and 
					$t1.article_id = $t4.idx and 
					($t1.descrip like '%$search%')
					$wfiltro
					order by $t1.fecreg desc ";
					$consulta=ejecutar($sql);
					$num_reg=numRows($consulta);
					$pages = new Paginator;
					$pages->items_total = $num_reg;
					$pages->mid_range = 10;
					$pages->paginate(40);
					$item = 0;
					$consulta=ejecutar($sql.$pages->limit);
					$num_reg=numRows($consulta);
					if($num_reg == 0)
					{ ?>
						<td colspan="3" ><div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?></div></td>
			            <?php
					}
					else
					{
			            while($fila=fetchArray($consulta))
			            {   
			            	$item++; ?> 
			            	<tr>
			            		<td valign="top">
			            			<div class="cat_nombre" align="left"><?=$fila["usu_nombres"].' '.$fila["usu_apellidos"]?></div>
			            			<div style="color:#0353a4" align="left"><?=$fila["usu_username"]?></div>
			            		</td>
			            		<td valign="top">
			            			<div>
			            				<?=isset($dataGlobal['submitted_on']) ? $dataGlobal['submitted_on'] : 'Submitted on' ?> 
			            				<span style="color:#0353a4">
			            					<?= strftime('%b %d, %Y', strtotime($fila['fecreg'])) ?> at 
			            					<?= strftime('%I:%M:%S %p', strtotime($fila['fecreg'])) ?> 
			            				</span>
			            				<?php 
			            					if($fila['comment_parent'] <> 0)
			            					{ ?>
			            						| In <span class="text_reply">reply</span> to 
			            						<?php
												$t1="comments";
												$t2="usuarios";
												$sql_1="select 
												$t1.*,
												$t2.idx as usu_idx, 
												$t2.username as usu_username,
												$t2.nombres as usu_nombres,
												$t2.apellidos as usu_apellidos,
												$t2.userlevel as usu_userlevel
												from
												$t1,$t2
												where
												$t1.idx = '".$fila['comment_parent']."' and
												$t1.user_id = $t2.idx
										 		";
												$consulta_1=ejecutar($sql_1);
												$num_reg=numRows($consulta_1);
												if($fila_1=fetchArray($consulta_1))
												{
													?>
			        								<span style="color:#0353a4">
			        									<?=$fila_1['usu_nombres'].' '.$fila_1['usu_apellidos']?>.
			        								</span> <?php 
			        							} 
			        						} 
			        					?>
			        				</div>
			        				<div><?=($fila['descrip'])?></div>
			        				<?php 
			        				if($fila['comm_stat_idx']==1 or $fila['comm_stat_idx']==2)
			        				{
			        					$wstatus="";
			        					if(!empty($status)) { $wstatus="&status=".$status; } 
			        					$wp="&p=";
			        					if(!empty($p)) { $wp="&p=".$p; }
			        					?>
			        					<div class="row-actions">
			        						<?php if($fila['comm_stat_idx']==2):?>
			        							<span class="unapprove"><a href="index.php?i=<?=$fila['idx']?>&action=unapprove<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_unapprove']) ? $dataGlobal['label_unapprove'] : 'Unapprove' ?></a></span>
			        						<?php endif;?>
			        						<?php if($fila['comm_stat_idx']==1):?>
			        							<span class="approve">	<a href="index.php?i=<?=$fila['idx']?>&action=approve<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_approve']) ? $dataGlobal['label_approve'] : 'Approve' ?></a></span>
			        						<?php endif;?>
			        						<?php if($fila['comment_parent'] == 0):?>
			        							<span class="reply"> | 	<a  href="#" idx="<?=$fila['idx']?>" class="modal-reply"><?=isset($dataGlobal['label_reply']) ? $dataGlobal['label_reply'] : 'Reply' ?></a></span>
			        						<?php endif;?>
			        						<span class="edit">  | 	<a href="comments-n.php?i=<?=$fila['idx']?>">Edit</a></span>
			        						<span class="spam">  | 	<a href="index.php?i=<?=$fila['idx']?>&action=spam<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_spam']) ? $dataGlobal['label_spam'] : 'Spam' ?></a></span>
			        						<span class="trash"> | 	<a href="index.php?i=<?=$fila['idx']?>&action=trash<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_trash']) ? $dataGlobal['label_trash'] : 'Trash' ?></a></span>
			        					</div> <?php 
			        				} 
			        				?>
			        				<?php if($fila['comm_stat_idx']==3):?>
			        					<div class="row-actions">
			        						<span class="edit"><a href="index.php?i=<?=$fila['idx']?>&action=notspam"><?=isset($dataGlobal['label_no_spam']) ? $dataGlobal['label_no_spam'] : 'Not Spam' ?></a></span>
			        						<span class="spam">|</span>
			        						<span class="spam"><a href="#" idx="<?=$fila["idx"]?>" class="deleteComments"><?=isset($dataGlobal['del_permanent']) ? $dataGlobal['del_permanent'] : 'Delete Permanently' ?></a></span>
			        					</div>
			        				<?php endif;?>
			        				<?php if($fila['comm_stat_idx']==4):?>
			        					<div class="row-actions">
			        						<span class="edit"><a href="index.php?i=<?=$fila['idx']?>&action=restore"><?=isset($dataGlobal['lable_restore']) ? $dataGlobal['lable_restore'] : 'Restore' ?></a></span>
			        						<span class="spam">|</span>
			        						<span class="spam"><a href="#" idx="<?=$fila["idx"]?>" class="deleteComments"><?=isset($dataGlobal['del_permanent']) ? $dataGlobal['del_permanent'] : 'Delete Permanently' ?></a></span>
			        					</div>
			        				<?php endif;?>
			        				<div>
										<span class="countLike"><?=$fila['count_like']?> <?=isset($dataGlobal['label_like']) ? $dataGlobal['label_like'] : 'Like' ?></span>
										<span class="countUnlike"><?=$fila['count_unlike']?> <?=isset($dataGlobal['label_unlike']) ? $dataGlobal['label_unlike'] : 'Unlike' ?></span>
									</div>
								</td>
								<td valign="top">
									<div> <?=$fila['art_nombre']?></div>
									<?php
										$t1="comments";
										$sql_post_pend="select 
										$t1.*
										from
										$t1
										where
										$t1.article_id = '".$fila['art_idx']."'  and
										$t1.comment_status_id = '1'
								 		";
										$consulta_post_pend=ejecutar($sql_post_pend);
										$num_post_pend=numRows($consulta_post_pend);
										$t1="comments";
										$sql_post_appr="select 
										$t1.*
										from
										$t1
										where
										$t1.article_id = '".$fila['art_idx']."'  and
										$t1.comment_status_id = '2'
								 		";
										$consulta_post_appr=ejecutar($sql_post_appr);
										$num_post_appr=numRows($consulta_post_appr);
									?>
									<div>
										<a href="index.php?p=<?=$fila['art_idx']?>" title="<?=$num_post_appr?> Approved - <?=$num_post_pend?> Pending">
											<span class="badge badge-info"><?=$num_post_appr?></span>
										</a>
										<span class="view_post">
											<a href="<?=SERVER?>blog/<?=$fila['art_slug']?>" target="_blank" >View Post</a>
										</span>
									</div>
								</td>
							</tr>
							<?php
						}
					}
				?>
				<tfoot>
					<tr>
						<td valign="top"><div align="left"><strong><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User' ?></strong></div></td>
						<td valign="top"><div align="left"><strong><?=isset($dataGlobal['label_comment']) ? $dataGlobal['label_comment'] : 'Comment' ?></strong></div></td>
						<td valign="top"><div align="left"><strong><?=isset($dataGlobal['in_response_to']) ? $dataGlobal['in_response_to'] : 'In Response to' ?></strong></div></td>
					</tr>
				</tfoot>
			</table>
			<?php	
				if ($pages->num_pages > 1){?>
					<div align="center" style="padding:8px;">
					  <?php echo $pages->display_pages(); ?> 
					</div> <?php 
				}	
			?>
		</div>
	</div>
</div>
<?php include('comments-reply.php');?>
<script>  
$(document).ready(function(){
    $(".deleteComments").click(function(event) {
      var i = $(this).attr('idx');;
      alerConfirm("<?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete' ?>", "<?=isset($dataGlobal['msj_del_item']) ? $dataGlobal['msj_del_item'] : 'Are you sure you want to delete this item?' ?>" , i, 'deleteComments');
    });
});
	var deleteComments = function(i)
	{
      var idx = i;
      var datos = {
      	'i': i,
      	'ope': 'comment',
      	'opc': '3',
      	'ajax': '1'
      };
      $.ajax({
      	url: 'save-comments.php',
      	type: 'POST',
      	data: datos,
      })
      .done(function(r) {
      	console.log("success");
      	if(r=='0'){location.href= 'index.php?status=trash';}
      	else{alerError('Error', r, 3000);}
      })
      .fail(function() {
      	console.log("error");
      	alerError('Error', 'SERVER 500', 3000);
      });
	}
</script>
<?php include(dirname(__FILE__)."/../../footer.php"); ?>