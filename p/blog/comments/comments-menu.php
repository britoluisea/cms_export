<script src="assets/filecookie/ace-extra.min.js"></script>
<script src="assets/filecookie/ace-elements.min.js"></script>
<script src="assets/filecookie/ace.min.js"></script>
<?php
//echo $f_pagina;
$menuLatBlog=array();
if ($f_pagina ==  'comments.php' 		)   {	$menuLatBlog[1]		= 'class="active"';	}	
?>
<a href="#" id="menu-toggler"><span></span></a><!-- menu toggler -->

	<div id="sidebar" class="sidebar" >
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>
 
    <ul class="nav nav-list">

       <li>
					  <a href="javascript:void(0)">
						<i class="icon-dashboard"></i>
						<span class="text-uppercase"><?=isset($dataGlobal['blog']) ? $dataGlobal['blog'] : 'Blog' ?></span>
						
					  </a>
					</li>

        <li <?=$menuLatBlog[1]?>>
            <a href="comments.php" class="dropdown-toggle" >
                <i class="icon-file-alt"></i>
                <span><?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?></span>               
            </a>
        </li>
         

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
    </script>
</div><!--/#sidebar-->