<?php
session_start();
include dirname(__FILE__)."/../../../cn/cnx.php";
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) 
{
    header("Location: ".dirname(__FILE__)."/../../../index.php");
    exit();
}
else
{
    $ajax = isset($_POST['ajax']) ? trim($_POST['ajax']) : '';
    $ope = isset($_POST['ope']) ? $_POST['ope'] : '';
    $opc = isset($_POST['opc']) ? $_POST['opc'] : '';
    $i = isset($_POST['i']) ? trim($_POST['i']) : '';
    $comment_status_id = isset($_POST['comment_status_id']) ? trim($_POST['comment_status_id']) : '';
    $descrip = isset($_POST['descrip']) ? trim($_POST['descrip']) : '';
    $fecreg = date("Y-m-d H:i:s");
    $wips = getenv('REMOTE_ADDR');
    $tb = "comments";        
    $error=0;
    if ($ope == "comment") {
        if ($opc == 2) {
            if ($comment_status_id == 1 or $comment_status_id == 2) {
                $sql_0 = "update $tb set  
                descrip = '$descrip',
                comment_status_id = '$comment_status_id',
                comment_status_id_salvar = '$comment_status_id'
                 where
                 idx = '$i' ";
            }
            if ($comment_status_id == 3 or $comment_status_id == 4) {
                $sql_0 = "update $tb set  
                descrip = '$descrip',
                comment_status_id = '$comment_status_id'
                 where
                 idx = '$i' ";
            }
            if (!$res_0 = ejecutar($sql_0)) {
                echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;                
            }
        }
        if ($opc == 3) 
        {
            $sql_0 = "delete from $tb where idx = '$i'";            
            if (!$res_0 = ejecutar($sql_0)) {
                $error=$sql_0." <br>Error al insertar y/o modificar tabla " ;                
            }
            
        }
    }
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "comment-reply") {
        $comment_status_id=2;        
        if ($opc == 1) 
        {
            $sql_1 = "select * from $tb where idx='$i' "; 
            $consulta_1 = ejecutar($sql_1);
            $atabla = array();
            if ($fila_1 = fetchAssoc($consulta_1)) {
                $atabla = $fila_1;
            }
            $sql_0 = "insert into comments (article_id, descrip, user_id, comment_parent, fecreg, comment_status_id, comment_status_id_salvar, ips)
                                  values ('" . $atabla['article_id'] . "', '$descrip', '" . $_SESSION['value_admin_idxamusuario'] . "', '" . $atabla['idx'] . "', '$fecreg', '".$comment_status_id."', '2', '$wips')";
            if (!$res_0 = ejecutar($sql_0)) {
                $error=$sql_0." <br>Error al insertar y/o modificar tabla " ;                
            }
        }
    }
    if($ajax=='')
    {
        if($comment_status_id=='1'){$status = 'pending';}
        elseif($comment_status_id=='2'){$status = 'approved';}
        elseif($comment_status_id=='3'){$status = 'spam';}
        elseif($comment_status_id=='4'){$status = 'trash';}
        else{$status = '';}
        header("Location: index.php?status=".$status);
    }
    else
    {
        echo $error;
    }
}