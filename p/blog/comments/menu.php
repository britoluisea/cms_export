<?php
  	$aimpComm=array();
 	//$comment_status="";
 	if(empty($status))
 	{
	  $aimpComm[0] = 'class="active"';
	}
	if($status=='pending')
	{
    	$aimpComm[1] = 'class="active"';
	}
	if($status=='approved')
	{
		$aimpComm[2] = 'class="active"';
	}
	if($status=='spam')
	{
		$aimpComm[3] = 'class="active"';
	}
	if($status=='trash')
	{
		$aimpComm[4] = 'class="active"';
	}
?>
<ul class="nav nav-tabs" role="tablist">
    <li role="All" <?=isset($aimpComm[0])? $aimpComm[0]: ''?>>
<?=isset($dataGlobal['label_all']) ? $dataGlobal['label_all'] : 'All' ?>
        <a href="index.php"  role="All" ></a>
    </li>
    <li role="Pending" <?=isset($aimpComm[1])? $aimpComm[1]: ''?>>
        <a href="index.php?status=pending"  role="Pending" ><?=isset($dataGlobal['label_pending']) ? $dataGlobal['label_pending'] : 'Pending' ?>  <span class="count">(<?=$numPen?>)</span></a>
    </li>
    <li role="Approved" <?=isset($aimpComm[2])? $aimpComm[2]: ''?>>
        <a href="index.php?status=approved"  role="Approved" ><?=isset($dataGlobal['label_approved']) ? $dataGlobal['label_approved'] : 'Approved' ?> <span class="count">(<?=$numApp?>)</span></a>
    </li>
    <li role="Spam" <?=isset($aimpComm[3])? $aimpComm[3]: ''?>>
        <a href="index.php?status=spam"  role="Spam" ><?=isset($dataGlobal['label_spam']) ? $dataGlobal['label_spam'] : 'Spam' ?> <span class="count">(<?=$numTra?>)</span></a> </a>
    </li>
    <li role="Trash" <?=isset($aimpComm[4])? $aimpComm[4]: ''?>>
        <a href="index.php?status=trash"  role="Trash" ><?=isset($dataGlobal['label_trash']) ? $dataGlobal['label_trash'] : 'Trash' ?> <span class="count">(<?=$numSpa?>)</span></a>
    </li>
</ul>

