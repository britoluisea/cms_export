<?php
session_start();
include dirname(__FILE__)."/../../../cn/cnx.php";
include dirname(__FILE__)."/../../../varRutas.php";
include dirname(__FILE__)."/../../system/functions.php";
include dirname(__FILE__)."/../../system/languages/languages.php";
if (!isset($_SESSION['user']['login']) || $_SESSION['user']['login'] == false) 
{
    header("Location: ".dirname(__FILE__)."/../../../index.php");
    exit();
}
else
{
	require(dirname(__FILE__).'/../../plugins/phpmailer/class.phpmailer.php');
	require(dirname(__FILE__).'/../../plugins/phpmailer/class.smtp.php');
	function htmlEmail()
	{
		ob_start(); //Abrimos el buffer			
	    include (dirname(__FILE__)."/../../plugins/phpmaileremail/comment-new-email.php");
	    $email_text = ob_get_contents(); //Vaciamos el buffer
	    ob_end_clean();
	    return $email_text;
	}
	$ope = isset($_POST['ope']) ? $_POST['ope'] : '';
	$slug = isset($_POST['slug']) ? trim($_POST['slug']) : '';
	$id = isset($_POST['id']) ? trim($_POST['id']) : '';
	$fecreg = date("Y-m-d H:i:s");
	$wips = getenv('REMOTE_ADDR');
	//echo'<pre>'; print_r($_POST); echo'</pre>';	
	if ($ope == "comment") 
	{
	    $wdescrip = isset($_POST['descrip']) ? trim($_POST['descrip']) : '';
		if(!empty($wdescrip))
		{
			$sql_0 = "insert into comments (article_id, descrip, user_id , fecreg, comment_status_id, comment_status_id_salvar, ips )
								  values ('$id', '$wdescrip', '".$_SESSION['user']['idx']."', '$fecreg', '1', '1', '$wips')"; 
			if (!$res_0 = ejecutar($sql_0)) {
				echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
				exit();
			}
			$idArt=obtener_idx($sql_0);
			$t1="comments";
			$t2="usuarios";
			$t4="articles";
			$sql="select 
			$t1.*,
			$t2.idx as usu_idx, 
			$t2.username as usu_username,
			$t2.nombres as usu_nombres,
			$t2.apellidos as usu_apellidos,
			$t2.userlevel as usu_userlevel,
			$t4.nombre as art_nombre,
			$t4.slug as art_slug
			from
			$t1,$t2,$t4
			where
			$t1.idx = '".$idArt."' and
			$t1.user_id = $t2.idx and 
			$t1.article_id = $t4.idx
			order by $t1.fecreg desc ";
			$consulta=ejecutar($sql);
		 	$atabla_comm = array();
			if ($fila_comm= fetchAssoc($consulta)) {
				$atabla_comm[0] = $fila_comm;
			}
			if ($_SERVER['HTTP_HOST'] <> 'localhost') {
				
				$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
	            $consulta_company = ejecutar($sql_company);
	            $atabla_company = array();
	            while ($fila_company = fetchAssoc($consulta_company)) {
	                $atabla_company['en'] = $fila_company;
	            }
				//Especificamos los datos y configuración del servidor
				$sqlPen="select * from comments  where comment_status_id='1'"; //Devuelve todos los Idiomas
				$consultaPen=ejecutar($sqlPen);
				$numPen=numRows($consultaPen);				
				$email_text = htmlEmail();
				$mail = new PHPMailer();
				$mail->From = $atabla_comm[0]['usu_username'];
				$mail->FromName = $atabla_comm[0]['usu_nombres'].' '.$atabla_comm[0]['usu_apellidos'];
				$please_moderate=isset($dataGlobal['please_moderate']) ? $dataGlobal['please_moderate'] : 'Please Moderate';
				$mail->Subject = $please_moderate.': "'.$atabla_comm[0]['art_nombre'].'"‏';
				$mail->AltBody = "";
				$mail->MsgHTML($email_text);
				//$mail->AddAttachment("adjunto.txt");
				//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
				$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
				$mail->IsHTML(true);
				$mail->CharSet = 'UTF-8';
				if (!$mail->send()) {
					echo "Mailer Error: " . $mail->ErrorInfo;
					header("Location: ".SERVER."blog/".$atabla_comm[0]['art_slug']);
					exit();
				} else {
					//echo "Message sent!";
				}
			}
			//header("Location: blog/".$slug."/#comment-".mysql_insert_id());
			$_SESSION['saved_comment'] =isset($dataGlobal['saved_comment']) ? $dataGlobal['saved_comment'] : 'Your comment has been sent and will be publishing within moments, appreciate your patience.';
			header("Location: ".SERVER."blog/".$atabla_comm[0]['art_slug']);
			exit();
		}
		else
		{
			header("Location: ".SERVER."comments-post.php");
			exit();
		}	
	}
	if ($ope == "comment_resp") 
	{
	    $comment_parent = isset($_POST['comment_parent']) ? trim($_POST['comment_parent']) : '';
		$wdescrip=isset($_POST["descrip_".$comment_parent]) ? trim($_POST["descrip_".$comment_parent]) : '';;	
		if(!empty($wdescrip))
		{
			$sql_0 = "insert into comments (article_id, descrip, user_id, comment_parent, fecreg, comment_status_id, comment_status_id_salvar, ips)
								  values ('$id', '$wdescrip', '".$_SESSION['user']['idx']."', '$comment_parent', '$fecreg', '1', '1', '$wips')"; 
			if (!$res_0 = ejecutar($sql_0)) {
				echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
				exit();
			}
			
			
			$idArt=obtener_idx($sql_0);
			$t1="comments";
			$t2="usuarios";
			$t4="articles";
			$sql="select 
			$t1.*,
			$t2.idx as usu_idx, 
			$t2.username as usu_username,
			$t2.nombres as usu_nombres,
			$t2.apellidos as usu_apellidos,
			$t2.userlevel as usu_userlevel,
			$t4.nombre as art_nombre,
			$t4.slug as art_slug
			from
			$t1,$t2,$t4
			where
			$t1.idx = '".$idArt."' and
			$t1.user_id = $t2.idx and 
			$t1.article_id = $t4.idx
			order by $t1.fecreg desc ";
			$consulta=ejecutar($sql);
		 	$atabla_comm = array();
			if ($fila_comm= fetchAssoc($consulta)) {
				$atabla_comm[0] = $fila_comm;
			}
			if ($_SERVER['HTTP_HOST'] <> 'localhost') {
				
				$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
	            $consulta_company = ejecutar($sql_company);
	            $atabla_company = array();
	            while ($fila_company = fetchAssoc($consulta_company)) {
	                $atabla_company['en'] = $fila_company;
	            }
				//Especificamos los datos y configuración del servidor
				$sqlPen="select * from comments  where comment_status_id='1'"; //Devuelve todos los Idiomas
				$consultaPen=ejecutar($sqlPen);
				$numPen=numRows($consultaPen);				
				$email_text = htmlEmail();
				$mail = new PHPMailer();
				$mail->From = $atabla_comm[0]['usu_username'];
				$mail->FromName = $atabla_comm[0]['usu_nombres'].' '.$atabla_comm[0]['usu_apellidos'];
				$please_moderate=isset($dataGlobal['please_moderate']) ? $dataGlobal['please_moderate'] : 'Please Moderate';
				$mail->Subject = $please_moderate.': "'.$atabla_comm[0]['art_nombre'].'"‏';
				$mail->AltBody = "";
				$mail->MsgHTML($email_text);
				//$mail->AddAttachment("adjunto.txt");
				//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
				$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
				$mail->IsHTML(true);
				$mail->CharSet = 'UTF-8';
				if (!$mail->send()) {
					echo "Mailer Error: " . $mail->ErrorInfo;
					//exit();
				} //else { echo "Message sent!"; }
			}
			
			
			$_SESSION['saved_comment'] = isset($dataGlobal['saved_comment']) ? $dataGlobal['saved_comment'] : 'Your comment has been sent and will be publishing within moments, appreciate your patience.';
			header("Location: ".SERVER."blog/".$atabla_comm[0]['art_slug']);
			exit();
		}
		else
		{
			header("Location: ".SERVER._INC."comments-post.php");
			exit();
		}	
	}
}