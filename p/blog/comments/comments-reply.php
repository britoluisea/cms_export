  <form action="save-comments.php" method="post" name="form1" id="form1-reply">
    <div class="modal fade" id="modal-reply">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only"><?=isset($dataGlobal['btn_close']) ? $dataGlobal['btn_close'] : 'close' ?></span>
            </button>
            <div class="edit"><h3 class="modal-title"><?=isset($dataGlobal['reply_comment']) ? $dataGlobal['reply_comment'] : 'Reply to Comment' ?></h3></div>
          </div>
          <div class="modal-body">
            
            <div style="padding:4px;"><strong><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Description' ?>:</strong></div>
            <textarea name="descrip" id="descrip" cols="45" rows="5" class="form-control" required="required"></textarea>
            <input type="hidden" id="reply-i" value=""/>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              <i class="fa fa-close"></i> <?=isset($dataGlobal['btn_close']) ? $dataGlobal['btn_close'] : 'close' ?>
            </button>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-save"></i> <?=isset($dataGlobal['label_reply']) ? $dataGlobal['label_reply'] : 'Reply' ?>
            </button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </form>
<script type="text/javascript">
  $(document).ready(function(){
    $(".modal-reply").click(function(event) {
      event.preventDefault();
      var i = $(this).attr('idx');;
      $("#reply-i").val(i);
      $('#modal-reply').modal({show:true});
    });
    $("#form1-reply").submit(function (event) {
      event.preventDefault();
      $input = $("#descrip");
      value = $.trim($input.val());
      if(value.length < 1 || $("#reply-i").val().length < 1) {
        $("#descrip").focus();
        return false;
      }
      else
      {
        var datos = {
          'i': $("#reply-i").val(),
          'ope': 'comment-reply',
          'opc': '1',
          'ajax': '1',
          'descrip': $("#descrip").val()
        };
        $.ajax({
          url: 'save-comments.php',
          type: 'POST',
          data: datos,
        })
        .done(function(r) {
          console.log("success");
          $("#reply-i").val('');
          $("#descrip").val('');
          $('#modal-reply').modal('hide'); 
          if(r=='0'){location.href= 'index.php?status=trash';}
          else{alerError('Error', r, 3000);}
        })
        .fail(function() {
          console.log("error");
          alerError('Error', 'SERVER 500', 3000);
        });
      }
    });

});

</script>