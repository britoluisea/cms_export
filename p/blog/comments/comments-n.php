<?php
    include(dirname(__FILE__)."/../../header.php"); 
    $i = isset($_GET['i']) ? $_GET['i'] : '';
    if(empty($_GET['i'])){header('location:index.php');}
    $wdisplay = "display:none";
?>
<style type="text/css">
    .help-inline
    {
    	color: red;
    	margin-bottom:10px;
    }
    /* Checkbox & Radio */
    input[type=checkbox] , input[type=radio] {
        opacity:0;
        position:absolute;
        z-index:12;
        width:18px; height:18px;
    }
    input[type=checkbox]:focus , input[type=radio]:focus ,
    input[type=checkbox]:checked , input[type=radio]:checked
    {
        outline:none !important;
    }

    input[type=checkbox] + .lbl , input[type=radio] + .lbl {
        position: relative; z-index:11;
        display:inline-block;
        margin:0;
        line-height:20px;

        min-height:14px;
        min-width:14px;
        font-weight:normal;
    }

    input[type=checkbox] + .lbl.padding-4::before , input[type=radio] + .lbl.padding-4::before  {
        margin-right:4px;
    }
    input[type=checkbox] + .lbl.padding-6::before , input[type=radio] + .lbl.padding-6::before {
        margin-right:6px;
    }
    input[type=checkbox] + .lbl.padding-8::before , input[type=radio] + .lbl.padding-8::before {
        margin-right:8px;
    }


    input[type=checkbox] + .lbl::before , input[type=radio] + .lbl::before {
        font-family:fontAwesome; font-weight:normal;
        font-size: 11px; color: #32A3CE;
        content:"\a0";
        display:inline-block;
        background-color: #FAFAFA;
        border: 1px solid #CCC;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05);
        inset 0px -15px 10px -12px rgba(0,0,0,0.05);
        border-radius: 0;
        display: inline-block;
        text-align:center;

        vertical-align:middle;

        height:13px; line-height:13px;
        min-width:13px;	

        margin-right:1px;
    }


    input[type=checkbox]:active + .lbl::before, input[type=checkbox]:checked:active + .lbl::before ,
    input[type=radio]:active + .lbl::before, input[type=radio]:checked:active + .lbl::before
    {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    input[type=checkbox]:checked + .lbl::before ,
    input[type=radio]:checked + .lbl::before
    {
        display:inline-block;
        content: '\f00c';
        background-color: #f5f8fC;
        border-color:#adb8c0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    }


    input[type=checkbox]:hover + .lbl::before ,
    input[type=radio]:hover + .lbl::before,
    input[type=checkbox] + .lbl:hover::before ,
    input[type=radio] + .lbl:hover::before
    {
        border-color:#FF893C;
    }


    input[type=checkbox].ace-checkbox-2 + .lbl::before {
        box-shadow: none;
    }
    input[type=checkbox].ace-checkbox-2:checked + .lbl::before {
        background-color: #F9A021;
        border-color:#F9A021;
        color: #FFF;
    }



    input[type=checkbox]:disabled + .lbl::before ,
    input[type=radio]:disabled + .lbl::before {
        background-color:#DDD !important;
        border-color:#CCC !important;
        box-shadow:none !important;

        color:#BBB;
    }


    input[type=radio] + .lbl::before {
        border-radius:32px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:36px;
    }
    input[type=radio]:checked + .lbl::before{
        content:"\2022";
    }
</style>       
<div class="container" id="main-container">  
    <br><br>              
    <div class="row">
        <form action="save-comments.php" method="post" enctype="multipart/form-data" name="form1">
            <?php  
        		$t1="comments";
        		$t2="usuarios";
        		$sql_1="select 
        		$t1.*,
        		$t2.idx as usu_idx, 
        		$t2.username as usu_username,
        		$t2.nombres as usu_nombres,
        		$t2.apellidos as usu_apellidos,
        		$t2.userlevel as usu_userlevel
        		from
        		$t1,$t2
        		where
        		$t1.idx = '".$i."' and
        		$t1.user_id = $t2.idx
         		";
            	$consulta_1=ejecutar($sql_1);
            	$atabla=array();
            	while($fila_1=fetchAssoc($consulta_1))
            	{
            		$atabla['en']=$fila_1;
            		/*echo "<pre>";
            		print_r($atabla);
            		echo "</pre>";*/
            		$opc = 2;
            	}
            ?>      
            <input type="hidden" name="ope" value="comment"/>
            <input type="hidden" name="opc" value="<?=$opc?>" />
            <input type="hidden" name="i" value="<?=$i?>" />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td>
                            <div style="font-size:20px; font-weight:bold; color:#2679b5">
                            </div>
                        </td>
                        <td align="right">
                            <button  name="save" id="save" type="submit" class="btn btn-small btn-primary"  value="save"><i class="fa fa-save "></i> Save</button>
                            <a href="index.php" class="btn btn-small btn-default "><i class="fa fa-reply"></i> Cancel</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-9">
                    <br>
                    <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
                        <tbody>
                            <tr>
                                <td class="widget-header-tag">
                                    <div style="padding:4px; font-size:14px; color:#669fc7">
                                        <?php if($opc == 1){echo "Add Comment ";}else{echo "Edit Comment ";}?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td height="120" valign="top" style="padding:10px;">
                                    <table border="0" cellpadding="4" cellspacing="4">
                                        <tr>
                                            <td valign="top"><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User' ?></td>
                                            <td>:</td>
                                            <td valign="top"><strong><?=$atabla['en']["usu_nombres"].' '.$atabla['en']["usu_apellidos"]?></strong></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email' ?></td>
                                            <td>:</td>
                                            <td valign="top"><strong><?=$atabla['en']["usu_username"]?></strong></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td valign="top">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                                        <tr>
                                            <td  valign="top">    
                                                <div class="tabbable">
                                                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                                                        <li  class="active" >
                                                            <a data-toggle="tab" href="#tab1" style="border-radius: 0;">
                                                                <i class="icon-list-alt"></i><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Description' ?>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="tab1" class="tab-pane active ">
                                                            <textarea name="descrip" id="descrip" cols="45" rows="8" class="form-control" style="border-radius: 0;"><?=$atabla['en']["descrip"]?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3">
                    <br>
                    <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
                        <tbody>
                            <tr>
                                <td class="widget-header-tag">
                                    <div style="padding:4px; font-size:14px; color:#669fc7"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status' ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td height="120" valign="top">
                                    <div class="control-group">
                                        <div class="radio">
                                            <label>
                                                <input name="comment_status_id" type="radio" class="ace" value="1" <?php if($atabla['en']["comment_status_id"]==1):?> checked="CHECKED" <?php endif;?>>
                                                <span class="lbl" style="color:#ffa500"> <?=isset($dataGlobal['label_pending']) ? $dataGlobal['label_pending'] : 'Pending' ?></span>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="comment_status_id" type="radio" class="ace" value="2" <?php if($atabla['en']["comment_status_id"]==2):?> checked="CHECKED" <?php endif;?>>
                                                <span class="lbl" style="color:#008000"> <?=isset($dataGlobal['label_approve']) ? $dataGlobal['label_approve'] : 'Approve' ?></span>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="comment_status_id" type="radio" class="ace" value="3" <?php if($atabla['en']["comment_status_id"]==3):?> checked="CHECKED" <?php endif;?>>
                                                <span class="lbl" style="color:#ff0000"> <?=isset($dataGlobal['label_spam']) ? $dataGlobal['label_spam'] : 'Spam' ?></span>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="comment_status_id" type="radio" class="ace" value="4" <?php if($atabla['en']["comment_status_id"]==4):?> checked="CHECKED" <?php endif;?>>
                                                <span class="lbl" style="color:#aa0000"> <?=isset($dataGlobal['label_trash']) ? $dataGlobal['label_trash'] : 'Trash' ?></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div style="margin-left:20px;">
                                        <div><?=isset($dataGlobal['ip_address']) ? $dataGlobal['ip_address'] : 'IP address' ?>: <strong><?=$atabla['en']["ips"]?></strong></div>
                                        <div><?=isset($dataGlobal['submitted_on']) ? $dataGlobal['submitted_on'] : 'Submitted on' ?> <strong> <?= strftime('%b %d, %Y', strtotime($atabla['en']['fecreg'])) ?> at <?= strftime('%I:%M:%S %p', strtotime($atabla['en']['fecreg'])) ?></strong></div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include(dirname(__FILE__)."/../../footer.php"); ?>