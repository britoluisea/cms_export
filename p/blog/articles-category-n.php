<?php
  include(dirname(__FILE__)."/../header.php");
  $opc = isset($_GET['opc']) ? $_GET['opc'] : '1';
  $i = isset($_GET['i']) ? $_GET['i'] : '';
  $sql_1="select * from articles_category where idx='$i' "; //Devuelve todos los Idiomas
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $atabla=$fila_1;
    $opc = 2; 
  }

?>
<br><br>
  <form action="save-category.php" method="post" name="form1">
    <input type="hidden" name="ope" value="articles-category"/>
    <input type="hidden" name="opc" value="<?=$opc?>" />
    <input type="hidden" name="i" value="<?=$i?>" />
    <table width="50%" border="0" align="center" cellpadding="2">
      <tr>
        <td colspan="3"><h2 class="h2"><?php if($opc == 1){echo "Add";}else{echo "Edit";}?> Category</h2></td>
      </tr>
      <tr>
        <td><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name' ?></td>
        <td width="10">:</td>
        <td>
          <input name="nombre" type="text" id="nombre" class="form-control" value="<?=stripcslashes(isset($atabla["nombre"]) ? $atabla["nombre"] : '')?>" size="35" />
        </td>
      </tr>
      <tr>
        <td><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status' ?></td>
        <td>:</td>
        <td>
          <select name="activo" class="form-control" style="width:150px">
            <?php $selected = "selected='selected' "; ?>
            <option value="1" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="1"){echo $selected;}?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active' ?></option>
            <option value="0" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="0"){echo $selected;}?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive' ?></option>  
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3">
          <button type="submit" class="btn btn-primary" id="button"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save' ?></button>
          <a class="btn btn-default" href="articles.php?tab=2"><i class="fa fa-reply"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel' ?></a>
        </td>
      </tr>
    </table>
  </form>
  <br><br><br><br><br><br><br><br>
<?php include(dirname(__FILE__)."/../footer.php"); ?>