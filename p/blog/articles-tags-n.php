<?php 
  include(dirname(__FILE__)."/../header.php");
  $opc = isset($_GET['opc']) ? $_GET['opc'] : '1';
  $i = isset($_GET['i']) ? $_GET['i'] : '';
  $sql_1="select * from articles_tag where idx='$i' ";
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $atabla=$fila_1;
    $opc = 2;
    $i = $atabla["idx"];
  }
?>
<style>
  .form-control {margin-bottom: 10px;}
</style>
<br><br>
<form action="save-tags.php" method="post"  name="form1">
  <table width="50%" align="center"  border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td>
          <div style="font-size:20px; font-weight:bold; color:#2679b5">
            <?php if($opc == 1){echo "New Tag";}else{echo "Edit Tag";}?>
          </div>
        </td>
        <td align="right">
          <button  name="save" id="save" type="submit" class="btn btn-small btn-success"  value="save">
            <i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save' ?>
          </button>
          <button name="savenew" id="savenew"  type="submit" class="btn btn-small btn-primary" value="savenew">
            <i class="fa fa-save "></i> <?=isset($dataGlobal['save_new_tag']) ? $dataGlobal['save_new_tag'] : 'Save & New Tag' ?>
          </button>
          <a href="articles.php?tab=3" class="btn btn-default "><i class="fa fa-reply"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel' ?></a>
        </td>
      </tr>
    </tbody>
  </table>  
  <input type="hidden" name="ope" value="articles-tag"/>
  <input type="hidden" name="opc" value="<?=$opc?>" />
  <input type="hidden" name="i" value="<?=$i?>" />
  <?php if(isset($_SESSION["error"])): ?>
    <div align="center"  class="estilo_error"><?=$_SESSION["error"]?></div>
    <?php unset($_SESSION['error']);	?>
  <?php endif;?>
  <table width="50%" align="center"  border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <table border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name' ?></td>
            <td><span class="asterisco">*</span></td>
            <td valign="top">
              <input name="nombre" type="text" class="form-control" id="nombre" value="<?=isset($atabla["nombre"]) ? $atabla["nombre"] : ''?>" size="80" placeholder="Name"/>
            </td>
          </tr>
          <tr>
            <td valign="top"><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Description' ?></td>
            <td>&nbsp;</td>
            <td valign="top">
              <textarea name="descrip" id="descrip" cols="45" rows="5" class="form-control"><?=isset($atabla["descrip"]) ? $atabla["descrip"] : ''?></textarea>
            </td>
          </tr>
          <tr>
            <td valign="top"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status' ?></td>
            <td>&nbsp;</td>
            <td valign="top">
              <select name="activo" class="form-control" style="width:150px">
                <?php $ss= "selected='selected'"?>
                <option value="1" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="1") {echo $ss ;}?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active' ?></option>
                <option value="0" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="0"){echo  $ss;} ?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive' ?></option>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
<script type="text/javascript">
  $(document).ready(function() {
    $('#nombre').keypress(function(e){
   		if(e.which == 13){
     	 return false;
   	  }
    });
  });
</script>
<?php include(dirname(__FILE__)."/../footer.php"); ?>