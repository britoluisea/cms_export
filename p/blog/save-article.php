<?php 
session_start();
include dirname(__FILE__)."/../../cn/cnx.php";
include dirname(__FILE__)."/../system/functions.php";
$widxamusuario = $_SESSION['value_admin_idx'];
$widxamusuario_pri = $_SESSION['value_admin_idxamusuario'];
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
    exit();
}
else
{
	// variables 
	$ope = isset($_POST['ope']) ? $_POST['ope'] : '';
	$opc = isset($_POST['opc']) ? $_POST['opc'] : '';
	$i = isset($_POST['i']) ? $_POST['i'] : '';
	$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : 'none';
	$descrip = isset($_POST['descrip']) ? $_POST['descrip'] : '';
	$detail = isset($_POST['detail']) ? $_POST['detail'] : '';
	$detail_home = isset($_POST['detail_home']) ? $_POST['detail_home'] : '';
	$descrip_share = isset($_POST['descrip_share']) ? $_POST['descrip_share'] : '';
	$fechahour = isset($_POST['fecreg']) ? date_format(date_create($_POST['fecreg']), 'Y-m-d H:i:s') : date("Y-m-d H:i:s");
	$imagen_show = isset($_POST['imagen_show']) ? $_POST['imagen_show'] : 1;
	$activo = isset($_POST['activo']) ? $_POST['activo'] : '1';
	$img = isset($_POST['img']) ? $_POST['img'] : '';
	$slug = isset($_POST['nombre']) ? urls_amigables(trim($nombre)) : '#';
	$idx_articles_category = isset($_POST['idx_articles_category']) ? $_POST["idx_articles_category"] : '';
	$idx_articles_tag = isset($_POST['idx_articles_tag']) ? $_POST["idx_articles_tag"] : '';
	$selectItemidx_articles_category = isset($_POST['selectItemidx_articles_category']) ? $_POST["selectItemidx_articles_category"] : '';
	$tb = "articles";
	//echo'<pre>';print_r($_POST);echo'</pre>';
	if ($ope == "articles") 
	{
	    //subida de archivo
	    $nombre_foto = $img;
	    $name_subir = $_FILES["foto"]["tmp_name"];
	    //echo "foto = ".$name_subir;
	    if ($name_subir <> "") 
	    {
	    	$proporcional=1;
	    	$quality=50;
	    	$sqls_1 = "SELECT * FROM articles_options where  idx = '1' limit 0,1";
	        $ress_1 = ejecutar($sqls_1);
	        if ($fila_1 = fetchArray($ress_1)) {
	            $width = $fila_1["width"];
	            $height = $fila_1["height"];
	            $proporcional = $fila_1["relativo"];
	            $quality = $fila_1["quality"];
	        }
	        $ruta = "../../imgcms/articles/";
			$large = $ruta."large/";
			$thumbs = $ruta."thumbs/";
			$small = $ruta."small/";
			$tmp = '../tmp/';
			$nombre_foto = "img_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999);
			if (!file_exists($ruta))  {mkdir($ruta,  0777, true);}
			if (!file_exists($large)) {mkdir($large, 0777, true);}
			if (!file_exists($thumbs)) {mkdir($thumbs, 0777, true);}
			if (!file_exists($small)) {mkdir($small, 0777, true);}
	        $tempFile =  $tmp .  basename($nombre_foto.'.jpg');
	        //echo $tempFile;
		    if(move_uploaded_file($name_subir,$tempFile))
		    {
		    	$entrada= $nombre_foto.'.jpg';
		    	$salida= $nombre_foto.'.jpg';
		    	$img_large=cortes_custom($tmp, $large, $entrada, $salida, $quality, $width, $height, $proporcional); // large  | $r = relativo 1 = true | 0 = false
	            $img_thumbs=cortes_custom($tmp, $thumbs, $entrada, $salida, $q=40, $w=450, $h=450, $r=1);  // thumbs  | $r = relativo 1 = true | 0 = false
	            $img_small=cortes_custom($tmp, $small, $entrada, $salida, $q=25, $w=120, $h=80, $r=1);  // small  | $r = relativo 1 = true | 0 = false
	            if(!$img_large){$nombre_foto= 'error.jpg';}else {$nombre_foto= $salida;}
	            @unlink($tempFile);
		    }
		    else{	echo'error';}
	    }
	    if ($opc == 1) {
	        $sql_0 = "insert into $tb (idx_usuario,nombre,imagen_show,imagen,detail,detail_home,descrip,descrip_share,fecreg,slug,activo )
							  values ('$widxamusuario_pri','$nombre','$imagen_show','$nombre_foto','$detail','$detail_home','$descrip','$descrip_share','$fechahour','$slug','$activo')"; //imagen de agregar pais
	        if (!$res_0 = ejecutar($sql_0)) {
	            //echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	            //exit();
	            $_SESSION["error"] = "Error: URL Exist.";
	        }
	        $idArticle = obtener_idx($sql_0);
	        $sql_0 = "update $tb set item = idx where item = '0'";
	        if (!$res_0 = ejecutar($sql_0)) {
	            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	            exit();
	        }
	        $idx_articles_tag = $_POST["idx_articles_tag"];
	        if (!empty($idx_articles_tag)) {
	            $cont = 0;
	            foreach ($idx_articles_tag as $item) {
	                $cont++;
	                $sql_0 = "insert into articles_detail (idx_articles, idx_articles_tag, item, activo) values ('$idArticle', '$item', '$cont', '1')"; //imagen de agregar pais
	                if (!$res_0 = ejecutar($sql_0)) {
	                    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                    exit();
	                }
	            }
	        }
	        if (!empty($idx_articles_category)) {
	            $cont = 0;
	            foreach ($idx_articles_category as $itemCat) {
	                $cont++;
	                $sql_0 = "insert into articles_detail_category (idx_articles, idx_articles_category, item, activo) values ('$idArticle', '$itemCat', '$cont', '1')"; //imagen de agregar pais
	                if (!$res_0 = ejecutar($sql_0)) {
	                    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                    exit();
	                }
	            }
	        } else {
	            $cont = 1;
	            $itemCat = 1; // idx Uncategorized
	            $sql_0 = "insert into articles_detail_category (idx_articles, idx_articles_category, item, activo) values ('$idArticle', '$itemCat', '$cont', '1')"; //imagen de agregar pais
	            if (!$res_0 = ejecutar($sql_0)) {
	                echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                exit();
	            }
	        }
	    }
	    if ($opc == 2) {
	        $sql_0 = "update $tb set nombre = '$nombre', imagen_show = '$imagen_show', imagen = '$nombre_foto', detail = '$detail', detail_home = '$detail_home', descrip = '$descrip', descrip_share = '$descrip_share', fecreg = '$fechahour', slug = '$slug', activo = '$activo' where idx = '$i' ";
	        if (!$res_0 = ejecutar($sql_0)) {
	            //echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	            //exit();
	            $_SESSION["error"] = "Error: URL Exist.";
	        }
	        $sql_0 = "delete from articles_detail where idx_articles = '$i' ";
	        if (!$res_0 = ejecutar($sql_0)) {
	            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	            exit();
	        }
	        $sql_0 = "delete from articles_detail_category where idx_articles = '$i' ";
	        if (!$res_0 = ejecutar($sql_0)) {
	            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	            exit();
	        }
	        $idx_articles_tag = $_POST["idx_articles_tag"];
	        if (!empty($idx_articles_tag)) {
	            $cont = 0;
	            foreach ($idx_articles_tag as $item) {
	                $cont++;
	                $sql_0 = "insert into articles_detail (idx_articles, idx_articles_tag, item, activo) values ('$i', '$item', '$cont', '1')";
	                if (!$res_0 = ejecutar($sql_0)) {
	                    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                    exit();
	                }
	            }
	        }
	        if (!empty($idx_articles_category)) {
	            $cont = 0;
	            foreach ($idx_articles_category as $itemCat) {
	                $cont++;
	                $sql_0 = "insert into articles_detail_category (idx_articles, idx_articles_category, item, activo) values ('$i', '$itemCat', '$cont', '1')"; //imagen de agregar pais
	                if (!$res_0 = ejecutar($sql_0)) {
	                    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                    exit();
	                }
	            }
	        } else {
	            $cont = 1;
	            $itemCat = 1; // idx Uncategorized
	            $sql_0 = "insert into articles_detail_category (idx_articles, idx_articles_category, item, activo) values ('$i', '$itemCat', '$cont', '1')"; //imagen de agregar pais
	            if (!$res_0 = ejecutar($sql_0)) {
	                echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
	                exit();
	            }
	        }
	    }
	}
	header("Location: articles.php?tab=1");
}