<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tbody>
    <tr>
      <td>
        <div>            
          <a href="articles-n.php" class="btn btn-small btn-success"><i class="fa fa-plus"></i> <?=isset($dataGlobal['add_post']) ? $dataGlobal['add_post'] : 'Add Post' ?> </a>
        </div>
      </td>
      <td>
        <div>
          <form id="form_busqueda" name="form_busqueda" class="form-inline" method="get" action="">
            <table align="right" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td width="80">                  
                  <div class="form-group">
                    <div class="input-group">
                      <select name="records" id="records" class="form-control" onChange="this.form.submit()">
                        <?php
                          for($ifor = 10; $ifor <= 100; $ifor= $ifor+10)
                          {
                            ?>
                            <option value="<?=$ifor?>" <?php if ($records == $ifor) {?>selected <?php } ?>>
                              <?=$ifor?>
                            </option>
                            <?php
                          } 
                        ?>
                      </select>
                    </div>
                  </div>  
                </td>
                <td>
                  <div class="form-group">
                    <label class="sr-only" for="search"><?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?></label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                      <input type="text" name="search" id="search" value="<?=$search?>" placeholder="<?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?>..." autocomplete="off" class="form-control">
                    </div>
                  </div>                  
                  <button type="submit" class="btn btn-dark" ><?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search' ?></button>                    
                  <a href="articles.php?tab=1" class="btn btn-default"> <?=isset($dataGlobal['label_all']) ? $dataGlobal['label_all'] : 'All' ?> </a>
                </td>
              </tr>
            </table>
          </form>
        </div>
      </td>
    </tr>
  </tbody>
</table>
<br>
<?php if(isset($_SESSION["error"])): ?>
  <div align="center"  class="alert alert-error"><?=$_SESSION["error"]?></div>
  <?php unset($_SESSION['error']);	?>
<?php endif;?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-hover">
  <thead>       
    <tr>
      <th width="100" ><div align="center"><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date' ?></div></th>
      <th width="60" ><div align="center"><?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover' ?></div></th>
      <th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name' ?></div></th>
      <th class="grilla_encab"><?=isset($dataGlobal['label_author']) ? $dataGlobal['label_author'] : 'Author' ?></th>
      <th class="grilla_encab"><?=isset($dataGlobal['categories']) ? $dataGlobal['categories'] : 'Categories' ?></th>
      <th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_tags']) ? $dataGlobal['label_tags'] : 'Tags' ?></div></th>
      <th width="70" class="grilla_encab"><div align="center"># <?=isset($dataGlobal['label_views']) ? $dataGlobal['label_views'] : 'Views' ?></div></th>
      <th width="30" class="grilla_encab"><div align="center"><i class="fa fa-comment"></i></div></th>
      <th width="45" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_link']) ? $dataGlobal['label_link'] : 'Link' ?></div></th>
      <th width="45" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions' ?></div></th>
    </tr>
  </thead>     
  <?php
    //$wfil = "articles.idx_articles_category = '6' and";
    $t="articles,usuarios";
    $sql="select 
      articles.*,
      usuarios.idx as usu_idx, 
      usuarios.username as usu_username,
      usuarios.nombres as usu_nombres,
      usuarios.apellidos as usu_apellidos,
      usuarios.userlevel as usu_userlevel
      from
      $t
      where
      articles.idx_usuario = usuarios.idx and 
      (articles.nombre like '%$search%')
      order by articles.fecreg desc 
    ";	
    $consulta=ejecutar($sql);
    $num_reg=numRows($consulta);
    $pages = new Paginator;
    $pages->items_total = $num_reg;
    $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
    $pages->paginate($records);
    $item = 0;
		//echo $sql.$pages->limit;
		$consulta=ejecutar($sql.$pages->limit);
		$num_reg=numRows($consulta);
		if($num_reg == 0)
		{ ?>
      <td colspan="10" ><div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?></div></td>
      <?php
		}
		else
		{
      while($fila=fetchArray($consulta))
      {
        $item++;
        ?> 
        <tr>
          <td valign="top">
            <div class="cat_nombre" align="center">
              <?= strftime('%b %d, %Y', strtotime($fila['fecreg'])) ?>
            </div>
            <div class="cat_nombre" align="center">
              <?= strftime('%I:%M:%S %p', strtotime($fila['fecreg'])) ?>
            </div>
            <div>
              <?php
                $wtexto = "";
                $wtextoclass = "";
                if ($fila['activo'] == 1){ $wtexto = isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active'; $wtextoclass = "label label-success"; }
                else{ $wtexto = isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'; $wtextoclass = "label label-important"; }
              ?>
              <div align="center" title="<?= $wtexto ?>">
                <span class="<?= $wtextoclass ?>">
                  <?= $wtexto ?>
                </span>
              </div>
            </div>   
          </td>
          <td valign="top">
            <div align="center" class="imagenesxp">
              <?php 
                $wimg="../../".IMG_CMS."articles/small/".$fila["imagen"];
                $thumbs="../../".IMG_CMS."articles/thumbs/".$fila["imagen"];
                
                if($fila["imagen"]=="" || !is_file($wimg))
                { ?>
                  <i class="fa fa-image fa-2x"></i><?php 
                }else{ ?> 
                    <img src="<?php echo $wimg ?>" style="border:1px solid #999999; padding:5px;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<img src='<?=$thumbs?>' style='border:1px solid #999999;padding:5px;width: 250px;max-width: 250px;max-height:initial;height:auto;' />"/>               
                  <?php 
                } 
              ?>           
            </div>
          </td>
          <td valign="top"><div class="cat_nombre" align="left"><?=$fila["nombre"]?></div></td>
          <td valign="top">
            <div>
              <?php
                if($fila["usu_nombres"] == "" and $fila["usu_apellidos"]==""){  echo $fila["usu_username"]; }
                else{ echo $fila["usu_nombres"]." ".$fila["usu_apellidos"]; } 
              ?>
            </div>
          </td>
          <td valign="top">
            <?php
              $sql_prodrel="select articles_detail_category.*, articles_category.nombre as tagnombre  
              from articles_detail_category,articles_category
              where
              articles_detail_category.idx_articles_category=articles_category.idx and
              articles_detail_category.idx_articles='".$fila["idx"]."'
              order by articles_category.item desc";
              $consulta_prodrel=ejecutar($sql_prodrel);
              $num_reg_art=numRows($consulta_prodrel);
  		        $aprodrel=array();
  		        while($fila_prodrel=fetchAssoc($consulta_prodrel))
  		        {	
  			       $aprodrel[] = $fila_prodrel["tagnombre"];
  		        }
              if(!empty($aprodrel)){ ?>
                <div align="left">
                  <?php	$wnombretag = implode(', ', $aprodrel);		?> <?=$wnombretag?>
                </div>
              <?php }else{ ?> <div style="float:left">None</div> <?php }
            ?>
          </td>
          <td valign="top">
            <?php
  		        $sql_prodrel="select articles_detail.*, articles_tag.nombre as tagnombre  
              from articles_detail,articles_tag where articles_detail.idx_articles_tag=articles_tag.idx and 
              articles_detail.idx_articles='".$fila["idx"]."' order by articles_tag.item desc";
              $consulta_prodrel=ejecutar($sql_prodrel);
  		        $num_reg_art=numRows($consulta_prodrel);
  		        $aprodrel=array();
  		        while($fila_prodrel=fetchAssoc($consulta_prodrel))
  		        {	
  			       $aprodrel[] = $fila_prodrel["tagnombre"];
  		        } 
  		        if(!empty($aprodrel)){ ?>
                <div align="left">
                  <?php	$wnombretag = implode(', ', $aprodrel);		?>
  		            <?=$wnombretag?>
                </div>
                <?php }else{ ?> <div style="float:left"><?=isset($dataGlobal['label_none']) ? $dataGlobal['label_none'] : 'None' ?></div> <?php } 
            ?>
          </td>
          <td valign="top"><div align="center"><?=$fila['conteo']?></div></td>
          <td valign="top">         
            <div>
              <?php
                $sql_5 = "SELECT idx FROM comments where article_id = '".$fila["idx"]."' and comment_status_id='2' ";
                $res_5=ejecutar($sql_5);
                $regNum=numRows($res_5);
              ?>
                <div align="center"><?=$regNum?></div>
            </div>
          </td>
          <td valign="top">
            <div align="center">
              <a href="<?=SERVER?>blog/<?=$fila['slug']?>" target="_blank">
                <i class="fa fa-link fa-lg"></i>
              </a>
            </div>
          </td>
          <td valign="top">
            <div class="btn-group">
              <button data-toggle="dropdown" class="btn btn-small  btn-primary dropdown-toggle text-uppercase">
                <i class="fa fa-cog"></i> <?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions' ?> <b class="caret"></b>
              </button>
              <ul class="dropdown-menu pull-right">
                <li>
                  <a href="articles-n.php?i=<?=$fila["idx"]?>" class="text-uppercase">
                    <i class="fa fa-pencil"></i> <?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Edit' ?>
                  </a>
                </li>
                <li>
                  <a href="#" class="deleteArticle text-uppercase" idx="<?=$fila["idx"]?>">
                    <i class="fa fa-remove "></i> <?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete' ?>
                  </a>
                </li>
              </ul>
            </div> 	
          </td>
        </tr>
        <?php
      }
    }
  ?>
  <tfoot>
    <tr>
      <th width="100" ><div align="center"><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date' ?></div></th>
      <th width="60" ><div align="center"><?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover' ?></div></th>
      <th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name' ?></div></th>
      <th class="grilla_encab"><?=isset($dataGlobal['label_author']) ? $dataGlobal['label_author'] : 'Author' ?></th>
      <th class="grilla_encab"><?=isset($dataGlobal['categories']) ? $dataGlobal['categories'] : 'Categories' ?></th>
      <th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_tags']) ? $dataGlobal['label_tags'] : 'Tags' ?></div></th>
      <th width="70" class="grilla_encab"><div align="center"># <?=isset($dataGlobal['label_views']) ? $dataGlobal['label_views'] : 'Views' ?></div></th>
      <th width="30" class="grilla_encab"><div align="center"><i class="fa fa-comment"></i></div></th>
      <th width="45" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_link']) ? $dataGlobal['label_link'] : 'Link' ?></div></th>
      <th width="45" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions' ?></div></th>
    </tr>
  </tfoot>
</table>
<?php	if ($pages->num_pages > 1)	
  { ?>       
    <div align="center" style="padding:8px;">
      <?php echo $pages->display_pages(); ?> 
    </div> <?php	
  }	
?>
<script>  
  $(document).ready(function() {
    $(".deleteArticle").click(function(event) {
      var i = $(this).attr('idx');;
      alerConfirm("<?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete' ?>", "<?=isset($dataGlobal['msj_del_item']) ? $dataGlobal['msj_del_item'] : 'Are you sure you want to delete this item?' ?>", i, 'deleteArticle');
    });
    // Enables popover
    $("[data-toggle=popover]").popover({ html : true });
  });
  var deleteArticle = function(i)
  {
    var idx = i;
    location.href= 'articles-delete.php?i='+idx+'>&opc=3';
  }
</script>