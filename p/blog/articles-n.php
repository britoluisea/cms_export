<?php 
  include(dirname(__FILE__)."/../header.php");
  $sql2 = "SELECT userlevel FROM usuarios where activo=1 and idx = '".$_SESSION['value_admin_idxamusuario']."'  limit 0,1";
  $res2=ejecutar($sql2);
  if ($col2=fetchArray($res2))
  {	
  	$userlevel	= $col2['userlevel'];
  }
  $opc = isset($_GET['opc']) ? $_GET['opc'] : '';
  $i = isset($_GET['i']) ? $_GET['i'] : '';
  $fechahour='';
  if (empty($opc))	
  {
  	$opc = 1;
  	$fechahour = date("m/d/Y H:i:s");
  	$wimagen_show=1;
  }
  $t="articles,usuarios";
  $sql_1="select 
    articles.*,
    usuarios.idx as usu_idx, 
    usuarios.username as usu_username,
    usuarios.nombres as usu_nombres,
    usuarios.apellidos as usu_apellidos,
    usuarios.userlevel as usu_userlevel
    from
    $t
    where
    articles.idx_usuario = usuarios.idx and
    articles.idx='$i'
  ";
  //$sql_1="select * from articles where idx='$i' ";
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
  	$atabla=$fila_1;
  	//	print_r($atabla)."<br>";
  	$opc = 2;
    //	$wfecha = strftime('%m/%d/%Y', strtotime($atabla["fecreg"]));
  	$i = $atabla["idx"];
  	//$g = $atabla["idx_articles_category"];     
    $fechahour= date_format(date_create($atabla["fecreg"]), 'm/d/Y H:i:s');
  	//echo $fechahour;
  	$wimagen_show=$atabla["imagen_show"];
  }
   	//$atabla['usu_userlevel'];
  	//echo "--->".$atabla["idx"];
  if($opc==2)
  {
    if($userlevel==2)
    {
      if($widxamusuario_pri <> $atabla["usu_idx"])
      {
  	   header("Location: ../access-denied.php");
  	   exit();
      }
    }
  }
  $wdisplay = "display:none";
?>
<script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>calendar/css/jquery-ui.min.css"  rel="stylesheet" />
<link href="<?=PLUGINS_P?>calendar/css/jquery-ui-timepicker-addon.css"  rel="stylesheet" />
<link href="<?=PLUGINS_P?>multiple-select/multiple-select.css" rel="stylesheet"/>
<link href="<?=PLUGINS_P?>chosen/css/chosen.css" rel="stylesheet" >
<script src="<?=PLUGINS_P?>multiple-select/jquery.multiple.select.js"></script>
<script src="<?=PLUGINS_P?>chosen/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>calendar/js/jquery-ui.min.js" type="text/javascript" ></script> 
<script src="<?=PLUGINS_P?>calendar/js/jquery-ui-timepicker-addon.js" type="text/javascript" ></script> 
<style>
  .btn, .ms-choice, .form-control{border-radius: 0px;}
  .ms-choice, .chzn-container-multi .chzn-choices li.search-field input[type="text"]{height: 34px;}
  span.placeholder {height: 34px;line-height: 34px;}
</style>
<div class="container-fluid" id="main-container">
  <br>
  <form action="save-article.php" method="post" enctype="multipart/form-data" name="form1">
    <?php
      $t="articles";
      $sql_1="select * from articles where idx='$i' ";
	    $consulta_1=ejecutar($sql_1);
      $atabla=array();
	     while($fila_1=fetchAssoc($consulta_1))
	     {
          $atabla=$fila_1;
          $opc = 2;
          $i = $atabla["idx"];
          //$g = $atabla["idx_articles_category"];
		      $fechahour= date_format(date_create($atabla["fecreg"]), 'm/d/Y H:i:s');
	     }
    ?>
    <input type="hidden" name="ope" value="articles"/>
    <input type="hidden" name="opc" value="<?=$opc?>" />
    <input type="hidden" name="i" value="<?=$i?>" />

    <div class="row">
      <div class="col-sm-9">
        <div style="font-size:20px; font-weight:bold; color:#2679b5">
          <?php if($opc == 1) { ?>
          <?=isset($dataGlobal['add_post']) ? $dataGlobal['add_post'] : 'Add Post' ?>
          <?php }else{ ?>
          <?=isset($dataGlobal['edit_post']) ? $dataGlobal['edit_post'] : 'edit Post' ?>
        <?php } ?>
        </div>
      </div>
      <div class="col-sm-3 text-center">
        <button  name="save" id="save" type="submit" class="btn btn-small btn-primary"  value="save"><i class=" fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save' ?></button>
        <a href="articles.php?tab=1" class="btn btn-small btn-default "><i class="fa fa-reply"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel' ?></a>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-9">
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
          <tr>
            <td width="50" valign="top"> <?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name' ?></td>
            <td width="10" valign="top">:</td>
            <td width="10"><span class="asterisco">*</span></td>
            <td valign="top">
              <input name="nombre" type="text" class="form-control" id="nombre" value="<?=isset($atabla["nombre"]) ? $atabla["nombre"] : '' ?>" />
            </td>
          </tr>
          <?php if(isset($atabla['slug'])){?>
            <tr>
              <td width="50"><?=isset($dataGlobal['label_link']) ? $dataGlobal['label_link'] : 'Link' ?>:</td>
              <td width="10" valign="top">:</td>
              <td width="10"></td>
              <td>                
                <a href="<?=SERVER.$atabla['slug']?>" targe="_blank" class="btn btn-dark " style="border:1px solid #ccc; background-color: #ddd; margin: 5px 0px;width:100%; text-align: left;"><?=SERVER.'blog/'.$atabla['slug']?></a>
              </td>
            </tr>
          <?php }?>
        </table>
        <br>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" style="border:1px solid  #cccccc">
          <tr>
            <td  valign="top">
              <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                  <li class="active" ><a data-toggle="tab" href="#tab1" style="border-radius:0px;"><i class="fa fa-list-alt"></i> <?=isset($dataGlobal['full_descrip']) ? $dataGlobal['full_descrip'] : 'Full Description' ?></a> </li>
                  <li class="" ><a data-toggle="tab" href="#tab2" style="border-radius:0px;"><i class="fa fa-list-alt"></i> <?=isset($dataGlobal['small_descrip']) ? $dataGlobal['small_descrip'] : 'Small Description' ?></a> </li>
                  <li class="" ><a data-toggle="tab" href="#tab3" style="border-radius:0px;"><i class="fa fa-list-alt"></i> <?=isset($dataGlobal['home_descrip']) ? $dataGlobal['home_descrip'] : 'Home Description' ?></a> </li>
                  <li class="" ><a data-toggle="tab" href="#tab4" style="border-radius:0px;"><i class="fa fa-rss-square"></i> <?=isset($dataGlobal['descrip_share']) ? $dataGlobal['descrip_share'] : 'Description for Social Share' ?></a> </li>
                </ul>
                <div class="tab-content" style="padding: 10px;">
                  <div id="tab1" class="tab-pane active">
                    <textarea name="descrip" id="descrip" ><?=isset($atabla["descrip"]) ? $atabla["descrip"] : ''?></textarea>
                    <script type="text/javascript">
                      var newCKEdit = CKEDITOR.replace('descrip',{height:'350',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
                      CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
                    </script> 
                  </div>
                  <div id="tab2" class="tab-pane  ">
                    <textarea name="detail" id="detail" cols="45" rows="6" class="form-control"><?=isset($atabla["detail"]) ? $atabla["detail"] : ''?></textarea>
                    <script type="text/javascript">
                      var newCKEdit = CKEDITOR.replace('detail',{height:'200',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
                      CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
                    </script> 
                  </div>
                  <div id="tab3" class="tab-pane">
                    <textarea name="detail_home" id="detail_home" cols="45" rows="6" class="form-control"><?=isset($atabla["detail_home"]) ? $atabla["detail_home"] : ''?></textarea>
                  </div>
                  <div id="tab4" class="tab-pane">
                    <textarea name="descrip_share" id="descrip_share" cols="45" rows="6" class="form-control"><?=isset($atabla["descrip_share"]) ? $atabla["descrip_share"] : ''?></textarea>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="col-sm-3">
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">          
          <tr>
            <td width="30"><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date' ?>:<span class="asterisco">*</span></td>
            <td colspan="3">
              <input type="text" name="fecreg" id="fecreg" value="<?=$fechahour?>" class="form-control" readonly style="width:90%; background-color: #fff;display: inline-block;"/><i class="fa fa-calendar fa-2x"></i>
            </td>
          </tr>
          <tr><td colspan="4">&nbsp;</td></tr>
          <tr>
            <td colspan="4">
              <?php
                $sql_options="SELECT * FROM  articles_options where  idx = '1' limit 0,1";
                $res_options=ejecutar($sql_options);
                $abanneroptions=array();
                if($fila_options=fetchAssoc($res_options))
                {
                  $abanneroptions[]=$fila_options;
                }
                $width  = $abanneroptions[0]["width"];
                $height = $abanneroptions[0]["height"];
              ?>
              <p>Cover:&nbsp;&nbsp;<i><strong> <?=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width' ?>:<?=$width?>px</strong></i></p>
            </td>            
          </tr>
          <tr>
            <td colspan="4">
              <div align="center" class="">
                <?php 
                  $wimg='../../imgcms/system/sin-icon.ico';
                  if(isset($atabla["imagen"]) && $atabla["imagen"]!="")
                  { 
                    $wimg="../../imgcms/articles/thumbs/".$atabla["imagen"];                    
                  }
                ?>
                <img id="preview" src="<?=$wimg ?>" alt="preview" style="border:1px solid #999999; padding:5px; width: 100%;" /> 
              </div>  
            </td>
          </tr>
          <tr>
            <td colspan="4" align="center">
              <input type="hidden" name="img" id="img" value="<?=isset($atabla["imagen"]) ? $atabla["imagen"] : '';?>"/>
              <input type="file" name="foto" id="foto" class="hide" accept="image/jpeg" />
              <button id="upload" type="button" class="btn btn-success"><?=isset($dataGlobal['btn_explore_folders']) ? $dataGlobal['btn_explore_folders'] : 'Explore Folders' ?></button>
              <script>
                var foto = document.getElementById('foto');
                var preview = document.getElementById('preview');
                var upload = document.getElementById( 'upload' );
                upload.onclick = function(event){event.preventDefault(); foto.click()}
                foto.onchange = function()
                {
                  preview.style.width='auto';
                  preview.src='<?=IMG_P?>loading.gif';
                  filePreview(this, preview);
                }             
              </script>
            </td>            
          </tr>
          <tr><td colspan="4">&nbsp;</td></tr>
          <tr>
            <td colspan="3" ><?=isset($dataGlobal['show_cover']) ? $dataGlobal['show_cover'] : 'Show cover' ?>:</td>
            <td width="200">
                <label class="radio-inline">
                  <input name="imagen_show" type="radio" class=" " value="1" <?php if($wimagen_show=="1"): ?> checked="CHECKED" <?php endif;?>>
                  <span class="lbl"> <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes' ?></span> 
                </label>
                <label class="radio-inline">
                  <input name="imagen_show" type="radio" class=" " value="0" <?php if($wimagen_show=="0"): ?> checked="CHECKED" <?php endif;?>>
                  <span class="lbl"> <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not' ?></span>
                </label>
            </td>
          </tr>
          <tr><td colspan="4">&nbsp;</td></tr>
          <tr>
            <td width="30"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status' ?></td>
            <td width="5">:</td>
            <td colspan="2">
              <select name="activo" class="form-control" style="width:150px">
                <option value="1" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="1") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active' ?></option>
                <option value="0" <?php if(isset($atabla["activo"]) && $atabla["activo"]=="0"){echo "selected='selected'";}?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive' ?></option>
              </select>
            </td>
          </tr>
        </table>
        <br>
        <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
          <tbody>
            <tr>
              <td class="widget-header-tag"><div style="padding:4px; font-size:14px; "><i class="fa fa-bookmark"></i> <?=isset($dataGlobal['categories']) ? $dataGlobal['categories'] : 'Categories' ?> </div></td>
            </tr>
            <tr>
              <td height="80" valign="top">
                <div style="padding:4px;">
                  <?php
                    $sql_prodrel="select * from articles_detail_category where idx_articles='$i' "; //Devuelve todos los Idiomas
                    $consulta_prodrel=ejecutar($sql_prodrel);
                    $aprodrel=array();
                    while($fila_prodrel=fetchAssoc($consulta_prodrel))
                    {	
                      $opc = 2;
                      $aprodrel[ $fila_prodrel["idx_articles_category"] ] = $fila_prodrel["idx_articles_category"];
                    }
                  ?>
                  <select multiple="multiple" id="idx_articles_category" name="idx_articles_category[]" data-placeholder="- Select Category -">
                    <?php
                      $sql="select * from articles_category order by item desc";
                      $consulta=ejecutar($sql);
                      if(numRows($consulta) > 0)
                      {
                        while($fila=fetchArray($consulta))
                        { ?>
                          <option value="<?php echo isset($fila['idx']) ? $fila['idx'] : '';?>" 
                            <?php if(in_array($fila['idx'], $aprodrel)){ echo'selected="selected"'; }?>><?=$fila['nombre']?></option>
                          <?php 
                        }
                      } 
                    ?>
                  </select>
                  <table width="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                      <td><input id="refreshInput_cat" type="text"  class="form-control"  placeholder="Name"></td>
                      <td><button type="button" id="refreshAdd_cat" class="btn btn-small btn-dark" style="cursor:pointer; width:100%"><?=isset($dataGlobal['label_add']) ? $dataGlobal['label_add'] : 'Add' ?></button></td>
                    </tr>
                    <tr>
                      <td colspan="2"><div id="timer_cat" style=" <?= $wdisplay ?>"><img src="img/loading.gif" /> <?=isset($dataGlobal['loading']) ? $dataGlobal['loading'] : 'Loading' ?></div></td>
                    </tr>
                  </table>
                  <div id="notification_cat" style="margin-bottom:5px; margin-top:5px;"></div>
                  <script>
                    var timeSlide = 100;
                    $('#timer_cat').css('display','none');
                    $("#refreshAdd_cat").click(function() {
                      $('.style_succes_cat, .style_error_cat').slideUp(timeSlide);
                      $input = $("#refreshInput_cat");
                      //$selected = $("#idx_articles_tag");
                      value = $.trim($input.val());			
                      if (!value) {
                        $input.focus();
                        return;
                      }
                      $.ajax({
                        type: "POST",
                        url: "article-cat-inc.php",
                        //dataType: 'html',
                        dataType: 'json',
    				            data: {nombre: value},
    				            beforeSend: function(objeto){
               		       $('#timer_cat').show();
                        },
                        complete: function(objeto, exito){
                          if(exito=="success"){
                           $('#timer_cat').hide();
                          }
                        },
                        success: function(data) {
                          if(data==0)
                          {
                            $('#notification_cat').html('<div class="style_error_cat"></div>');
                            $('.style_error_cat').hide(0).html('Error: URL Exist.');
                            $('.style_error_cat').slideDown('slow');
                            setTimeout(function(){ $('.style_error_cat').slideUp('slow'); },(timeSlide + 4000));
                          }
                          else
                          {	
                            $("#idx_articles_category").append('<option value='+data.idx+' selected="selected">'+data.nombre+'</option>');
                            $("#refreshInput_cat").val('');
                            $("#refreshInput_cat").focus();
                            $('#idx_articles_category').multipleSelect("refresh");
                          }
                        },
                      });
                    });
                  </script>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <br>
        <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
          <tbody>
            <tr>
              <td class="widget-header-tag"><div style="padding:4px; font-size:14px; "> <i class="fa fa-tags"></i><?=isset($dataGlobal['label_tags']) ? $dataGlobal['label_tags'] : 'Tags' ?> </div></td>
            </tr>
            <tr>
              <td height="80" valign="top">
                <div style="padding:4px;">
                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                    <tr>
                      <td>
                        <?php
                          $sql_prodrel="select * from articles_detail where idx_articles='$i' "; //Devuelve todos los Idiomas
                          $consulta_prodrel=ejecutar($sql_prodrel);
                          $aprodrel=array();
                          while($fila_prodrel=fetchAssoc($consulta_prodrel))
                          {	
                            $opc = 2;
                            $aprodrel[ $fila_prodrel["idx_articles_tag"] ] = $fila_prodrel["idx_articles_tag"];
                          }
                        ?>
                        <select id="idx_articles_tag" name="idx_articles_tag[]"  multiple="multiple" data-placeholder="- Select Tags -" class="form-control">
                          <?php
                            $sql_art="select * from articles_tag order by item desc";
                            $consulta_art=ejecutar($sql_art);
                            while($fila_art=fetchArray($consulta_art))
                            { ?>
                              <option value="<?php echo isset($fila_art['idx']) ? $fila_art['idx'] : '';?>" 
                                <?php if(in_array($fila_art['idx'], $aprodrel)){echo'selected="selected"';} ?>><?=$fila_art['nombre']?></option>
                              <?php
                            }
                          ?>
                        </select>
                      </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td><input id="refreshInput" type="text"  class="form-control"  placeholder="Name"></td>
                      <td><button type="button" id="refreshAdd" class="btn btn-small btn-dark" style="cursor:pointer; width:100%"><?=isset($dataGlobal['label_add']) ? $dataGlobal['label_add'] : 'Add' ?></button></td>
                    </tr>
                    <tr>
                      <td colspan="2"><div id="timer" style=" <?= $wdisplay ?>"><img src="<?=SERVER_P?>img/loading.gif" /> <?=isset($dataGlobal['loading']) ? $dataGlobal['loading'] : 'Loading' ?></div></td>
                    </tr>
                  </table>
                  <script>
                    var timeSlide = 100;
                    $('#timer').css('display','none');
                    $("#refreshAdd").click(function() {
                      $('.style_succes, .style_error').slideUp(timeSlide);
                      $input = $("#refreshInput");
                      $selected = $("#idx_articles_tag");
                      value = $.trim($input.val());
                      if (!value) { $input.focus(); return; }
                      $.ajax({
                        type: "POST",
                        url: "article-tag-inc.php",
                        //dataType: 'html',
                        dataType: 'json',
                        data: {nombre: value},
                        beforeSend: function(objeto){
                          $('#timer').show();
                        },
                        complete: function(objeto, exito){
                          if(exito=="success"){ $('#timer').hide(); } 
                        },
                        success: function(data) {
                          if(data==0)
                          {
                            $('#notification').html('<div class="style_error"></div>');
                            $('.style_error').hide(0).html('Error: URL Exist.');
                            $('.style_error').slideDown('slow');
                            setTimeout(function(){ $('.style_error').slideUp('slow'); },(timeSlide + 4000)); 
                          }
                          else
                          {	
                            $("#idx_articles_tag").append('<option value='+data.idx+' selected="selected">'+data.nombre+'</option>');
                            $('#idx_articles_tag').trigger("liszt:updated");
                            $("#refreshInput").val('');
                            $("#refreshInput").focus();
                          }
                        },
                      });
                    });
                  </script>
                  <div id="notification" style="margin-bottom:5px; margin-top:5px;"></div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </form>
</div>
<!--/.fluid-container#main-container--> 
<script type="text/javascript">
  $(document).ready(function() {

    $( "#fecreg" ).datetimepicker({
      changeMonth: true,
            changeYear: true,
      dateFormat: 'mm/dd/yy',
      autoclose: true,
      controlType: 'select',
      timeFormat: 'HH:mm:ss',
      
    });

    $('#idx_articles_category').multipleSelect({
      filter: true,
      isOpen: false,
      keeyOpen: true,
      width: "100%",
    });
    $("#idx_articles_tag").chosen({width: "100%"});
  });
</script>
<?php include(dirname(__FILE__)."/../footer.php"); ?>