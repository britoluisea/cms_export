<?php
	include dirname(__FILE__)."/system/languages/languages.php";
	ini_set("session.cookie_lifetime",43200); //duracion de la session
	ini_set("session.gc_maxlifetime", 43200); //duracion de la cookie
	session_start();
	$_SESSION['var_sec_admin']=false;
	$_SESSION['value_array'] = $_POST;	
	$usuarios = str_replace("/","",$_POST['usuarios']);
	$claves   = str_replace("/","",$_POST['claves']);
	$remember   = isset($_POST['$remember']) ? str_replace("/","",$_POST['$remember']) : '';
	$_SESSION['value_temp_usuarios'] 	= $usuarios;
	$_SESSION['value_temp_remember'] 	= $remember;
	$_SESSION['value_temp_logeo'] 		= 'NO';
	if ($usuarios!="" or $claves!="" )
	{
		include_once "../cn/cnx.php";
		$wnum_intentos 	= 3; // numero intentos
		$wtiempo_bloquear 	= 30; // tiempo a bloquear
		$sql	= "SELECT loginattempts  , logintimeunblock from usuarioscompany WHERE idx = '1'";
		$res=ejecutar($sql);	
		if ($col=fetchArray($res))
		{
			$wnum_intentos 	= $col['loginattempts']; // numero intentos
			$wtiempo_bloquear 	= $col['logintimeunblock']; // tiempo bloqueado
		}
		define("COOKIE_EXPIRE", (60*60*24*100));      //100 days by default
		define("COOKIE_PATH", "/");                 //Avaible in whole domain
		//define("TIME_PERIOD", "30");
		define("TIME_PERIOD", $wtiempo_bloquear);
		$wip = $_SERVER["REMOTE_ADDR"];
		$sql	= "SELECT attempts, (CASE when lastlogin is not NULL and DATE_ADD(LastLogin, INTERVAL ".TIME_PERIOD." MINUTE)>NOW() then 1 else 0 end) as denied ".
	   " FROM login_attempts WHERE ip = '$wip'";
	   $res=ejecutar($sql);	
		if ($col=fetchArray($res))
		{
			if ($col['denied'] == 0 and $col['attempts'] == $wnum_intentos)
			{
				$sql2 	= "delete from login_attempts WHERE ip = '$wip'"; 
				$res2	= ejecutar($sql2);				
			}
		}
		$attempts = 0;
		$sql = "SELECT * FROM login_attempts WHERE ip = '$wip'"; 
		$res=ejecutar($sql);	
		if ($col=fetchArray($res))
		{
			$attempts = $col["attempts"];
		}
		if($attempts >= $wnum_intentos) 
		{
			$_SESSION['error_array'] = isset($dataGlobal['msj_access_denied_part1']) ? $dataGlobal['msj_access_denied_part1'] : "Access denied for ";
			$_SESSION['error_array'] .=$wtiempo_bloquear;
			$_SESSION['error_array'] .=isset($dataGlobal['msj_access_denied_part2']) ? " ".$dataGlobal['msj_access_denied_part2'] : " minutes.";
			header("Location: index.php");
			exit();
		}
		else
		{	
			$usuarios	= addslashes($_POST['usuarios']);
			$claves		= md5(md5(addslashes($_POST['claves'])));
			$sql3 = "SELECT * FROM usuarios where activo=1 and  username = '".$usuarios."'   and claves = '".$claves."'  limit 0,1";
			$res3=ejecutar($sql3);
			if ($col3=fetchArray($res3))
			{				
				unset($_SESSION['error_array']);
				unset($_SESSION['value_array']['usuarios']);
				unset($_SESSION['var_sec_admin']);
				unset($_SESSION['value_admin_idx']);
				unset($_SESSION['value_admin_idxamusuario']);
				unset($_SESSION['value_admin_idxusuariossp']);
				unset($_SESSION['value_admin_usuarios']);
				unset($_SESSION['value_admin_userlevel']);
				unset($_SESSION['value_admin_nomcar']);
				unset($_SESSION['value_temp_usuarios']);
				unset($_SESSION['value_temp_remember']);		
				unset($_SESSION['value_admin_nombres']);		
				unset($_SESSION['value_admin_apellidos']);		
				if ($col3['horaope'] == '1' or $col3['horaope'] == '2')
				{
					$_SESSION['var_sec_admin'] 				= true;
					$_SESSION['value_admin_idx']			= $col3['idx'];
					$_SESSION['value_admin_idxamusuario']	= $col3['idxamusuario'];
					$_SESSION['value_admin_idxusuariossp']	= $col3['idxusuariossp'];				
					$_SESSION['value_admin_usuarios']		= $col3['username'];
					$_SESSION['value_admin_nombres']		= $col3['nombres'];
					$_SESSION['value_admin_apellidos']		= $col3['apellidos'];					
					$_SESSION['value_admin_userlevel']		= $col3['userlevel'];
					if ($_SESSION['value_admin_userlevel'] == '999')
					{
						$_SESSION['value_admin_idx']	= $col3['idx'];
					}
					$sql2 = "SELECT nomcar FROM usuarios where activo=1 and idx = '".$_SESSION['value_admin_idxamusuario']."'  limit 0,1";
					$res2=ejecutar($sql2);
					if ($col2=fetchArray($res2))
					{
						$_SESSION['value_admin_nomcar']		= $col2['nomcar'];
					}
                    //echo $s; exit;
					$sql2 	= "delete from login_attempts WHERE ip = '$wip'"; 
					$res2	= ejecutar($sql2);
					$wfecreg =  date("Y-m-d");
					$wminutos =  date("H:i:s");
					$sql2 	= "INSERT INTO activity_log (idxamusuario,fecreg,minutos,descrip) values ('".$_SESSION['value_admin_idxamusuario']."', '$wfecreg', '$wminutos','Logged in.')";
					$res2	= ejecutar($sql2);
					$_SESSION['value_temp_logeo'] 		= 'SI';
					if($remember== 1){
						$_SESSION['value_temp_usuarios'] 	= $usuarios;
						$_SESSION['value_temp_remember'] 	= $remember;
					}
					else
					{
						$_SESSION['value_temp_usuarios'] 	= '';
						$_SESSION['value_temp_remember'] 	= '';
					}
					if($remember== 1){
       			  		//setcookie("cookname", $_SESSION['value_admin_usuarios'], time()+COOKIE_EXPIRE, COOKIE_PATH);				  	
						//echo "entra=>".$remember.$_SESSION['value_user_usuarios'];															
					}
					else
					{
					  //setcookie("cookname", "", time()+COOKIE_EXPIRE, COOKIE_PATH);
					}

                    //agregar registro de autenticacion
                    $authData = array();
                    $authData['var_sec_admin'] = $_SESSION['var_sec_admin'];
					$authData['value_admin_idx'] = $_SESSION['value_admin_idx'];
					$authData['value_admin_idxamusuario'] = $_SESSION['value_admin_idxamusuario'];
					$authData['value_admin_idxusuariossp'] = $_SESSION['value_admin_idxusuariossp'];
					$authData['value_admin_usuarios'] = $_SESSION['value_admin_usuarios'];
					$authData['value_admin_userlevel'] = $_SESSION['value_admin_userlevel'];
					$authData['value_admin_nomcar'] = $_SESSION['value_admin_nomcar'];
					$authData['value_temp_usuarios'] = $_SESSION['value_temp_usuarios'];
					$authData['value_temp_remember'] = $_SESSION['value_temp_remember'];
					$authData['value_admin_nombres'] = $_SESSION['value_admin_nombres'];
					$authData['value_admin_apellidos'] = $_SESSION['value_admin_apellidos'];
                    //echo $s; exit;
                    include 'authSession.php';
                    addAuthSession($authData);
					header("Location: home.php");
					exit();
				}
				else
				{
					header("Location: index.php");
					exit();
				}
			}		
			else
			{			
				if($attempts == 0)
				{						  
					$sql2 = "INSERT INTO login_attempts (attempts,ip,lastlogin) values (1, '$wip', NOW())";
					$res2=ejecutar($sql2);
				}
				else
				{
					$sql2 = "UPDATE login_attempts SET attempts= attempts + 1, lastlogin=NOW() WHERE ip = '$wip'"; 
					$res2=ejecutar($sql2);
				}
				$_SESSION['error_array'] = isset($dataGlobal['msj_access_denied']) ? $dataGlobal['msj_access_denied'] : "Invalid username or password.";
				header("Location: index.php");	
				exit();	
			}
		}		
	}	
	else
	{
		$_SESSION['error_array'] = isset($dataGlobal['msj_access_denied2']) ? $dataGlobal['msj_access_denied2'] : "Username or password not entered.";
		header("Location: index.php");	
		exit();	
	}
?>