<?php
/*
 * CKFinder
 * ========
 * http://cksource.com/ckfinder
 * Copyright (C) 2007-2013, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
 */
if (!defined('IN_CKFINDER')) exit;

/**
 * @package CKFinder
 * @subpackage ErrorHandler
 * @copyright CKSource - Frederico Knabben
 */

/**
 * Include base error handling class
 */
require_once CKFINDER_CONNECTOR_LIB_DIR . "/ErrorHandler/Base.php";

/**
 * File upload error handler
 *
 * @package CKFinder
 * @subpackage ErrorHandler
 * @copyright CKSource - Frederico Knabben
 */
class CKFinder_Connector_ErrorHandler_QuickUpload extends CKFinder_Connector_ErrorHandler_Base {
    /**
     * Throw file upload error, return true if error has been thrown, false if error has been catched
     *
     * @param int $number
     * @param string $text
     * @access public
     */
    public function throwError($number, $uploaded = false, $exit = true) {
        if ($this->_catchAllErrors || in_array($number, $this->_skipErrorsArray)) {
            return false;
        }

        $oRegistry = & CKFinder_Connector_Core_Factory :: getInstance("Core_Registry");
        $sFileName = $oRegistry->get("FileUpload_fileName");
        $sFileUrl = $oRegistry->get("FileUpload_url");
        $sEncodedFileName = CKFinder_Connector_Utils_FileSystem::convertToConnectorEncoding($sFileName);        
        //editado por britoluisea, creando adaptacion para el nuevo CKEDITOR, ya q este necesita salida JSON y no html
        $retornando_command ='';
        if (!empty($_GET['responseType']) && $_GET['responseType']=='json') {
            $errorMessage = CKFinder_Connector_Utils_Misc::getErrorMessage($number, $sEncodedFileName);

            if (!$uploaded) {
                $sFileUrl = "";
                $sFileName = "";
                $sEncodedFileName = "";
            }

            //$funcNum = preg_replace("/[^0-9]/", "", $_GET['CKEditorFuncNum']);
            $funcNum = 1;
            $retornando_command = "window.parent.CKEDITOR.tools.callFunction($funcNum, '" . str_replace("'", "\\'", $sFileUrl . CKFinder_Connector_Utils_Misc::encodeURIComponent($sEncodedFileName)) . "', '" .str_replace("'", "\\'", $errorMessage). "');";
            $r= array();
            $r['fileName']=$sFileName;
            $r['uploaded']=$uploaded;
            $r['url']=$sFileUrl.$sFileName;
            $error_json = str_replace("'", "\\'", $errorMessage);
            if(!empty($error_json)){                
                $r['error']=array("number"=>$number, 'message'=>$error_json) ;
            }
            echo json_encode($r);
            exit;
        }
        else {
            if (!$uploaded) {
                $retornando_command = "window.parent.OnUploadCompleted(" . $number . ", '', '', '') ;";
            } else {
                $retornando_command = "window.parent.OnUploadCompleted(" . $number . ", '" . str_replace("'", "\\'", $sFileUrl . CKFinder_Connector_Utils_Misc::encodeURIComponent($sEncodedFileName)) . "', '" . str_replace("'", "\\'", $sEncodedFileName) . "', '') ;";
            }
        }

        /**
         * echo <script> is not called before CKFinder_Connector_Utils_Misc::getErrorMessage
         * because PHP has problems with including files that contain BOM character.
         * Having BOM character after <script> tag causes a javascript error.
         */
        header('Content-Type: text/html; charset=utf-8');
        echo "<script type=\"text/javascript\">";
        echo $retornando_command;
        echo "</script>";

        if ($exit) {
            exit;
        }
    }
}
