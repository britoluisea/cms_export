<div><?=isset($dataGlobal['email_review1']) ? $dataGlobal['email_review1'] : 'A new review on the post' ?> "<?=$atabla_comm[0]['project']?>" <?=isset($dataGlobal['email_review2']) ? $dataGlobal['email_review2'] : 'is waiting for your approval' ?></div>
<br>
<div><strong><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User' ?>:</strong> <?=$atabla_comm[0]['nombres'].' '.$atabla_comm[0]['apellidos']?> (IP: <?=$atabla_comm[0]['ips']?>)</div>
<div><strong><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email' ?>:</strong> <?=$atabla_comm[0]['username']?></div>
<div><strong><?=isset($dataGlobal['review']) ? $dataGlobal['review'] : 'Review' ?>: </strong></div>
<div><?=nl2br($atabla_comm[0]['descrip']);?></div>
<div><strong><?=isset($dataGlobal['rating_stars']) ? $dataGlobal['rating_stars'] : 'What is your overall rating of this business?' ?>:</strong>
<?=number_format($atabla_comm[0]['stars'],1)?></div>
<br />

<div>
<?=isset($dataGlobal['approve_it']) ? $dataGlobal['approve_it'] : 'Approve it' ?>:
<a href="<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=approve" target="_blank">
<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=approve 
</a>
</div>

<div>
<?=isset($dataGlobal['trash_it']) ? $dataGlobal['trash_it'] : 'Trash it' ?>:
<a href="<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=trash" target="_blank">
<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=trash
</a>
</div>

<div>
<?=isset($dataGlobal['spam_it']) ? $dataGlobal['spam_it'] : 'Spam it' ?>: 
<a href="<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=spam" target="_blank">
<?=SERVER_P?>review/acciones.php?i=<?=$atabla_comm[0]['idx']?>&action=spam
</a>
</div>

<div><?=isset($dataGlobal['comment_msj1']) ? $dataGlobal['comment_msj1'] : 'urrently' ?> <?=$numPen?> <?=isset($dataGlobal['comment_msj2']) ? $dataGlobal['comment_msj2'] : 'comments are waiting for approval. Please visit the moderation panel' ?>:</div>

<div>
<a href="<?=SERVER_P?>review/acciones.php?status=pending" target="_blank">
<?=SERVER_P?>review/acciones.php?status=pending
</a>
</div>
