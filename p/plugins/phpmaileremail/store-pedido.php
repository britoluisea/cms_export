<?php
$sql_1 = "select * from store_notification_template where idx='1' and lg='" . _LGPANEL . "' ";
$consulta_1 = mysql_query($sql_1, Conectar::conex());
$num = mysql_num_rows($consulta_1);
$aproducto = array();
while ($fila_1 = mysql_fetch_assoc($consulta_1)) {
    $aproducto[0] = $fila_1;
    $wdescrip1 = $aproducto[0]["descrip_header"];
    $wdescrip2 = $aproducto[0]["descrip_footer"];
}
?> 
<div><?php echo nl2br($wdescrip1); ?></div>
<br />
<div>Dear <strong><?= $data["nombres"] . " " . $data["apellidos"] ?></strong></div>
<div>Email: <strong><?=$data["username"]?></strong></div>
<div>Address: <strong><?=$data["address"]?></strong></div>
<div>City: <strong><?=$data["city"]?></strong></div>
<div><span>State: <strong><?=$data["state"]?></strong></span> - <span>Zip Code: <strong><?=$data["zip_code"]?></strong></span> </div>
<div>Phone: <strong><?=$data["phone"]?></strong></div>
<div>Comment:</div>
<div><strong><?=nl2br($data["descrip"])?></strong></div>
<br />
<div><span style="color:#000000; font-size:18px; font-weight:bold;">Order #<?=$wnumpedido?></span></div>
<table class="table table-bordered table-striped table-hover" width="100%" align="center" border="0">
                    <thead>        
                        <tr>
                            <th colspan="2"><div align="left">
                              ITEMS IN YOUR SHOPPING CART
                          </div></th>
                          <th><div align="center">QUANTITY</div></th>
                    <th><div align="right">PRICE</div></th>
                    <th><div align="right">ITEM TOTAL</div></th>
                    </tr>
                    </thead>      

                    <tbody>
<?php
$subTotal=0;
$total=0;	
$_SESSION["subtotal"]=0;
$_SESSION["total"]=0;	
?>
                        <?php foreach ($_SESSION["carrito"] as $item) : ?>
                            <tr>   
                                <td width="80" valign="top">
                               
          <div align="center" style="margin-top:10px; margin-bottom:10px; ">
            <?php
			if($item["imagen"]=="")
				{
					$wimg="img/img_blanco.png";
				}
		  	else
				{
					//$wimg="img_".$lg."/".$carrito_ver[$ifor]->getImagen();
					$wimg="imgcms/store/product/".$item["nomcar"]."/small/".$item["imagen"];
					
				}
				$img_width = 'width="50"'; // default
				$img = @imagecreatefromjpeg($wimg);
				if ($img)
				{
					$img_width = imagesx($img);
					$img_height = imagesy($img);
					if ($img_width == $img_height)
					{
						$img_width = 'width="50"';
						$img_height = '';									
					}
					elseif ($img_width > $img_height)
					{
						$img_width = 'width="50"';
						$img_height = '';																
					}
					else
					{
						$img_width = '';
						$img_height = 'height="50"';																
					}
				}
			?>
            <img src="<?php echo $wimg?>" <?=$img_width?> <?=$img_height?> style="border:1px solid #CCC; padding:5px;" />
            </div>
      
                               </td>
                                <td valign="top">
                                     <div style="font-size:14px;"><?= $item['nombre'] ?></div>
                                             <?php
	if($item["precio_descuento"]==1)
	{
	?>
          <div><strong>Price Each: </strong><span style="text-decoration:line-through;"><?=_PRODUCTOMONEDASIMBOLODE?><?=number_format($item["precio_anterior"],2)?></span></div>
          <?php
	}
	?>
          <?php if(!empty($item["codigo"])){ ?> <div><strong>Codigo: </strong><?=$item["codigo"]?></div><?php } ?>
          <?php if(!empty($item["idxcolor"])){ ?><div><strong>Color: </strong> <?=$item["wnombrecolor"]?></div> <?php } ?>
          <?php if(!empty($item["idxsize"])){ ?> <div><strong>Size: </strong><?=$item["wnombresize"]?> </div><?php } ?>
                                </td>
                                <td valign="top">
                                <div align="right"><?=$item["cantid"]?></div>
                                </td>
                                <td valign="top">
                                <div class="car_nombre" style="text-align:right"><?=_PRODUCTOMONEDASIMBOLODE?><?=number_format($item["precio"],2)?></div>
                                </td>
                                <td valign="top">
                                <div align="right" > <strong>  <?=_PRODUCTOMONEDASIMBOLODE?><?=number_format($item["precio"] * $item["cantid"],2) ?> </strong>  </div>
                                </td>
                            </tr>
                            <?php
							$subTotal=$subTotal + ($item["precio"] * $item["cantid"]);
							$total=$subTotal;
							
							?>
                        <?php endforeach; ?>   
                        
                    </tbody></table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td><table border="0" align="right" cellpadding="2" cellspacing="2">
      <tr>
        <td><strong>Total </strong></td>
        <td>:</td>
        <td>
        <?php
		 $_SESSION["subtotal"]=$subTotal;
		$_SESSION["total"]=$total;
		?>
        <div style="font-size:16px"><strong><?=_PRODUCTOMONEDASIMBOLODE?><?php echo number_format($total,2);?></strong></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<div><?php echo nl2br($wdescrip2); ?></div>
    
    