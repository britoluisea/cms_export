				</div>
				<!-- /#container-fluid xyz -->
			</div>
			<!-- /#page-content-wrapper -->
		</div>
		<!-- /#wrapper -->
		<footer> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div style="padding-right: 50px;">
							<?php if(isset($wwhite_label_footer)){echo $wwhite_label_footer;}?>		
						</div>
					</td>
					<td width="20%"><div align="right" class="class_version"><?=isset($dataGlobal['Version']) ? $dataGlobal['Version'] : 'Version'?> <?=VERSION?></div></td>
				</tr>
			</table>
		</footer>		
		<div class="modal fade" id="iframeModal">
       <div class="modal-dialog">
           <div class="modal-content bmd-modalContent">
               <div class="modal-body">
                   <div class="close-button">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   </div>
                   <div class="embed-responsive embed-responsive-16by9" id="embed-responsive-conter">
                       <iframe class="embed-responsive-item" frameborder="0"></iframe>
                   </div>
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
<?php include(dirname(__FILE__)."/foot.php"); ?>   