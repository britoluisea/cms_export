<?php
  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;

    // Instantiate a new mail object
//    $message = new email(array('X-Mailer: osCommerce'));
    $message = new email(array(''));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }
  
  
    function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }
  
//BOC e-mail with attachment
function tep_mail_pdf_anhang($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address, $file, $filetype)
{
	if (SEND_EMAILS != 'true') return false;
	
	if (eregi('Content-Type:', $to_name)) return false;
	if (eregi('Content-Type:', $to_email_address)) return false;
	if (eregi('Content-Type:', $email_subject)) return false;
	if (eregi('Content-Type:', $email_text)) return false;
	if (eregi('Content-Type:', $from_email_name)) return false;
	if (eregi('Content-Type:', $from_email_address)) return false;
	if ( (strstr($to_name, "\n") != false) || (strstr($to_name, "\r") != false) ) return false;
	if ( (strstr($to_email_address, "\n") != false) || (strstr($to_email_address, "\r") != false) ) return false;
	if ( (strstr($email_subject, "\n") != false) || (strstr($email_subject, "\r") != false) ) return false;
	if ( (strstr($from_email_name, "\n") != false) || (strstr($from_email_name, "\r") != false) ) return false;
	if ( (strstr($from_email_address, "\n") != false) || (strstr($from_email_address, "\r") != false) ) return false;
	
	$message = new email(array('X-Mailer: osCommerce Mailer'));
	
	$text = strip_tags($email_text);
	if (EMAIL_USE_HTML == 'true') {
		$message->add_html($email_text, $text);
	} else {
		$message->add_text($text);
	}
	$attachment = fread(fopen($file, "r"), filesize($file));
	$message->add_attachment($attachment, $file, $filetype);
	//   $message->add_html_image($file_content, $file);
	
	$message->build_message();
	$message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
}
//EOC e-mail with attachment
  

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function tep_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return ereg_replace('(' . implode('|', $from) . ')', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }

function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }






?>