document.addEventListener("DOMContentLoaded", function(){
	if(!Notification){
		alert('Las notificaciones no se soportan en su navegador, se recomienda usar uno mas actual');
		return;
	}
	else
	{
		if(Notification.permission !== "granted")
		{
			Notification.requestPermission();
		}
	}
});


function noti(title, msg, timeout) {
	var e=0;
	if(e=1,0==isIE8orlower()&&$.sound_on){
		var f=document.createElement("audio");
		f.setAttribute("src","customize/sound/noti.mp3");
		f.addEventListener("load",function(){f.play()},!0);
		f.pause();
		f.play();
	}

	var options = { body: msg };
	options.icon = "customize/images/icon-cms.png";

	//Show the notification
	var notif = new Notification(title, options);

	//Autohide notification if specified
	if (timeout && typeof (timeout) == "number")
		setTimeout(closeNotification, timeout, notif);

	return notif;
}

//Closes a notification
function closeNotification(notif) {
	notif.close();
}


function alerError(title, body, time){
	$.smallBox({
          title : title,
          content : body,
          color : "#c26565",
          bgColor : "#953b39",
          timeout: time,
          icon : "fa fa-warning"
        });
}
function alerNormal(title, body, time){
	$.smallBox({
          title : title,
          content : body,
          color : "#3276b1",
          bgColor : "rgba(0, 0, 0, 0.14)",
          timeout: time,
          icon : "fa fa-refresh fa-spin"
        });
}
function alerAct(title, body, time){
	var data ={};
	data.title= title;
	data.content= body;
	data.color= "#3276b1";
	data.bgColor= "rgba(0, 0, 0, 0.14)";
	if (time && typeof (time) == "number" && time>0) { data.timeout= time; }
	data.icon= "fa fa-refresh fa-spin";
	$.smallBox(data);
}
window.alerValid= function(title, body, time){	
	$.smallBox({
          title : title,
          content : body,
          color : "#739E73",
          bgColor : "#598259",
          timeout: time,
          icon : "fa fa-check"
        });
}
function alerConfirm(title, body, id, func){
	$.smallBox({
          title : title,
          content : body+"<p class='text-align-right'><a href='javascript:void(0);' class='btn btn-primary btn-sm' onclick='"+func+"("+id+")'>Yes</a> <a href='javascript:void(0);'  class='btn btn-danger btn-sm' style='padding:5px 10px;'>No</a></p>",
          color : "#d4a047",
          bgColor : "rgba(0, 0, 0, 0.1)",
          icon : "fa fa-trash"
        });
}
function alerAviso(title, body, time){
	$.smallBox({
          title : title,
          content : body,
          color : "#d4a047",
          bgColor : "rgba(0, 0, 0, 0.1)",
          timeout: time,
          icon : "fa fa-warning "
        });
}
