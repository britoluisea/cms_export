limpiar_campos = function()
{
	$('.limp').val('');
	$('.lim_select').each(function(index, el) {
		el.selectedIndex=0;
	});
	$('#save').removeAttr('disabled');
	$('#divSmallBoxes').html('');
}
 window.closeModal = function(){ 
 	$('#iframeModal').modal('hide'); 
 	window.location.reload();
 };
 window.closeModal_hide = function(){ 
 	$('#iframeModal').modal('hide'); 
 };
$(function() {
	$.fn.bmdIframe = function( options ) {
		var self = this;
		var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: '640px',
            defaultH: '360px'
        }, options );
        $(settings.classBtn).on('click', function(e) {
	        var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
	        var dataStyle ={	        	
				'height': $(this).attr('data-bmdHeight') || settings.defaultH,
				'width': $(this).attr('data-bmdWidth') || settings.defaultW,
	        };
	        var dataAttr = {
				'src': $(this).attr('data-bmdSrc'),
				'scrolling': $(this).attr('data-scrolling') || 'no',
			};
			// obteniendo wl width y le sumo 30 del padding para q el iframe se mantenga dentro del modal
      if(dataStyle.width.substr(-1) == '%')
      {
        var widthModal =dataStyle.width;
			  $('#iframeModal .close-button').css('display', 'none');
        $('#iframeModal .modal-dialog').css({'width': widthModal, 'height': widthModal, 'margin': 0});
        $('#iframeModal .modal-content').css({'width': widthModal, 'height': widthModal});
      }
      else
      {
			  var widthModal = parseInt(dataStyle.width.substr(-5, 3)) + parseInt(30); 
        widthModal = widthModal+'px'; 
        $('#iframeModal .modal-dialog').css('width', widthModal);
      }
			$('#embed-responsive-conter').css(dataStyle);
			if ( allowFullscreen ) {dataAttr.allowfullscreen = "";}
			// stampiamo i nostri dati nell'iframe
			//debugger;
			$(self).find("iframe").attr(dataAttr);
			$(self).find("iframe").css(dataStyle);
        });
        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function(){
        	$(this).find('iframe').html("").attr("src", "");
        });
        return this;
    };
});
$(document).ready(function(){
    $("#iframeModal").bmdIframe();
});
var q = function(e){return document.querySelector(e);}
var BrowseServer =function(serverPreview)
{
	var finder = new CKFinder();
	finder.basePath = 'images/';
	finder.selectActionFunction = serverPreview;
	finder.popup();
}
var serverPreview = function(fileUrl){image.value=fileUrl; preview.src=fileUrl;}
var filePreview =function(input, preview){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.readAsDataURL(input.files[0]);
		reader.onload = function (e) {
			preview.src = e.target.result;
			preview.style.width='100%';
		}
	}
}
var sliderRangeVal = function(input, div, str)
{
	div.innerHTML = input.value+str;
    input.oninput = function() {
      div.innerHTML = this.value+str;
    }
}
var addBtn = function(idEditor){
  event.preventDefault();
  var cls = $("#botones option:selected").attr('title').trim('string');
  var add = '<a href="'+server+'" class="btn btn-'+cls+'">'+cls+'</a>';
  var editor = eval(idEditor);
  if ( editor.mode == 'wysiwyg' )
  {
    editor.insertHtml( add );
  }
}

var addIcons = function(idEditor){
  event.preventDefault();
  var cls = $("#iconos option:selected").attr('title').trim('string');
  var add = '<i class="fa fa-'+cls+'"></i>';
  var editor = eval(idEditor);
  if ( editor.mode == 'wysiwyg' )
  {
    editor.insertHtml( add );
  }
}
var addBtnIcons = function(idEditor){
  event.preventDefault();
  var i = $("#iconos option:selected").attr('title').trim('string');
  var icon = '<i class="fa fa-'+i+'"></i>';  
  var cls = $("#botones option:selected").attr('title').trim('string');
  var align = $("#align-icon").val();
  var btn = '';
  if(align=='right'){btn='<a href="'+server+'" class="btn btn-'+cls+'">'+cls+' '+icon+'</a>';}
  else{btn='<a href="'+server+'" class="btn btn-'+cls+'">'+icon+' '+cls+'</a>';}
  var editor = eval(idEditor);
  if ( editor.mode == 'wysiwyg' )
  {
    editor.insertHtml( btn );
  }
}