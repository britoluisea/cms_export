<?php
require_once(dirname(__FILE__)."/../header_box.php");
$i = isset($_GET['i']) ? $_GET['i'] : '';
if (empty($opc)){ $opc = 1; }
?>
<style>
</style>
<form action="save.php" method="post" name="form1">
<input type="hidden" name="ope" value="rating-reviews"/>
  <input type="hidden" name="opc" value="3" />
  <input type="hidden" name="i" value="<?=$i?>" />
   <div class="modalcread modalcread-edit">  
        <div class="modal-header ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="self.parent.tb_remove();">×</button>
            <div class="delete"> <h2 class="h2"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></h2></div>
        </div>
        <div class="modal-body">
            <div class="alert alert-warning">
                <strong><?=isset($dataGlobal['del_review']) ? $dataGlobal['del_review'] : 'Delete Review?'?></strong>
            </div>
        </div>
        <div class="modal-footer">
            <button name="button" type="submit" class="btn btn-success" id="button"><i class="fa fa-remove"></i> <?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></button>
            <button name="button2" type="button" class="btn btn-default" id="button2" ><i class="fa fa-reply"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?></button>
        </div>
    </div>
  </form>
  <script>
    $("#button2").click(function(event) {
      event.preventDefault();
      window.parent.closeModal(); 
    });
  </script>
<?php include(dirname(__FILE__)."/../foot.php");?>