<?php
include('../../cn/cnx.php');
session_start();
$ope = isset($_POST['ope']) ? $_POST['ope'] : '';
$opc = isset($_POST['opc']) ? $_POST['opc'] : '';
$i = isset($_POST['i']) ? $_POST['i'] : '';
if(!empty($ope))
{
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "rating-reviews") 
    {
        //echo "Ingrese";
        //exit();
        $tb = "rating_reviews";
        $fecreg = isset($_POST['fecreg']) ? $_POST['fecreg'] : date('d/m/Y h:m:i');
        $comment_status_id = isset($_POST['comment_status_id']) ? $_POST['comment_status_id'] : '';
        $nombres = isset($_POST['nombres']) ? $_POST['nombres'] : '';
        $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : '';
        $username = isset($_POST['username']) ? $_POST['username'] : '';
        $project = isset($_POST['project']) ? $_POST['project'] : '';
        $city = isset($_POST['city']) ? $_POST['city'] : '';
        $state = isset($_POST['state']) ? $_POST['state'] : '';
        $descrip = isset($_POST['descrip']) ? addslashes($_POST['descrip']) : '';
        $telephone = isset($_POST['telephone']) ? $_POST['telephone'] : '';
        $genero = isset($_POST['genero']) ? $_POST['genero'] : '';
        $avatar = isset($_POST['avatar']) ? $_POST['avatar'] : '';
        $wfecha = substr($fecreg, 6, 4) . "-" . substr($fecreg, 0, 2) . "-" . substr($fecreg, 3, 2);
        $whour = substr($fecreg, 11, 2) . "-" . substr($fecreg, 14, 2) . "-" . substr($fecreg, 17, 2);
        $fechahour = $wfecha . " " . $whour;

        if($comment_status_id==1)
        {
           $status='pending';
        }
        elseif($comment_status_id==2)
        {
           $status='approved';
        }
        elseif($comment_status_id==3)
        {
           $status='spam';
        }
        elseif($comment_status_id==4)
        {
           $status='trash';
        }
        else{$status='';}
        if ($opc == 2) 
        {
            if ($comment_status_id == 1 or $comment_status_id == 2) 
            {
                $sql_0 = "update $tb set  
                nombres = '$nombres',
                apellidos = '$apellidos',
                username = '$username',
                project = '$project',
                city = '$city',
                state = '$state',
                descrip = '$descrip',
                telephone = '$telephone',
                genero = '$genero',
                avatar = '$avatar',
                fecreg = '$fechahour',
                comment_status_id = '$comment_status_id',
                comment_status_id_salvar = '$comment_status_id'
                where
                idx = '$i' ";
            }
            if ($comment_status_id == 3 or $comment_status_id == 4) 
            {
                $sql_0 = "update $tb set  
                descrip = '$descrip',
                comment_status_id = '$comment_status_id'
                where
                idx = '$i' ";
            }
            if (!$res_0 = ejecutar($sql_0)) {
                echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
                exit();
            }
        }
        if ($opc == 3) {
            $t1 = "rating_reviews";
            $sql_1 = "select 
            *
            from
            $t1
            where
            idx = '" . $i . "' or
            comment_parent = '" . $i . "'
            ";
            $consulta_1 = ejecutar($sql_1);
            $num_reg = numRows($consulta_1);
            if ($num_reg == 1) {
                $sql_0 = "delete from $tb where idx = '$i'";
                if (!$res_0 = ejecutar($sql_0)) {
                    echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
                    exit();
                }
            } else {
                $_SESSION["error"] = "Comment has existing records.";
            }
        }
        header("Location: index.php?status=".$status);
        exit();
    }
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "rating-reviews-widget") 
    {
        $title = isset($_POST['title']) ? $_POST['title'] : '';
        $protocolo = isset($_POST['protocolo']) ? $_POST['protocolo'] : '';
        $web = isset($_POST['web']) ? $_POST['web'] : '';
        $destino = isset($_POST['destino']) ? $_POST['destino'] : '';
        $business_name = isset($_POST['business_name']) ? $_POST['business_name'] : '';
        $num_reg_limit = isset($_POST['num_reg_limit']) ? $_POST['num_reg_limit'] : '';
        $show_hide = isset($_POST['show_hide']) ? $_POST['show_hide'] : '';
        $num_car = isset($_POST['num_car']) ? $_POST['num_car'] : '';
        $descrip = isset($_POST['descrip']) ? $_POST['descrip'] : '';
        $sql_0 = "update rating_reviews_widget set  
        title = '$title',
        protocolo = '$protocolo',
        web = '$web',
        destino = '$destino',
        business_name = '$business_name',
        num_reg_limit = '$num_reg_limit',
        show_hide = '$show_hide',
        num_car = '$num_car',
        descrip ='$descrip'
        where idx = 1";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
            exit();
        }
        $_SESSION['data_guarda_bien'] = 1;
        header("Location: widget.php");
        exit();
    }
    //--------------------------------------------
    if ($ope == "rating-reviews-widget-scroll") 
    {
        $pausa = isset($_POST['pausa']) ? $_POST['pausa'] : '';
        $velocidad = isset($_POST['velocidad']) ? $_POST['velocidad'] : '';
        $opcdireccion = isset($_POST['opcdireccion']) ? $_POST['opcdireccion'] : '';
        $aleatorio = isset($_POST['aleatorio']) ? $_POST['aleatorio'] : '';
        $control = isset($_POST['control']) ? $_POST['control'] : '';
        $alto = isset($_POST['alto']) ? $_POST['alto'] : '';
        $num_car = isset($_POST['num_car']) ? $_POST['num_car'] : '';
        $activo = isset($_POST['activo']) ? $_POST['activo'] : '';
        $sql_0 = "update rating_reviews_widget_scroll set  
        pausa = '$pausa',
        velocidad = '$velocidad',
        opcdireccion = '$opcdireccion',
        aleatorio = '$aleatorio',
        control = '$control',
        alto = '$alto',
        num_car = '$num_car',
        activo = '$activo'
        where idx = 1";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
            exit();
        }
        $_SESSION['data_guarda_bien'] = 1;
        header("Location: widget-scroll.php");
        exit();
    }
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "rating-reviews-widget-c") 
    {
        $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
        $num_col = isset($_POST['num_col']) ? $_POST['num_col'] : '';
        $num_reg = isset($_POST['num_reg']) ? $_POST['num_reg'] : '';
        $background_color = isset($_POST['background_color']) ? $_POST['background_color'] : '';
        $num_car = isset($_POST['num_car']) ? $_POST['num_car'] : '';
        $text_color = isset($_POST['text_color']) ? $_POST['text_color'] : '';
        $sql_0 = "update rating_reviews_widget_c set  
        nombre = '$nombre',
        num_col = '$num_col',
        num_reg = '$num_reg',
        background_color = '$background_color',
        num_car = '$num_car',
        text_color = '$text_color'
        where idx = 1";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
            exit();
        }
        $_SESSION['data_guarda_bien'] = 1;
        header("Location: widget.php");
        exit();
    }
    //--------------------------------------------
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "rating-reviews-options") {
        $header_title  = isset($_POST['header_title']) ? $_POST['header_title'] : '';
        $header_business_name  = isset($_POST['header_business_name']) ? $_POST['header_business_name'] : '';
        $assign_page  = isset($_POST['assign_page']) ? $_POST['assign_page'] : '';
        $write_review  = isset($_POST['write_review']) ? $_POST['write_review'] : '';
        $header_review_message  = isset($_POST['header_review_message']) ? $_POST['header_review_message'] : '';
        $num_reg_page  = isset($_POST['num_reg_page']) ? $_POST['num_reg_page'] : '';
        $counter_show  = isset($_POST['counter_show']) ? $_POST['counter_show'] : '';
        $field_project  = isset($_POST['field_project']) ? $_POST['field_project'] : '';
        $field_project_show  = isset($_POST['field_project_show']) ? $_POST['field_project_show'] : '';
        $show_date  = isset($_POST['show_date']) ? $_POST['show_date'] : '';
        $sql_0 = "update rating_reviews_options set  
                header_title = '$header_title', 
                header_business_name ='$header_business_name',
                assign_page ='$assign_page',
                write_review ='$write_review',
                header_review_message = '$header_review_message',
                num_reg_page = '$num_reg_page', 
                counter_show = '$counter_show',
                field_project = '$field_project',
                field_project_show = '$field_project_show',
                show_date = '$show_date'
                where idx = 1 ";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
            exit();
        }
        $_SESSION['data_guarda_bien'] = 1;
        header("Location: options.php");
        exit();
    }
    //--------------------------------------------
    //--------------------------------------------
    if ($ope == "rating-reviews-options-form") {
        $descrip_form = isset($_POST['descrip_form']) ? $_POST['descrip_form'] : '';
        $sql_0 = "update rating_reviews_options set
                descrip_form = '$descrip_form'
                where idx = 1 ";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla ";
            exit();
        }
        $_SESSION['data_guarda_bien'] = 1;
        header("Location: options-form.php");
        exit();
    }
    //--------------------------------------------
}
else{header("Location: index.php");}