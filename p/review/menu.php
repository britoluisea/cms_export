<aside style=" height: 500px;width: 100%;">
   <div id="resize">&laquo;</div><div id="resize2">&raquo;</div>
    <div id="lateral">
      <div id="navl1">
        <h2 class="text-uppercase"><?=isset($dataGlobal['rating_reviews']) ? $dataGlobal['rating_reviews'] : 'Rating & Reviews'?></h2>
        <ul id="menua" class="genial_menu">
        <li class="menucat1">
            <ul class="submenu">
                <li >
                    <a href="<?=SERVER_P?>review/index.php"  >
                        <i class="fa fa-list-alt"></i>
                        <span style="font-size:12px;"><?=isset($dataGlobal['label_listing']) ? $dataGlobal['label_listing'] : 'Listing'?></span>
                    </a>            
                </li>
            </ul>
        </li>
        <li class="menucat1">
            <ul class="submenu">
                <li ><a href="<?=SERVER_P?>review/widget.php"><?=isset($dataGlobal['widget_review']) ? $dataGlobal['widget_review'] : 'Widget Review'?></a></li>
            </ul>
        </li>
        <li class="menucat1">
            <ul class="submenu">
                <li ><a href="<?=SERVER_P?>review/options.php"><?=isset($dataGlobal['opt_review']) ? $dataGlobal['opt_review'] : 'Options Review'?></a></li>
                <li ><a href="<?=SERVER_P?>review/options-form.php"><?=isset($dataGlobal['opt_form_review']) ? $dataGlobal['opt_form_review'] : 'Options Form'?></a></li>
                <li ><a href="<?=SERVER_P?>review/notification.php"><?=isset($dataGlobal['label_notif']) ? $dataGlobal['label_notif'] : 'Notification'?></a></li>
            </ul>
        </li>
        </ul>
        <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="fa fa-double-angle-left" ></i>
    </div>
      </div>
     </div>
</aside>
