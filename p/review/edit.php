<?php include(dirname(__FILE__)."/../header.php");?>
<?php
$i = isset($_GET['i']) ? $_GET['i'] : '';
$opc = isset($_GET['opc']) ? $_GET['opc'] : '';
if(empty($_GET['i']))
{
  header('location:index.php');
  exit();
}
?>
<?php
$wdisplay = "display:none";
?>
<?php
	$sql_2="select * from rating_reviews_options where idx='1' "; //Devuelve todos los Idiomas
	$consulta_2=ejecutar($sql_2);
	$atabla_opt_rat=array();
	while($fila_2=fetchAssoc($consulta_2))
	{
		$atabla_opt_rat['en']=$fila_2;
		//print_r($atabla)."<br>";
	}
?>
<link rel="stylesheet" media="all" type="text/css" href="<?=PLUGINS_P?>calendar/themes/base/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="<?=PLUGINS_P?>calendar/css/jquery-ui-timepicker-addon.css" />
<!--<script type="text/javascript" src="calendar/js/jquery-1.11.0.min.js"></script> -->
<script type="text/javascript" src="<?=PLUGINS_P?>calendar/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="<?=PLUGINS_P?>calendar/js/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript">
	$(function() {
		$( "#fecreg" ).datetimepicker({
			changeMonth: true,
            changeYear: true,
			dateFormat: 'mm/dd/yy',
			showOn: "both",
			buttonImage: "<?=SERVER_P?>img/calendar.gif",
			buttonImageOnly: true,
			autoclose: true,
			
			controlType: 'select',
			timeFormat: 'HH:mm:ss',
			
		});
	});
</script>

<style type="text/css">
.help-inline
{
	color: red;
	margin-bottom:10px;
}
 /* Checkbox & Radio */
    input[type=checkbox] , input[type=radio] {
        opacity:0;
        position:absolute;
        z-index:12;
        width:18px; height:18px;
    }
    input[type=checkbox]:focus , input[type=radio]:focus ,
    input[type=checkbox]:checked , input[type=radio]:checked
    {
        outline:none !important;
    }

    input[type=checkbox] + .lbl , input[type=radio] + .lbl {
        position: relative; z-index:11;
        display:inline-block;
        margin:0;
        line-height:20px;

        min-height:14px;
        min-width:14px;
        font-weight:normal;
    }

    input[type=checkbox] + .lbl.padding-4::before , input[type=radio] + .lbl.padding-4::before  {
        margin-right:4px;
    }
    input[type=checkbox] + .lbl.padding-6::before , input[type=radio] + .lbl.padding-6::before {
        margin-right:6px;
    }
    input[type=checkbox] + .lbl.padding-8::before , input[type=radio] + .lbl.padding-8::before {
        margin-right:8px;
    }


    input[type=checkbox] + .lbl::before , input[type=radio] + .lbl::before {
        font-family:fontAwesome; font-weight:normal;
        font-size: 11px; color: #32A3CE;
        content:"\a0";
        display:inline-block;
        background-color: #FAFAFA;
        border: 1px solid #CCC;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05);
        inset 0px -15px 10px -12px rgba(0,0,0,0.05);
        border-radius: 0;
        display: inline-block;
        text-align:center;

        vertical-align:middle;

        height:13px; line-height:13px;
        min-width:13px;	

        margin-right:1px;
    }


    input[type=checkbox]:active + .lbl::before, input[type=checkbox]:checked:active + .lbl::before ,
    input[type=radio]:active + .lbl::before, input[type=radio]:checked:active + .lbl::before
    {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    input[type=checkbox]:checked + .lbl::before ,
    input[type=radio]:checked + .lbl::before
    {
        display:inline-block;
        content: '\f00c';
        background-color: #f5f8fC;
        border-color:#adb8c0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    }


    input[type=checkbox]:hover + .lbl::before ,
    input[type=radio]:hover + .lbl::before,
    input[type=checkbox] + .lbl:hover::before ,
    input[type=radio] + .lbl:hover::before
    {
        border-color:#FF893C;
    }


    input[type=checkbox].ace-checkbox-2 + .lbl::before {
        box-shadow: none;
    }
    input[type=checkbox].ace-checkbox-2:checked + .lbl::before {
        background-color: #F9A021;
        border-color:#F9A021;
        color: #FFF;
    }



    input[type=checkbox]:disabled + .lbl::before ,
    input[type=radio]:disabled + .lbl::before {
        background-color:#DDD !important;
        border-color:#CCC !important;
        box-shadow:none !important;

        color:#BBB;
    }


    input[type=radio] + .lbl::before {
        border-radius:32px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:36px;
    }
    input[type=radio]:checked + .lbl::before{
        content:"\2022";
    }
    .form-control{width: 90%; margin: 2%;}
</style>   
<div class="row" id="main-container">
  <div  class="xs-col-12 col-sm-3 " style="    max-width: 250px;">
    <?php include("menu.php");?>
  </div>
  <div class="xs-col-12 col-sm-9">
     <br>
    
<form action="save.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
 <?php // echo "userlevel=>".$userlevel?>
  <?php  
		$t1="rating_reviews";
		$sql_1="select 
		$t1.*
		from
		$t1
		where
		$t1.idx = '".$i."'
 		";
	$consulta_1=ejecutar($sql_1);
	$atabla=array();
	while($fila_1=fetchAssoc($consulta_1))
	{
		$atabla['en']=$fila_1;
		/*echo "<pre>";
		print_r($atabla);
		echo "</pre>";*/
		$wfecha = substr($atabla['en']["fecreg"], 5, 2) . "/" . substr($atabla['en']["fecreg"], 8, 2) . "/" . substr($atabla['en']["fecreg"], 0, 4);
		$whour = substr($atabla['en']["fecreg"], 11, 2) . ":" . substr($atabla['en']["fecreg"], 14, 2) . ":" . substr($atabla['en']["fecreg"], 17, 2);
		$fechahour=$wfecha." ".$whour;
		$opc = 2;
	}
 	//$atabla['en']['usu_userlevel'];
?>      
<input type="hidden" name="ope" value="rating-reviews"/>
<input type="hidden" name="opc" value="<?=$opc?>" />
<input type="hidden" name="i" value="<?=$i?>" />
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td>
<div style="font-size:20px; font-weight:bold; color:#2679b5">
<?php
    if($opc == 1)
	{
		//echo "Add Comment ";
	}else{
		//echo "Edit Comment ";
	}
	?>
</div>
                                </td>
                                <td align="right">

                                    <button  name="save" id="save" type="submit" class="btn btn-small btn-primary"  value="save"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
                                   <a href="index.php" class="btn btn-default "><i class="fa fa-chevron-left"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?></a>

                                </td>
                            </tr>
                        </tbody></table>
                        
	<div class="row-fluid">

	 <div class="col-sm-9" >
     <br>
     <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc;">
                                                <tbody><tr>
                                                    <td class="widget-header-tag">
                                                        <div style="padding:4px; font-size:14px; color:#669fc7">
                                                           <?=isset($dataGlobal['edit_review']) ? $dataGlobal['edit_review'] : 'Edit Review'?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="top" style=" padding:10px;">

                                               <table width="100%" border="0" cellspacing="2" cellpadding="2" >
  <tr>
    <td width="50%" valign="top">
      <table width="100%" border="0" cellpadding="8" cellspacing="0">
 
        <tr>
          <td colspan="3" valign="top">
            <div><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?> <span class="asterisco">*</span></div>
            <input type="text" name="nombres" id="nombres" class="form-control" value="<?=$atabla['en']["nombres"];?>"/>
            </td>
          </tr>
        <tr>
          <td colspan="3" valign="top">
            <div><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?> <span class="asterisco">*</span></div>
            <input type="text" name="city" id="city" class="form-control" value="<?=$atabla['en']["city"];?>"/>
            </td>
        </tr>
        <tr>
          <td colspan="3" valign="top">
            <div><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> <span class="asterisco">*</span></div>
            
            <input type="text" name="username" id="username" class="form-control" value="<?=$atabla['en']["username"];?>" />
            
            </td>
        </tr>
        <tr>
          <td colspan="3" valign="top">
             <div><?=isset($dataGlobal['label_gender']) ? $dataGlobal['label_gender'] : 'Gender'?> <span class="asterisco">*</span></div>
      <select name="genero" id="genero" class="form-control"  >
      <option value="1" <?php if($atabla['en']["genero"]==1): ?> selected="selected" <?php endif; ?>><?=isset($dataGlobal['label_male']) ? $dataGlobal['label_male'] : 'Male'?></option>
      <option value="0" <?php if($atabla['en']["genero"]==0): ?> selected="selected" <?php endif; ?>><?=isset($dataGlobal['label_famale']) ? $dataGlobal['label_famale'] : 'Female'?></option>
  </select>
          </td>
        </tr>
        </table>
    </td>
    <td width="50%" valign="top">
      <table width="100%" border="0" cellpadding="8" cellspacing="0">

        <tr>
          <td colspan="3" valign="top">
            
            <div><?=isset($dataGlobal['last_name']) ? $dataGlobal['last_name'] : 'Last Name'?> <span class="asterisco">*</span></div>
            <input type="text" name="apellidos" id="apellidos" class="form-control" value="<?=$atabla['en']["apellidos"];?>"/>
            </td>
          </tr>
        <tr>
          <td colspan="3" valign="top">
            <div><?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?> <span class="asterisco">*</span></div>
            <input type="text" name="state" id="state" class="form-control"  value="<?=$atabla['en']["state"];?>"/>
            
            </td>
        </tr>
        <tr>
          <td colspan="3" valign="top">
            
            <div><?=isset($dataGlobal['telephone']) ? $dataGlobal['telephone'] : 'Telephone'?><span class="asterisco"></span></div>
            <input type="text" name="telephone" id="telephone" class="form-control" value="<?=$atabla['en']["telephone"];?>" />
            
            
            </td>
        </tr>
        <tr>
          <td colspan="3" valign="top">
          <div>*<?=isset($dataGlobal['rating_stars']) ? $dataGlobal['rating_stars'] : 'What is your overall rating of this business?'?><span class="asterisco"></span></div>
          <div><span class="badge badge-warning"><?php echo number_format($atabla['en']["stars"],1);?></span></div>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><br><br>

                                         <table width="100%" border="0" cellspacing="0" cellpadding="8">
  <tr>
    <td>
      <div class="tabbable">
                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                            <li  class="active" >
                                <a data-toggle="tab" href="#tab1"><i class="fa fa-user"></i> <?=isset($dataGlobal['label_avatar']) ? $dataGlobal['label_avatar'] : 'Avatar'?> <span class="asterisco">*</span></a>
                            </li>
                      </ul>
      
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane active ">
      <?php
		$sql=" select *  from  rating_reviews_avatar where activo='1'";	
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		while($fila=fetchArray($consulta))
		{   
		?>
      <table style="width: 14.28571428571429%;" align="left" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td >
            
            <div style="border:0px solid #000;">
              <div class="controls" align="center">
                <label class="">
                <input name="avatar" type="radio" value="<?=$fila['idx']?>" <?php if($atabla['en']["avatar"]==$fila['idx']): ?> checked="checked" <?php endif; ?>  ><span class="lbl"></span>
                <div align="center" class="picture-wri-xy">                  
                  <img  src="<?=SERVER?>/imgcms/write-review/avatar/<?=$fila['imagen']?>" class="img-responsive img-circle"/>
                  </div>	  
                </label>
                </div>   
              </div>
            </td>
          </tr>
        </table>
      
      <?php } ?>
      </td>
  </tr>
  </table>
</div></div>
        <br><br>

    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
    <td  valign="top">
    
                    <div class="tabbable">
                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                            <li  class="active" >
                                <a data-toggle="tab" href="#tab1"><i class="fa fa-list-alt"></i> <?=isset($dataGlobal['review']) ? $dataGlobal['review'] : 'Review'?></a>
                            </li>
                      </ul>

                        <div class="tab-content">
                            <div id="tab1" class="tab-pane active " style="padding-left: 10px;">
                            <div><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></div>
                            <input type="text" name="project" id="project" class="form-control" value="<?=$atabla['en']["project"];?>" />
                            <div><?=isset($dataGlobal['your_review']) ? $dataGlobal['your_review'] : 'Your Review'?> <span class="asterisco">*</span></div>
                                 <textarea name="descrip" id="descrip" cols="45" rows="5" class="form-control"><?=$atabla['en']["descrip"]?></textarea>
                          </div>
                            
                      </div>
                    </div>
    </td>
    </tr>
    </table>
                                                  </td>
                                                </tr>
                                            </tbody></table>
  

	</div>

<div class="col-sm-3" >
<br>
<table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
                                                <tbody><tr>
                                                    <td class="widget-header-tag">
                                                        <div style="padding:4px; font-size:14px; color:#669fc7">
                                                            <?=isset($dataGlobal['status']) ? $dataGlobal['status'] : 'Status'?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="120" valign="top">

                                               <div class="control-group">
												
												<div class="radio">
													<label>
														<input name="comment_status_id" type="radio" class="ace" value="1" <?php if($atabla['en']["comment_status_id"]==1):?> checked="CHECKED" <?php endif;?>>
														<span class="lbl" style="color:#ffa500"> <?=isset($dataGlobal['label_pending']) ? $dataGlobal['label_pending'] : 'Pending'?></span>
													</label>
												</div>

												<div class="radio">
													<label>
														<input name="comment_status_id" type="radio" class="ace" value="2" <?php if($atabla['en']["comment_status_id"]==2):?> checked="CHECKED" <?php endif;?>>
														<span class="lbl" style="color:#008000"> <?=isset($dataGlobal['label_approve']) ? $dataGlobal['label_approve'] : 'Approve'?></span>
													</label>
												</div>

												<div class="radio">
													<label>
														<input name="comment_status_id" type="radio" class="ace" value="3" <?php if($atabla['en']["comment_status_id"]==3):?> checked="CHECKED" <?php endif;?>>
														<span class="lbl" style="color:#ff0000"> <?=isset($dataGlobal['label_spam']) ? $dataGlobal['label_spam'] : 'Spam'?></span>
													</label>
												</div>
												
												<div class="radio">
													<label>
														<input name="comment_status_id" type="radio" class="ace" value="4" <?php if($atabla['en']["comment_status_id"]==4):?> checked="CHECKED" <?php endif;?>>
														<span class="lbl" style="color:#aa0000"> <?=isset($dataGlobal['label_trash']) ? $dataGlobal['label_trash'] : 'Trash'?></span>
													</label>
												</div>
											</div>
                                            <div style="margin-left:20px;">
                                            <div><?=isset($dataGlobal['ip_address']) ? $dataGlobal['ip_address'] : 'IP address'?>: <strong><?=$atabla['en']["ips"]?></strong></div>
                                           </div>
                                                  </td>
                                                </tr>
                                            </tbody></table><br>
                                            

   <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
                                                <tbody><tr>
                                                    <td class="widget-header-tag">
                                                        <div style="padding:4px; font-size:14px; color:#669fc7">
                                                            <?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date'?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="120" valign="top">

                                               
                                            <div style="margin-left:10px; margin-top:4px;">
                                            <div><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date'?> <span class="asterisco">*</span></div>
                                     <input type="text"  name="fecreg" id="fecreg" class="form-control" value="<?=$fechahour?>" readonly style="width:82%; padding:3px;display: inline-block;"/>
                                           </div>
                                                  </td>
                                                </tr>
                                            </tbody></table> <br>
                                            

   <table width="100%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid  #cccccc">
                                                <tbody><tr>
                                                    <td class="widget-header-tag">
                                                        <div style="padding:4px; font-size:14px; color:#669fc7">
                                                            <?=isset($dataGlobal['assing_gallery']) ? $dataGlobal['assing_gallery'] : 'Assing Gallery'?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="120" valign="top">
                                                    <?php 
$query_u1 = "SELECT * FROM galerias_lista";
$u1 = ejecutar($query_u1);
$row_u1 = fetchAssoc($u1);
$totalRows_u1 = numRows($u1);
$sql_idx_gallery = "SELECT idx_gallery FROM rating_reviews WHERE idx='".$_GET['i']."' ";
$query_idx_gallery = ejecutar($sql_idx_gallery);
$idx_gallery = fetchAssoc($query_idx_gallery);
$gallery=$idx_gallery['idx_gallery'];
?>
<select id="gallery" name="gallery" class="form-control" >
<option value="0" <?php if($gallery=='NULL' || $gallery=='null'){echo'selected="true"';}?>>None</option>
<?php if($totalRows_u1 > 0){ do { ?>
<option value="<?php echo $row_u1['idx']?>" <?php if($gallery==$row_u1['idx']){echo'selected="true"';}?>><?php echo $row_u1['nombre']?></option>
<?php } while ($row_u1 = fetchAssoc($u1)); }?>
</select>
<script>
jQuery("#gallery").change(function(event) {
	var gallery = jQuery(this).val();
	var idx = <?=$_GET['i']?>;
	if(gallery == 0) {jQuery("#idgalerias").html('');}

		$.ajax({
			url: 'assing_gallery.php',
			type: 'POST',
			data: {
				'idx_gallery': gallery,
				'idx': idx,
				},
		})
		.done(function(data) {
			console.log("success");
			if(data=='error') {jQuery("#idgalerias").html("<?=isset($dataGlobal['assing_gallery_msj1']) ? $dataGlobal['assing_gallery_msj1'] : 'Error NOT Assing Gallery'?>");}
			else if(data=='error2') {jQuery("#idgalerias").html("<?=isset($dataGlobal['assing_gallery_msj2']) ? $dataGlobal['assing_gallery_msj2'] : 'Gallery without images'?>");}
			else if(data=='error3') {jQuery("#idgalerias").html("<?=isset($dataGlobal['assing_gallery_msj3']) ? $dataGlobal['assing_gallery_msj3'] : 'System error'?>");}
			else{jQuery("#idgalerias").html(data);}
		})
		.fail(function() {
			console.log("error");
		});
	
});
</script>
<div id="idgalerias">
<?php
if($totalRows_u1 > 0){
	$select_sql="SELECT a.nomcar, b.imagen FROM galerias_lista AS a INNER JOIN detgaleriaslista AS b ON a.idx=b.idgalerias WHERE a.idx='".$gallery."'  order by a.idx ASC limit 0, 5";
	$query_select=ejecutar($select_sql);
	if(numRows($query_select) > 0)
	{
		$imagenes='';
		while($img=fetchAssoc($query_select))
		{ 
			$ruta="../imgcms/galeria/".$img['nomcar']."/small/".$img['imagen'];  
			$imagenes.='<img src="'.$ruta.'" style="width:50px;" />';
		}
		echo $imagenes;
	}
}
?>
</div>
                                                    </td>
                                                </tr>
                                            </tbody></table>                                         
</div>

	</div>
    
    </form>
</div>
  </div>
</div>           
<?php include(dirname(__FILE__)."/../footer.php");?>