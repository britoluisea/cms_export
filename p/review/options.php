<?php include(dirname(__FILE__)."/../header.php");
$i = isset($_GET['i']) ? $_GET['i'] : '';
?>
  <script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
  <script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>            
<style>
    /* Checkbox & Radio */
    input[type=checkbox] , input[type=radio] {
        opacity:0;
        position:absolute;
        z-index:12;
        width:18px; height:18px;
    }
    input[type=checkbox]:focus , input[type=radio]:focus ,
    input[type=checkbox]:checked , input[type=radio]:checked
    {
        outline:none !important;
    }
    input[type=checkbox] + .lbl , input[type=radio] + .lbl {
        position: relative; z-index:11;
        display:inline-block;
        margin:0;
        line-height:20px;
        min-height:14px;
        min-width:14px;
        font-weight:normal;
    }
    input[type=checkbox] + .lbl.padding-4::before , input[type=radio] + .lbl.padding-4::before  {
        margin-right:4px;
    }
    input[type=checkbox] + .lbl.padding-6::before , input[type=radio] + .lbl.padding-6::before {
        margin-right:6px;
    }
    input[type=checkbox] + .lbl.padding-8::before , input[type=radio] + .lbl.padding-8::before {
        margin-right:8px;
    }
    input[type=checkbox] + .lbl::before , input[type=radio] + .lbl::before {
        font-family:fontAwesome; font-weight:normal;
        font-size: 11px; color: #32A3CE;
        content:"\a0";
        display:inline-block;
        background-color: #FAFAFA;
        border: 1px solid #CCC;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05),
        inset 0px -15px 10px -12px rgba(0,0,0,0.05);
        border-radius: 0;
        display: inline-block;
        text-align:center;
        vertical-align:middle;
        height:13px; line-height:13px;
        min-width:13px; 
        margin-right:1px;
    }
    input[type=checkbox]:active + .lbl::before, input[type=checkbox]:checked:active + .lbl::before ,
    input[type=radio]:active + .lbl::before, input[type=radio]:checked:active + .lbl::before
    {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }
    input[type=checkbox]:checked + .lbl::before ,
    input[type=radio]:checked + .lbl::before
    {
        display:inline-block;
        content: '\f00c';
        background-color: #f5f8fC;
        border-color:#adb8c0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
    }
    input[type=checkbox]:hover + .lbl::before ,
    input[type=radio]:hover + .lbl::before,
    input[type=checkbox] + .lbl:hover::before ,
    input[type=radio] + .lbl:hover::before
    {
        border-color:#FF893C;
    }
    input[type=checkbox].ace-checkbox-2 + .lbl::before {
        box-shadow: none;
    }
    input[type=checkbox].ace-checkbox-2:checked + .lbl::before {
        background-color: #F9A021;
        border-color:#F9A021;
        color: #FFF;
    }
    input[type=checkbox]:disabled + .lbl::before ,
    input[type=radio]:disabled + .lbl::before {
        background-color:#DDD !important;
        border-color:#CCC !important;
        box-shadow:none !important;
        color:#BBB;
    }
    input[type=radio] + .lbl::before {
        border-radius:32px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:36px;
    }
    input[type=radio]:checked + .lbl::before{
        content:"\2022";
    }
          .radio {
    margin-left:-18px;
  margin-top:1px;
  margin-bottom:10px;
}
</style>
<div class="row" id="main-container">
  <div  class="xs-col-12 col-sm-3 " style=" max-width: 250px;">
      <?php include("menu.php");?>
    </div>
    <div class="xs-col-12 col-sm-9">
     <br> 
            <form action="save.php" method="post" enctype="multipart/form-data" name="form1">
                      <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
                <tr>
                          <td><div class="navipage"><strong><?=isset($dataGlobal['opt_review']) ? $dataGlobal['opt_review'] : 'Options Review'?></strong></div></td>
                          <td><table border="0" align="right" cellpadding="2" cellspacing="2">
                              <tr>
                              <td><button  name="save" id="save" type="submit" class="btn btn-small btn-primary"  value="save"><i class="icon-save icon-white"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
                                  <a href="<?=$_SERVER["PHP_SELF"]?>" class="btn btn-small btn-default"><i class="icon-reply"></i> <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?></a></td>
                            </tr>
                            </table></td>
                        </tr>
              </table>
                      <?php
if (isset($_SESSION['data_guarda_bien']) && $_SESSION['data_guarda_bien'] == 1 )
{
?>
                      <table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="Estilo_data_guardado1"  >
                <tr>
                          <td><div align="center"><strong><?=isset($dataGlobal['data_saved']) ? $dataGlobal['data_saved'] : 'Data Saved'?> <br />
                            </strong></div></td>
                        </tr>
              </table>
                      <?php
unset ($_SESSION['data_guarda_bien']);
}
?>
                      <?php
  $i = 1;
  $sql_1="select * from rating_reviews_options where idx='$i' "; //Devuelve todos los Idiomas
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $atabla['en']=$fila_1;
    //print_r($atabla)."<br>";
    $opc = 2;
    //$wfecha = strftime('%m/%d/%Y', strtotime($atabla['en']["fecreg"]));
    $i = $atabla['en']["idx"];
  }
?>
                      <input type="hidden" name="ope" value="rating-reviews-options"/>
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                          <td width="100"><strong><?=isset($dataGlobal['label_header']) ? $dataGlobal['label_header'] : 'Header'?></strong></td>
                          <td width="10">&nbsp;</td>
                          <td>&nbsp;</td>
                </tr>
                <tr>
                          <td valign="top"><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></td>
                          <td valign="top">:</td>
                          <td valign="top"><input type="text" name="header_title" id="header_title" class="form-control" value="<?=$atabla['en']["header_title"]?>"></td>
                </tr>
                <tr>
                          <td valign="top"><?=isset($dataGlobal['business_name']) ? $dataGlobal['business_name'] : 'Business Name'?></td>
                          <td valign="top">:</td>
                          <td valign="top"><input type="text" name="header_business_name" id="header_business_name" class="form-control" value="<?=$atabla['en']["header_business_name"]?>"></td>
                </tr>
                <tr>
                          <td valign="top"><?=isset($dataGlobal['assign_page']) ? $dataGlobal['assign_page'] : 'Assign Page'?></td>
                          <td valign="top">:</td>
                          <td valign="top">
                            <select name="assign_page" id="assign_page"  class="form-control  lim_select" style=" ">
                              <?php 
                                $sql="SELECT id, titulo FROM informativo WHERE activo=1 AND ( usamarketing = 5 OR usamarketing = 1 ) AND titulo != 'home' ORDER BY titulo ASC";
                                $q = ejecutar($sql);
                                while ($f = fetchArray($q)) { ?>
                                  <option value="<?=$f['id']?>" <?php if(isset($atabla['en']["assign_page"]) && $atabla['en']["assign_page"]==$f['id']){echo'selected';}?>><?=$f['titulo']?></option>
                                  <?php
                                }
                              ?>
                            </select>
                          </td>
                </tr>
                <tr>
                          <td valign="top"><?=isset($dataGlobal['write_review']) ? $dataGlobal['write_review'] : 'Write a Review'?></td>
                          <td valign="top">:</td>
                          <td valign="top">
                            <select name="write_review" id="write_review"  class="form-control  lim_select" style=" ">
                              <?php 
                                $sql="SELECT id, titulo FROM informativo WHERE activo=1 AND ( usamarketing = 5 OR usamarketing = 1 ) AND titulo != 'home' ORDER BY titulo ASC";
                                $q = ejecutar($sql);
                                while ($f = fetchArray($q)) { ?>
                                  <option value="<?=$f['id']?>" <?php if(isset($atabla['en']["write_review"]) && $atabla['en']["write_review"]==$f['id']){echo'selected';}?>><?=$f['titulo']?></option>
                                  <?php
                                }
                              ?>
                            </select>
                          </td>
                </tr>
                <tr>
                          <td valign="top"><?=isset($dataGlobal['review_message']) ? $dataGlobal['review_message'] : 'Review Message'?></td>
                          <td valign="top">:</td>
                          <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                          <td colspan="3" valign="top"><textarea name="header_review_message" id="header_review_message" class="form-control"><?=$atabla['en']["header_review_message"]?>
</textarea>
                    <script type="text/javascript">
        var newCKEdit = CKEDITOR.replace('header_review_message',{height:'150',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
        CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
        </script></td>
                </tr>
                <tr>
                          <td valign="top">&nbsp;</td>
                          <td valign="top">&nbsp;</td>
                          <td valign="">&nbsp;</td>
                </tr>
              </table>
                      <table border="0" cellpadding="2" cellspacing="2" width="100%">
                <tr>
                          <td><div style="font-size:14px;font-weight:bold;"><?=isset($dataGlobal['pagination']) ? $dataGlobal['pagination'] : 'Pagination'?></div></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td width="100">&nbsp;</td>
                          <td><div style="font-size:14px;font-weight:bold;"><?=isset($dataGlobal['label_counter']) ? $dataGlobal['label_counter'] : 'Counter'?></div></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                <tr>
                          <td><?=isset($dataGlobal['number_records_page']) ? $dataGlobal['number_records_page'] : 'Number of records for page'?></td>
                          <td>:</td>
                          <td><select name="num_reg_page" id="num_reg_page" class="form-control">
                              <?php
        for($ifor = 10; $ifor <= 100; $ifor= $ifor+10)
        {
      ?>
                              <option value="<?=$ifor?>" <?php if ($atabla['en']["num_reg_page"] == $ifor) {?>selected <?php } ?>>
                            <?=$ifor?>
                            </option>
                              <?php
        }
            ?>
                            </select>
                    rows</td>
                          <td>&nbsp;</td>
                          <td><?=isset($dataGlobal['show_counter']) ? $dataGlobal['show_counter'] : 'Show Counter'?></td>
                          <td>:</td>
                          <td>
                          <div class="radio">
                      <label class="radio inline">
                        <input name="counter_show" type="radio" class="form-control" value="1" <?php if($atabla['en']["counter_show"]=="1"): ?> checked="CHECKED" <?php endif;?>>
                        <span class="lbl"> <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes'?></span> </label>
                      <label class="radio inline">
                        <input name="counter_show" type="radio" class="form-control" value="0" <?php if($atabla['en']["counter_show"]=="0"): ?> checked="CHECKED" <?php endif;?>>
                        <span class="lbl"> <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not'?></span> </label>
                    </div>
                          </td>
                        </tr>
                <tr>
                  <td style="font-size:14px;font-weight:bold;"><?=isset($dataGlobal['label_date']) ? $dataGlobal['label_date'] : 'Date'?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><?=isset($dataGlobal['show_date_time']) ? $dataGlobal['show_date_time'] : 'Show Date / Time'?></td>
                  <td>:</td>
                  <td>
                  <select name="show_date" class="form-control" id="show_date" style="width:150px">
        <option value="1"
         <?php
         if($atabla['en']["show_date"]=="1")
     {
      echo "selected='selected'";
     }
         ?>><?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes'?></option>
        <option value="0" <?php
         if($atabla['en']["show_date"]=="0")
     {
      echo "selected='selected'";
     }
         ?>><?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not'?></option>
        </select>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                          <td colspan="7">&nbsp;</td>
                        </tr>
                <tr>
                          <td colspan="7">&nbsp;</td>
                        </tr>
                <tr>
                          <td colspan="7">&nbsp;</td>
                        </tr>
              </table><br><br>
                    </form>
        </div>
  </div>
</div>           
<?php include(dirname(__FILE__)."/../footer.php");?>