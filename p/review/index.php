<?php
include(dirname(__FILE__)."/../header.php");
//echo $comment_status;
$action = isset($_GET['action']) ? $_GET['action'] : '';
$i = isset($_GET['i']) ? $_GET['i'] : '';
$status = isset($_GET['status']) ? $_GET['status'] : '';
$search  = isset($_GET['search']) ? $_GET['search'] : '';

?>
<style>
	iframe#TB_iframeContent {
    background: #fff;
    border: 1px solid #ddd;
}
button.close {
    background: transparent;
    box-shadow: none;
    border: 0px;
    float: right;
    font-size: 28px;
}
</style> 
<div class="row" id="main-container">
	<div  class="xs-col-12 col-sm-3 " style=" max-width: 250px;">
		<?php include("menu.php");?>
	</div>
	<div class="xs-col-12 col-sm-9"  >
		<br>
		<!-- PAGE CONTENT BEGINS HERE -->
			<div id="tabs">
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tbody>
					<tr>
						<td>
							<?php
								$sqlAll="select * from rating_reviews "; //Devuelve todos los Idiomas
								$consultaAll=ejecutar($sqlAll);
								$numAll=numRows($consultaAll);
								$sqlPen="select * from rating_reviews  where comment_status_id='1'"; //Devuelve todos los Idiomas
								$consultaPen=ejecutar($sqlPen);
								$numPen=numRows($consultaPen);
							 	$sqlApp="select * from rating_reviews  where comment_status_id='2'"; //Devuelve todos los Idiomas
								$consultaApp=ejecutar($sqlApp);
								$numApp=numRows($consultaApp);
							 	$sqlTra="select * from rating_reviews  where comment_status_id='3'"; //Devuelve todos los Idiomas
								$consultaTra=ejecutar($sqlTra);
								$numSpa=numRows($consultaTra);
							 	$sqlSpa="select * from rating_reviews  where comment_status_id='4'"; //Devuelve todos los Idiomas
								$consultaSpa=ejecutar($sqlSpa);
								$numTra=numRows($consultaSpa);
							  	$aimpComm=array();
							 	//$comment_status="";
							 	if(empty($status))
							 	{
								  $aimpComm[0] = 'class="active"';
								}
								if($status=='pending')
								{
							    	$aimpComm[1] = 'class="active"';
								}
								if($status=='approved')
								{
									$aimpComm[2] = 'class="active"';
								}
								if($status=='spam')
								{
									$aimpComm[3] = 'class="active"';
								}
								if($status=='trash')
								{
									$aimpComm[4] = 'class="active"';
								}
							?>                   
							<ul class="nav nav-tabs" role="tablist">
								<li role="All" <?=isset($aimpComm[0])? $aimpComm[0]: ''?>>
							        <a href="<?=$_SERVER['PHP_SELF']?>"  role="All" >All</a>
							    </li>
							    <li role="Pending" <?=isset($aimpComm[1])? $aimpComm[1]: ''?>>
							        <a href="<?=$_SERVER['PHP_SELF']?>?status=pending"  role="Pending" >
							        	Pending  <span class="count">(<?=$numPen?>)</span>
							        </a>
							    </li>
							    <li role="Pending" <?=isset($aimpComm[2])? $aimpComm[2]: ''?>>
							        <a href="<?=$_SERVER['PHP_SELF']?>?status=approved"  role="Approved" >
							        	Approved  <span class="count">(<?=$numApp?>)</span>
							        </a>
							    </li>
							    <li role="Pending" <?=isset($aimpComm[3])? $aimpComm[3]: ''?>>
							        <a href="<?=$_SERVER['PHP_SELF']?>?status=spam"  role="Spam" >
							        	Spam  <span class="count">(<?=$numSpa?>)</span>
							        </a>
							    </li>
							    <li role="Pending" <?=isset($aimpComm[4])? $aimpComm[4]: ''?>>
							        <a href="<?=$_SERVER['PHP_SELF']?>?status=trash"  role="Trash" >
							        	Trash  <span class="count">(<?=$numTra?>)</span>
							        </a>
							    </li>
							</ul>
						</td>
                        <td>
                        	<div>
                        		<form id="form_busqueda" name="form_busqueda" class="estilocampos" method="get" action="index.php">
                        			<table align="right" border="0" cellspacing="2" cellpadding="2">
                        				<tr>
                        					<td>
                        						<span class="">
                        							<span class="input-icon">
                        								<i class="icon-search nav-search-icon"></i>
                        								<input type="text" name="search" id="search" value="<?=$search?>" placeholder="<?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search'?>" autocomplete="off" class="input-block-level">
                        							</span>
                        						</span>
                        						<button type="submit" class="btn btn-primary"><?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search'?></button>
                        						<a href="<?=$_SERVER["PHP_SELF"]?> " class="btn btn-default"> <?=isset($dataGlobal['label_all']) ? $dataGlobal['label_all'] : 'All'?> </a>
                        					</td>
                        				</tr>
                        			</table>
                                </form>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php 
	            if(isset($_SESSION["error"]))
	            {?>
	            	<div align="center"  class="alert alert-error"><?=$_SESSION["error"]?></div>
	            	<?php unset($_SESSION['error']);
	            } 
            ?>
            <div class="tab-content">
            	<br>
	            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-hover">
	            	<thead>
	            		<tr>
							<th ><div align="left"><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User'?></div></th>
							<th ><div align="left"><?=isset($dataGlobal['review']) ? $dataGlobal['review'] : 'Review'?></div></th>
							<th ><div align="center"><?=isset($dataGlobal['label_star']) ? $dataGlobal['label_star'] : 'Star'?></div></th>
						</tr>
					</thead>     
					<?php
						$wfiltro="and (comment_status_id='1' OR comment_status_id='2')";
						if($status=='pending'){ $wfiltro=" and comment_status_id='1' "; }
						if($status=='approved'){ $wfiltro=" and comment_status_id='2' "; }
						if($status=='spam'){$wfiltro=" and comment_status_id='3' ";}
						if($status=='trash'){$wfiltro=" and comment_status_id='4' ";}
						if(!empty($p)){$wfiltro = $wfiltro." and article_id='$p' ";}
						$t1="rating_reviews";
						$t3="rating_reviews_status";
						$sql="select 
						$t1.*,
						$t3.idx as comm_stat_idx,
						$t3.nombre as comm_stat_nombre
						from
						$t1,$t3
						where
						$t1.comment_status_id = $t3.idx and  
						($t1.descrip like '%$search%')
						$wfiltro
						order by $t1.fecreg desc ";
						$consulta=ejecutar($sql);
						$num_reg=numRows($consulta);
						$pages = new Paginator();
						$pages->items_total = $num_reg;
						$pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
						//$pages->paginate($num_reg_page_cms);	//cuantos debe de mostrar por pagina
						$pages->paginate(40);
						$item = 0;
						//echo $sql.$pages->limit;
						$consulta=ejecutar($sql.$pages->limit);
						$num_reg=numRows($consulta);
						if($num_reg == 0)
						{ ?> 
							<td colspan="3"  align="center" style="color:#F00">
								<?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record'?>
							</td>
							<?php
						}
						else
						{ 
							while($fila=fetchArray($consulta))
							{    
								$item++;
								?>
								<tr>
									<td valign="top">
										<div style="font-weight:bold">
											<?=isset($dataGlobal['review_by']) ? $dataGlobal['review_by'] : 'Review by'?>
							    			<span >
												<?= $fila["nombres"]?> <?=$fila["apellidos"]?>.
											</span>in <span style="color:#5486a3"><?= $fila["city"]?>, <?= strtoupper($fila["state"])?></span>
										</div>
							          	<div style="color:#0353a4" align="left"><?=$fila["username"]?></div>
							        </td>
							        <td valign="top">
							        	<div>
							        		<?=isset($dataGlobal['submitted_on']) ? $dataGlobal['submitted_on'] : 'Submitted on'?> <span style="color:#0353a4"><?= strftime('%b %d, %Y', strtotime($fila['fecreg'])) ?> at <?= strftime('%I:%M:%S %p', strtotime($fila['fecreg'])) ?> </span>
							            	<?php 
								            	if($fila['comment_parent'] <> 0)
								            	{ ?>
								            		| <span class="text_reply"><?=isset($dataGlobal['in_reply_to']) ? $dataGlobal['in_reply_to'] : 'In reply to'?></span>  
									            	<?php
														$t1="comments";
														$t2="usuarios";
														$sql_1="select 
														$t1.*,
														$t2.idx as usu_idx, 
														$t2.username as usu_username,
														$t2.nombres as usu_nombres,
														$t2.apellidos as usu_apellidos,
														$t2.userlevel as usu_userlevel
														from
														$t1,$t2
														where
														$t1.idx = '".$fila['comment_parent']."' and
														$t1.user_id = $t2.idx
											 			";
														$consulta_1=ejecutar($sql_1);
														$num_reg=numRows($consulta_1);
														if($fila_1=fetchArray($consulta_1))
														{ ?>
															<span style="color:#0353a4"><?=$fila_1['usu_nombres'].' '.$fila_1['usu_apellidos']?>.</span> <?php 
														}  					
												} 
											?>
	           							</div>
	           							<div><?=nl2br($fila['descrip'])?></div>
	           							<?php 
	           								$wstatus=''; $wp='';
	           								if($fila['comm_stat_idx']==1 or $fila['comm_stat_idx']==2)
	           								{
	           									if(!empty($status)) { $wstatus="&status=".$status; }
	           									if(!empty($p)) { $wp="&p=".$p; }
	           								
			           							?>
									          	<div class="row-actions">
										            <?php if($fila['comm_stat_idx']==2){?>
										            <span class="unapprove"><a href="acciones.php?i=<?=$fila['idx']?>&action=unapprove<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_unapprove']) ? $dataGlobal['label_unapprove'] : 'Unapprove'?></a></span>
										            <?php }?>
										            <?php if($fila['comm_stat_idx']==1){?>
										            <span class="approve">	<a href="acciones.php?i=<?=$fila['idx']?>&action=approve<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_approve']) ? $dataGlobal['label_approve'] : 'Approve'?></a></span>
										            <?php }?>
										            <?php if($fila['comment_parent'] == 0){?>
										            <?php }?>
										            <span class="edit">  | 	<a href="edit.php?i=<?=$fila['idx']?>"><?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Edit'?></a></span>
										            <span class="spam">  | 	<a href="acciones.php?i=<?=$fila['idx']?>&action=spam<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_spam']) ? $dataGlobal['label_spam'] : 'Spam'?></a></span>
										            <span class="trash"> | 	<a href="acciones.php?i=<?=$fila['idx']?>&action=trash<?=$wstatus?><?=$wp?>"><?=isset($dataGlobal['label_trash']) ? $dataGlobal['label_trash'] : 'Trash'?></a></span>
									            </div>
	          									<?php 
	          								}
	          							?>
	          							<?php 
	          								if($fila['comm_stat_idx']==3){?>
									        	<div class="row-actions">
										            <span class="edit"><a href="acciones.php?i=<?=$fila['idx']?>&action=notspam"><?=isset($dataGlobal['label_no_spam']) ? $dataGlobal['label_no_spam'] : 'Not Spam'?></a></span>
										            <span class="spam">|</span>
										            <span class="spam"><a href="#" class="bmd-modalButton" data-toggle="modal" data-bmdSrc="delete.php?i=<?=$fila["idx"]?>" data-bmdWidth="640px" data-bmdHeight="380px" data-target="#iframeModal"  data-Scrolling="no"  data-bmdVideoFullscreen="true" ><?=isset($dataGlobal['del_permanent']) ? $dataGlobal['del_permanent'] : 'Delete Permanently'?></a></span>
									            </div>
									            <?php 
									        } 
									    ?>
									    <?php 
									    	if($fila['comm_stat_idx']==4)
									    	{?>
									    		<div class="row-actions">
										            <span class="edit"><a href="acciones.php?i=<?=$fila['idx']?>&action=restore"><?=isset($dataGlobal['label_restore']) ? $dataGlobal['label_restore'] : 'Restore'?></a></span>
										            <span class="spam">|</span>
										            <span class="spam"><a href="delete.php?i=<?=$fila["idx"]?>" class="bmd-modalButton" data-toggle="modal" data-bmdSrc="delete.php?i=<?=$fila["idx"]?>" data-bmdWidth="640px" data-bmdHeight="380px" data-target="#iframeModal"  data-Scrolling="no"  data-bmdVideoFullscreen="true"><?=isset($dataGlobal['del_permanent']) ? $dataGlobal['del_permanent'] : 'Delete Permanently'?></a></span>
										        </div>
										        <?php 
										    }
										?>
										<div>
											<span class="countLike"><?=$fila['count_like']?> Yes</span>
											<span class="countUnlike"><?=$fila['count_unlike']?> No</span>
										</div>
							          	<div style=" margin: 10px;">
											<?php
											$sql_idx_gallery = "SELECT idx_gallery FROM rating_reviews WHERE idx='".$fila['idx']."' ";
											$query_idx_gallery = ejecutar($sql_idx_gallery);
											$totalRows_u1 = numRows($query_idx_gallery );
											if($totalRows_u1 > 0){
											$idx_gallery = fetchAssoc($query_idx_gallery);
											$gallery=$idx_gallery['idx_gallery'];
												$select_sql="SELECT a.nomcar, b.imagen FROM galerias_lista AS a INNER JOIN detgaleriaslista AS b ON a.idx=b.idgalerias WHERE a.idx='".$gallery."'  order by a.idx ASC limit 0, 5";
												$query_select=ejecutar($select_sql);
												if(numRows($query_select) > 0)
												{
													$imagenes='';
													while($img=fetchAssoc($query_select))
													{ 
														$ruta=dirname(__FILE__)."/../imgcms/galeria/".$img['nomcar']."/small/".$img['imagen'];  
														$imagenes.='<img src="'.$ruta.'" style="width:50px;margin-right:5px;" />';
													}
													echo $imagenes;
												}
											}
											?>          
							            </div>
	        						</td>
	        						<td valign="top">
	        							<div align="center">
	        								<span class="badge badge-warning"><?php echo number_format($fila["stars"],1);?></span>
	        							</div>
	        						</td>
	        					</tr>
	        					<?php
	        				}
	        			}
	        		?>
	        		<tfoot>
	        			<tr>
	        				<th ><div align="left"><?=isset($dataGlobal['label_user']) ? $dataGlobal['label_user'] : 'User'?></div></th>
							<th ><div align="left"><?=isset($dataGlobal['review']) ? $dataGlobal['review'] : 'Review'?></div></th>
							<th ><div align="center"><?=isset($dataGlobal['label_star']) ? $dataGlobal['label_star'] : 'Star'?></div></th>
	        			</tr>
	        		</tfoot>
	        	</table>
	        </div>
        	<?php	
	        	if ($pages->num_pages > 1)	
	        	{ ?>
	        		<div align="center" style="padding:8px;">
	        			<?php echo $pages->display_pages(); ?> 
	        			<?php //echo $pages->display_jump_menu()?> 
	        			<?php //echo $pages->display_items_per_page()?> 
	        		</div> 
	        		<?php	
	        	}	
        	?>
        </div>
	</div>
</div>           
<script>
	$(document).ready(function() {
		document.querySelector('aside').style.height=document.querySelector('.xs-col-12.col-sm-9').offsetHeight+'px';
	});
</script>
<?php include(dirname(__FILE__)."/../footer.php");?>