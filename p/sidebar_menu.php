            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                    <li >
                        <a href="<?=SERVER_P?>home.php">
                            <span class="fa-stack fa-lg "><i class="fa fa-lg fa-dashboard "></i></span><?=isset($dataGlobal['dashboard']) ? $dataGlobal['dashboard'] : 'Dashboard'?>
                        </a>
                    </li>
                    <?php if (isset($atabla_perm_menu['9008']['idxpermisocategory']) && $atabla_perm_menu['9008']['idxpermisocategory'] > 0) { ?>        
                        <li ><a href="<?=SERVER_P?>form/index.php">
                            <span class="fa-stack fa-lg "><i class="fa fa-dot-circle-o"></i></span><?=isset($dataGlobal['leads']) ? $dataGlobal['leads'] : 'Leads'?>
                        </a></li>
                    <?php } ?>
                    <?php if (isset($atabla_perm_menu['17026']['idxpermisocategory']) && $atabla_perm_menu['17026']['idxpermisocategory'] > 0) { ?>   
                        <li ><a href="<?=SERVER_P?>review/index.php">
                            <span class="fa-stack fa-lg "><i class="fa fa-lg fa-fw fa-comments-o"></i></span><?=isset($dataGlobal['review']) ? $dataGlobal['review'] : 'Review'?>
                        </a></li>
                    <?php } ?>
                    <?php if (isset($atabla_perm_menu['9005']['idxpermisocategory']) && $atabla_perm_menu['9005']['idxpermisocategory'] > 0) { ?>
                        <li ><a href="<?=SERVER_P?>gallery/index.php">
                            <span class="fa-stack fa-lg "><i class="glyphicon glyphicon-picture "></i></span><?=isset($dataGlobal['gallery']) ? $dataGlobal['gallery'] : 'Gallery'?>
                        </a></li>
                    <?php } ?>                    
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->
