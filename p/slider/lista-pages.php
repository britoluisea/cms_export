<?php
session_start();
include dirname(__FILE__)."/../system/languages/languages.php";
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
}
else
{
	include "../../cn/cnx.php";
	$r=array();
    $r['result']=false;
	$sql="select a.*, b.titulo from bannershome as a inner join informativo as b on a.page=b.id where a.page!=0 order by a.item asc ";	
	$consulta=ejecutar($sql);
	$num_reg=numRows($consulta);
	if($num_reg == 0)
	{
		$r['lista']= isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record';
	}
	else
	{
    	$r['result']=true;
    	$cont=0;
		while($fila=fetchArray($consulta))
		{
			$r['lista'][$cont]['idx']=$fila['idx'];
			$r['lista'][$cont]['item']=$fila['item'];
			$r['lista'][$cont]['nombre']=$fila['nombre'];
			$r['lista'][$cont]['nombre_align']=$fila['nombre_align'];
			$r['lista'][$cont]['descrip']=stripslashes($fila['descrip']);;
			if(!file_exists('../../imgcms/headers/small/'.$fila['imagen']))
			{ 
				$r['lista'][$cont]['imagen']=''; 
				$r['lista'][$cont]['imgsize']=0;
			}
			else
			{
				$r['lista'][$cont]['imagen']=$fila['imagen'];
				$r['lista'][$cont]['imgsize']=filesize('../../imgcms/headers/slider/'.$fila['imagen']);
			}
			$r['lista'][$cont]['slug']=$fila['slug'];
			$r['lista'][$cont]['activo']=$fila['activo'];
			$r['lista'][$cont]['titulo']=$fila['titulo'];
			$cont++;
		}
	}
	echo json_encode($r);
}
?>