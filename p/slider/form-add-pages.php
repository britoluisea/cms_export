<?php include(dirname(__FILE__)."/../header.php"); ?>
<style type="text/css">
  .form-control{
    margin-bottom: 10px;
  }
  .dropzone { min-height: 280px !important; }
  .dropzone .dz-message {
    margin: 100px 0 100px 5% !important;
    text-align: center !important;
    width: 100% !important;
}
</style>
<div class="">
  <div class="container" role="document">
    <div class="modal-content" style="box-shadow: none; border:0px;">
      <div class="modal-header">
        <h2 class="h2 modal-title" id="formAddBannerLabel"></h2>
      </div>
      <div class="modal-body"> 
        <form>        
          <input type="hidden" name="ope" id="ope" value="banner-home"/>
          <input type="hidden" name="opc" id="opc"  value="1"/>
          <input type="hidden" name="i" id="i"  />
          <table align="right" width="200px" border="0" cellpadding="0" cellspacing="0" style=" margin-bottom: 10px;">
            <tbody>
              <tr>
                <td align="right" >
                  <button  name="save" type="submit" class="save btn btn-primary" >
                    <i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ?  $dataGlobal['btn_save'] : 'Save'?>
                  </button>
                  <a href="#" class="btn btn-default cancelar"><i class="fa fa-chevron-left"></i> <?=isset($dataGlobal['btn_cancel']) ?  $dataGlobal['btn_cancel'] : 'Cancel'?></a>
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <table align="center" width="100%" border="0"  cellpadding="2" cellspacing="8" >
            <tr >
              <td  valign="top" width="120px" class="h4">
                <?=isset($dataGlobal['label_title']) ?  $dataGlobal['label_title'] : 'Title'?>:
              </td>
              <td>
                <input name="nombre" type="text" id="nombre" class="form-control limp"  />
              </td>
              <td valign="top" width="30px">
                <select name="nombre_align" id="nombre_align" class="form-control   lim_select" style=" width: 90px; margin-left: 10px;">
                  <option value="center"><?=isset($dataGlobal['label_center']) ?  $dataGlobal['label_center'] : 'Center'?></option>
                  <option value="right"><?=isset($dataGlobal['label_right']) ?  $dataGlobal['label_right'] : 'Right'?></option>
                  <option value="left"><?=isset($dataGlobal['label_left']) ?  $dataGlobal['label_left'] : 'Left'?></option>
                </select>
              </td>
              <td valign="top" width="30px">
                <select name="mostrar_nombre" id="mostrar_nombre" class="form-control   lim_select" style=" width: 90px; margin-left: 10px;">
                  <option value="1"><?=isset($dataGlobal['label_show']) ?  $dataGlobal['label_show'] : 'Show'?></option>
                  <option value="0"><?=isset($dataGlobal['label_hide']) ?  $dataGlobal['label_hide'] : 'Hide'?></option>
                </select>
              </td>
            </tr>  
            <tr id="listPages">
              <td valign="top" width="120px" class="h4"><?=isset($dataGlobal['assign_page']) ?  $dataGlobal['assign_page'] : 'Assign page'?>:</td>
              <td colspan="3">
                <select name="page" id="page"  class="form-control  lim_select" style=" ">
                  <?php 
                    $sql="SELECT id, titulo FROM informativo WHERE activo=1 AND ( usamarketing = 5 OR usamarketing = 1 ) AND titulo != 'home' ORDER BY titulo ASC";
                    $q = ejecutar($sql);
                    while ($f = fetchArray($q)) {
                      echo '<option value="'.$f['id'].'">'.$f['titulo'].'</option>';                        
                    }
                  ?>
                </select>
              </td>
            </tr>       
            <tr>
              <td valign="top" width="120px" class="h4"><?=isset($dataGlobal['box_content']) ?  $dataGlobal['box_content'] : 'Box content'?>:</td>
              <td valign="top" colspan="2">

                <script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
                <script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
                <script type="text/javascript">
                  $(document).ready(function() {
                    var window_height = 250; 
                    var newCKEdit = CKEDITOR.replace('descrip',{height:window_height,allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
                    CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
                  });
                  </script>
                <div class="col-sm-12">
                  <?php 
                    include dirname(__FILE__). "/../system/btn-icons.php";
                    addBtnIcons($idEditor='CKEDITOR.instances.descrip');
                  ?>
                </div>
                <textarea name="descrip" id="descrip" class="limp"></textarea>
              </td>
              <td valign="top" width="30px">
                <select name="mostrar_descrip" id="mostrar_descrip" class="form-control   lim_select" style=" width: 90px; margin-left: 10px;">
                  <option value="1"><?=isset($dataGlobal['label_show']) ?  $dataGlobal['label_show'] : 'Show'?></option>
                  <option value="0" ><?=isset($dataGlobal['label_hide']) ?  $dataGlobal['label_hide'] : 'Hide'?></option>
                </select>
              </td>
            </tr>
              <tr>
                <td colspan="4" style="margin-top: 10px;">&nbsp;</td>
              </tr>
            <tr>
              <td valign="top" width="120px" class="h4">
                <?=isset($dataGlobal['label_status']) ?  $dataGlobal['label_status'] : 'Status'?>:
              </td>
              <td colspan="2">
                <select name="activo" id="activo" class="form-control lim_select" style="">
                  <option value="1"><?=isset($dataglobal['label_active']) ?  $dataGlobal['label_active'] : 'Active'?></option>
                  <option value="0"><?=isset($dataglobal['label_inactive']) ?  $dataGlobal['label_inactive'] : 'Inactive'?></option>
                </select>
              </td>
              <td valign="top" width="30px">&nbsp;</td>
            </tr>  
              <?php 
                $sql = "SELECT weight_inner FROM bannershome_options WHERE idx='1' ";
                $pesoMax = fetchArray(ejecutar($sql));
                $peso = ($pesoMax['weight_inner']*1024)*1024;
              ?>
              <input type="hidden" id="pesoMax" value="<?=$peso?>">
              <input type="hidden" id="formAdd" value="formPage">
            </tbody>
          </table>
        </form>
        <br>
          <table align="center" width="90%" border="0" cellpadding="0" cellspacing="0" style=" margin-top: 10px; margin-bottom: 50px;">
            <tbody>
              <tr>
                <td valign="top" width="120px" class="h4"><?=isset($dataglobal['label_background']) ?  $dataGlobal['label_background'] : 'Inactive'?>: <span class="asterisco">*</span><br />
                  <i id="medidas"></i>
                </td>
                <td colspan="3" style="position:relative;">
                  <div id="formDropZone" class="bg-dropzone" style="margin-bottom: 10px;"></div>
                  <input type="hidden" id="imagen" class="limp">
                </td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
              </tr>
            </tbody>
          </table>
          <br>
      </div>
    </div>
  </div>
</div>
<link href="<?=CSS_P?>dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?=JS_P?>dropzone.js"></script> 
<?php include "print_msj.php"; ?>
<script src="js/dropzone-banner2.js"></script> 
<?php $id = isset($_GET["id"]) ? $_GET["id"] : 0; ?>
<script>
  Dropzone.autoDiscover = false;
$(document).ready(function()
{ 
  $('.save').click(function(event) {
    event.preventDefault();
    save();
  });
  $('.cancelar').click(function(event) {
    event.preventDefault();
    window.location.href= 'index.php';
  });
  var idx = parseInt('<?=$id?>');
  if(idx > 0 ){ 
    $('#formAddBannerLabel').text(''+print_msj("msj_slider_js25")+'');
    //$('#formAddBanner').modal({show:true});
    $("#formDropZone").html("<form id='dZUpload' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");    
    var myAwesomeDropzone = cargarModal();
    myAwesomeDropzone.maxThumbnailFilesize=$("#pesoMax").val();
    myAwesomeDropzone.maxFileSize=$("#pesoMax").val();
    myAwesomeDropzone.params.typeUpload='innerHeader';
    var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
    myDropzone.on("complete", function(file,response) {
      $('.dz-remove').html('<i class="fa fa-trash"></i> '+print_msj("btn_remove")+'');
    });
    traer_item(idx);
  }
  else
  {
    crearInner();
    $('#opc').val('1');
  }
  vaciar_tmp();  
});
</script> 
<?php include(dirname(__FILE__)."/../footer.php"); ?>