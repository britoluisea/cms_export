<?php 
session_start();
include "../../cn/cnx.php";
include dirname(__FILE__)."/../system/languages/languages.php";
$r=array();
$r['result']=false;
$r['msj']=false;
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    $r['msj']='error, session expired';
}
else
{
	@ini_set("memory_limit", "3000M");
	@ini_set( 'upload_max_size' , '3000M' );
	@ini_set( 'post_max_size', '3000M');
	@ini_set( 'max_execution_time', '30000' );
    set_time_limit(900); // 15 minutes
   	$sql_1="select * from bannershome_options where idx='1' "; //Devuelve todos los Idiomas
   	$consulta_1=ejecutar($sql_1);
   	$atabla=array();
   	if($fila_1=fetchAssoc($consulta_1))
   	{ $atabla=$fila_1; }
	$Folder = "../../imgcms/headers/video/";// nueva ruta
	$type='';
	$extension='';
	$r['file']=$_FILES;
	if( @is_uploaded_file($_FILES['video']['tmp_name']) ) 
    {
		$tempFile = $_FILES['video']['tmp_name'];
        // get uploaded file extension 
        $ext = strtolower(pathinfo($_FILES['video']['name'], PATHINFO_EXTENSION));	  
        $nombre_video = "video";
	    $targetFile =  $Folder .  basename($nombre_video.'.'.$ext);
	    //$size = $atabla['weight'];
	    $size = 52428800; //50 MB
	    $size_file= $_FILES['video']['size']/1024/1024; // bytes -> Kb -> Mb
		if($size_file > $size)
		{
			$r['msj']=isset($dataGlobal['error_max_file']) ? $dataGlobal['error_max_file'] : 'Error max file size';
			$r['msj'].=' ('.($size).' MB)';
		}
		elseif($_FILES['video']['type'] !='video/mp4')
		{
			$r['msj']=isset($dataGlobal['error_video_format']) ? $dataGlobal['error_video_format'] :'format not allowed, only mp4';	
		}
		else
		{		    
		    if(!move_uploaded_file($tempFile,$targetFile))
		    {
		    	$r['msj']=isset($dataGlobal['labe_error']) ? $dataGlobal['labe_error'] :'error';
		    }
		    else
		    {       	
	    		$r['result']=true;
		    }
		}
	}else{$r['msj']=isset($dataGlobal['file_not_selected']) ? $dataGlobal['file_not_selected'] :'file not selected';}
}
	echo json_encode($r);
?>