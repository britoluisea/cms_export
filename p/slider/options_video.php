<?php
  $sql="SELECT * FROM bannershome_video where id = 1";
  $v = fetchArray(ejecutar($sql));
  $width_poster=$v['width_poster'];
  $height_poster=$v['height_poster'];
  $quality_poster=$v['quality_poster'];
  $weight_poster=$v['weight_poster'];
  $weight_video=$v['weight_video'];
  $overlay_video=$v['overlay'];
  $template_video=$v['template_video'];
?> 
<div class="col-sm-4">
  <table width="100%" border="0" cellpadding="2" cellspacing="2" align="left">
    <tr>
      <td colspan="3" align="left">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" >
        <div  style="font-size:20px; color:#2679b5">
          <strong><?=isset($dataGlobal['video_header_options']) ? $dataGlobal['video_header_options'] : 'Options Video'?></strong>
        </div>
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td colspan="3" align="left">
        <button id="save_options_video" type="button" class="btn btn-primary"  >
          <i class="fa fa-save "></i> <?=isset($dataGlobal['video_header_save']) ? $dataGlobal['video_header_save'] : 'Save Video'?>
        </button>
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td  width="100" ><?=isset($dataGlobal['label_width_poster']) ? $dataGlobal['label_width_poster'] : 'Poster Width'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="width_poster-value"></div>
        <input type="range" id="width_poster" value="<?=$width_poster?>" min="500" max="2500"  step="10" class="slider-range width_poster">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td ><?=isset($dataGlobal['label_height_poster']) ? $dataGlobal['label_height_poster'] : 'Poster Height'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="height_poster-value"></div>
        <input type="range" id="height_poster" value="<?=$height_poster?>" min="100" max="1650"  step="10" class="slider-range height_poster">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td ><?=isset($dataGlobal['label_quality_poster']) ? $dataGlobal['label_quality_poster'] : 'Poster Quality'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="quality_poster-value"></div>
        <input type="range" id="quality_poster" value="<?=$quality_poster?>" min="5" max="100"  step="5" class="slider-range quality_poster">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td ><?=isset($dataGlobal['label_max_file_poster']) ? $dataGlobal['label_max_file_poster'] : 'Max file Poster'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="weight_poster-value"></div>
        <input type="range" id="weight_poster" value="<?=$weight_poster?>" min="1" max="20"  step="1" class="slider-range weight_poster">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td ><?=isset($dataGlobal['label_max_file_video']) ? $dataGlobal['label_max_file_video'] : 'Max file Video'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="weight_video-value"></div>
        <input type="range" id="weight_video" value="<?=$weight_video?>" min="10" max="50"  step="5" class="slider-range weight_video">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td ><?=isset($dataGlobal['label_overlay']) ? $dataGlobal['label_overlay'] : 'Overlay'?>:<span class="asterisco">*</span></td>
      <td>&nbsp;</td>
      <td align="center">
        <div class="value-range" id="overlay_video-value"></div>
        <input type="range" id="overlay_video" value="<?=$overlay_video?>" min="0" max="1" value="50" step="0.10" class="slider-range overlay">
      </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
      <td><div align="left"><?=isset($dataGlobal['label_layout']) ? $dataGlobal['label_layout'] : 'Layout'?>:</div></td>
      <td>&nbsp;</td>
      <td valign="top">
        <select name="template_video" id="template_video" class="form-control">
          <?php
            $dir =  dirname(__FILE__)."/../../".$_SESSION['THEME']."video/"; 
            chdir ($dir);  ///vaciar carpeta de video
            $dh=opendir('.');
            while ($file = readdir ($dh)) {
                  if ($file != "." && $file != "..") { ?>
                  <option value="<?=$file?>" <?php if($template_video==$file) { echo "selected='selected'"; } ?>><?=strtoupper(substr($file, 0,-4))?></option>
                  <?php }
            }
            closedir($dh);
          ?>
        </select>
      </td>
    </tr>
    <tr><td colspan="3" align="left">&nbsp;</td></tr>
	</table>
</div>