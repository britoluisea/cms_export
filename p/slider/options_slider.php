  <div class="col-sm-4">
    <table width="100%" border="0" cellpadding="2" cellspacing="2" align="left">
      <tr>
        <td colspan="3" align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3" >
          <div  style="font-size:20px; color:#2679b5">
            <strong><?=isset($dataGlobal['slider_header_options']) ? $dataGlobal['slider_header_options'] : 'Options Slider'?></strong>
          </div>
        </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>  
        <tr>
          <td colspan="3" align="left">
            <button id="save_options_slider" type="button" class="btn btn-primary"  >
              <i class="fa fa-save "></i> <?=isset($dataGlobal['slider_header_save']) ? $dataGlobal['slider_header_save'] : 'Save Slider'?>
            </button>
          </td>
        </tr>      
        <tr><td colspan="3" >&nbsp;</td></tr>        
        <tr >
          <td width="100"><div align="left"><?=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <div class="value-range" id="width_slider-value"></div>
            <input type="range" id="width_slider" value="<?=$width_slider?>" min="500" max="2500"  step="10" class="slider-range width_slider">
          </td>
        </tr> 
        <tr><td colspan="3" >&nbsp;</td></tr>         
        <tr id="alto">
          <td><div align="left"><?=isset($dataGlobal['label_heigth']) ? $dataGlobal['label_heigth'] : 'Height'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <div class="value-range" id="height_slider-value"></div>
            <input type="range" id="height_slider" value="<?=$height_slider?>" min="100" max="1650"  step="10" class="slider-range height_slider">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_quality']) ? $dataGlobal['label_quality'] : 'Quality'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <div class="value-range" id="quality_slider-value"></div>
            <input type="range" id="quality_slider" value="<?=$quality_slider?>" min="5" max="100"  step="5" class="slider-range quality_slider">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_max_file_img']) ? $dataGlobal['label_max_file_img'] : 'Max file Img'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">            
            <div class="value-range" id="weight_slider-value"></div>
            <input type="range" id="weight_slider" value="<?=$weight_slider?>" min="1" max="20"  step="1" class="slider-range weight_slider">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_speed_time']) ? $dataGlobal['label_speed_time'] : 'Speed Time'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">     
            <div class="value-range" id="speed-value"></div>
            <input type="range" id="speed" value="<?=$speed?>" min="1" max="10"  step="1" class="slider-range speed">
          </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
          <td ><?=isset($dataGlobal['label_overlay']) ? $dataGlobal['label_overlay'] : 'Overlay'?>:<span class="asterisco">*</span></td>
          <td>&nbsp;</td>
          <td align="center">
            <div class="value-range" id="overlay_slider-value"></div>
            <input type="range" id="overlay_slider" value="<?=$overlay_slider?>" min="0" max="1" value="50" step="0.10" class="slider-range overlay">
          </td>
        </tr>
        <tr ><td colspan="3" >&nbsp;</td></tr>
        <tr >
          <td><div align="left"><?=isset($dataGlobal['label_direction']) ? $dataGlobal['label_direction'] : 'Direction'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">     
            <select name="direction" id="direction" class="form-control text-uppercase">
              <option value="horizontal" <?php if($direction=="horizontal") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_horizontal']) ? $dataGlobal['label_horizontal'] : 'Horizontal'?></option>
              <option value="vertical" <?php if($direction=="vertical") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_vertical']) ? $dataGlobal['label_vertical'] : 'Vertical'?></option>
            </select>
          </td>
        </tr> 
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['show_nav']) ? $dataGlobal['show_nav'] : 'Show Nav'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <select name="arrows" id="arrows" class="form-control text-uppercase">
              <option value="1" <?php if($arrows=="1") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes'?></option>
              <option value="0" <?php if($arrows=="0") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not'?></option>
            </select>
          </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['show_arrows']) ? $dataGlobal['show_arrows'] : 'Show Arrows'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <select name="ico_nav" id="ico_nav" class="form-control  text-uppercase">
              <option value="1" <?php if($ico_nav=="1") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes'?></option>
              <option value="0" <?php if($ico_nav=="0") { echo "selected='selected'"; } ?>><?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not'?></option>
            </select>
          </td>
        </tr>
        <tr><td colspan="3" align="left">&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_effect']) ? $dataGlobal['label_effect'] : 'Effect'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <select name="effect" id="effect" class="form-control text-uppercase">
              <option value="fade" <?php if($effect=="fade") { echo "selected='selected'";}?> ><?=isset($dataGlobal['label_effect_fade']) ? $dataGlobal['label_effect_fade'] : 'Fade'?></option>
              <option value="slide" <?php if($effect=="slide") { echo "selected='selected'";}?> ><?=isset($dataGlobal['label_effect_slide']) ? $dataGlobal['label_effect_slide'] : 'Slide'?></option>
              <option value="cube" <?php if($effect=="cube") { echo "selected='selected'";}?> ><?=isset($dataGlobal['label_effect_cube']) ? $dataGlobal['label_effect_cube'] : 'Cube'?></option>
              <option value="coverflow" <?php if($effect=="coverflow") { echo "selected='selected'";}?> ><?=isset($dataGlobal['label_effect_cover']) ? $dataGlobal['label_effect_cover'] : 'Coverflow'?></option>
              <option value="flip" <?php if($effect=="flip") { echo "selected='selected'";}?> ><?=isset($dataGlobal['label_effect_flip']) ? $dataGlobal['label_effect_flip'] : 'Flip'?></option>
            </select>
          </td>
        </tr>
        <tr><td colspan="3" align="left">&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_layout']) ? $dataGlobal['label_layout'] : 'Layout'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <select name="template_slider" id="template_slider" class="form-control">
              <?php
                $dir =  dirname(__FILE__)."/../../".$_SESSION['THEME']."slider/"; 
                chdir ($dir);  ///vaciar carpeta de video
                $dh=opendir('.');
                while ($file = readdir ($dh)) {
                      if ($file != "." && $file != "..") { ?>
                      <option value="<?=$file?>" <?php if($template_slider==$file) { echo "selected='selected'"; } ?>><?=strtoupper(substr($file, 0,-4))?></option>
                      <?php }
                }
                closedir($dh);
              ?>
            </select>
          </td>
        </tr>
        <tr><td colspan="3" align="left">&nbsp;</td></tr>
    </table>
  </div>
