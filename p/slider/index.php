<?php include(dirname(__FILE__)."/../header.php");?>
<div class="container" >
	<div id="tabs"><br><br>
		<!-- Nav tabs -->
		<?php include("menu.php");?>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="Options" class="tab-pane" id="tabs-3" style="padding-top: 20px;">
				<?php include("options.php"); ?>			
			</div>
			<div role="Bannerl" class="tab-pane active" id="tabs-1" style="padding-top: 20px;">				
				<?php include("tabla.php"); ?>
			</div>
			<div role="Bannerl" class="tab-pane" id="tabs-2" style="padding-top: 20px;">				
				<?php include("tabla-pages.php"); ?>
			</div>
			<div role="Code" class="tab-pane" id="tabs-5" style="padding-top: 20px;">		
				<?php include("video.php"); ?>					
			</div>
		</div>
	</div>
</div>
<link href="<?=CSS_P?>dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?=JS_P?>dropzone.js"></script> 
<?php include "print_msj.php"; ?>
<script src="js/dropzone-banner2.js"></script> 
<script>
Dropzone.autoDiscover = false;
$(document).ready(function()
{ 
  	subirPoster();
  	getList();
  	getListPages();
  	vaciar_tmp();
  	$('#save_options_slider').click(function(event) {
    	event.preventDefault();
    	save_options_slider();
	}); 
  	$('#save_options_video').click(function(event) {
    	event.preventDefault();
    	save_options_video();
	}); 
  	$('#save_options_inner').click(function(event) {
    	event.preventDefault();
    	save_options_inner();
	});  
   	window.closeModalVideo = function(){ 
 		$('#iframeModal').modal('hide'); 
 	};  
	window.AlertVideo = function(){ 
 		alerValid('Success', 'proceso exitoso', 1000);
 	};  
	window.showVideo = function(){ 
 		$("#showVideo").show('slow');
 		$("#video").val('video.mp4');
 	};
	window.hideVideo = function(){ 
 		$("#showVideo").hide('slow'); 		
 		$("#video").val('');
 	};
 	$("#guardarVideo").click(function(event) {
 		event.preventDefault();
 		guardarVideo();
 	});
 	<?php if($poster!=''){?>
 		  	$("#dZUpload").css('background-image', 'url('+server+'imgcms/headers/slider/<?=$poster?>)');
          	$("#dZUpload").css('background-size', 'cover');
          	$("#dZUpload").css('background-position', 'center');
          	$("#dZUpload").css('background-repeat', 'no-repeat');
            $("#dZUpload").css('color', '#fff');             
            var message ='<span class="text-center">';
            message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
            message+='<span class="font-lg"><i class="fa fa-caret-right text-dafault"></i> ';
            message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
            message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
            $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
            $("#dZUpload .dz-message").css('text-shadow', '2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000');
            $(".dropzone .dz-message").css({
                  'padding-top': '50px',
                  'padding-bottom': '50px'
                });                       
    <?php } ?>
});
</script> 
<?php include(dirname(__FILE__)."/../footer.php"); ?>