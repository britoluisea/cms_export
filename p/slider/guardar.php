<?php
session_start();
include dirname(__FILE__)."/../system/languages/languages.php";
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
}
else
{
    include "../../cn/cnx.php";
    include "../system/functions.php";
    $datosForm = isset($_POST['datosForm']) ? $_POST['datosForm'] : ''; 
    $ope = isset($datosForm['ope']) ? $datosForm['ope'] : '';
    if(!isset($datosForm['ope']) ){$ope = isset($_POST['ope']) ? $_POST['ope'] : '';}
    $opc = isset($datosForm['opc']) ? $datosForm['opc'] : '';
    $i = isset($datosForm['i']) ? $datosForm['i'] : '';
    $r=array();
    $r['result']=false;
    $r['msj']=false;
    $r['ope']=$ope;
    $r['opc']=$opc;
    $r['i']=$i;
    if ($ope == "banner-home") 
    {
        $tb = "bannershome";
        $nombre_foto ='';
        $wruta = "../../imgcms/headers";// nueva ruta
        $temp = isset($_SESSION['img_banner']) ? $_SESSION['img_banner'] : '';
        if ($temp != "") 
        {
            $width = '1500';
            $height = '700';
            $relativo=0;
            $quality=75;
            $a=array();
            if(isset($_SESSION['typeUpload']) && $_SESSION['typeUpload']=='posterVideo')
            {
                $sql="select * from bannershome_video where id='1' "; 
                if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
                $width = $a['width_poster'];
                $height = $a['height_poster'];
                $quality = $a['quality_poster'];
            }
            elseif(isset($_SESSION['typeUpload']) && $_SESSION['typeUpload']=='innerHeader')
            {
                $sql="select * from bannershome_options where idx='1' "; 
                if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
                $width = $a['width_inner'];
                $height = $a['height_inner'];
                $quality = $a['quality_inner'];
            }
            else
            {
                $sql="select * from bannershome_options where idx='1' "; 
                if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
                $width = $a['width'];
                $height = $a['height'];
                $quality = $a['quality'];
            }
            //small
            $wanchosmall = 100;
            $waltosmall = 46;            
        }
        if ($opc == 1) 
        { //agregar nuevo
            $wnombre = trim($datosForm['nombre']);
            $descrip = addslashes($datosForm['descrip']);
            $slug = urls_amigables($wnombre);
            $s= "SELECT page FROM $tb WHERE page='".$datosForm['page']."' ";
            $num = numRows(ejecutar($s));
            if($num > 0 && $datosForm['page']!=0)
            {
                $r['msj']= isset($dataGlobal['error_assign_page']) ? $dataGlobal['error_assign_page'] : "Error already exists a banner assigned to this page";
            }
            else
            {
                $sql_0 = "insert into $tb (nombre, nombre_align, mostrar_nombre, imagen, descrip, mostrar_descrip, slug, page, activo )values ( ";
                $sql_0 .=" '".$wnombre."',"; 
                $sql_0 .=" '".$datosForm['nombre_align']."',"; 
                $sql_0 .=" '".$datosForm['mostrar_nombre']."',"; 
                $sql_0 .=" '".$temp."',";   
                $sql_0 .=" '".$descrip."',";     
                $sql_0 .=" '".$datosForm['mostrar_descrip']."',"; 
                $sql_0 .=" '".$slug."',"; 
                $sql_0 .=" '".$datosForm['page']."',"; 
                $sql_0 .=" '".$datosForm['activo']."' )";   
                if (!$res_0 = ejecutar($sql_0)) 
                {
                    $r['insert']= $sql_0." <br>Error al insertar  tabla " ;
                    $r['msj']='';
                }
                else
                {
                    $sql_1 = "update $tb set item = idx where item = '0'";
                    !$res_1 = ejecutar($sql_1);
                    $crear_cortes = crear_imagen_Slider_banner("../tmp/", $wruta, $temp, $quality, $width, $height, $wanchosmall, $waltosmall, $relativo=0);
                    if($crear_cortes > 0){$r['cortes']='error al crear cortes '.$crear_cortes;}
                    else{ $r['result']=true; }
                }
            }
        }
        if ($opc == 2) 
        { // actualizar
            $wnombre = trim($datosForm['nombre']);
            $descrip = addslashes($datosForm['descrip']);
            $slug = urls_amigables($wnombre);
            $sql="SELECT imagen FROM $tb WHERE idx='".$datosForm['i']."' ";
            $imagen_actual = fetchArray(ejecutar($sql));
            $sql_0 = "UPDATE $tb SET ";
            $sql_0 .= "nombre = '$wnombre',";
            $sql_0 .=" nombre_align = '".$datosForm['nombre_align']."', ";
            $sql_0 .=" mostrar_nombre = '".$datosForm['mostrar_nombre']."', ";
            if ($temp != "") { $sql_0 .=" imagen = '".$temp."', "; }
            else{ $sql_0 .=" imagen = '".$datosForm['imagen']."', "; }
            $sql_0 .=" descrip = '".$descrip."', ";
            $sql_0 .=" mostrar_descrip = '".$datosForm['mostrar_descrip']."', ";
            $sql_0 .=" slug = '".$slug."', ";
            $sql_0 .=" page = '".$datosForm['page']."', ";
            $sql_0 .=" activo = '".$datosForm['activo']."'";
            $sql_0 .=" WHERE idx = '".$datosForm['i']."'   ";
            if (!$res_0 = ejecutar($sql_0)) {
                $r['update']= $sql_0." <br>Error al  modificar tabla " ;
            }
            else
            {
                $r['update']= $sql_0;
                if ($temp != "") 
                {
                    $crear_cortes = crear_imagen_Slider_banner("../tmp/", $wruta, $temp, $quality, $width, $height, $wanchosmall, $waltosmall, $relativo=0);                    
                    if($crear_cortes > 0){$r['cortes']='error al crear cortes '.$crear_cortes;}
                    else
                    { 
                        $r['msj']='';
                        $r['result']=true; 
                        //eliminar las imagenes viejas de los directorios
                        @unlink($wruta."/slider/" . $imagen_actual['imagen']); 
                        @unlink($wruta."/small/" . $imagen_actual['imagen']);           
                    }
                }
                else{$r['result']=true; $r['msj']='';}
            }
        }
        if ($opc == 3) { // eliminar
            $sqls = "SELECT imagen FROM  $tb where  idx = '".$datosForm['i']."' ";
            $cols = fetchArray(ejecutar($sqls));
            $sql_0 = "delete from $tb where idx = '".$datosForm['i']."' ";
            if (!$res_0 = ejecutar($sql_0)) {
                $r['sql_0']=$sql_0." <br>Error al insertar y/o modificar tabla " ;
            }
            else
            {
                @unlink($wruta."/slider/" . $cols['imagen']);
                @unlink($wruta."/small/" . $cols['imagen']);

                $r['slider']=$wruta."/small/" . $cols['imagen'];
                $r['result']=true;
            }   
        }
    }//fin del crud
    //--------------------------------------------
    // configuraciones
    //--------------------------------------------
    if ($ope == "slider") 
    {
        $t = "bannershome_options";       
        $sql = "UPDATE $t SET ";
        $sql .="width='".$_POST['width']."',";
        $sql .="height='".$_POST['height']."', ";
        $sql .="quality='".$_POST['quality']."', ";
        $sql .="weight='".$_POST['weight']."', ";
        $sql .="overlay='".$_POST['overlay']."', ";
        $sql .="template_slider='".$_POST['template']."', ";
        $sql .="speed='".$_POST['speed']."', ";
        $sql .="direction='".$_POST['direction']."', ";
        $sql .="arrows='".$_POST['arrows']."', ";
        $sql .="effect='".$_POST['effect']."', ";
        $sql .="ico_nav='".$_POST['ico_nav']."' ";
        $sql .="where idx='1' ";
        $r['sql']=$sql;
        if (!$res = ejecutar($sql)) {
            $r['msj']=isset($dataGlobal['error_inser_upda']) ? $dataGlobal['error_inser_upda'] : "Error inserting and / or modifying table";
        }else{$r['result']=true;}        
    }
    if ($ope == "inner") 
    {
        $t = "bannershome_options";       
        $sql = "UPDATE $t SET ";
        $sql .="width_inner='".$_POST['width']."', ";
        $sql .="height_inner='".$_POST['height']."', ";
        $sql .="quality_inner='".$_POST['quality']."', ";
        $sql .="weight_inner='".$_POST['weight']."', ";
        $sql .="overlay_inner='".$_POST['overlay']."', ";
        $sql .="template_inner='".$_POST['template']."' ";
        $sql .="where idx='1' ";
        $r['sql']=$sql;
        if (!$res = ejecutar($sql)) {
            $r['msj']=isset($dataGlobal['error_inser_upda']) ? $dataGlobal['error_inser_upda'] : "Error inserting and / or modifying table";
        }else{$r['result']=true;}        
    }
    if ($ope == "video") 
    {
        $t = "bannershome_video";       
        $sql = "UPDATE $t SET ";
        $sql .="width_poster='".$_POST['width']."',";
        $sql .="height_poster='".$_POST['height']."', ";
        $sql .="quality_poster='".$_POST['quality']."', ";
        $sql .="weight_poster='".$_POST['weight']."', ";
        $sql .="weight_video='".$_POST['weight_video']."', ";
        $sql .="overlay='".$_POST['overlay']."', ";
        $sql .="template_video='".$_POST['template']."' ";
        $sql .="where id='1' ";
        $r['sql']=$sql;
        if (!$res = ejecutar($sql)) {
            $r['msj']=isset($dataGlobal['error_inser_upda']) ? $dataGlobal['error_inser_upda'] : "Error inserting and / or modifying table";
        }else{$r['result']=true;}        
    }
    echo json_encode($r);
}
?>