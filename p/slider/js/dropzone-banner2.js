function crearNew()
{ 
    $('#formAddBannerLabel').text(print_msj("msj_slider_js"));
    /*$('#formAddBanner').modal({show:true});*/
    var myAwesomeDropzone = cargarModal();
    myAwesomeDropzone.maxThumbnailFilesize=$("#pesoMax").val();
    myAwesomeDropzone.maxFileSize=$("#pesoMax").val();
    myAwesomeDropzone.params.typeUpload='';
    $("#formDropZone").html("<form id='dZUpload' method='post' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");      
      var message ='<span class="text-center">';
      message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
      message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
      message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
      message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
      $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
      var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
      myDropzone.on("complete", function(file,response) {
        $('.dz-remove').html('<i class="fa fa-trash"></i> '+print_msj("btn_remove")+'');
      });
      myDropzone.on("sending", function(file, xhr, formData) {
      /*Called just before each file is sent*/
        xhr.ontimeout = (() => {
          /*Execute on case of timeout only*/
            console.log('Server Timeout');
            $('#divSmallBoxes').html('');
            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_drop_js5")+'', 9000);
        });
      });
} 

function crearInner()
{ 
    $('#formAddBannerLabel').text(print_msj("msj_slider_js"));
    /*$('#formAddBanner').modal({show:true});*/
    var myAwesomeDropzone = cargarModal();
    myAwesomeDropzone.maxThumbnailFilesize=$("#pesoMax").val();
    myAwesomeDropzone.maxFileSize=$("#pesoMax").val();
    myAwesomeDropzone.params.typeUpload='innerHeader';
    $("#formDropZone").html("<form id='dZUpload' method='post' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");      
      var message ='<span class="text-center">';
      message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
      message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
      message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
      message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
      $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
      var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
      myDropzone.on("complete", function(file,response) {
        $('.dz-remove').html('<i class="fa fa-trash"></i> '+print_msj("btn_remove")+'');
      });
      myDropzone.on("sending", function(file, xhr, formData) {
      /*Called just before each file is sent*/
        xhr.ontimeout = (() => {
          /*Execute on case of timeout only*/
            console.log('Server Timeout');
            $('#divSmallBoxes').html('');
            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_drop_js5")+'', 9000);
        });
      });
}
function subirPoster()
{ 
    $('#formAddBannerLabel').text(print_msj("msj_slider_js"));
    /*$('#formAddBanner').modal({show:true});*/
    var myAwesomeDropzone = cargarModal();
    myAwesomeDropzone.maxThumbnailFilesize=$("#weight_poster").val();
    myAwesomeDropzone.maxFileSize=$("#weight_poster").val();
    myAwesomeDropzone.params.typeUpload='posterVideo';
    $("#formDropZone").html("<form id='dZUpload' method='post' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");      
      var message ='<span class="text-center">';
      message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
      message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
      message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
      message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
      $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
      var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
      myDropzone.on("complete", function(file,response) {
        $('.dz-remove').html('<i class="fa fa-trash"></i> '+print_msj("btn_remove")+'');
      });
      myDropzone.on("sending", function(file, xhr, formData) {
      /*Called just before each file is sent*/
        xhr.ontimeout = (() => {
          /*Execute on case of timeout only*/
            console.log('Server Timeout');
            $('#divSmallBoxes').html('');
            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_drop_js5")+'', 9000);
        });
      });
}   
function cargarModal()
{ 
  var myAwesomeDropzone = {
          url: "subir-img.php",
          method: "post",
          addRemoveLinks: true,
          paramName: "banner",
          maxFiles: 1, // cantidad
          /*maxThumbnailFilesize: 0,
          maxFileSize: 0,*/
          timeout: 3000000,
          dictRemoveFile: ''+print_msj("btn_remove")+'',
          uploadMultiple: false,
          acceptedFiles: "image/jpeg",
          params: {typeUpload:''},
          success: function (file, response) {
            var r = JSON.parse(response);
              var imgName = r.preview;
              //debugger;
              if(!r.result){
                file.previewElement.classList.add("dz-error");
                $('.dz-error-message').text(r.msj);
                alerError(''+print_msj("msj_slider_js15")+'', r.alert, 3000);
              }
              else
              {
                file.previewElement.classList.add("dz-success");
                $("#formDropZone").css('border-color', '#E91E63');
                $("#dZUpload").css('padding-left', '0px');
                $("#imagen").val('tmp/'+imgName);
                $("#dZUpload").css('background-image', 'url('+server_p+'tmp/'+imgName+') ');
                $(".dropzone .dz-preview").css({
                  'margin-top': '50px',
                  'margin-bottom': '134px'
                });
                $("#dZUpload").css('background-size', 'cover');
                $("#dZUpload").css('background-position', 'center');
                $("#dZUpload").css('background-repeat', 'no-repeat');
                $("#dZUpload").css('text-shadow', 'none');
              }
              console.log("Successfully uploaded : " + imgName);
          },
          error: function (file, response) {
            file.previewElement.classList.add("dz-error");
            console.log('error '+response)
          },
          removedfile: function(file, serverFileName)
          {
            var name = file.name;
                  var element;
                  (element = file.previewElement) != null ? 
                  element.parentNode.removeChild(file.previewElement) : 
                  false;
                  $("#imagen").val('');
                  $("#dZUpload").removeClass('dz-started'); 
                  $("#dZUpload").removeClass('dz-max-files-reached'); 
                  $("#formDropZone").css('border-color', '#0087F7');
                  $("#dZUpload").css('padding-left', '54px');
                  $("#dZUpload").css('color', '#000');
                  $("#dZUpload").css('background-image', 'none');                  
                  $(".dropzone .dz-preview").css({
                    'margin-top': '16px',
                    'margin-bottom': '16px'
                  });
                  var message ='<span class="text-center">';
                  message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
                  message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
                  message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
                  message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
                  $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
                  vaciar_tmp();
          },
          maxfilesexceeded: function(file) {
                  alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_drop_js4")+'', 3000);
                  var name = file.name;
                  var element;
                  (element = file.previewElement) != null ? 
                  element.parentNode.removeChild(file.previewElement) : 
                  false;
          },
          uploadprogress: function(file, progress, bytesSent, xhr){
            //console.log('progress= '+progress);               
            $(".dropzone .dz-preview").css({
              'margin-top': '16px',
              'margin-bottom': '16px'
            });
            $('span.dz-upload').css('width', progress);
          }
  } // FIN myAwesomeDropzone
  return myAwesomeDropzone;
}
function save()
{
    var datosForm= {};
    datosForm.i =$('#i').val();
    datosForm.ope ='banner-home';
    datosForm.opc =$('#opc').val();
    datosForm.nombre =$('#nombre').val();
    datosForm.nombre_align= $('#nombre_align').val();
    datosForm.mostrar_nombre= $('#mostrar_nombre').val();
    $('#descrip').val($('iframe.cke_wysiwyg_frame').contents().find("body").html());
    datosForm.descrip = $('#descrip').val();
    datosForm.mostrar_descrip = $('#mostrar_descrip').val();
    datosForm.imagen= $("#imagen").val();
    datosForm.activo= $("#activo").val();
    datosForm.page= $("#page").val();
    if(datosForm.imagen.length==0)
    {
      alerAviso(''+print_msj("msj_slider_js4")+'', ''+print_msj("msj_slider_js1")+'', 3000);
    }
    else
    {
      $('.save').attr('disabled', 'true');
      alerAct(''+print_msj("msj_slider_js2")+'...', ''+print_msj("msj_slider_js3")+'', 0);
      $.ajax({
        url: 'guardar.php',
        type: 'POST',
        data: {datosForm},     
        success: function(data, textStatus, xhr) {
          console.log(data);
          $('.save').removeAttr('disabled');
          var r ={};
          if(r= JSON.parse(data))          
          {
            if(!r.result){
              $('#divSmallBoxes').html('');
              alerError(''+print_msj("msj_slider_js15")+'', r.msj, 3000);
            }else{
              $('#divSmallBoxes').html('');
              alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
              window.location.href='index.php';
            }
          }
          else
          {
            $('#divSmallBoxes').html('');
            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js6")+'', 3000);
          }
        },
        error: function(xhr, textStatus, errorThrown) {
          //called when there is an error
          console.log('error = '+textStatus);          
          $('.save').removeAttr('disabled');
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js7")+'.', 3000);
        },
        complete: function(xhr, textStatus) {
          //called when complete
          console.log('datos =' +datosForm);          
        }
      });
    }
}
function vaciar_tmp(){
  $.get('../system/vaciar_tmp.php', function(data) {
    console.log('vaciando tmp = '+data);
  });
}
function getList(){
  $.get('lista.php', function(data) {
    $('#banner_tbody').html('<tr><td colspan="8" ><div align="center" style="color:#F00">'+print_msj("msj_slider_js2")+'...</div></td></tr>');
    //debugger;
    var r ={};
    if(r= JSON.parse(data))    
    {
      var r = JSON.parse(data);
      if(!r.result)
      {
        $('#banner_tbody').html('<tr><td colspan="8" ><div align="center" style="color:#F00">'+print_msj("msj_slider_js8")+'</div></td></tr>');
      }
      else
      {
        var body_html='';
        var cont=1;
        var tb='bannershome';
        for (var i =0; i < Object.keys(r.lista).length; i++) 
        {
          body_html+='<tr class="placeholder">';
          body_html+='<td>'+ cont + '</td>';
          if(r.lista[i]['imagen'] =='')
          { body_html+='<td align="center"><i class="fa fa-file-photo-o fa-3x"></i></td>';}
          else
          {
            body_html+='<td align="center">';
            body_html+='<img src="'+server+'imgcms/headers/small/'+r.lista[i]['imagen']+'"';
            body_html+=' alt="small_banner" style="width: 100px;height: 50px;border:1px solid #999999; padding:5px;">';
            body_html+='</td>'; 
          }
          body_html+='<td><strong>'+ r.lista[i]['nombre'] + '<strong></td>';
          if(r.lista[i]['activo'] ==0)
          {
            body_html+='<td align="center">';
            body_html+='<span class="label label-danger">';
            body_html+=''+print_msj("msj_slider_js9")+'</span>';
            body_html+='</td>';
          }
          else
          {        
            body_html+='<td align="center">';
            body_html+='<span class="label label-success">';
            body_html+=''+print_msj("msj_slider_js10")+'</span>';
            body_html+='</td>'; 
          }
          if(cont > 1)
          {
            body_html+='<td><a href="#" onclick="ordenar_banner(\''+tb+'\', \'s\', \''+(parseInt(r.lista[i]['item']) - parseInt(1))+'\');">';
            body_html+='<img src="'+server_p+'img/subir.png" border="0" />';
            body_html+='</a></td>';
          }else{body_html+='<td></td>';}
          if(cont < Object.keys(r.lista).length)
          {
            body_html+='<td><a href="#" onclick="ordenar_banner(\''+tb+'\', \'b\', \''+(parseInt(r.lista[i]['item']) + parseInt(1))+'\');">';
            body_html+='<img src="'+server_p+'img/bajar.png" border="0" />';
            body_html+='</a></td>';
          }else{body_html+='<td></td>';}
          body_html+='<td align="center" valign="middle"><a href="#" class="btn btn-small btn-primary" ';
          body_html+='onclick="cargar_item(\''+r.lista[i]['idx']+'\')">';
          body_html+='<i class="fa fa-edit"></i> '+print_msj("msj_slider_js11")+'</a></td>';
          body_html+='<td align="center" valign="middle"><a href="#" class="btn btn-small btn-danger" ';
          body_html+='onclick="confirmDelete('+r.lista[i]['idx']+')" style="padding:6px 15px;">';
          body_html+='<i class="fa fa-trash"></i> '+print_msj("msj_slider_js24")+'</a></td>';
          body_html+='</tr>';
          cont++;
        }
        $('#banner_tbody').html(body_html);
      }
    }
    else
    {
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
    }
  });
}
function getListPages(){
  $.get('lista-pages.php', function(data) {
    $('#banner_tbody_pages').html('<tr><td colspan="8" ><div align="center" style="color:#F00">'+print_msj("msj_slider_js3")+'...</div></td></tr>');
    //debugger;
    var r ={};
    if(r= JSON.parse(data))    
    {
      var r = JSON.parse(data);
      if(!r.result)
      {
        $('#banner_tbody_pages').html('<tr><td colspan="8" ><div align="center" style="color:#F00">'+print_msj("msj_slider_js8")+'</div></td></tr>');
      }
      else
      {
        var body_html='';
        var cont=1;
        var tb='bannershome';
        for (var i =0; i < Object.keys(r.lista).length; i++) 
        {
          body_html+='<tr class="placeholder">';
          body_html+='<td>'+ cont + '</td>';
          if(r.lista[i]['imagen'] =='')
          { body_html+='<td align="center"><i class="fa fa-file-photo-o fa-3x"></i></td>';}
          else
          {
            body_html+='<td align="center">';
            body_html+='<img src="'+server+'imgcms/headers/small/'+r.lista[i]['imagen']+'"';
            body_html+=' alt="small_banner" style="width: 100px;height: 50px;border:1px solid #999999; padding:5px;">';
            body_html+='</td>'; 
          }
          body_html+='<td><strong>'+ r.lista[i]['nombre'] + '<strong></td>';
          body_html+='<td align="center">'+r.lista[i]['titulo']+'</td>';          
          if(r.lista[i]['activo'] ==0)
          {
            body_html+='<td align="center">';
            body_html+='<span class="label label-danger">';
            body_html+=''+print_msj("msj_slider_js9")+'</span>';
            body_html+='</td>';
          }
          else
          {        
            body_html+='<td align="center">';
            body_html+='<span class="label label-success">';
            body_html+=''+print_msj("msj_slider_js10")+'</span>';
            body_html+='</td>'; 
          }
          body_html+='<td align="center" valign="middle"><a href="#" class="btn btn-small btn-primary" ';
          body_html+='onclick="cargar_item_pages(\''+r.lista[i]['idx']+'\')">';
          body_html+='<i class="fa fa-edit"></i> '+print_msj("msj_slider_js11")+'</a></td>';
          body_html+='<td align="center" valign="middle"><a href="#" class="btn btn-small btn-danger" ';
          body_html+='onclick="confirmDelete('+r.lista[i]['idx']+')" style="padding:6px 15px;">';
          body_html+='<i class="fa fa-trash"></i> '+print_msj("msj_slider_js24")+'</a></td>';
          body_html+='</tr>';
          cont++;
        }
        $('#banner_tbody_pages').html(body_html);
      }
    }
    else
    {
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
    }
  });
}
var confirmDelete= function(idx){
  $('#divSmallBoxes').html('');
  alerConfirm(''+print_msj("msj_slider_js23")+'', ''+print_msj("msj_slider_js13")+'', idx, 'deleteBanner');
}
var deleteBanner = function(idx){
  alerAct(''+print_msj("msj_slider_js2")+'', ''+print_msj("msj_slider_js3")+'', 0);
  var datosForm ={};
  datosForm.ope ='banner-home';
  datosForm.opc =3;
  datosForm.i= idx;
  $.ajax({
    url: 'guardar.php', 
    type: 'POST',
    data: {datosForm},
    complete: function(xhr, textStatus) {
      //called when complete
    },
    success: function(data, textStatus, xhr) {
      //called when successful
      var r ={};
      if(r= JSON.parse(data))      
      {
        var r = JSON.parse(data);
        if(!r.result)
        {
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js13")+'', 2000);
        }
        else
        {
          getList();
          getListPages();
          $('#divSmallBoxes').html('');
          alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
        }
      }
      else
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
    }
  });
}
var ordenar_banner = function(tb, t, item)
{
  alerAct(''+print_msj("msj_slider_js22")+'', ''+print_msj("msj_slider_js3")+'.', 0);
  $.get('../system/subir_bajar.php?tb='+tb+'&t='+t+'&item='+item, function(data) {
    /*debugger;*/
    var r ={};
    if(r= JSON.parse(data))    
    {
      var r = JSON.parse(data);
      if(!r.result)
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js21")+'', 3000);
      }
      else
      {
        getList();
        $('#divSmallBoxes').html('');
        alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
      }
    }
    else
    {
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
    }
  });
}
var traer_item = function(idx)
{
  $.ajax({
    url: 'consultarItem.php', 
    type: 'POST',
    data: {'idx': idx},
    beforeSend: function(){
      alerAct(''+print_msj("msj_slider_js2")+'', ''+print_msj("msj_slider_js20")+'', 2000);
      console.log('Loading');
    },
    success: function(data, textStatus, xhr) {
      //called when successful
      var r ={};
      if(r= JSON.parse(data))      
      {
        var r = JSON.parse(data);
        if(!r.result)
        {
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js19")+'', 2000);
        }
        else
        {
          $('#i').val(idx);
          $('#ope').val('banner-home');
          $('#opc').val('2');
          $('#nombre').val(r.lista['nombre']);
          $('#nombre_align').val(r.lista['nombre_align']);
          $('#mostrar_nombre').val(r.lista['mostrar_nombre']);
          $('#page').val(r.lista['page']);
          $("#imagen").val(r.lista['imagen']);
          $('#descrip').val(r.lista['descrip']);
          $('#mostrar_descrip').val(r.lista['mostrar_descrip']);
          $("#activo").val(r.lista['activo']);
          $("#formDropZone").css('border-color', '#E91E63');
          $("#dZUpload").css('padding-left', '0px');
          if(r.lista['imagen'] != '' )
          {
            $("#dZUpload").css('background-image', 'url('+server+'imgcms/headers/slider/'+r.lista['imagen']+')');
            $("#dZUpload").css('color', '#fff');             
            var message ='<span class="text-center">';
            message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
            message+='<span class="font-lg"><i class="fa fa-caret-right text-dafault"></i> ';
            message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
            message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
            $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
            $("#dZUpload .dz-message").css('text-shadow', '2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000, 2px 2px 6px #000');                       
          }
          else{$("#dZUpload").css('background-image', 'none');}
          $("#dZUpload").css('background-size', 'cover');
          $("#dZUpload").css('background-position', 'center');
          $("#dZUpload").css('background-repeat', 'no-repeat');
        }
      }      
      else
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
    }
  });
}
var cargar_item_pages = function(idx){
  window.location.href= 'form-add-pages.php?id='+idx;
}
var cargar_item = function(idx){
  window.location.href= 'form-add.php?id='+idx;
}
var guardarVideo= function()
{
  $('#guardarVideo').attr('disabled', 'true');
  $('#descrip_video').val($('iframe.cke_wysiwyg_frame').contents().find("body").html());
  var datosForm= {};
  datosForm.autoplay =$('#autoplay').prop('checked') ? 1 : 0;
  datosForm.muted =$('#muted').prop('checked') ? 1 : 0;
  datosForm.activo =$('#activo').prop('checked') ? 1 : 0;
  datosForm.table_ipad =$('#table_ipad').prop('checked') ? 1 : 0;
  datosForm.mobile =$('#mobile').prop('checked') ? 1 : 0;
  datosForm.descrip_video = $('#descrip_video').val();
  datosForm.imagen= $("#imagen").val();
  datosForm.video= $("#video").val();
  datosForm.ope= 'video';
  if(datosForm.activo==1 && (datosForm.imagen.length==0 || datosForm.video.length==0))
  {
    alerAviso(''+print_msj("msj_slider_js4")+'', ''+print_msj("msj_slider_js16")+'', 3000);
  }
  else
  {
    alerAct(''+print_msj("msj_slider_js2")+'...', ''+print_msj("msj_slider_js3")+'', 0);
    $.ajax({
      url: 'guardarVideo.php',
      type: 'POST',
      data: {datosForm},     
      success: function(data, textStatus, xhr) {
        console.log(data);
        $('#guardarVideo').removeAttr('disabled');
        var r ={};
        if(r= JSON.parse(data))          
        {
          if(!r.result){
            $('#divSmallBoxes').html('');
            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js6")+'', 3000);
          }else{
            $('#divSmallBoxes').html('');
            alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
            vaciar_tmp();
          }
        }
        else
        {
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
        console.log('error = '+textStatus);          
        $('#guardarVideo').removeAttr('disabled');
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js7")+'.', 3000);
      },
      complete: function(xhr, textStatus) {
        //called when complete
        console.log('datos =' +datosForm);          
      }
    });
  }
}
var save_options_slider = function()
{
  $('#save_options_slider').attr('disabled', 'true');
  alerAct(''+print_msj("msj_slider_js2")+'', ''+print_msj("msj_slider_js3")+'', 0);
  var datosForm ={
    'ope':'slider',
    'width': $('#width_slider').val(),
    'height': $('#height_slider').val(),
    'quality': $('#quality_slider').val(),
    'weight': $('#weight_slider').val(),
    'overlay': $('#overlay_slider').val(),
    'template': $('#template_slider').val(),
    'speed': $('#speed').val(),
    'direction': $('#direction').val(),
    'arrows': $('#arrows').val(),
    'ico_nav': $('#ico_nav').val(),
    'effect': $('#effect').val(),
  };
  $.ajax({
    url: 'guardar.php', 
    type: 'POST',
    data: datosForm,    
    complete: function(xhr, textStatus) {
      //called when complete
    },
    success: function(data, textStatus, xhr) {
      //called when successful
      var r ={};
      if(r= JSON.parse(data))
      {
        $('#save_options_slider').removeAttr('disabled');
        var r = JSON.parse(data);
        if(!r.result)
        {
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
        }
        else
        {
          $('#divSmallBoxes').html('');
          alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
        }
      }
      else
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
      $('#save_options_slider').removeAttr('disabled');
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
    }
  });
}
var save_options_video= function()
{
  var datosForm ={
    'ope':'video',
    'width': $('#width_poster').val(),
    'height': $('#height_poster').val(),
    'quality': $('#quality_poster').val(),
    'weight': $('#weight_poster').val(),
    'weight_video': $('#weight_video').val(),
    'overlay': $('#overlay_video').val(),
    'template': $('#template_video').val(),
  };    
  $('#save_options_video').attr('disabled', 'true');
  alerAct(''+print_msj("msj_slider_js2")+'...', ''+print_msj("msj_slider_js3")+'', 0);
  $.ajax({
    url: 'guardar.php',
    type: 'POST',
    data: datosForm,     
    success: function(data, textStatus, xhr) {
      console.log(data);
      $('#save_options_video').removeAttr('disabled');
      var r ={};
      if(r= JSON.parse(data))          
      {
        if(!r.result){
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js6")+'', 3000);
        }else{
          $('#divSmallBoxes').html('');
          alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
          $("#weight_poster").val(datosForm.weight);
        }
      }
      else
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
      console.log('error = '+textStatus);          
      $('#save_options_video').removeAttr('disabled');
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js7")+'.', 3000);
    },
    complete: function(xhr, textStatus) {
      //called when complete
      console.log('datos =' +datosForm);          
    }
  });    
}
var save_options_inner= function()
{
  $('#save_options_inner').attr('disabled', 'true');
  alerAct(''+print_msj("msj_slider_js2")+'', ''+print_msj("msj_slider_js3")+'', 0);
  var datosForm ={
    'ope':'inner',
    'width': $('#width_inner').val(),
    'height': $('#height_inner').val(),
    'quality': $('#quality_inner').val(),
    'weight': $('#weight_inner').val(),
    'overlay': $('#overlay_inner').val(),
    'template': $('#template_inner').val(),
  };
  console.log(datosForm);
  $.ajax({
    url: 'guardar.php', 
    type: 'POST',
    data: datosForm,    
    complete: function(xhr, textStatus) {
      //called when complete
    },
    success: function(data, textStatus, xhr) {
      //called when successful
      var r ={};
      if(r= JSON.parse(data))
      {
        $('#save_options_inner').removeAttr('disabled');
        var r = JSON.parse(data);
        if(!r.result)
        {
          $('#divSmallBoxes').html('');
          alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
        }
        else
        {
          $('#divSmallBoxes').html('');
          alerValid(''+print_msj("msj_slider_js18")+'', ''+print_msj("msj_slider_js5")+'', 1000);
        }
      }
      else
      {
        $('#divSmallBoxes').html('');
        alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
      }
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
      $('#save_options_inner').removeAttr('disabled');
      $('#divSmallBoxes').html('');
      alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js17")+'', 2000);
    }
  });
}