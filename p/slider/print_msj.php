<script>
  var print_msj = function(msjs){ 
    var msjs_js ={
    "btn_remove"    :   "<?=isset($dataGlobal['btn_remove']) ?  $dataGlobal['btn_remove'] : 'Remover'?>",
    "msj_drop_js"   :   "<?=isset($dataGlobal['msj_drop_js']) ?  $dataGlobal['msj_drop_js'] : 'Drop files'?>",
    "msj_drop_js2"    :   "<?=isset($dataGlobal['msj_drop_js2']) ?  $dataGlobal['msj_drop_js2'] : 'to upload'?>",
    "msj_drop_js3"    :   "<?=isset($dataGlobal['msj_drop_js3']) ?  $dataGlobal['msj_drop_js3'] : 'Or Click'?>",
    "msj_drop_js4"    :   "<?=isset($dataGlobal['msj_drop_js4']) ?  $dataGlobal['msj_drop_js4'] : 'has exceeded the file limit'?>",
    "msj_drop_js5"    :   "<?=isset($dataGlobal['msj_drop_js5']) ?  $dataGlobal['msj_drop_js5'] : 'Server Timeout'?>",

    "msj_slider_js"    :   "<?=isset($dataGlobal['label_add_new']) ?  $dataGlobal['label_add_new'] : 'Add New'?>",
    "msj_slider_js1"    :   "<?=isset($dataGlobal['campos_obligatorios']) ?  $dataGlobal['campos_obligatorios'] : 'tiene campos importante sin rellenar'?>",
    "msj_slider_js2"    :   "<?=isset($dataGlobal['loading']) ?  $dataGlobal['loading'] : 'Loading'?>",
    "msj_slider_js3"    :   "<?=isset($dataGlobal['please_wait']) ?  $dataGlobal['please_wait'] : 'please wait'?>",
    "msj_slider_js4"    :   "<?=isset($dataGlobal['warning']) ?  $dataGlobal['warning'] : 'Warning'?>",
    "msj_slider_js5"    :   "<?=isset($dataGlobal['msj_success_js']) ?  $dataGlobal['msj_success_js'] : 'successful process'?>",
    "msj_slider_js6"    :   "<?=isset($dataGlobal['msj_error_saving']) ?  $dataGlobal['msj_error_saving'] : 'There was an error saving'?>",
    "msj_slider_js7"    :   "<?=isset($dataGlobal['msj_error_internal_server']) ?  $dataGlobal['msj_error_internal_server'] : 'An internal server error has occurred'?>",
    "msj_slider_js8"    :   "<?=isset($dataGlobal['label_no_record']) ?  $dataGlobal['label_no_record'] : 'No Record'?>",
    "msj_slider_js9"    :   "<?=isset($dataGlobal['label_inactive']) ?  $dataGlobal['label_inactive'] : 'Inactive'?>",
    "msj_slider_js10"    :   "<?=isset($dataGlobal['label_active']) ?  $dataGlobal['label_active'] : 'Active'?>",
    "msj_slider_js11"    :   "<?=isset($dataGlobal['label_edit']) ?  $dataGlobal['label_edit'] : 'Edit'?>",
    "msj_slider_js12"    :   "<?=isset($dataGlobal['msj_error_processing']) ?  $dataGlobal['msj_error_processing'] : 'There was an error processing'?>",
    "msj_slider_js13"    :   "<?=isset($dataGlobal['msj_del_item']) ?  $dataGlobal['msj_del_item'] : 'Are you sure you want to delete this item?'?>",
    "msj_slider_js14"    :   "<?=isset($dataGlobal['no_removed_banner']) ?  $dataGlobal['no_removed_banner'] : 'the banner could not be removed'?>",
    "msj_slider_js15"    :   "<?=isset($dataGlobal['label_error']) ?  $dataGlobal['label_error'] : 'Error'?>",
    "msj_slider_js16"    :   "<?=isset($dataGlobal['active_video']) ?  $dataGlobal['active_video'] : 'to activate video you must complete all the fields'?>",
    "msj_slider_js17"    :   "<?=isset($dataGlobal['could_not_save']) ?  $dataGlobal['could_not_save'] : 'could not save'?>",
    "msj_slider_js18"    :   "<?=isset($dataGlobal['label_success']) ?  $dataGlobal['label_success'] : 'Success'?>",
    "msj_slider_js19"    :   "<?=isset($dataGlobal['item_not_loaded']) ?  $dataGlobal['item_not_loaded'] : 'The item could not be loaded'?>",
    "msj_slider_js20"    :   "<?=isset($dataGlobal['obtaining_item']) ?  $dataGlobal['obtaining_item'] : 'Obtaining item'?>",
    "msj_slider_js21"    :   "<?=isset($dataGlobal['error_ordering']) ?  $dataGlobal['error_ordering'] : 'There was an error ordering'?>",
    "msj_slider_js22"    :   "<?=isset($dataGlobal['updating_list']) ?  $dataGlobal['updating_list'] : 'Updating list'?>",
    "msj_slider_js23"    :   "<?=isset($dataGlobal['del_banner']) ?  $dataGlobal['del_banner'] : 'Eliminar Banner'?>",
    "msj_slider_js24"    :   "<?=isset($dataGlobal['label_del']) ?  $dataGlobal['label_del'] : 'Delete'?>",
    "msj_slider_js25"    :   "<?=isset($dataGlobal['edit_banner']) ?  $dataGlobal['edit_banner'] : 'Edit Banner'?>"
    };
    //debugger;
    return msjs_js[msjs]; 
  }
</script>