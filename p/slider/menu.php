
<ul class="nav nav-tabs" role="tablist">
    <li role="Banner" class="active">
        <a href="#tabs-1" aria-controls="tabs-1" role="Banner" data-toggle="tab"><?=isset($dataGlobal['slider_image']) ? $dataGlobal['slider_image'] : 'Slider Image'?></a>
    </li>
    <li role="video Home" >
        <a href="#tabs-5" aria-controls="tabs-4" role="video Home" data-toggle="tab"><?=isset($dataGlobal['slider_video']) ? $dataGlobal['slider_video'] : 'Slider Video'?></a>
    </li>
    <li role="Banner" class="">
        <a href="#tabs-2" aria-controls="tabs-2" role="Banner" data-toggle="tab"><?=isset($dataGlobal['inner_header']) ? $dataGlobal['inner_header'] : 'Inner Header Image'?></a>
    </li>
    <li role="Options" >
        <a href="#tabs-3" aria-controls="tabs-3" role="Options" data-toggle="tab"><?=isset($dataGlobal['label_options']) ? $dataGlobal['label_options'] : 'Options'?></a>
    </li>
</ul>