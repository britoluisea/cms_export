<?php 
	require_once(dirname(__FILE__)."/../header_box.php"); 
  	$sql="SELECT * FROM bannershome_video where id = 1";
  	$v = fetchArray(ejecutar($sql));
  	$weight_video=$v['weight_video'];
  	$video=$v['video'];
	//verificando si hay video en la carpeta
	$dir =  dirname(__FILE__)."/../../imgcms/headers/video/"; 
	$check=0;
	chdir ($dir);  ///vaciar carpeta de video
	$dh=opendir('.');
	while ($file = readdir ($dh)) {
	   if ($file != "." && $file != "..") {
	     $check++;
	    }
	}
	closedir($dh);
	if($check == 0 || $video=='')
	{ ?>
		<div id="formDropZone" class="bg-dropzone" style="margin-bottom: 10px;"></div>
		<?php 
	}
	else
	{ ?>
		<div class="col-xs-12 col-sm-12 ">
			<div class="thumbnail">
				<div class="caption">
					<h3 class="h3 text-center">
						<?=isset($dataGlobal['empty_folder_full']) ?  $dataGlobal['empty_folder_full'] : 'Full folder should empty it to add'?>						
					</h3>
					<p class="text-center">
						<button class="btn btn-danger btn-lg" id="vaciar">
							<i class="fa fa-folder-o"></i>
							<?=isset($dataGlobal['label_empty']) ?  $dataGlobal['label_empty'] : 'Empty'?>
						</button>
					</p>
				</div>
			</div>
		</div>
		<?php
	}
?>
<br>
<p class="text-center">
	<button class="btn btn-default" id="close"><?=isset($dataGlobal['btn_close']) ?  $dataGlobal['btn_close'] : 'Close'?></button>
</p>
<?php
if($check == 0 || $video=='')
{ ?>
	<link href="<?=CSS_P?>dropzone.css" type="text/css" rel="stylesheet" />
	<script src="<?=JS_P?>dropzone.js"></script> 
	<?php include "print_msj.php"; ?>
	<script>    
		Dropzone.autoDiscover = false;
	    var myAwesomeDropzone = {
	          url: "subir-video.php",
	          method: "post",
	          addRemoveLinks: true,
	          paramName: "video",
	          maxFiles: 1, // cantidad
	          maxThumbnailFilesize: '<?=$weight_video?>',
	          maxFileSize: '<?=$weight_video?>',
	          timeout: 3000000,
	          dictRemoveFile: ''+print_msj("btn_remove")+'',
	          uploadMultiple: false,
	          acceptedFiles: "video/mp4",
	          params: {typeUpload:'uploadVideo'},
	          success: function (file, response) {
	            var r = JSON.parse(response);
	              var imgName = r.preview;
	              //debugger;
	              if(!r.result){
	                file.previewElement.classList.add("dz-error");
	                $('.dz-error-message').text(r.msj);
	              }
	              else
	              {
	                file.previewElement.classList.add("dz-success");
	                $("#formDropZone").css('border-color', '#E91E63');
	                $("#dZUpload").css('padding-left', '0px');
	                window.parent.AlertVideo();
	                window.parent.showVideo();
	                window.parent.closeModalVideo(); 
	              }
	              console.log("Successfully uploaded : " + imgName);
	          },
	          error: function (file, response) {
	            file.previewElement.classList.add("dz-error");
	            console.log('error '+response)
	            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js12")+'', 3000);
	            $('.dz-error-message').text(''+print_msj("msj_slider_js12")+'');
	          },
	          removedfile: function(file, serverFileName)
	          {
	            var name = file.name;
	                  var element;
	                  (element = file.previewElement) != null ? 
	                  element.parentNode.removeChild(file.previewElement) : 
	                  false;
	                  $("#dZUpload").removeClass('dz-started'); 
	                  $("#dZUpload").removeClass('dz-max-files-reached'); 
	                  $("#formDropZone").css('border-color', '#0087F7');
	                  $("#dZUpload").css('padding-left', '54px');
	                  $("#dZUpload").css('color', '#000');                  
	                  var message ='<span class="text-center">';
	                  message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
	                  message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
	                  message+='Drop files <span class="font-xs">to upload</span></span><span>&nbsp;&nbsp;';
	                  message+='<h4 class="display-inline h4"> (Or Click)</h4></span></span></span>';
	                  $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
	                  vaciar_video();
	          },
	          maxfilesexceeded: function(file) {
	                  alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_drop_js4")+'', 3000);
	                  var name = file.name;
	                  var element;
	                  (element = file.previewElement) != null ? 
	                  element.parentNode.removeChild(file.previewElement) : 
	                  false;
	          },
	          uploadprogress: function(file, progress, bytesSent, xhr){
	            //console.log('progress= '+progress);                           
	            $('span.dz-upload').css('width', progress);
	            if(progress==100){console.log(file)}
	          }
	  	} // FIN myAwesomeDropzone
	  	$("#formDropZone").html("<form id='dZUpload' method='post' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");      
	      var message ='<span class="text-center">';
	      message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
	      message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
      	  message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
      	  message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
	      $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
	      var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
	      myDropzone.on("complete", function(file,response) {
	        $('.dz-remove').html('<i class="fa fa-trash"></i> Remove');
	      });
	      myDropzone.on("sending", function(file, xhr, formData) {
	      /*Called just before each file is sent*/
	        xhr.ontimeout = (() => {
	          /*Execute on case of timeout only*/
	            console.log('Server Timeout');
	            $('#divSmallBoxes').html('');
	            alerError(''+print_msj("msj_slider_js15")+'', ''+print_msj("msj_slider_js5")+'', 9000);
	        });
	      });
	  	
	</script>
	<?php
}?>
<script>
	$(document).ready(function() {			
	    $("#close").click(function(event) {
	      event.preventDefault();
	      window.parent.closeModalVideo(); 
	    });
	    $("#vaciar").click(function(event) {
	      event.preventDefault();
	      vaciar_video();
	    	window.parent.AlertVideo();
	      	window.parent.closeModalVideo(); 
	      	window.parent.hideVideo(); 
	    });
	    <?php if($check == 0 || $video==''){ ?>vaciar_video(); <?php }?>
   	});
   	function vaciar_video(){
  		$.get('../system/vaciar_video.php', function(data) {
    		console.log(data);
  		});
	}
</script>
<?php include(dirname(__FILE__)."/../foot.php");?>