<?php 
session_start();
include "../../cn/cnx.php";
include "../system/functions.php";
include dirname(__FILE__)."/../system/languages/languages.php";
@ini_set("memory_limit", "3000M");
@ini_set( 'upload_max_size' , '3000M' );
@ini_set( 'post_max_size', '3000M');
@ini_set( 'max_execution_time', '30000' );
set_time_limit(0);
$r=array();
$r['result']=false;
$r['msj']=false;
$r['alert']=false;
$r['preview']=false;
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    $r['msj']=isset($dataGlobal['error_session_exp']) ? $dataGlobal['error_session_exp'] : 'error, session expired';
    $r['alert']=isset($dataGlobal['error_session_exp']) ? $dataGlobal['error_session_exp'] : 'error, session expired';
}
else
{
	//expandiendo memoria
   	$a=array();
    if(isset($_POST['typeUpload']) && $_POST['typeUpload']=='posterVideo')
    {
    	$_SESSION['typeUpload']=$_POST['typeUpload'];
    	$sql="select * from bannershome_video where id='1' "; 
   		if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
   		$anchoIMG = $a['width_poster'];
   		$altoIMG = $a['height_poster'];
   		$size = $a['weight_poster'];
   		$quality = $a['quality_poster'];
    }
    elseif(isset($_POST['typeUpload']) && $_POST['typeUpload']=='innerHeader')
    {
    	$_SESSION['typeUpload']=$_POST['typeUpload'];
    	$sql="select * from bannershome_options where idx='1' "; 
   		if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
   		$anchoIMG = $a['width_inner'];
   		$altoIMG = $a['height_inner'];
   		$size = $a['weight_inner'];
   		$quality = $a['quality_inner'];
    }
    else
    {
    	$_SESSION['typeUpload']='';
    	$sql="select * from bannershome_options where idx='1' "; 
   		if($f=fetchAssoc(ejecutar($sql))){$a=$f; }
   		$anchoIMG = $a['width'];
   		$altoIMG = $a['height'];
   		$size = $a['weight'];
   		$quality = $a['quality'];
    }
    if(!file_exists("../tmp/")){@mkdir("../tmp/");}
	$Folder = '../tmp/';
	$type='';
	$extension='';
	if (!empty($_FILES)) 
	{
		$tempFile = $_FILES['banner']['tmp_name'];	  
        $nombre_foto = "img_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999);
	    $targetFile =  $Folder .  basename($nombre_foto.'.jpg');
	    $size_file= $_FILES['banner']['size']/1024/1024; // bytes -> Kb -> Mb
	    $imagen = getimagesize($_FILES['banner']['tmp_name']);    //Sacamos la información
  		$ancho = $imagen[0];              //Ancho
  		$alto = $imagen[1];               //Alto
 
		if($size_file > $size)
		{
			$r['msj']=isset($dataGlobal['error_max_file']) ? $dataGlobal['error_max_file'] : 'Error max file size';
			$r['msj'].=' ('.($size).' MB)';
			$r['alert']=$r['msj'];
		}
		elseif($_FILES['banner']['type'] !='image/jpeg')
		{
			$r['msj']=isset($dataGlobal['error_file_jpg']) ? $dataGlobal['error_file_jpg'] : 'format not allowed, it must be jpeg / jpg';	
			$r['alert']=$r['msj'];
		}
		elseif($ancho < $anchoIMG || $alto < $altoIMG)
		{
			$r['msj']=isset($dataGlobal['error_resolution']) ? $dataGlobal['error_resolution'] : 'Low resolution image. Your image must be width ';
			$r['msj'].=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width';
			$r['msj'].=' '.$anchoIMG.'px ';
			$r['msj'].=isset($dataGlobal['label_height']) ? $dataGlobal['label_height'] : 'Height';
			$r['msj'].=' '.$altoIMG.'px ';
			$r['msj'].=isset($dataGlobal['or_higher']) ? $dataGlobal['or_higher'] : ' or higher';
			$r['alert']=$r['msj'];
		}
		else
		{		    
		    if(!move_uploaded_file($tempFile,$targetFile))
		    {
		    	$r['msj']=isset($dataGlobal['labe_error']) ? $dataGlobal['labe_error'] :'error';
		    }
		    else
		    {
		    	$_SESSION['img_banner']=basename($nombre_foto.'.jpg');
	        	$nombre_foto2 = basename("preview" . $nombre_foto.'.jpg');
		    	$img_preview =  basename($Folder .  $nombre_foto2);
		    	//creando vista previa
	            cortes_custom($Folder, $Folder, $_SESSION['img_banner'], $nombre_foto2, $q=30, $w=900, $h=450, $relativo=1);
	    		$r['preview']=$img_preview;
	    		$r['img_banner']=$_SESSION['img_banner'];
	    		$r['result']=true;
		    }
		}
	}
}
	echo json_encode($r);
?>