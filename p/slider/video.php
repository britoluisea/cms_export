<?php
  $sql="SELECT * FROM bannershome_video where id = 1";
  $q = ejecutar($sql);
  $v = fetchArray($q);
  $autoplay=$v['autoplay'];
  $muted=$v['muted'];
  $poster=$v['poster'];
  $video=$v['video'];
  $table_ipad=$v['table_ipad'];
  $mobile=$v['mobile'];
  $descrip_video=stripslashes($v['descrip_video']);
  $activo=$v['activo'];
?>  
  <form  method="POST" role="form">
    <p class="text-right">
      <button type="submit" id="guardarVideo" class="btn btn-primary"><i class="fa fa-save"></i> <?=isset($dataGlobal['video_header_save']) ? $dataGlobal['video_header_save'] : 'Save Video'?></button>
    </p>
    <div class="col-sm-12">
      <div style="font-size:20px; color:#2679b5">
        <strong><?=isset($dataGlobal['video_header_options']) ? $dataGlobal['video_header_options'] : 'Optiones Video'?></strong>
      </div>
      <table width="40%" style="margin-right: 5%;" border="0" align="left" cellpadding="8" cellspacing="0">
        <tr><td colspan="4"><br></td></tr>
        <tr>
            <td width="120px" class="h4"><?=isset($dataGlobal['auto_play']) ? $dataGlobal['auto_play'] : 'Auto Play'?>:<span class="asterisco">*</span></td>
            <td align="center" width="80px">
              <button type="button" class="btn btn-default" id="stop" style="width:100%;">
                <i class="fa fa-pause fa-lg"></i>
              </button>
          </td>
          <td align="center" width="80px" valign="middle">
            <!-- Rectangular switch -->
            <label class="switch">
              <input type="checkbox" id="autoplay" <?php if($autoplay==1){echo "checked='true'";}?> >
              <span class="slider"></span>
            </label>
          </td>
          <td align="center" width="80px">
              <button type="button" class="btn btn-default" id="play" style="width:100%;">
              <i class="fa fa-play fa-lg"></i><br>
            </button>
          </td>
        </tr>
        <tr><td colspan="4"><br></td></tr>
        <tr>
            <td width="120px" class="h4"><?=isset($dataGlobal['label_muted']) ? $dataGlobal['label_muted'] : 'Muted'?>:<span class="asterisco">*</span></td>
            <td align="center" width="80px">
              <button type="button" class="btn btn-default" style="width:100%;">
                <i class="fa fa-volume-off fa-lg"></i>
              </button>
          </td>
          <td align="center" width="80px" valign="middle">
            <!-- Rectangular switch -->
            <label class="switch">
              <input type="checkbox" id="muted" <?php if($muted==1){echo "checked='true'";}?>  >
              <span class="slider"></span>
            </label>
          </td>
          <td align="center" width="80px">
              <button type="button" class="btn btn-default" style="width:100%;">
              <i class="fa fa-volume-up fa-lg"></i><br>
            </button>
          </td>
        </tr>
        <tr><td colspan="4"><br></td></tr>
        <tr>
            <td width="120px" class="h4"><?=isset($dataGlobal['label_poster']) ? $dataGlobal['label_poster'] : 'Poster'?>:<span class="asterisco">*</span></td>
            <td colspan="3">
              <button type="button" class="btn btn-default" data-toggle="modal" href='#imgPoster' style="width: 100%"><i class="fa fa-image"></i> <?=isset($dataGlobal['cover_image']) ? $dataGlobal['cover_image'] : 'Cover image'?></button>
              <input type="hidden" name="imagen" id="imagen" value='<?=$poster?>'>
            </td>
        </tr>
        <tr><td colspan="4"><br></td></tr>
        <tr>
            <td width="120px" class="h4"><?=isset($dataGlobal['label_video']) ? $dataGlobal['label_video'] : 'Video'?>:<span class="asterisco">*</span></td>
            <td colspan="3">
              <button type="button" class="btn btn-default bmd-modalButton" data-toggle="modal" data-bmdSrc="addVideo.php" data-bmdWidth="640px" data-bmdHeight="200px" data-target="#iframeModal"  data-Scrolling="no"  data-bmdVideoFullscreen="true"  style="width: 100%"><i class="fa fa-file-video-o"></i> <?=isset($dataGlobal['btn_update']) ? $dataGlobal['btn_update'] : 'Upload'?></button>

              <button type="button" id="showVideo" class="btn btn-default bmd-modalButton" data-toggle="modal" data-bmdSrc="<?=SERVER?>imgcms/headers/video/video.mp4" data-bmdWidth="640px" data-bmdHeight="360px" data-target="#iframeModal"  data-Scrolling="no"  data-bmdVideoFullscreen="true"  style="width: 100%; margin-top: 10px; display: <?php if($video!=''){echo'block';}else{echo'none';}?>">
                <i class="fa fa-eye"></i> <?=isset($dataGlobal['btn_preview']) ? $dataGlobal['btn_preview'] : 'Preview'?></button>
              <input type="hidden" name="video" id="video" value='<?=$video?>'>
            </td>
        </tr>
      </table>
      <table width="40%"  border="0" align="left" cellpadding="8" cellspacing="0">
        <tr><td colspan="4"><br></td></tr>        
        <tr>
          <td width="120px" class="h4"><?=isset($dataGlobal['label_activate']) ? $dataGlobal['label_activate'] : 'Activate'?>:<span class="asterisco">*</span></td>
          <td align="center" width="80px">
              <button type="button" class="btn btn-default" style="width:100%;">
                <i class="fa fa-minus-square   fa-lg"></i>
              </button>
          </td>
          <td align="center" width="80px" valign="middle">
            <!-- Rectangular switch -->
            <label class="switch">
              <input type="checkbox" id="activo" <?php if($activo==1){echo "checked='true'";}?>  >
              <span class="slider"></span>
            </label>
          </td>
          <td align="center" width="80px">
            <button type="button" class="btn btn-default" style="width:100%;">
              <i class="fa fa-check-square fa-lg"></i><br>
            </button>
          </td>
        </tr>
        <tr><td colspan="4"><br></td></tr>
        <tr>
          <td width="120px" class="h4"><?=isset($dataGlobal['tablet_ipad']) ? $dataGlobal['tablet_ipad'] : 'Tablet or Ipad'?>:<span class="asterisco">*</span></td>
          <td align="center" width="80px">
              <button type="button" class="btn btn-default" style="width:100%;">
                <i class="fa fa-eye-slash fa-lg"></i>
              </button>
          </td>
          <td align="center" width="80px" valign="middle">
            <!-- Rectangular switch -->
            <label class="switch">
              <input type="checkbox" id="table_ipad" <?php if($table_ipad==1){echo "checked='true'";}?>  >
              <span class="slider"></span>
            </label>
          </td>
          <td align="center" width="80px">
            <button type="button" class="btn btn-default" style="width:100%;">
              <i class="fa fa-eye fa-lg"></i><br>
            </button>
          </td>
        </tr>
        <tr><td colspan="4"><br></td></tr>   
        <tr>
          <td width="120px" class="h4"><?=isset($dataGlobal['label_mobile']) ? $dataGlobal['label_mobile'] : 'Mobile'?>:<span class="asterisco">*</span></td>
          <td align="center" width="80px">
              <button type="button" class="btn btn-default" style="width:100%;">
                <i class="fa fa-eye-slash fa-lg"></i>
              </button>
          </td>
          <td align="center" width="80px" valign="middle">
            <!-- Rectangular switch -->
            <label class="switch">
              <input type="checkbox" id="mobile" <?php if($mobile==1){echo "checked='true'";}?>  >
              <span class="slider"></span>
            </label>
          </td>
          <td align="center" width="80px">
            <button type="button" class="btn btn-default" style="width:100%;">
              <i class="fa fa-eye fa-lg"></i><br>
            </button>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-sm-12">      
      <script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
      <script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          var window_height = 350; 
          var newCKEdit = CKEDITOR.replace('descrip_video',{height:window_height,allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
          CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
        });
      </script>
      <div style="font-size:20px; color:#2679b5">
        <strong><?=isset($dataGlobal['box_video']) ? $dataGlobal['box_video'] : 'Box Video'?></strong>
      </div>      
      <div class="col-sm-12">
        <?php 
          include dirname(__FILE__). "/../system/btn-icons.php";
          addBtnIcons($idEditor='CKEDITOR.instances.descrip_video');
        ?>
      </div>
      <textarea name="descrip_video" id="descrip_video" ><?=stripslashes($descrip_video)?></textarea>      
    </div>
  </form>
<div class="modal fade" id="imgPoster">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?=isset($dataGlobal['cover_image']) ? $dataGlobal['cover_image'] : 'Cover image'?></h4>
      </div>
      <div class="modal-body">
        <div id="formDropZone" class="bg-dropzone" style="margin-bottom: 10px;"></div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="weight_poster" value="<?=$weight_poster?>">
