<table width="100%" align="center" border="0" cellspacing="2" cellpadding="2" >
					<tbody>
						<tr>
							<td height="60">
								<a href="form-add.php" class="btn btn-small btn-primary"><i class="fa fa-plus"></i> <?=isset($dataGlobal['add_new_slide']) ? $dataGlobal['add_new_slide'] : 'Add new slide'?></a>
							</td>
							<td valign="top">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<table  align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered sorted_table">
					<thead>
						<tr>
							<th width="50" ><div align="center"><?=isset($dataGlobal['label_item']) ? $dataGlobal['label_item'] : 'Item'?></div></th>
							<th width="60" ><div align="center"><?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover'?></div></th>
							<th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></div></th>
							<th width="70" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></div></th>
							<th width="60" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_up']) ? $dataGlobal['label_up'] : 'Up'?></div></th>
							<th width="60" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_dw']) ? $dataGlobal['label_dw'] : 'Dw'?></div></th>
							<th width="80" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Edit'?></div></th>
							<th width="80" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></div></th>
						</tr>
					</thead>
					<tbody  id="banner_tbody">
						<?php
							$sql="select * from bannershome where page='0'  order by item asc";	
							$consulta=ejecutar($sql);
							$num_reg=numRows($consulta);
							if($num_reg == 0)
							{?>
								<tr><td colspan="8"  align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record'?></td></tr>
								<?php
							}
							else
							{?>
								<tr><td colspan="8" align="center"><?=isset($dataGlobal['loading']) ? $dataGlobal['loading'] : 'Loading'?>...</td></tr>
								<?php
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th width="50" ><div align="center"><?=isset($dataGlobal['label_item']) ? $dataGlobal['label_item'] : 'Item'?></div></th>
							<th width="60" ><div align="center"><?=isset($dataGlobal['label_cover']) ? $dataGlobal['label_cover'] : 'Cover'?></div></th>
							<th class="grilla_encab"><div align="left"><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></div></th>
							<th width="70" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></div></th>
							<th width="60" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_up']) ? $dataGlobal['label_up'] : 'Up'?></div></th>
							<th width="60" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_dw']) ? $dataGlobal['label_dw'] : 'Dw'?></div></th>
							<th width="80" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Edit'?></div></th>
							<th width="80" class="grilla_encab"><div align="center"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></div></th>
						</tr>
					</tfoot>
				</table>