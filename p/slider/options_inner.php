  <div class="col-sm-4">  
    <table width="100%" border="0" cellpadding="2" cellspacing="2" align="left">
      <tr><td colspan="3" align="left">&nbsp;</td></tr>
        <tr>
          <td colspan="3" >
            <div  style="font-size:20px; color:#2679b5">
              <strong><?=isset($dataGlobal['inner_header_options']) ? $dataGlobal['inner_header_options'] : 'Options Inner Header'?></strong>
            </div>
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td colspan="3" align="left">
            <button id="save_options_inner" type="button" class="btn btn-primary"  >
              <i class="fa fa-save "></i> <?=isset($dataGlobal['inner_header_save']) ? $dataGlobal['inner_header_save'] : 'Save Inner Header'?>
            </button>
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td width="100"><div align="left"><?=isset($dataGlobal['label_width']) ? $dataGlobal['label_width'] : 'Width'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">          
            <div class="value-range" id="width_inner-value"></div>
            <input type="range" id="width_inner" value="<?=$width_inner?>" min="500" max="2500"  step="10" class="slider-range width_inner">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td width="100"><div align="left"><?=isset($dataGlobal['label_height']) ? $dataGlobal['label_height'] : 'Height'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">          
            <div class="value-range" id="height_inner-value"></div>
            <input type="range" id="height_inner" value="<?=$height_inner?>" min="100" max="1650"  step="10" class="slider-range height_inner">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr><tr>
          <td><div align="left"><?=isset($dataGlobal['label_quality']) ? $dataGlobal['label_quality'] : 'Quality'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">
            <div class="value-range" id="quality_inner-value"></div>
            <input type="range" id="quality_inner" value="<?=$quality_inner?>" min="5" max="100"  step="5" class="slider-range quality_inner">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td><div align="left"><?=isset($dataGlobal['label_max_file_img']) ? $dataGlobal['label_max_file_img'] : 'Max file Img'?>:</div></td>
          <td>&nbsp;</td>
          <td valign="top">            
            <div class="value-range" id="weight_inner-value"></div>
            <input type="range" id="weight_inner" value="<?=$weight_inner?>" min="1" max="20"  step="1" class="slider-range weight_inner">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
        <tr>
          <td ><?=isset($dataGlobal['label_overlay']) ? $dataGlobal['label_overlay'] : 'Overlay'?>:<span class="asterisco">*</span></td>
          <td>&nbsp;</td>
          <td align="center">
            <div class="value-range" id="overlay_inner-value"></div>
            <input type="range" id="overlay_inner" value="<?=$overlay_inner?>" min="0" max="1" value="50" step="0.10" class="slider-range overlay">
          </td>
        </tr>
        <tr><td colspan="3" >&nbsp;</td></tr>
    <tr>
      <td><div align="left"><?=isset($dataGlobal['label_layout']) ? $dataGlobal['label_layout'] : 'Layout'?>:</div></td>
      <td>&nbsp;</td>
      <td valign="top">
        <select name="template_inner" id="template_inner" class="form-control">
          <?php
            $dir =  dirname(__FILE__)."/../../".$_SESSION['THEME']."innerHeader/"; 
            chdir ($dir);  ///vaciar carpeta de inner
            $dh=opendir('.');
            while ($file = readdir ($dh)) {
                  if ($file != "." && $file != "..") { ?>
                  <option value="<?=$file?>" <?php if($template_inner==$file) { echo "selected='selected'"; } ?>><?=strtoupper(substr($file, 0,-4))?></option>
                  <?php }
            }
            closedir($dh);
          ?>
        </select>
      </td>
    </tr>
    <tr><td colspan="3" align="left">&nbsp;</td></tr>
    </table>
  </div>