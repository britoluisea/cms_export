  <?php
      $sql_1="select * from bannershome_options where idx='1' "; //Devuelve todos los Idiomas
      $consulta_1=ejecutar($sql_1);
      $atabla=array();
      if($fila_1=fetchAssoc($consulta_1))
      { $atabla=$fila_1; }
      $width_slider=$atabla['width'];
      $height_slider=$atabla['height'];
      $quality_slider=$atabla['quality'];
      $weight_slider=$atabla['weight'];
      $overlay_slider=$atabla['overlay'];
      $speed=$atabla['speed'];
      $direction=$atabla['direction'];
      $arrows=$atabla['arrows'];
      $ico_nav=$atabla['ico_nav'];
      $effect=$atabla['effect'];
      $template_slider=$atabla['template_slider'];
      // Inner Header
      $width_inner=$atabla['width_inner'];
      $height_inner=$atabla['height_inner'];
      $quality_inner=$atabla['quality_inner'];
      $weight_inner=$atabla['weight_inner'];
      $overlay_inner=$atabla['overlay_inner'];
      $template_inner=$atabla['template_inner'];
  ?>
  <div class="row">
    <?php include "options_slider.php"; ?>
    <?php include "options_video.php"; ?>
    <?php include "options_inner.php"; ?>
  </div>
  <script>
    $(document).ready(function()
    { 
      // options slider  
      sliderRangeVal(q("#width_slider"), q("#width_slider-value"), ' px');
      sliderRangeVal(q("#height_slider"), q("#height_slider-value"), ' px');
      sliderRangeVal(q("#quality_slider"), q("#quality_slider-value"), ' %');
      sliderRangeVal(q("#weight_slider"), q("#weight_slider-value"), ' MB');
      sliderRangeVal(q("#overlay_slider"), q("#overlay_slider-value"), '');
      sliderRangeVal(q("#speed"), q("#speed-value"), 'sec');
      // options inner header
      sliderRangeVal(q("#width_inner"), q("#width_inner-value"), ' px');
      sliderRangeVal(q("#height_inner"), q("#height_inner-value"), ' px');
      sliderRangeVal(q("#quality_inner"), q("#quality_inner-value"), ' %');
      sliderRangeVal(q("#weight_inner"), q("#weight_inner-value"), ' MB');
      sliderRangeVal(q("#overlay_inner"), q("#overlay_inner-value"), '');
      // options video
      sliderRangeVal(q("#width_poster"), q("#width_poster-value"), ' px');
      sliderRangeVal(q("#height_poster"), q("#height_poster-value"), ' px');
      sliderRangeVal(q("#quality_poster"), q("#quality_poster-value"), ' %');
      sliderRangeVal(q("#weight_poster"), q("#weight_poster-value"), ' MB');
      sliderRangeVal(q("#weight_video"), q("#weight_video-value"), ' MB');
      sliderRangeVal(q("#overlay_video"), q("#overlay_video-value"), '');
    });
</script>