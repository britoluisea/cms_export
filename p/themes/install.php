<?php include(dirname(__FILE__)."/../header.php"); ?>
<div class="col-sm-3" style=" padding-left: 0px;">
	<?php include(dirname(__FILE__)."/../web-master/menu.php"); ?>
</div>
<div class="col-sm-8">
	<h2 class="h2"><?=isset($dataGlobal['installer_template']) ?  $dataGlobal['installer_template'] : 'Installer Template'?></h2>
	<table width="100%" border="0">
		<tr>
			<td>
				<div id="formDropZone" class="bg-dropzone" style="margin-bottom: 10px;"></div>
			</td>
			
		</tr>
	</table>
</div>
<link href="<?=CSS_P?>dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?=JS_P?>dropzone.js"></script> 
<script>
	var print_msj = function(msjs){ 
		var msjs_js ={
		"btn_remove"		:		"<?=isset($dataGlobal['btn_remove']) ?  $dataGlobal['btn_remove'] : 'Remover'?>",
		"msj_drop_js"		:		"<?=isset($dataGlobal['msj_drop_js']) ?  $dataGlobal['msj_drop_js'] : 'Drop files'?>",
		"msj_drop_js2"		:		"<?=isset($dataGlobal['msj_drop_js2']) ?  $dataGlobal['msj_drop_js2'] : 'to upload'?>",
		"msj_drop_js3"		:		"<?=isset($dataGlobal['msj_drop_js3']) ?  $dataGlobal['msj_drop_js3'] : 'Or Click'?>",
		"msj_drop_js4"		:		"<?=isset($dataGlobal['msj_drop_js4']) ?  $dataGlobal['msj_drop_js4'] : 'has exceeded the file limit'?>",
		"msj_drop_js5"		:		"<?=isset($dataGlobal['msj_drop_js5']) ?  $dataGlobal['msj_drop_js5'] : 'Server Timeout'?>",
		"msj_theme_js"		:		"<?=isset($dataGlobal['msj_theme_js']) ?  $dataGlobal['msj_theme_js'] : 'Successful upload'?>",
		"msj_theme_js2"		:		"<?=isset($dataGlobal['msj_theme_js1']) ?  $dataGlobal['msj_theme_js1'] : 'Extracting installation files, please wait'?>",
		"msj_theme_js2"		:		"<?=isset($dataGlobal['msj_theme_js2']) ?  $dataGlobal['msj_theme_js2'] : 'Extraction NOT completed'?>",
		"msj_theme_js3"		:		"<?=isset($dataGlobal['msj_theme_js3']) ?  $dataGlobal['msj_theme_js3'] : 'Extraction completed successfully'?>",
		"msj_theme_js4"		:		"<?=isset($dataGlobal['msj_theme_js4']) ?  $dataGlobal['msj_theme_js4'] : 'Do you want to activate the installed theme?'?>",
		"msj_theme_js5"		:		"<?=isset($dataGlobal['msj_theme_js5']) ?  $dataGlobal['msj_theme_js5'] : 'Activation completed successfully'?>",
		"msj_theme_js6"		:		"<?=isset($dataGlobal['msj_theme_js6']) ?  $dataGlobal['msj_theme_js6'] : 'could not be activated, activate manually from the option activate template'?>"
		};
		//debugger;
		return msjs_js[msjs]; 
	}
</script>
<script src="js/config.js"></script>
<script>
Dropzone.autoDiscover = false;
$(document).ready(function(){cargarInstalador();});
</script>
<?php include(dirname(__FILE__)."/../footer.php"); ?>