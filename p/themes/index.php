<?php include(dirname(__FILE__)."/../header.php"); ?>
<div class="col-sm-3" style=" padding-left: 0px;">
	<?php include(dirname(__FILE__)."/../web-master/menu.php"); ?>
</div>
<div class="col-sm-8">
	<h2 class="h2"><?=isset($dataGlobal['template_active']) ?  $dataGlobal['template_active'] : 'Active Template'?></h2>
	<table width="100%" border="0">
		<tr>
			<td>
				<?php
					
					$dir= '../../template/';
					if ($handle = opendir("$dir"))
					{
						$result = true;
						while ((($dir_theme=readdir($handle))!==false) && ($result))
						{
							if ($dir_theme!='.' && $dir_theme!='..')
							{
								if (is_dir("$dir/$dir_theme")){
									//echo 'carpeta= '.$dir_theme.'<br>';
									$s="SELECT * from themes WHERE name='".$dir_theme."' AND del=0";
									$q = ejecutar($s);									
									$num = numRows($q);
									if($num > 0)
									{
										while($t=fetchAssoc($q))
										{ ?>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<div class="thumbnail">
													<?php 
														$folder = '../../template/'.$t['name'];
														$infoTheme = $folder.'/infoTheme.json';
														$iconCms = $_SESSION['SERVER'].'imgcms/system/icon-cms.png';
														if(!is_file($infoTheme))
														{
															$screenshot=$iconCms;
															$name = $t['name'];
															$info = '';
														}
														else
														{
															$json = file_get_contents($infoTheme);
															$data = json_decode($json, true);
															if(!is_file($folder.'/'.$data['screenshots']))
															{$screenshot=$iconCms;}
															else{$screenshot=$folder.'/'.$data['screenshots'];}
															$name = $data['Theme_Name'];
															$zip =  $t['name'].'.zip';
															$info=$data['Description'];
														}
													?>
													<img src="<?=$screenshot?>" alt="screenshots">
													<div class="caption">
														<?php

														?>
														<h3 class="h3"><?=$name?></h3>
														<p>
															<?=$info?>
														</p>
														<p>
															<?php
																$disabled='';
																$on='primary';
																$check='<i class="fa fa-check-square-o"></i> ';
																$onclick="onclick='activateTheme(\"$zip\")';";
																if($t['active']==1)
																{
																	$disabled=''; 
																	$on='success';
																	$check='<i class="fa fa-check-circle"></i> ';
																}
															?>
															<button class="btn btn-<?=$on?>" <?=$onclick?> ><?=$check?>Active</button>
															<button type="button" class="btn btn-default bmd-modalButton" data-toggle="modal" data-bmdsrc="infoTheme.php?json=<?=$infoTheme?>" data-bmdwidth="900px" data-bmdheight="400px" data-target="#iframeModal" data-scrolling="no" data-bmdvideofullscreen="true">
																<i class="fa fa-info-circle"></i> <?=isset($dataGlobal['label_info']) ?  $dataGlobal['label_info'] : 'Info'?> 
															</button>
														</p>
													</div>
												</div>
											</div>
											<?php
										}
									}
									else
									{
										?>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
												<div class="thumbnail">
													<?php 
														$folder = '../../template/'.$dir_theme;
														$infoTheme = $folder.'/infoTheme.json';
														$iconCms = $_SESSION['SERVER'].'imgcms/system/icon-cms.png';
														if(!is_file($infoTheme))
														{
															$screenshot=$iconCms;
															$name = $dir_theme;
															$info = '';
														}
														else
														{
															$json = file_get_contents($infoTheme);
															$data = json_decode($json, true);
															if(!is_file($folder.'/'.$data['screenshots']))
															{$screenshot=$iconCms;}
															else{$screenshot=$folder.'/'.$data['screenshots'];}
															$name = $data['Theme_Name'];
															$zip =  $dir_theme.'.zip';
															$info=$data['Description'];
														}
													?>
													<img src="<?=$screenshot?>" alt="screenshots">
													<div class="caption">
														<?php

														?>
														<h3 class="h3"><?=$name?></h3>
														<p>
															<?=$info?>
														</p>
														<p>
															<?php
																$disabled='';
																$on='primary';
																$check='<i class="fa fa-check-square-o"></i> ';
																$onclick="onclick='activateTheme(\"$zip\")';";
															?>
															<button class="btn btn-<?=$on?>" <?=$onclick?> ><?=$check?>Active</button>
															<button type="button" class="btn btn-default bmd-modalButton" data-toggle="modal" data-bmdsrc="infoTheme.php?json=<?=$infoTheme?>" data-bmdwidth="900px" data-bmdheight="400px" data-target="#iframeModal" data-scrolling="no" data-bmdvideofullscreen="true">
																<i class="fa fa-info-circle"></i> <?=isset($dataGlobal['label_info']) ?  $dataGlobal['label_info'] : 'Info'?>
															</button>
														</p>
													</div>
												</div>
											</div>
											<?php
									}
								}
							}
						}
					}
					closedir($handle);			
				?>
				
			</td>
			
		</tr>
	</table>
</div>
<script>
	var print_msj = function(msjs){ 
		var msjs_js ={
		"btn_remove"		:		"<?=isset($dataGlobal['btn_remove']) ?  $dataGlobal['btn_remove'] : 'Remover'?>",
		"msj_drop_js"		:		"<?=isset($dataGlobal['msj_drop_js']) ?  $dataGlobal['msj_drop_js'] : 'Drop files'?>",
		"msj_drop_js2"		:		"<?=isset($dataGlobal['msj_drop_js2']) ?  $dataGlobal['msj_drop_js2'] : 'to upload'?>",
		"msj_drop_js3"		:		"<?=isset($dataGlobal['msj_drop_js3']) ?  $dataGlobal['msj_drop_js3'] : 'Or Click'?>",
		"msj_drop_js4"		:		"<?=isset($dataGlobal['msj_drop_js4']) ?  $dataGlobal['msj_drop_js4'] : 'has exceeded the file limit'?>",
		"msj_drop_js5"		:		"<?=isset($dataGlobal['msj_drop_js5']) ?  $dataGlobal['msj_drop_js5'] : 'Server Timeout'?>",
		"msj_theme_js"		:		"<?=isset($dataGlobal['msj_theme_js']) ?  $dataGlobal['msj_theme_js'] : 'Successful upload'?>",
		"msj_theme_js2"		:		"<?=isset($dataGlobal['msj_theme_js1']) ?  $dataGlobal['msj_theme_js1'] : 'Extracting installation files, please wait'?>",
		"msj_theme_js2"		:		"<?=isset($dataGlobal['msj_theme_js2']) ?  $dataGlobal['msj_theme_js2'] : 'Extraction NOT completed'?>",
		"msj_theme_js3"		:		"<?=isset($dataGlobal['msj_theme_js3']) ?  $dataGlobal['msj_theme_js3'] : 'Extraction completed successfully'?>",
		"msj_theme_js4"		:		"<?=isset($dataGlobal['msj_theme_js4']) ?  $dataGlobal['msj_theme_js4'] : 'Do you want to activate the installed theme?'?>",
		"msj_theme_js5"		:		"<?=isset($dataGlobal['msj_theme_js5']) ?  $dataGlobal['msj_theme_js5'] : 'Activation completed successfully'?>",
		"msj_theme_js6"		:		"<?=isset($dataGlobal['msj_theme_js6']) ?  $dataGlobal['msj_theme_js6'] : 'could not be activated, activate manually from the option activate template'?>"
		};
		//debugger;
		return msjs_js[msjs]; 
	}
</script>
<script src="js/config.js"></script>
<?php include(dirname(__FILE__)."/../footer.php"); ?>