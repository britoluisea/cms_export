var instalador = function()
{ 
  var myAwesomeDropzone = {
          url: "uploadTheme.php",
          method: "post",
          addRemoveLinks: true,
          paramName: "theme",
          maxFiles: 1, // cantidad
          /*maxThumbnailFilesize: 0,
          maxFileSize: 0,*/
          timeout: 3000000,
          dictRemoveFile: print_msj("btn_remove"),
          uploadMultiple: false,
          acceptedFiles: "application/zip",
          params: {typeUpload:''},
          success: function (file, response) {
            var r = JSON.parse(response);
            var name = file.name;
              if(!r.result){
                file.previewElement.classList.add("dz-error");
                $('.dz-error-message').text(r.msj);
                alerError('Error', r.alert, 3000);
              }
              else
              {
                file.previewElement.classList.add("dz-success");
                alerValid('Success', ''+print_msj("msj_theme_js")+'', 2000);
                extraerZIP(name);
              }
              console.log("Successfully uploaded ");
          },
          error: function (file, response) {
            file.previewElement.classList.add("dz-error");
            console.log('error '+response)
          },
          removedfile: function(file, serverFileName)
          {
            var name = file.name;
                  var element;
                  (element = file.previewElement) != null ? 
                  element.parentNode.removeChild(file.previewElement) : 
                  false;
                  $("#dZUpload").removeClass('dz-started'); 
                  $("#dZUpload").removeClass('dz-max-files-reached'); 
                  var message ='<span class="text-center">';
                  message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
                  message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
                  message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
                  message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
                  $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
                  borrar_theme(name);
          },
          maxfilesexceeded: function(file) {
                  alerError('Error', ''+print_msj("msj_drop_js4")+'', 3000);
                  var name = file.name;
                  var element;
                  (element = file.previewElement) != null ? 
                  element.parentNode.removeChild(file.previewElement) : 
                  false;
          },
          uploadprogress: function(file, progress, bytesSent, xhr){
            //console.log('progress= '+progress);               
            $('span.dz-upload').css('width', progress);
          }
  } // FIN myAwesomeDropzone
  return myAwesomeDropzone;
}
var cargarInstalador =function()
{ 
    var myAwesomeDropzone = instalador();
    myAwesomeDropzone.params.typeUpload='';
    $("#formDropZone").html("<form id='dZUpload' method='post' class='dropzone borde-dropzone' style='cursor: pointer;' enctype='multipart/form-data'></form>");      
      var message ='<span class="text-center">';
      message+='<span class="font-lg visible-xs-block visible-sm-block visible-lg-block">';
      message+='<span class="font-lg"><i class="fa fa-caret-right text-danger"></i> ';
      message+=''+print_msj("msj_drop_js")+' <span class="font-xs">'+print_msj("msj_drop_js2")+'</span></span><span>&nbsp;&nbsp;';
      message+='<h4 class="display-inline h4"> ('+print_msj("msj_drop_js3")+')</h4></span></span></span>';
      $("#dZUpload").html('<div class="dz-default dz-message"><span>'+message+'</span></div>');
      var myDropzone = new Dropzone("#dZUpload", myAwesomeDropzone); 
      myDropzone.on("complete", function(file,response) {
        $('.dz-remove').html('<i class="fa fa-trash"></i> Remove');
      });
      myDropzone.on("sending", function(file, xhr, formData) {
      /*Called just before each file is sent*/
        xhr.ontimeout = (() => {
          /*Execute on case of timeout only*/
            console.log('Server Timeout');
            $('#divSmallBoxes').html('');
            alerError('Error', ''+print_msj("msj_drop_js5")+'', 9000);
        });
      });
}
var borrar_theme = function(name){
  $.get('borrar_theme.php?name='+name, function(data) {
    console.log(data);
  });
}
var extraerZIP = function(name){
  alerAct('Loading...', ''+print_msj("msj_theme_js1")+'', 2000);
  $.get('extraerZIP.php?name='+name, function(data) {
    console.log(data);
    if(data=='error')
    {
      alerError('Error', ''+print_msj("msj_theme_js2")+'', 5000); 
      borrar_theme(name);
      borrarFolderTheme(name);
    }
    else if(data=='error2')
    {
      alerValid('Success', ''+print_msj("msj_theme_js3")+'', 2000); 
    }
    else
    {
      alerValid('Success', ''+print_msj("msj_theme_js3")+'', 2000);
      alerConfirm('Activate theme', ''+print_msj("msj_theme_js4")+'', '"'+name+'"', 'activateTheme');
    }
  });
}
var borrarFolderTheme = function(name){
  //debugger;
  $.get('borrarFolderTheme.php?name='+name, function(data) {
    console.log(data);
  });
}
var activateTheme = function(name){
  $.get('activateTheme.php?name='+name, function(data) {
    console.log(data);
    if(data=='ok')
    {      
      alerValid('Success', ''+print_msj("msj_theme_js5")+'', 1000);
      setTimeout(function(){ window.location.href='index.php';},1000);
    }
    else
    {
      alerError('Error', ''+print_msj("msj_theme_js6")+'', 5000); 
    }
  });
}
  window.EliminarTheme = function(name){  
     borrarFolderTheme(name);
     borrar_theme(name);
     window.location.href='index.php'
   };