<?php 
require_once("../header_box.php");
$infoTheme = isset($_GET['json']) ? $_GET['json'] : '';
$f = date('Y-m-d H:i:s');
if(!is_file($infoTheme))
{
	$msj=isset($dataGlobal['not_available']) ?  $dataGlobal['not_available'] : 'Not available';
	echo'<h2 class="h2"></h2>';
}
else
{

	$json = file_get_contents($infoTheme);
	$data = json_decode($json, true);
	$folder = '../../template/'.$data['Theme_Name'];
	$iconCms = $_SESSION['SERVER'].'imgcms/system/icon-cms.png';
	if(!is_file($folder.'/'.$data['screenshots']))
	{$screenshot=$iconCms;}
	else{$screenshot=$folder.'/'.$data['screenshots'];}
	?>
	<div class="col-sm-6">
		<img src="<?=$screenshot?>" alt="screenshot" class="img-responsive">
	</div>
	<div class="col-sm-6">
	<?php	
		foreach ($data as $key => $value) {
			echo '<h4 class="h4"><strong>'.$key.'</strong> : '.$value.'</h4>';
		}
	?>
	</div>
	<button onclick="window.parent.EliminarTheme('<?=$data['Theme_Name']?>.zip');" type="button" class="btn btn-danger">
		<?=isset($dataGlobal['btn_borrar_theme']) ?  $dataGlobal['btn_borrar_theme'] : 'Delete Theme'?>
	</button><?php
}
include("../foot.php");?>