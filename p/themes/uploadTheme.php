<?php 
session_start();
include "../../cn/cnx.php";
include dirname(__FILE__)."/../system/languages/languages.php";
$r=array();
$r['result']=false;
$r['msj']=false;
$r['alert']=false;
$r['preview']=false;
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    $r['msj']='error, session expiro';
    $r['alert']='error, session expiro';
}
else
{
	//expandiendo memoria
	@ini_set("memory_limit", "3000M");
	@ini_set( 'upload_max_size' , '3000M' );
	@ini_set( 'post_max_size', '3000M');
	@ini_set( 'max_execution_time', '30000' );
    set_time_limit(0);
	$Folder = '../../themesZIP/';
	$type='';
	$extension='';
	if (!empty($_FILES)) 
	{
		$tempFile = $_FILES['theme']['tmp_name'];
 		$targetFile =  $Folder .  basename($_FILES['theme']['name']);
		if(is_file($Folder.$targetFile))
		{
			$r['alert']=isset($dataGlobal['msj_theme']) ?  $dataGlobal['msj_theme'] : 'It is not possible to complete the installation, there is already a template with the same name';		
		}
		else
		{
			if($_FILES['theme']['type'] !='application/zip')
			{
				$r['msj']=isset($dataGlobal['msj2_theme']) ?  $dataGlobal['msj2_theme'] : 'Format not allowed, it must be zip';	
				$r['alert']=isset($dataGlobal['msj2_theme']) ?  $dataGlobal['msj2_theme'] : 'Format not allowed, it must be zip';	
			}
			else
			{		    
			    if(!move_uploaded_file($tempFile,$targetFile))
			    {
			    	$r['msj']=isset($dataGlobal['msj3_theme']) ?  $dataGlobal['msj3_theme'] : 'Error loading template';
			    	$r['alert']=isset($dataGlobal['msj3_theme']) ?  $dataGlobal['msj3_theme'] : 'Error loading template';
			    }
			    else
			    {
		    		$r['result']=true;
			    }
			}	
		}
	}
}
echo json_encode($r);
?>