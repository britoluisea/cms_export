<?php
  include(dirname(__FILE__)."/../header.php");
  $sql="SELECT a.idx AS idx_m, a.name, t.name_type FROM menu_name AS a INNER JOIN menu_type AS t ON a.type=t.idx ORDER BY a.idx ASC ";  
  $consulta=ejecutar($sql);
  	 
  //print_r($fila_1);
?>
      <section style="min-height: 500px;">
        <div class="container">
        	<div class="h3"><?=isset($dataGlobal['marketing']) ? $dataGlobal['marketing'] : 'Marketing'?> / <strong><?=isset($dataGlobal['menu_website']) ? $dataGlobal['menu_website'] : 'Menu Website'?></strong></div>
        	<br>
        	<button type="button" class="btn btn-success bmd-modalButton" data-toggle="modal" data-bmdsrc="menu-n.php" data-bmdwidth="640px" data-bmdheight="280px" data-target="#iframeModal" data-scrolling="no" data-bmdvideofullscreen="true">
                <i class="fa fa-plus"></i> <?=isset($dataGlobal['add_menu']) ? $dataGlobal['add_menu'] : 'Add Menu'?>    
            </button>
        	<br><br>
        	<?php
                if (isset($_SESSION['data_error']) && $_SESSION['data_error'] == 1 )
                { ?>
                  <div class="alert alert-danger text-center" style="width: 400px; margin: 0 auto;"><strong><?=isset($dataGlobal['error_request6']) ? $dataGlobal['error_request6'] : 'Error when processing request'?>.</strong></div>
                  <?php
                  unset ($_SESSION['data_error']);
                }
            ?>
            <table border="0" align="center" class="table table-striped table-bordered sorted_table">
            	<thead>
	                <tr>
	                  <th style="text-align: center;"><?=isset($dataGlobal['label_id']) ? $dataGlobal['label_id'] : 'Id'?></th>
	                  <th><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></th>
	                  <th><?=isset($dataGlobal['label_type']) ? $dataGlobal['label_type'] : 'Type'?></th>
	                  <th style="text-align: center;"><?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions'?></th>
	                </tr>
	            </thead>
                <tbody>
                	<?php
                		$cont=0;
                		while($f=fetchAssoc($consulta))
                		{ 
                			$cont++;
                			?>
                			<tr>
                				<td width="50px"  style="text-align: center;"><?=$cont?></td>
                				<td><a href="menu-list.php?i=<?=$f['idx_m']?>"><?=$f['name']?></a></td>
                				<td><?=$f['name_type']?></td>
                				<td width="100px"  style="text-align: center;">
                					<div class="btn-group">
                						<button data-toggle="dropdown" class="btn btn-small  btn-primary dropdown-toggle text-uppercase" aria-expanded="false">
                							<i class="fa fa-cog"></i> <?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions'?> <b class="caret"></b>
                						</button>
                						<ul class="dropdown-menu pull-right">
                							<li>
                								<a href="#" class="bmd-modalButton" data-toggle="modal" data-bmdsrc="menu-n.php?i=<?=$f['idx_m']?>" data-bmdwidth="640px" data-bmdheight="280px" data-target="#iframeModal" data-scrolling="no" data-bmdvideofullscreen="true">
                									<i class="fa fa-pencil"></i> <?=isset($dataGlobal['edit_menu']) ? $dataGlobal['edit_menu'] : 'Edit Menu'?>
                								</a>
                							</li>
                							<li>
                								<a href="menu-list.php?i=<?=$f['idx_m']?>">
                									<i class="fa fa-list"></i> <?=isset($dataGlobal['menu_item']) ? $dataGlobal['menu_item'] : 'Menu Item'?>
                								</a>
                							</li>
                							<?php if($f['idx_m']>1){ ?>
                							<li>
                								<a href="#" class="delMenu" onclick="confirmDelete(<?=$f['idx_m']?>)">
                									<i class="fa fa-remove "></i> <?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?>
                								</a>
                							</li>
                						<?php } ?>
                						</ul>
					                </div>
                				</td>
                			</tr>
                		  <?php  
                		} 
                	?>
                </tbody>
                <tfoot>
	                <tr>
                    <th style="text-align: center;"><?=isset($dataGlobal['label_id']) ? $dataGlobal['label_id'] : 'Id'?></th>
                    <th><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></th>
                    <th><?=isset($dataGlobal['label_type']) ? $dataGlobal['label_type'] : 'Type'?></th>
                    <th style="text-align: center;"><?=isset($dataGlobal['label_actions']) ? $dataGlobal['label_actions'] : 'Actions'?></th>
	                </tr>
                </tfoot>
            </table>
        </div>
      </section>
      <script>
      	var confirmDelete= function(idx){
  			$('#divSmallBoxes').html('');
  			alerConfirm("<?=isset($dataGlobal['del_menu']) ? $dataGlobal['del_menu'] : 'Delete Menu'?>", "<?=isset($dataGlobal['del_menu_msj']) ? $dataGlobal['del_menu_msj'] : 'are you sure you want to delete this menu?, keep in mind that the options will also be eliminated'?>", idx, 'delMenu');
		}
		var delMenu = function(idx){
		  alerAct("<?=isset($dataGlobal['loading']) ? $dataGlobal['loading'] : 'Loading'?>", "<?=isset($dataGlobal['please_wait']) ? $dataGlobal['please_wait'] : 'Please wait'?>", 0);
		  var datosForm ={};
		  datosForm.opc =3;
		  datosForm.i= idx;
		  $.ajax({
		    url: 'menu-save.php', 
		    type: 'POST',
		    data: datosForm,
		    complete: function(xhr, textStatus) {
		      //called when complete
		      window.location.reload(true);
		    },
		    success: function(data, textStatus, xhr) {
		      //called when successful
		      var r ={};
		      /*if(r= JSON.parse(data))      
		      {
		        var r = JSON.parse(data);
		        if(!r.result)
		        {
		          $('#divSmallBoxes').html('');
		          alerError('Error', 'no se pudo eliminar', 2000);
		        }
		        else
		        {
		          $('#divSmallBoxes').html('');
		          alerValid('Success', 'successful process', 1000);
		        }
		      }
		      else
		      {
		        $('#divSmallBoxes').html('');
		        alerError('Error', 'There was an error processing', 3000);
		      }*/
		      
		    },
		    error: function(xhr, textStatus, errorThrown) {
		      //called when there is an error
		      $('#divSmallBoxes').html('');
		      alerError("<?=isset($dataGlobal['label_error']) ? $dataGlobal['label_error'] : 'Error'?>", "<?=isset($dataGlobal['msj_error_internal_server']) ? $dataGlobal['msj_error_internal_server'] : 'An internal server error has occurred'?>", 2000);
		    }
		  });
		}
      </script>
<?php include(dirname(__FILE__)."/../footer.php");?>
