<?php
require_once("../header_box.php");
$opc = isset($_GET['opc']) ? $_GET['opc'] : '';
$i = isset($_GET['i']) ? $_GET['i'] : '';
$id = isset($_GET['id']) ? $_GET['id'] : '';
if(empty($opc)){ $opc = 1; }
?>
<form action="menu-list-save.php" method="post" name="form1">
  <?php
  $sql_1="SELECT a.name, a.id_content, a.id_router, a.url, a.nivel FROM menu_items AS a LEFT JOIN router AS b ON a.id_router=b.idx  WHERE a.id='".$id."' AND idx_menu='".$i."' ";
  //echo $sql_1;
  $consulta_1=ejecutar($sql_1);
  $a=array();
  $n = numRows($consulta_1);
  if($n > 0){
    $fila_1=fetchAssoc($consulta_1);
    $a=$fila_1;
    $opc = 2;  
  }
?>
<input type="hidden" name="opc" value="<?=$opc?>">
<input type="hidden" name="i" value="<?=$i?>">
<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="id_router" value="<?=isset($a["id_router"]) ? $a["id_router"]: '0'?>">
   <div class="modalcread modalcread-edit">  
        <div class="modal-header ">
            <div class="delete">
              <h3 class="h3" style="margin:0px;">
                <?php if($opc == 1){?>
                    <?=isset($dataGlobal['add_item']) ? $dataGlobal['add_item'] : 'Add Item'?>
                  <?php }else{?>
                    <?=isset($dataGlobal['edit_item']) ? $dataGlobal['edit_item'] : 'Edit Item'?>
                  <?php } ?>
                
              </h3>
            </div>
        </div>
        <div class="modal-body">
            <table width="95%" border="0" align="center" cellpadding="2">
            <tr>
              <td width="80"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
              <td width="10">:</td>
              <td>
                <input name="name" type="text" id="name" class="form-control" value="<?=isset($a["name"]) ? $a["name"]: ''?>"   />
              </td>
            </tr>
            <tr>
              <td><?=isset($dataGlobal['label_url']) ? $dataGlobal['label_url'] : 'Url'?></td>
              <td>:</td>
              <td>               
                <input name="url" type="text" id="url" class="form-control" value="<?=isset($a['url']) ? $a['url'] : ''?>"  />
              </td>
            </tr>
            <tr>
              <td><?=isset($dataGlobal['assign_page']) ? $dataGlobal['assign_page'] : 'Assign page'?></td>
              <td>:</td>
              <td>
                <select name="page" id="page" class="form-control">
                  <option value="0" selected="selected"><?=isset($dataGlobal['label_none']) ? $dataGlobal['label_none'] : 'none'?></option>
                  <?php
                    $sql="SELECT id, titulo FROM informativo WHERE usamarketing > 0 ORDER BY titulo ASC";
                    $q = ejecutar($sql);
                    while($f = fetchAssoc($q))
                    {
                      $s = '';
                      if(isset($a['id_content']) && $a['id_content']== $f['id']){$s='selected="selected"';}
                      ?>
                        <option value="<?=$f['id']?>" <?=$s?>><?=$f['titulo']?></option>
                      <?php   
                    }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>Level</td>
              <td>:</td>
              <td>
                <select name="nivel" id="nivel" class="form-control">
                  <option value="0" selected="selected"><?=isset($dataGlobal['label_none']) ? $dataGlobal['label_none'] : 'none'?></option>
                  <?php
                    $sql="SELECT id, name FROM menu_items WHERE id<>'".$id."' AND nivel = 0 AND idx_menu='".$i."'  ORDER BY name ASC";
                    //echo $sql;
                    $q = ejecutar($sql);
                    while($f = fetchAssoc($q))
                    {
                      $s = '';
                      if(isset($a['nivel']) && $a['nivel'] == $f['id']){$s='selected="selected"';}
                      ?>
                        <option value="<?=$f['id']?>" <?=$s?>> <?=$f['name']?></option>
                      <?php   
                    }
                  ?>
                </select>
              </td>
            </tr>
          </table>
     </div>
        <div class="modal-footer">
          <button name="button" type="submit" class="btn btn-small btn-primary" id="button"><i class="fa fa-save"></i> <?=isset($dataGlobal['bnt_save']) ? $dataGlobal['bnt_save'] : 'Save'?></button>
          <button id="close" type="button" class="btn btn-small btn-default" id="button"><i class="fa fa-close"></i> <?=isset($dataGlobal['btn_close']) ? $dataGlobal['btn_close'] : 'Close'?></button>
        </div>
  </div>
  </form>
  <script>
    $("#close").click(function(event) {
      event.preventDefault();
      window.parent.closeModal_hide(); 
    });
  </script>
<?php include("../foot.php");?>