<?php
  include(dirname(__FILE__)."/../header.php");
  if (isset($_POST['ope']) && $_POST['ope'] == "google-analytics") 
  {
    $sql_0 = "update webmarketing set  googleanalyticscode = '".$_POST['code']."'  where idx = '1' ";
    $res_0 = ejecutar($sql_0);
    $_SESSION['data_guarda_bien'] = 1;
  }
  $sql_1="select googleanalyticscode from webmarketing where idx='1' ";  
  $consulta_1=ejecutar($sql_1);
  $fila_1=fetchAssoc($consulta_1); 
  //print_r($fila_1);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="height100">
  <tr>
    <td width="0%" valign="top">
      <?php include_once 'preferences-menu-universal-meta-tags.php'; ?>
    </td>
    <td width="100%" valign="top">
      <section>
        <div id="contenido">
          <form action="google-analytics.php" method="post" enctype="multipart/form-data" name="form1">
            <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
                <tr>
                  <td>
                    <div class="navipage">
                      <?=isset($dataGlobal['marketing']) ? $dataGlobal['marketing'] : 'Marketing'?> / 
                      <strong>
                        <?=isset($dataGlobal['google_analytics']) ? $dataGlobal['google_analytics'] : 'Google Analytics'?>
                      </strong>
                    </div>
                  </td>
                  <td>
                    <table border="0" align="right" cellpadding="5" cellspacing="0">
                      <tr>
                        <td>
                          <input type="submit" class="btn btn-primary" value="Save" />
                        </td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
            </table>
            <input type="hidden" name="ope" value="google-analytics"/>
            <table width="100%" border="0" align="center">
                <tr>
                  <td width="612" valign="top">
                    <table width="650" border="0" align="center" cellpadding="3" cellspacing="0">
                      <tr>
                        <td valign="top">
                          <?=isset($dataGlobal['google_analytics_ID']) ? $dataGlobal['google_analytics_ID'] : 'Please Insert your PROPERTY ID'?>: <strong></strong>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top">
                          <input type="text" name="code" id="code" value="<?=$fila_1["googleanalyticscode"]?>" style="width:100%" />
                        </td>
                      </tr>
                      <tr><td valign="top">&nbsp;</td></tr>
                      <tr><td valign="top">&nbsp;</td></tr>
                    </table>
                  </td>
                </tr>
            </table>
            <?php
                if (isset($_SESSION['data_guarda_bien']) && $_SESSION['data_guarda_bien'] == 1 )
                { ?>
                  <div class="alert alert-success text-center" style="width: 200px; margin: 0 auto;"><strong><?=isset($dataGlobal['data_saved']) ? $dataGlobal['data_saved'] : 'Data Saved'?></strong></div>
                  <?php
                  unset ($_SESSION['data_guarda_bien']);
                }
            ?>
          </form>
        </div>
      </section>
    </td>
  </tr>
</table>
<?php include(dirname(__FILE__)."/../footer.php");?>
