<?php
require_once("../header_box.php");
$opc = isset($_GET['opc']) ? $_GET['opc'] : '';
$i = isset($_GET['i']) ? $_GET['i'] : '';
if (empty($opc)){ $opc = 1; }
?>
<form action="menu-save.php" method="post" name="form1">
  <?php
  $sql_1="select a.*, t.* from menu_name as a inner join menu_type as t on a.type=t.idx where a.idx='".$i."' ";
  $consulta_1=ejecutar($sql_1);
  $a=array();
  while($fila_1=fetchAssoc($consulta_1))
  {
    $a=$fila_1;
    $opc = 2;  
  }
?>
<input type="hidden" name="opc" value="<?=$opc?>">
<input type="hidden" name="i" value="<?=$i?>">
   <div class="modalcread modalcread-edit">  
        <div class="modal-header ">
            <div class="delete">
              <h3 class="h3" style="margin:0px;">
                <?php if($opc == 1){?>
                  <?=isset($dataGlobal['add_menu']) ? $dataGlobal['add_menu'] : 'Add Menu'?>    
                <?php }else{?>
                  <?=isset($dataGlobal['edit_menu']) ? $dataGlobal['edit_menu'] : 'edit Menu'?>  
                <?php } ?>
              </h3>
            </div>
        </div>
        <div class="modal-body">
            <table width="95%" border="0" align="center" cellpadding="2">
            <tr>
              <td width="50"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
              <td width="10">:</td>
              <td>
                <input name="name" type="text" id="name" class="form-control" value="<?=isset($a["name"]) ? stripcslashes($a["name"]): ''?>"   />
              </td>
            </tr>
            <tr>
              <td><?=isset($dataGlobal['label_type']) ? $dataGlobal['label_type'] : 'Type'?></td>
              <td>:</td>
              <td>
                <select name="type" id="type" class="form-control">
                  <?php
                    $sql="select * from menu_type where idx > 1 order by idx asc ";
                    $q = ejecutar($sql);
                    while($f = fetchAssoc($q))
                    {
                      $s = '';
                      if(isset($a['type']) && $a['type'] == $f['idx']){$s='selected="selected"';}
                      ?>
                        <option value="<?=$f['idx']?>" <?=$s?>><?=$f['name_type']?></option>
                      <?php   
                    }
                  ?>
                </select>
              </td>
            </tr>
          </table>
     </div>
        <div class="modal-footer">
          <button name="button" type="submit" class="btn btn-small btn-primary" id="button"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
          <button id="close" type="button" class="btn btn-small btn-default" id="button"><i class="fa fa-close"></i> <?=isset($dataGlobal['btn_close']) ? $dataGlobal['btn_close'] : 'close'?></button>
        </div>
  </div>
  </form>
  <script>
    $("#close").click(function(event) {
      event.preventDefault();
      window.parent.closeModal(); 
    });
  </script>
<?php include("../foot.php");?>