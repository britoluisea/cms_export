<aside>
   <div id="resize">&laquo;</div><div id="resize2">&raquo;</div>
    <div id="lateral">
      <div id="navl1">
        <h2><?=isset($dataGlobal['marketing']) ? $dataGlobal['marketing'] : 'Marketing'?></h2>
        <ul id="menua" class="genial_menu">
          <li id="menucat1" class="menucat1"><a class="expanded"><?=isset($dataGlobal['universal_meta']) ? $dataGlobal['universal_meta'] : 'Universal Meta Tags'?></a>
            <ul>
                <li><a href="universal-meta-tags.php"><?=isset($dataGlobal['universal_meta']) ? $dataGlobal['universal_meta'] : 'Universal Meta Tags'?></a></li>
                <li><a href="xml-sitemaps.php"><?=isset($dataGlobal['xml_site']) ? $dataGlobal['xml_site'] : 'XML Sitemaps'?></a></li>
                <li><a href="robot-files.php"><?=isset($dataGlobal['robot_file']) ? $dataGlobal['robot_file'] : 'Robot File'?></a></li>
                <li><a href="google-analytics.php"><?=isset($dataGlobal['google_analytics']) ? $dataGlobal['google_analytics'] : 'Google Analytics'?></a></li>
              </ul>
            </li>
        </ul>
      </div>
     </div>
</aside>
