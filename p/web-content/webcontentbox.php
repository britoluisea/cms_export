				<div class="webcontentpage">						
						<div id="tabs">
							<!-- Nav tabs -->
			              <ul class="nav nav-tabs" role="tablist">
			                <li role="presentation" class="active">
			                	<a href="#tabs-1" aria-controls="tabs-1" role="tab" data-toggle="tab"><?=isset($dataGlobal['content_box']) ?  $dataGlobal['content_box'] : 'Content box'?></a>
			                </li>
			                <li role="presentation">
			                	<a href="#tabs-3" aria-controls="tabs-3" role="tab" data-toggle="tab"><?=isset($dataGlobal['label_code']) ?  $dataGlobal['label_code'] : 'Code'?></a>
			                </li>
			              </ul>

			              <!-- Tab panes -->
			              <div class="tab-content">
			                <div role="tabpanel" class="tab-pane active" id="tabs-1">
			                	<a href="<?=$_SERVER['PHP_SELF']?>?i=<?=$i?>&ed=2&buscar=<?=$buscar?>&num=<?=$num?>" title="Edit Web Content" class="btn btn-primary" style="float: right;"><i class="fa fa-edit"></i> <?=isset($dataGlobal['label_edit']) ?  $dataGlobal['label_edit'] : 'Edit'?></a>
			                	<br>
			                  <?php include 'tabs1.php'; ?>
			                </div>
			                <div role="tabpanel" class="tab-pane" id="tabs-3">
								<?php include 'tabs3.php'; ?>
							</div>
			              </div>
            		</div>
            		<!--/ tabs-->
            	</div>
