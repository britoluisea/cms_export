<?php 
$buscar = isset($_GET['buscar']) ? $_GET['buscar'] : ''; 
$num = isset($_GET['num']) ? $_GET['num'] : ''; 
?>
<aside>
   <div id="resize">&laquo;</div>
   <div id="resize2">&raquo;</div>
   <div id="lateral">
      	<div id="navl1">
        	<h2><?=isset($dataGlobal['web_content_box']) ?  $dataGlobal['web_content_box'] : 'Web Content Boxes'?></h2>
        	<ul id="menua" class="genial_menu">
        		<form id="form_busqueda" name="form_busqueda" method="get" action="">
	        		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
	        			<tr>
	        				<td height="29">
	        					<table border="0" cellspacing="3" cellpadding="0" align="center">
	        						<tr>
	        							<td class="estilocampos sidercampo" width="160">
	        								<br>
												<div class="input-group">
													<input type="text" id="buscar" name="buscar" class="form-control" placeholder="<?=isset($dataGlobal['label_search']) ?  $dataGlobal['label_search'] : 'Search'?>..." value="<?php if(isset($buscar) && !empty($buscar)){echo$buscar;} ?>" >
													<span class="input-group-btn">
														<button class="btn btn-default" onclick="javascript:form_busqueda();" type="button" style="background-color: #f9f9f9;"><i class="fa fa-search"></i></button>
													</span>
											    </div><!-- /input-group -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</form>
				</ul>
				<h2 style="display: none;"><?=isset($dataGlobal['web_content_box']) ?  $dataGlobal['web_content_box'] : 'Web Content Boxes'?></h2>
				<ul id="menua" class="genial_menu">
					<li id="menucat2" class="menucat1"><a class="expanded" style="display: none;"></a>
						<ul>
							<?php
								$t="informativo";
								if(!empty($buscar))
								{
									$sql="select * from $t where activo = 1 and usamarketing = 0 and (titulo like '%$buscar%' ) order by titulo ";
								}
								else
								{
									$sql="select * from $t where activo = 1 and usamarketing = 0 AND theme_idx='".$_SESSION['THEME_IDX']."'  order by titulo ";
								}
								$consulta=ejecutar($sql);
								$num_registros=numRows($consulta);
								if($num_registros==0)
								{
								}
								$registros=30;
								$pagina=$num;
								if(is_numeric($pagina)) { $inicio=(($pagina-1)*$registros); }
								else { $inicio=0; $pagina=1; }
								$consulta=ejecutar($sql." LIMIT $inicio,$registros");
								$paginas=ceil($num_registros/$registros);
									$item = 0;
									while($fila=fetchArray($consulta))
									{
										$item++;
										?>
										<li>
											<a href="<?=$_SERVER['PHP_SELF']?>?i=<?=$fila["id"]?>&buscar=<?=$buscar?>&num=<?=$num?>">
												<?=($item -$registros ) + ($pagina * $registros)?>.-  <?=$fila["titulo"]?>
											</a>
										</li>
										<?php
									}
								?>
  						</ul>
  					</li>
  				</ul>
  			</div>
  			<div id="pg2">
  				<?php
  				if($paginas > 1)
  				{
  					$envpag = $_SERVER["PHP_SELF"]."?buscar=$buscar";
  					if($pagina>1)
  						echo "<a href='$envpag&num=". ($pagina-1) ."'>&#171;</a>"." ";
  					if($pagina<2)
  						echo "<span class='disabled'>&#171;</span>"." ";
  					for($cont=1;$cont<=$paginas;$cont++)
  					{
  						if($cont==$pagina)
  							//				echo $cont." ";
  							echo "<a class='current' href='$envpag&num=". $cont." "."'>$cont</a> ";
  						else
  							echo "<a href='$envpag&num=". $cont."'>$cont</a> ";
  					}
  					if($pagina<$paginas)
						echo "<a href='$envpag&num=". ($pagina+1) ."'>&#187;</a>";
					if($pagina>=$paginas)
						echo "<span class='disabled'>&#187;</span>";
				}
				?>
			</div>
	</div>
</aside>
