        <?php  $i = isset($_GET['i']) ? $_GET['i'] : ''; ?>
        <form action="save-web-content.php" method="post" enctype="multipart/form-data" name="form1">
          <div>
            <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
              <tr>
                <td>
                  <div class="navipage"> <?=isset($dataGlobal['web_content_box']) ?  $dataGlobal['web_content_box'] : 'Web Content Box'?> / <strong><?=$atabla["titulo"]?></strong></div>
                </td>
              </tr>
            </table>
            <?php
            if (isset($_SESSION['data_guarda_bien']) && $_SESSION['data_guarda_bien'] == 1 )
            {
              ?>              
              <table width="400" border="0" align="center" cellpadding="0" cellspacing="5" class="Estilo_data_guardado1"  >
                  <tr>
                    <td>
                      <div align="center">
                        <strong>
                          <?=isset($dataGlobal['data_saved']) ?  $dataGlobal['data_saved'] : 'Data Saved'?><br />
                        </strong>
                      </div>
                    </td>
                  </tr>
                </table>
              <?php
                unset ($_SESSION['data_guarda_bien']);
            }
            ?>
            <input type="hidden" name="ope" value="web-content"/>
            <input type="hidden" name="opc" value="2" />
            <input type="hidden" name="i" value="<?=$i?>" />
            <input type="hidden" name="buscar" value="<?=$buscar?>" />
            <input type="hidden" name="num" value="<?=$num?>" />
            <input type="hidden" name="titulo" value="<?=$atabla["titulo"]?>" />
            <div id="tabs">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#tabs-1" aria-controls="tabs-1" role="tab" data-toggle="tab">
                  <?=isset($dataGlobal['label_content']) ?  $dataGlobal['label_content'] : 'Content'?>
                  </a>
                </li>
                <li role="presentation">
                  <a href="#tabs-2" aria-controls="tabs-2" role="tab" data-toggle="tab">
                    <?=isset($dataGlobal['label_metas']) ?  $dataGlobal['label_metas'] : 'Metas'?>
                  </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tabs-1">

                  <table border="0" align="right" cellpadding="5" cellspacing="0">
                    <tr>
                      <td>
                        <input name="button" type="submit" class="btn btn-success" id="button2" style="margin-right: 10px;" value="<?=isset($dataGlobal['btn_save']) ?  $dataGlobal['btn_save'] : 'Save'?>" />
                      </td>
                      <td>
                        <a class="btn btn-default" href="<?=$_SERVER['PHP_SELF']?>?i=<?=$i?>&buscar=<?=$buscar?>&num=<?=$num?>"><?=isset($dataGlobal['btn_cancel']) ?  $dataGlobal['btn_cancel'] : 'Cancel'?></a>
                      </td>
                    </tr>
                  </table>
                  <br>
                  <br>
                  <script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
                  <script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
                  <script type="text/javascript">
                    $(document).ready(function() {
                      var window_height = 250; 
                      var newCKEdit = CKEDITOR.replace('FCKeditor1',{height:window_height,allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
                      CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
                    });
                    </script>
                  <div class="col-sm-12">
                    <?php 
                      include dirname(__FILE__). "/../system/btn-icons.php";
                      addBtnIcons($idEditor='CKEDITOR.instances.FCKeditor1');
                    ?>
                  </div>
                  <textarea name="FCKeditor1" id="FCKeditor1" ><?=stripslashes($atabla["descri"])?></textarea>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabs-2">
                  <?php include 'form-meta-custom-page.php'; ?>
                </div>
              </div>
              <!-- / div $ed == '2'  -->
                <input name="usamarketing" id="usamarketing" type="hidden"   value="<?=$atabla["usamarketing"]?>"     /> <!--campo de control para el tipo de meta, obligatorio para la edicion html  -->
                <input name="title" id="titleEdit" type="hidden"   value="<?=$atabla['title']?>"     /> <!--campo de control para el tipo de meta, obligatorio para la edicion html  -->
                <input name="keywords" id="keywordsEdit" type="hidden"   value="<?=$atabla['keywords']?>"     /> <!--campo de control para el tipo de meta, obligatorio para la edicion html  -->
                <input name="description" id="descriptionEdit" type="hidden"   value="<?=$atabla['description']?>"     /> <!--campo de control para el tipo de meta, obligatorio para la edicion html  -->
            </div>
            <!-- / tabs  -->
          </div>
        </form>



              