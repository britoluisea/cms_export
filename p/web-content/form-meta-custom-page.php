<style>.btn_save_metas{padding: 5px 20px;color:#fff !important;}</style>
<?php
	$metaType='';
	$preview_img_cover='';
	$img_cover='';
	$active='0';
	$check="";
	$check2="";
	//consultar meta title, description, description
	$sql_meta_page=" select title, keywords, description from informativo where id='".$i."' ";
	$query_meta_page= ejecutar($sql_meta_page);
	$custom=fetchAssoc($query_meta_page);
	$titlecustom = $custom['title'];
	$keywordscustom= $custom['keywords'];
	$descriptioncustom = $custom['description'];

	///IMG COVER
	$sql_cons=" select img from img_cover_page where id_page='".$i."' ";
	$query_cons= ejecutar($sql_cons);
	if($num_cons=numRows($query_cons) > 0)
	{
		$img=fetchAssoc($query_cons);
		$img_cover='../../'.$img['img'];
	}

	//validar meta custom
	$sql_cons_meta_page=" select id_page from meta_page where 	id_page='".$i."' and active=1 ";
	$query_cons_meta_page= ejecutar($sql_cons_meta_page);
	if($num_cons_meta_page=numRows($query_cons_meta_page) > 0)
	{
		$title = $custom['title'];
		$keywords= $custom['keywords'];
		$description = $custom['description'];
		$metaType='Custom';
		$preview_img_cover='table-row';
		$active='1';
		$check2="checked='true'";
	}
	else
	{
		$sql_webmarketing="select * from webmarketing where idx='1' "; //Devuelve todos los Idiomas
		$consulta_webmarketing=ejecutar($sql_webmarketing);
		$atabla_webmarketing=array();
		while($fila_webmarketing=fetchAssoc($consulta_webmarketing))
		{
			$atabla_webmarketing=$fila_webmarketing;
		}
		$title = $atabla_webmarketing['title'];
		$keywords= $atabla_webmarketing['keywords'];
		$description = $atabla_webmarketing['description'];
		$metaType='Universal';
		$preview_img_cover='none';
		$check="checked='true'";
	}

?>
<div id="div_marketing2">

		<br />
		<table>
			<tr>
				<td align="center">
					<label class="universal metasOp" style="border-color:<?php if($active==0){echo'#2196F3';}?>;">
						<i class="fa fa-globe fa-4x"></i><br>
						<?=isset($dataGlobal['universal_metas']) ?  $dataGlobal['universal_metas'] : 'Universal Metas'?>
					</label>
				</td>
				<td align="center" valign="middle">
					<!-- Rectangular switch -->
					<label class="switch">
					  <input type="checkbox" id="checkMetas" <?=$check2?> >
					  <span class="slider"></span>
					</label>
				</td>
				<td align="center">
					<label class="custom metasOp" style="border-color:<?php if($active==1){echo'#2196F3';}?>;">
						<i class="fa fa-edit fa-4x"></i><br>
						<?=isset($dataGlobal['custom_metas']) ?  $dataGlobal['custom_metas'] : 'Custom Metas'?>
					</label>
				</td>
			</tr>
		</table>
		<div class="table-responsive">   
			<table  class="table table-hover">
				<thead>
					<tr style="border: 1px solid #ccc;">
						<th><?=isset($dataGlobal['label_title']) ?  $dataGlobal['label_title'] : 'Title'?></th>
						<th><?=isset($dataGlobal['label_keywords']) ?  $dataGlobal['label_keywords'] : 'Keywords'?></th>
						<th><?=isset($dataGlobal['label_descript']) ?  $dataGlobal['label_descript'] : 'Description'?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tm"><?=$title?></td>
						<td class="km"><?=nl2br($keywords)?></td>
						<td class="dm"><?=nl2br($description)?></td>
					</tr>
					<tr class="preview_img_cover" style="display:<?=$preview_img_cover?>;">
				  		<td  colspan="3">
				  			<strong>
				  				<?=isset($dataGlobal['label_img_cover']) ?  $dataGlobal['label_img_cover'] : 'Image - cover'?>  
				  				[<?=isset($dataGlobal['label_max_width']) ?  $dataGlobal['label_max_width'] : 'Max width'?> 500 px ]
				  			</strong>
				  		</td>
					</tr>
					<tr class="preview_img_cover" style="display:<?=$preview_img_cover?>;">
				  		<td class="im"  colspan="3">
				  			<?php if(!is_file($img_cover)){?>
				  				<i class="fa fa-image fa-4x"></i>
				  			<?php } else {?>
				  				<img src="<?=$img_cover?>" alt="img_cover" style="width:200px;" >
				  			<?php } ?>
				  		</td>
					</tr>
					<tr>
				  		<td colspan="3">  
				  			<div id="botones2" style="display: <?php if($active=='1'){echo'block';}else{echo'none';}?>">
				  				<a href="#" id="Edit_Metas" title="Edit Metas" class="btn btn-primary " style="color:#fff;padding: 12px 14px 11px 21px;">
				  					<i class="fa fa-edit"></i> 
				  					<?=isset($dataGlobal['edit_custom_metas']) ?  $dataGlobal['edit_custom_metas'] : 'Edit Custom metas'?> 
				  				</a>
				  		&nbsp;&nbsp;
				  				<button type="button"   id="btn_del_metas"  class="btn btn-danger"  style="padding: 12px 14px 11px 21px;">
				  					<i class="fa fa-close"></i> 
				  					<?=isset($dataGlobal['disable_custom']) ?  $dataGlobal['disable_custom'] : 'Disable Custom'?> 
				  				</button>
				  			</div>
				  			<div id="preload2" style="display:none;">
				  				<?=isset($dataGlobal['msj_loading']) ?  $dataGlobal['msj_loading'] : 'Loading, wait please'?>
				  			</div>
				  		</td>
					</tr>				
				</tbody>
 			</table>
		</div>
</div>
<div id="camposEdit_Metas" style="display:none;">
	<form enctype="multipart/form-data" id="formuploadajax" method="post">
		<div class="table-responsive">   
			<table  class="table table-hover">
				<thead>
					<tr>
						<td colspan="3" style="border: 1px solid #ccc; color:#006699; font-size:20px;">
							<strong>
								<?=isset($dataGlobal['custom_metas']) ?  $dataGlobal['custom_metas'] : 'Custom Metas'?>
							</strong>
						</td>
					</tr>
					<tr>
						<th><?=isset($dataGlobal['label_title']) ?  $dataGlobal['label_title'] : 'Title'?></th>
						<th><?=isset($dataGlobal['label_keywords']) ?  $dataGlobal['label_keywords'] : 'Keywords'?></th>
						<th><?=isset($dataGlobal['label_descript']) ?  $dataGlobal['label_descript'] : 'Description'?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
				    	<td><textarea id="titleWeb" class="form-control"><?=$titlecustom?></textarea> </td>
				    	<td><textarea   id="keywordsMeta"  class="form-control"><?=nl2br($keywordscustom)?></textarea></td>
				    	<td><textarea   id="descripMeta"  class="form-control"><?=nl2br($descriptioncustom)?></textarea></td>
				  	</tr>
				  	<tr>
				   		<td  colspan="3">
				  			<strong>
				  				<?=isset($dataGlobal['label_img_cover']) ?  $dataGlobal['label_img_cover'] : 'Image - cover'?>  
				  				[<?=isset($dataGlobal['label_max_width']) ?  $dataGlobal['label_max_width'] : 'Max width'?> 500 px ]
				  			</strong>
				  		</td>
			  		</tr>
			  		<tr>
			  			<td class="im" style="width:200px;">
			  				<img src="<?=$img_cover?>" alt="img_cover" id="preview" style="width:200px;" >
			  			</td>
			  		</tr>
			  		<tr>
			  			<td  colspan="3">
			  				<?=isset($dataGlobal['add_img_metas']) ?  $dataGlobal['add_img_metas'] : 'Add/Update Image cover'?>
			  				<label class="switch">
							  <input type="checkbox" id="cambiar_img" >
							  <span class="slider"></span>
							</label>
						</td>
			  		</tr>
			  		<tr  >
			  			<td  colspan="3">
			  				<button id="img_cover_btn" type="button" class="btn btn-success" style="display:none;">
			  					<?=isset($dataGlobal['Explore Folders']) ?  $dataGlobal['Explore Folders'] : 'Explore Folders'?>
			  				</button>
			  				<input type="file" id="img_cover" style="display:none;">
			  			</td>
			  		</tr>
				</tbody>
				<tfoot>
					<tr>
						<td  colspan="3">
							<div id="botones">
				  				<button type="button"   id="btn_save_metas"  class="bnt btn-primary"  style="padding: 12px 14px 11px 21px; border: 0;"><i class="fa fa-floppy-o" ></i> Save Metas</button>  
				  				<button type="button"   id="btn_cancel_metas"  class="bnt btn-danger" style="padding: 12px 14px 11px 21px; border: 0;"  ><i class="fa fa-close" ></i> Cancel</button>
				  			</div>
				  			<div id="preload" style="display:none;">
								<?=isset($dataGlobal['msj_loading']) ?  $dataGlobal['msj_loading'] : 'Loading, wait please'?>
				  			</div>
				  		</td>
				  	</tr>
			  	</tfoot>
			</table>
 		</div>
		<input type="hidden" id="i_metas" value="<?=$i?>" />
		<input type="hidden" id="meta_page" value="1" />
 	</form>
</div>
<div id="msj_img"></div>
<script>
	var img = document.getElementById('img_cover');
                var preview = document.getElementById('preview');
                var upload = document.getElementById( 'img_cover_btn' );
                upload.onclick = function(event){event.preventDefault(); img.click()}
                img.onchange = function()
                {
                  preview.style.width='auto';
                  preview.src='<?=IMG_P?>loading.gif';
                  filePreview(this, preview);
                } 
 		//marketing("<?=$atabla["usamarketing"]?>");
 		$("#Edit_Metas").click(function(event) {
 			event.preventDefault();
 			$("#div_marketing2").hide('slow');
 			$("#camposEdit_Metas").show('slow');
 		});
 		$('.custom').click(function(event) {
 			$('#checkMetas').click();
 		}); 		
 		$('.universal').click(function(event) {
 			$('#checkMetas').click();
 		});
 		$('#checkMetas').click(function(event) {
 			if($(this).prop('checked'))
 			{
 				$("#btn_save_metas").click(); 
 				$('.custom').css('border-color', '#2196F3');
 				$('.universal').css('border-color', '#ccc');
 			}
 			else
 			{
 				$("#btn_del_metas").click();
 				$('.custom').css('border-color', '#ccc');
 				$('.universal').css('border-color', '#2196F3');
 			}
 		});


 		$("#cambiar_img").click(function(e){
 			if($(this).prop('checked'))
 			{
 				$("#img_cover_btn").show('slow');
 			}
 			else
 			{
 				$("#img_cover_btn").hide('slow');
 				$("#img_cover").val('');
 			}
 		});
 		$("#btn_cancel_metas").click(function(){
 						$("#camposEdit_Metas").hide('slow');
 						$("#div_marketing2").show('slow');
 		});
		///activar o modificar meta custom
 		$("#btn_save_metas").click(function(e){
        e.preventDefault();
        $("#botones").hide('slow');
        $("#preload").show('slow');
        var title = $('#titleWeb').val();
			var keywords = $('#keywordsMeta').val();
			var descrip = $('#descripMeta').val();
			var i = $('#i_metas').val();
			var meta_page = $('#meta_page').val();
			var inputFileImage = document.getElementById('img_cover');
			var file = inputFileImage.files[0];
			var data = new FormData(document.getElementById("formuploadajax"));
			data.append('titulo', title);
			data.append('key', keywords);
			data.append('desc', descrip);
			data.append('i', i);
			data.append('meta_page', meta_page);
			if($('#img_cover').val() != "" && $("#cambiar_img").prop('checked')){
				data.append('image',file);
				data.append('upload', true);
			}
			var url = 'save-meta-custom-page.php';
			$.ajax({
				url:url,
				type:'POST',
				contentType:false,
				data:data,
				processData:false,
				cache:false,
				success:function(data)
				{
					var r= JSON.parse(data);
					if(!r.result)
					{
						$('#preload').html("<strong><?=isset($dataGlobal['error_loading']) ?  $dataGlobal['error_loading'] : 'Error Loading'?></strong> ");
        			$("#botones").show('slow');
					}
					else
					{
        			$("#botones").show('slow');
        			$("#preload").hide('slow');
 						$("#camposEdit_Metas").hide('slow');
 						$(".tm").html(r.t);
 						$(".tmc").val(r.t);
 						$(".km").html(r.k);
 						$(".kmc").val(r.k);
 						$(".dm").html(r.d);
 						$(".dmc").val(r.d);
 						$("#usamarketing").val(1);
 						$("#titleEdit").val(r.t);
 						$("#keywordsEdit").val(r.k);
 						$("#descriptionEdit").val(r.d);
 						$(".metaType").html('Custom');
 						$("#div_marketing2").show('slow');
 						$("#botones2").show('slow');
 						if(r.img=='OK')
 						{
 							if(r.img_not=='no')
 							{
 								$(".im").html('<i class="fa fa-image fa-4x"></i>');	
 							}
 							else
 							{
 								$(".im").html('<img src="../'+r.ruta+'" alt="img_cover" style="width:200px;" >');
 							} 							
 							$('#msj_img').html("");
 							$('.preview_img_cover').show('slow');
 						}
					}
					if(r.img!='OK' && r.img!=false){$('#msj_img').html("<strong>"+r.img+"</strong> ");}

				}
			});
     });
		///desactivar meta custom y volver a meta universal
		$("#btn_del_metas").click(function(e){
        	e.preventDefault();
        	if(confirm("<?=isset($dataGlobal['confirm_custom_metas']) ?  $dataGlobal['confirm_custom_metas'] : 'Are you sure about change Custom metas to Univeral metas?'?>"))
        	{
        		$("#preload2").show('slow');
					var data = new FormData(document.getElementById("formuploadajax"));
					data.append('i', '<?=$i?>');
					data.append('meta_page', '2');
					var url = 'save-meta-custom-page.php';
					$.ajax({
						url:url,
						type:'POST',
						contentType:false,
						data:data,
						processData:false,
						cache:false,
						success:function(data)
						{
							var r= JSON.parse(data);
							if(!r.result)
							{
								$('#preload2').html("<strong><?=isset($dataGlobal['error_loading']) ?  $dataGlobal['error_loading'] : 'Error Loading'?></strong> ");
        						$("#botones2").show('slow');
							}
							else
							{
		        				$("#preload2").hide('slow');
		 						$("#camposEdit_Metas").hide('slow');
		 						$(".tm").html(r.t);
		 						$(".km").html(r.k);
		 						$(".dm").html(r.d);
		 						$("#div_marketing2").show('slow');
		 						$("#botones2").hide('slow');
 								$(".metaType").html('Universal');
 								$("#usamarketing").val(5);
		 						$("#titleEdit").val(r.t);
		 						$("#keywordsEdit").val(r.k);
		 						$("#descriptionEdit").val(r.d);
		 						if(r.img=='OK')
		 						{
		 							if(r.img_not=='no')
		 							{
		 								$(".im").html('<i class="fa fa-image fa-4x"></i>');	
		 							}
		 							else
		 							{
		 								$(".im").html('<img src="../'+r.ruta+'" alt="img_cover" style="width:200px;" >');
		 							} 
		 							$('#msj_img').html("");
		 							$('.preview_img_cover').show('slow');
		 						}
		 						else{
		 							$('.preview_img_cover').hide('slow');
		 						}
							}

						}
					});
        	}
 		});
</script>
