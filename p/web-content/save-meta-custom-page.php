<?php
//error_reporting (E_ALL ^ E_NOTICE);
session_start(); //Do not remove this
include "../../cn/cnx.php"; //BD
include dirname(__FILE__)."/../system/languages/languages.php";
//only assign a new timestamp if the session variable is empty
		$result= array();
		$result['result']=false;
		$id_page=$_POST["i"];
if (isset($_POST['meta_page']) && $_POST['meta_page']==1)
{

	if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
	    $_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')); //assign the timestamp to the session variable
	}

		$t=addslashes($_POST["titulo"]);
		$k=addslashes($_POST["key"]);
		$d=addslashes($_POST["desc"]);
		$result['img']=false;
		$result['t']=$t;
		$result['k']=$k;
		$result['d']=$d;
		$result['id_page']=$id_page;

	#########################################################################################################
	# CONSTANTS																								#
	# You can alter the options below																		#
	#########################################################################################################
	// Creamos la cadena aletoria para cambiar el nombre del comprobante
	$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	$str_out = "";  ////// prefijo
	for($i=0;$i<12;$i++)
	{
		$str_out .= substr($str,rand(0,62),1);
	}
	$upload_dir = "../../imgcms/img_cover_page"; 				// The directory for the images to be saved in
	$upload_path = $upload_dir."/";				// The path to where the image will be saved
	$large_image_location=''; /// control de img
	$large_image_prefix = "img_cover_page_".$id_page."_".$str_out; 			// The prefix name to large image
	$thumb_image_prefix = "thumbnail_";			// The prefix name to the thumb image
	$large_image_name = $large_image_prefix;     // New name of the large image (append the timestamp to the filename)
	//$thumb_image_name = $thumb_image_prefix.$_SESSION['random_key'];     // New name of the thumbnail image (append the timestamp to the filename)
	$max_file = "25"; 							// Maximum file size in MB
	$max_width = "500";							// Max width allowed for the large image
	$thumb_width = "100";						// Width of thumbnail image
	$thumb_height = "100";						// Height of thumbnail image
	// Only one of these image types should be allowed for upload
	$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
	$allowed_image_ext = array_unique($allowed_image_types); // do not change this
	$image_ext = "";	// initialise variable, do not change this.
	foreach ($allowed_image_ext as $mime_type => $ext) {
	    $image_ext.= strtoupper($ext)." ";
	}
	//Create the upload directory with the right permissions if it doesn't exist
	if(!is_dir($upload_dir)){
		mkdir($upload_dir, 0777);
		chmod($upload_dir, 0777);
	}

	##########################################################################################################
	# IMAGE FUNCTIONS																						 #
	# You do not need to alter these functions																 #
	##########################################################################################################
	function resizeImage($image,$width,$height,$scale) {
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image);
				break;
		    case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image);
				break;
		    case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image);
				break;
	  	}
		imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

		switch($imageType) {
			case "image/gif":
		  		imagegif($newImage,$image);
				break;
	      	case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
		  		imagejpeg($newImage,$image,90);
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$image);
				break;
	    }

		chmod($image, 0777);
		return $image;
	}
	//You do not need to alter these functions
	function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}
	//You do not need to alter these functions
	function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}

	if (isset($_POST["upload"])  ) {
		//Get the file information
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$userfile_size = $_FILES['image']['size'];
		$userfile_type = $_FILES['image']['type'];
		$filename = basename($_FILES['image']['name']);
		$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));

		//Only process if the file is a JPG, PNG or GIF and below the allowed limit
		if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {

			foreach ($allowed_image_types as $mime_type => $ext) {
				//loop through the specified image types and if they match the extension then break out
				//everything is ok so go and check file size
				if($file_ext==$ext && $userfile_type==$mime_type){
					$error = "";
					break;
				}else{
					$error = isset($dataGlobal['invalid_format']) ?  $dataGlobal['invalid_format'] : "Invalid Format";
				}
			}
			//check if the file size is above the allowed limit
			if ($userfile_size > ($max_file*1048576)) {
				$error.= isset($dataGlobal['msj_error_metas2']) ?  $dataGlobal['msj_error_metas2'] : "Exceeded the maximum allowed size.";
				$error.= " Max ".$max_file."MB ";
			}

		}else{
			$error= isset($dataGlobal['msj_error_metas']) ?  $dataGlobal['msj_error_metas'] : "Select an image for upload";
		}
		//Everything is ok, so we can upload the image.
		if (strlen($error)==0){


				//this file could now has an unknown file extension (we hope it's one of the ones set above!)
				$large_image_location = $upload_path .   $large_image_name.'.'.$file_ext;//Image Locations
				$image =  'imgcms/img_cover_page/'.$large_image_name.'.'.$file_ext;//Image Locations
				if (!move_uploaded_file($userfile_tmp, $large_image_location)) {
					echo "Ocurrio un error al subir el archivo. No pudo guardarse."; // msj en consola
					}
					else
					{			
						chmod($large_image_location, 0777);
						$width = getWidth($large_image_location);
						$height = getHeight($large_image_location);
						//Scale the image if it is greater than the width set above
						if ($width > $max_width){
							$scale = $max_width/$width;
							$uploaded = resizeImage($large_image_location,$width,$height,$scale);
						}else{
							$scale = 1;
							$uploaded = resizeImage($large_image_location,$width,$height,$scale);
						}
						$result['ruta']=$image;
						$sql_cons=" select img from img_cover_page where id_page='".$id_page."' ";
						$sql_insert=" insert into  img_cover_page VALUES (null, '".$id_page."', '".$image."' )";
						$sql_update=" update img_cover_page set img='".$image."' where id_page='".$id_page."' ";
						$query_cons= ejecutar($sql_cons);
						if($num_cons=numRows($query_cons) > 0)
						{
							$result['sql_cons']=$sql_cons;
							$image_location = fetchAssoc($query_cons);
							$img_unlink ='../../'.$image_location['img'];
							$result['img_unlink']=$img_unlink;
							if (file_exists($img_unlink)) {
								$result['unlink']=false;
								if(unlink($img_unlink)){
									$result['unlink']=true;
								}
							}
							$query_img= ejecutar($sql_update);
						}
						else
						{
							$query_img= ejecutar($sql_insert);
						}
						if($query_img){  $result['img']='OK'; } 
					}  
		}else{
			$result['img']=$error;
		}
	}
	else
	{
		$sql_cons=" select img from img_cover_page where id_page='".$id_page."' ";
		$query_cons= ejecutar($sql_cons);
		if($num_cons=numRows($query_cons) > 0)
		{
			$result['sql_cons']=$sql_cons;
			$image_location = fetchAssoc($query_cons);
			if(!is_file('../'.$image_location['img'])){$result['img_not']='no';}else{$result['img_not']='si';}
			$result['ruta']=$image_location['img'];
			$result['img']='OK';
		}
	}
		
	#########################################################################################################
	# TABLAS INFORMATIVO Y META_PAGE #
	#########################################################################################################
	$sql_page="update informativo set title='".$t."', keywords='".$k."', description='".$d."' where id='".$id_page."' ";
	$query_info= ejecutar($sql_page);
	if($query_info)
	{
		$sql_insert_meta_page=" insert into  meta_page VALUES (null, '".$id_page."', 1 )";
		$sql_update_meta_page=" update meta_page set active=1 where id_page='".$id_page."' and active=0 ";
		$sql_cons_meta_page=" select active from meta_page where id_page='".$id_page."'   "; 
		$query_cons= ejecutar($sql_cons_meta_page);
		if($num_cons=numRows($query_cons) > 0) 
		{
			////reactivar
		 	$active_meta_page=fetchAssoc($query_cons);
		 	if($active_meta_page['active']==0)
		 	{
		 		$query_meta_page= ejecutar($sql_update_meta_page);
		 		$result['query_meta_page']=$sql_update_meta_page;
		 	}
		 	$result['result']=true;
		} 
		else 
		{
			$query_meta_page= ejecutar($sql_insert_meta_page);
			if($query_meta_page)
			{
				$result['result']=true;
			}
			else
			{
				$result['error_query']=$sql_insert_meta_page;
			}
		}
	}
	else
	{
		$result['error_query']=$sql_page;
	}
	echo json_encode($result);
}
///desactivar
elseif (isset($_POST['meta_page']) && $_POST['meta_page']==2)
{
	$sql_update_meta_page=" update meta_page set active=0 where id_page='".$id_page."' and active=1 ";
	$query_meta_page= ejecutar($sql_update_meta_page);
	if($query_meta_page)
	{
		$sql_webmarketing="select * from webmarketing where idx='1' "; //Devuelve todos los Idiomas
		$consulta_webmarketing=ejecutar($sql_webmarketing);
		$atabla_webmarketing=array();
		while($fila_webmarketing=fetchAssoc($consulta_webmarketing))
		{
			$atabla_webmarketing=$fila_webmarketing;
		}
		$result['sql']	= $sql_update_meta_page;
		$result['t']	= stripslashes($atabla_webmarketing['title']);
		$result['k']	= stripslashes($atabla_webmarketing['keywords']);
		$result['d']	= stripslashes($atabla_webmarketing['description']);
		$result['result']=true;
	}
	echo json_encode($result);
	exit();
}
