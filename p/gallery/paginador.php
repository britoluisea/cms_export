<style>
ul.paginationx {
    display: inline-block;
    padding: 0;
    margin: 0;
}

ul.paginationx li {display: inline;}

ul.paginationx li a {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
}

ul.paginationx li a.activex {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}

ul.paginationx li a:hover:not(.active) {background-color: #ddd;}
</style>
<table border="0" width="50%" align="center">
    <tr>
	<td align="center">
<?php if ($totalRows_c > $maxRows_c) { // Show if recordset empty ?>
<ul class="paginationx">
<?php 
$directorio = $x1;
$carpeta_web1 = strrchr($directorio, "/");
$m = $carpeta_web1;
$str = $m;
$tam = substr($m, 1, strlen($m)); 
	
for ($cont=1;$cont <= $totalPages_c; $cont++)
{
	if($cont == $pageNum_c){ ?>
    
<li class="active">
<?php	echo "<a class='activex' style='cursor:pointer'>$cont</a> "; ?>
</li>
<?php }else{ ?>
<li>
<?php 	echo "<a style='cursor:pointer' href='".$tam."?pageNum_c=".$cont."'>$cont</a> "; ?>
</li>
<?php } }  ?>
</ul>
<?php } // Show if recordset empty ?>
</td>
  </tr>
</table>
