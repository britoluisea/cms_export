<?php require_once("../header_box.php");?>
	<link href="<?=CSS_P?>dropzone.css" type="text/css" rel="stylesheet" />
    <link href="<?=CSS_P?>customize.css" rel="stylesheet">
	<script src="<?=JS_P?>dropzone.js"></script> 
	<script type="text/javascript">
var loadData = function(){
$.ajax({
    type: "POST",
    url: "img-sql1.php",
}).done(function(data) {
    console.log('img-sql1.php data = '+data);
})
}
var loadDataw = loadData();
//////////////////////////////////////////////////7
function capturar() {
 $.ajax({
 type : 'text',
 url : 'img-sql2.php',
success : function (resultados) {
console.log('img-sql2.php resultados = '+resultados);
}
 });
}
var resultadox = capturar();
	$(document).ready(function()
	{
		Dropzone.autoDiscover = false;
		var contClose=0;
		$("#dropzone").dropzone({
			//url: "gallery-upload.php?i=<?php echo $_GET['i']; ?>",
			maxThumbnailFilesize: 25000000,// MB,
			timeout:40000000,
			addRemoveLinks: true,
          	dictRemoveFile: "<?=isset($dataGlobal['btn_remove']) ? $dataGlobal['btn_remove'] : 'Remove'?>",
			maxFileSize: 40000000,
			parallelUploads: 1,
			autoProcessQueue: true,	
			params: {typeUpload:''},
			dictResponseError: "<?=isset($dataGlobal['msj_error_internal_server']) ? $dataGlobal['msj_error_internal_server'] : 'An internal server error has occurred'?>",
			acceptedFiles: 'image/jpeg', init: function(){
	                    this.on("error", function(file, errorMessage) {
            				file.previewElement.classList.add("dz-error");
	                        $('.dz-error-message').text(file.name+' '+errorMessage);
	                        alerError('Error', file.name+" "+errorMessage+"<?=isset($dataGlobal['only_jpg']) ? $dataGlobal['only_jpg'] : 'Only jpg'?>", 3000);			                
	             		});	
	            	},
	        accept: function(file, done)
	        {
			    //debugger;
			    console.log('agregando '+file);
	        	done();
	        },
			success: function (file, response) {
				//debugger;
				file.previewElement.classList.add("dz-success");
			},
			complete: function(file, response)
			{
				if(file.status == "success")
				{
					//debugger;
					console.log('listo '+file);
	            	$(file.previewElement).find('.dz-progress').css('display', 'none');
					file.previewElement.classList.add("dz-success");
					$(file.previewElement).find('.dz-success-mark').css({
						'opacity': ' 0.7',
						'display': 'block'
					});
					var imgAccept = parseInt($('.dropzone .dz-preview').length); 
					var imgError = parseInt($('.dropzone .dz-preview.dz-error').length); 
					var totalIMG = imgAccept - imgError; 
			    	contClose++;
					if(contClose==totalIMG )
					{
						parent.location.href="img.php?i=<?php echo $_GET['i']; ?>";
					}
				}
			},
			error: function(file, response) {
            	console.log('error '+response)
            	file.previewElement.classList.add("dz-error");
	            $(file.previewElement).find('.dz-progress').css('display', 'none');
				//alert("Error subiendo el archivo " + file.name + " y el peso permitido es de 15MB");
				//window.location.reload();
			},
			removedfile: function(file, serverFileName)
			{
				var name = file.name;
				$.ajax({
					type: "POST",
					url: "gallery-upload.php?delete=true&i=<?php echo $_GET['i']; ?>",
					data: "filename="+name,
					success: function(data)
					{
						var json = JSON.parse(data);
						if(json.res == true)
						{
							console.log("El elemento fué eliminado: " + name); 
						}
						var element;
						(element = file.previewElement) != null ? 
						element.parentNode.removeChild(file.previewElement) : 
						false;						
			        	if($('.dropzone .dz-preview').length < 1)
					    {
					    	$('.dropzone.dz-started .dz-message').css('display', 'block');
					    }else{ $('.dropzone.dz-started .dz-message').css('display', 'none');}
					}
				});
			},
	        maxfilesexceeded: function(file) {
	                  alerError("<?=isset($dataGlobal['label_error']) ? $dataGlobal['label_error'] : 'Error'?>", "<?=isset($dataGlobal['msj_drop_js4']) ? $dataGlobal['msj_drop_js4'] : 'has exceeded the file limit'?>", 3000);
	                  var name = file.name;
	                  var element;
	                  (element = file.previewElement) != null ? 
	                  element.parentNode.removeChild(file.previewElement) : 
	                  false;
	        },
	        uploadprogress: function(file, progress, bytesSent, xhr){
	            console.log('progress= '+progress);               
	            $(file.previewElement).find('span.dz-upload').css({
	            	'width': progress,
	            	'text-align': 'center',
	            	'color': '#fff'
	            });
	            $(file.previewElement).find('span.dz-upload').html(parseInt(progress)+' %');
	        }
		});
	});
	</script>
<?php
include 'img-sql.php';
?>
<div class="container">	
<br />
<div class="pull-right">
<a href="img.php?i=<?php echo $_GET['i']; ?>" id="iD_boton" class="btn btn-sm btn-primary" target="_top"><i class="fa fa-mail-reply "></i> <?=isset($dataGlobal['label_back']) ? $dataGlobal['label_back'] : 'Back'?></a>	
</div>
 <br />
<br />
<div class="panel panel-primary">
<div class="panel-body">
<div>
<form  action="gallery-upload.php?i=<?php echo $_GET['i']; ?>" enctype="multipart/form-data" class="dropzone bg-dropzone" id="dropzone" >
  <div class="fallback">
  </div>
</form>
</div>
</div>
</div>
</div>
<?php require_once("../foot.php");?>
