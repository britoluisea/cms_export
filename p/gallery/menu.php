<?php $tab = isset($_GET['tab']) ? $_GET['tab'] : '1';?>
<ul class="nav nav-tabs" role="tablist">
    <li role="List" class="<?php if($tab=='1'){echo'active';}?>">
      <a href="#tabs-1" aria-controls="tabs-1" role="List" data-toggle="tab">
        <?=isset($dataGlobal['label_list']) ? $dataGlobal['label_list'] : 'List'?>
      </a>
    </li>
    <li role="Widget" class="<?php if($tab=='2'){echo'active';}?>">
      <a href="#tabs-2" aria-controls="tabs-2" role="Widget" data-toggle="tab">
        <?=isset($dataGlobal['label_widget']) ? $dataGlobal['label_widget'] : 'Widget'?>
      </a>
    </li>
    <li role="Options" class="<?php if($tab=='3'){echo'active';}?>">
      <a href="#tabs-3" aria-controls="tabs-3" role="Options" data-toggle="tab">
        <?=isset($dataGlobal['label_options']) ? $dataGlobal['label_options'] : 'Options'?>
      </a>
    </li>
</ul> 
 