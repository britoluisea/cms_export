<?php
require_once("../header.php");
$is = 1;
$query_c1 = "SELECT * FROM galerias_lista_options where idx='$is'";
$c1 = ejecutar($query_c1);
$row_c1 = fetchAssoc($c1);
$totalRows_c1 = numRows($c1);


require_once("url.php");
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_c = $row_c1['numregweb'];
$pageNum_c = 0;
if (isset($_GET['pageNum_c'])) {
  $pageNum_c = $_GET['pageNum_c'];
}if(!$pageNum_c){
$pageNum_c = 1;
$startRow_c = 0;
}else{
$startRow_c = ($pageNum_c  -1) * $maxRows_c;
}

$query_c = "SELECT * FROM galerias_lista ORDER BY item DESC";
$query_limit_c = sprintf("%s LIMIT %d, %d", $query_c, $startRow_c, $maxRows_c);
$c = ejecutar($query_limit_c);
$row_c = fetchAssoc($c);

if (isset($_GET['totalRows_c'])) {
  $totalRows_c = $_GET['totalRows_c'];
} else {
  $all_c = ejecutar($query_c);
  $totalRows_c = numRows($all_c);
}
$totalPages_c = ceil($totalRows_c/$maxRows_c);

$queryString_c = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_c") == false && 
        stristr($param, "totalRows_c") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_c = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_c = sprintf("&totalRows_c=%d%s", $totalRows_c, $queryString_c);
?>