<?php require_once("../header_box.php");?>
<?php
include("../system/function.php");
include("name_table_bd.php"); // $tb_a, $tb_b no borrar
$colname_u = isset($_GET['idx']) ? $_GET['idx'] : '-1';
$query_u = sprintf("SELECT * FROM $tb_b WHERE idx = %s", $colname_u);
$u = ejecutar($query_u);
$row_u = fetchAssoc($u);
$totalRows_u = numRows($u);
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = "UPDATE $tb_b SET ";
  $updateSQL.=" nombre='".$_POST['nombre']."', ";
  $updateSQL.=" descripcion='".$_POST['descripcion']."' ";
  $updateSQL.=" WHERE idx='".$_POST['idx']."' ";

  $Result1 = ejecutar($updateSQL);
 if($Result1) { ?>
     <script>
            window.parent.closeModal();
    </script>
<?php } } ?>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
  <table border="0" align="center" cellpadding="10" cellspacing="0">
    <tr>
      <td bgcolor="#FFFFFF">
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" >
<input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>" />
<input type="hidden" name="i" value="<?php echo $_GET['i']; ?>" />
<input type="hidden" name="MM_update" value="form1">
  <table border="0" align="center" cellpadding="2">   
    <tr>
      <td colspan="4">
       <h3 class="cat_titulo h3">
              <?=isset($dataGlobal['edit_name']) ? $dataGlobal['edit_name'] : 'Edit Name'?>
        </h3>
       <br />
       <br /></td>
    </tr>
    <tr>
      <td width="15">&nbsp;</td>
      <td><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="nombre" type="text" id="nombre" class="form-control" value="<?php echo $row_u['nombre']; ?>" size="50"/>
      </span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td valign="top"><?=isset($dataGlobal['label_descript']) ? $dataGlobal['label_descript'] : 'Description'?></td>
      <td>&nbsp;</td>
      <td><textarea name="descripcion" id="descripcion" rows="5" class="form-control" ><?php echo $row_u['descripcion']; ?></textarea></td>
    </tr>
  </table>
  <br />
  <table border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><button type="submit" class="btn btn-success" name="button2" id="button" />
        <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>
      </button> </td>
      <td> <a href="#" class="btn btn-default" onclick="window.parent.closeModal_hide();" style="cursor:pointer;" />
        <?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?>
      </a></td>
    </tr>
  </table>
  </form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
//-->
</script>
</td>
    </tr>
  </table>
<?php require_once("../foot.php");?>