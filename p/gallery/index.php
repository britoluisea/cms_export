<?php 
include("gallery-list_c.php");
include("name_table_bd.php");
include("../system/function.php");
?>
<div class="container-fluid">
  <div id="tabs"><br>
    <!-- Nav tabs -->
    <?php include("menu.php");?>
    <!-- Tab panes -->
    <div class="tab-content">
      <div role="List" class="tab-pane <?php if($tab=='1'){echo'active';}?>" id="tabs-1" style="padding-top: 20px;">       
        <?php include("lista.php"); ?>
      </div>
      <div role="Widget" class="tab-pane <?php if($tab=='2'){echo'active';}?>" id="tabs-2" style="padding-top: 20px;">
        <?php include("widget.php"); ?>      
      </div>
      <div role="Options" class="tab-pane <?php if($tab=='3'){echo'active';}?>" id="tabs-3" style="padding-top: 20px;">   
        <?php include("options.php"); ?>         
      </div>
    </div>
  </div>
</div>
<?php include(dirname(__FILE__)."/../footer.php"); ?>
<!-- modal confirmar eliminar registro -->
