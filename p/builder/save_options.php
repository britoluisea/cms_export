<?php 
session_start();
$widxamusuario = $_SESSION['value_admin_idx'];
$widxamusuario_pri = $_SESSION['value_admin_idxamusuario'];
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: index.php");
    exit();
}
include "../../cn/cnx.php";

$ope = isset($_POST['ope']) ? $_POST['ope'] : '';
$opc = isset($_POST['opc']) ? $_POST['opc'] : '';
$i = isset($_POST['i']) ? $_POST['i'] : '';
$showpagebuilder = isset($_POST['showpagebuilder']) ? $_POST['showpagebuilder'] : '';
if ($ope == "page-builder-seo-option") {
    $sql_0 = "update page_builder_seo_option set showpagebuilder = '$showpagebuilder' where idx = 1";
    if (!$res_0 = ejecutar($sql_0)) {
        echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
        exit();
    }
    $_SESSION['data_guarda_bien'] = 1;
    header("Location: index.php?tab=3");
    exit();
}