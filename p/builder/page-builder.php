<?php
	 if(empty($g))
	 {
		$sql="select * from page_group order by nombre";
		 $consulta=ejecutar($sql);	
   		 if($fila=fetchArray($consulta))
  		  {
 			$g=$fila["idx"];
		 }
	 }
?>
<script>
function eliminar_registro_propio(idx, idx_grupo, ciudad) {
	if (confirm("<?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?> : "+ciudad+" ?")) 
	{ 
		window.location.href='delete.php?i='+idx+'&ope=page-builder&opc=3&g='+idx_grupo;
	   //return true;  
	}
    else  
	{
   		return false; 
	}	
}
</script>
 <table class="marginbot12" width="100%" border="0" cellspacing="0" cellpadding="0">
 	<tr>
 		<td width="347"> 
 			<a href="page-builder-n.php?g=<?=$g?>"  class="boton1 addbtn" title="Add Page Builder SEO">
 				<span>+</span> <?=isset($dataGlobal['label_add']) ? $dataGlobal['label_add'] : 'Add'?> <?=isset($dataGlobal['page_builder_SEO']) ? $dataGlobal['page_builder_SEO'] : 'Page Builder SEO'?>
 			</a>
 		</td>
		<td width="517">
		</td>
	</tr>
</table>
<div class="estilotabla">
	<?php if(isset($_SESSION["error"])){ ?>
		<div align="center"  class="estilo_error"><?=$_SESSION["error"]?></div>
		<?php unset($_SESSION['error']);	?>
	<?php } ?>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th width="10" ><?=isset($dataGlobal['label_item']) ? $dataGlobal['label_item'] : 'Item'?></th>
			<th width="150"  align="left"><?=isset($dataGlobal['label_group']) ? $dataGlobal['label_group'] : 'Group'?></th>
			<th  align="left"><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?>, <?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?></th>
			<th width="10"  align="center"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></th>
			<th width="10"  align="center"><?=isset($dataGlobal['label_link']) ? $dataGlobal['label_link'] : 'Link'?></th>
			<th width="10"  align="center"><?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Status'?></th>
			<th width="10"  align="center"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></th>
		</tr>
		<?php	  
			$t="page_builder,page_group";
			$sql="select page_builder.*, page_group.nombre as pgrnombre from $t where page_builder.idx_grupo = page_group.idx and page_group.idx = '$g'   order by pgrnombre,page_builder.ciudad";	
			$consulta=ejecutar($sql);
			$num_registros=numRows($consulta);
			$registros=40;
			$pagina=isset($_GET["num"]) ? $_GET["num"] : '';
			if(is_numeric($pagina)) { $inicio=(($pagina-1)*$registros); }
			else { $inicio=0; 	$pagina=1; }		
			$consulta=ejecutar($sql." LIMIT $inicio,$registros");			
			$paginas=ceil($num_registros/$registros);
			if($num_registros==0) { $paginas=1; }
			$item = 0;
			while($fila=fetchArray($consulta))
			{ 
				$item++; ?>
				<tr>
					<td height="25"  align="center"><?=($item -$registros ) + ($pagina * $registros)?></td>
	  				<td align="left">
	    				<?=stripcslashes($fila["pgrnombre"])?>
	  				</td>
	  				<td style="text-align:left;"><?=stripcslashes($fila["ciudad"])?>, <?=stripcslashes($fila["estado"])?></td>
	  				<td>
	    				<div align="center" class="Estilo_tipoestado<?=$fila["activo"]?>">
	      				<?php
							if($fila["activo"]=="1") {echo "Active";}
							else {echo "Inactive";}
						?>        
	    				</div>
		    		</td>
		  			<td align="center">
		  				<a href="<?=SERVER?>pbs/<?=$fila['slug']?>" target="_blank"><i class="fa fa-link"></i></a>
		  			</td>
					<td align="center">
	  					<a href="page-builder-n.php?i=<?=$fila["idx"];?>" title="Edit <?=$fila["ciudad"]?>" class="btn btn-primary">
	  						<i class="fa fa-edit"></i>
		  				</a>
		  			</td>
		  			<td align="center">
		  				<a class="btn btn-danger" href="#" onclick="eliminar_registro_propio(<?=$fila["idx"]?>, '<?=$g?>', '<?=$fila["ciudad"]?>');">
							<i class="fa fa-close"></i>
	  					</a>
	  				</td>
				</tr>
		   			<?php
	  		} ?>
	</table>
</div>
<div id="pg">
    <?php
    	if($paginas > 1)
    	{
    		$envpag = "index.php?g=$g";
    		if($pagina>1) {echo "<a href='$envpag&num=". ($pagina-1) ."'>Previous</a>"." ";}
			if($pagina<2)
			{
				$prev=isset($dataGlobal['label_prev']) ? $dataGlobal['label_prev'] : 'Previous';
				echo "<span class='disabled'>".$prev."</span>"." ";
				for($cont=1;$cont<=$paginas;$cont++)
				{
					if($cont==$pagina) {echo "<a class='current' href='$envpag&num=". $cont." "."'>$cont</a> ";}
					else {echo "<a href='$envpag&num=". $cont."'>$cont</a> ";}
				}
			}
		
			if($pagina<$paginas) {echo "<a href='$envpag&num=". ($pagina+1) ."'>Next</a>"; }
			if($pagina>=$paginas) {
				$next=isset($dataGlobal['label_next']) ? $dataGlobal['label_next'] : 'Next';
				echo "<span class='disabled'>".$next."</span>";}		
		}
	?>
</div>

