<?php $tab = isset($_GET['tab']) ? $_GET['tab'] : '1';?>
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="dropdown <?php if($tab=='1'){echo'active';}?>"> 
		<a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false"><?=isset($dataGlobal['page_builder_SEO']) ? $dataGlobal['page_builder_SEO'] : 'Page Builder SEO'?> <span class="caret"></span></a> 
		<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 			
			<?php
				$sql = "SELECT * FROM page_group order by nombre ";
				$res=ejecutar($sql);
				while ($col=fetchArray($res))
				{ ?>
					<li>
						<a href="index.php?g=<?=$col['idx']?>" >			
							<?=$col['nombre']?>							
						</a>
					</li> 
					<?php
				}
			?>
		</ul> 
	</li>
    <li role="Group" class="<?php if($tab=='2'){echo'active';}?>">
        <a href="#tabs-2" aria-controls="tabs-2" role="Group" data-toggle="tab"><?=isset($dataGlobal['label_group']) ? $dataGlobal['label_group'] : 'Group'?></a>
    </li>
    <li role="Options" class="<?php if($tab=='3'){echo'active';}?>">
        <a href="#tabs-3" aria-controls="tabs-3" role="Options" data-toggle="tab"><?=isset($dataGlobal['label_options']) ? $dataGlobal['label_options'] : 'Options'?></a>
    </li>
</ul>