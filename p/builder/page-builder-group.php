<script>
function eliminar_registro_propio2(idx,nombre) {
	if (confirm("<?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?> : "+nombre+" ? \n<?=isset($dataGlobal['del_group_msj']) ? $dataGlobal['del_group_msj'] : 'If you delete the group, you could delete the dependent records from the'?>")) 
	{ 
		window.location.href='group_save.php?i='+idx+'&ope=page-group&opc=3&username='+nombre+'';
	   //return true;  
	}
    else  
	{
   		return false; 
	}	
}

</script>
<table class="marginbot12" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="347"> 
			<a href="page-builder-group-n.php"  class="boton1 addbtn" title="Add Image">
				<span>+</span> <?=isset($dataGlobal['add_group']) ? $dataGlobal['add_group'] : 'Add Group'?>
			</a>
		</td>
		<td width="517"></td>
	</tr>
</table>
<div class="estilotabla">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<th width="60" align="center"><?=isset($dataGlobal['label_item']) ? $dataGlobal['label_item'] : 'Item'?></th>
			<th align="left"><?=isset($dataGlobal['label_']) ? $dataGlobal['label_'] : ''?>Group</th>
			<th width="100" align="center"><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></th>
			<th width="50" align="center"><?=isset($dataGlobal['label_edit']) ? $dataGlobal['label_edit'] : 'Edit'?></th>
			<th width="60" align="center"><?=isset($dataGlobal['label_del']) ? $dataGlobal['label_del'] : 'Delete'?></th>
		</tr>
		<?php
		$t="page_group";
		$sql="select * from $t order by nombre";
		$consulta=ejecutar($sql);
		$num_registros=numRows($consulta);
		$registros=20;
		$pagina=isset($_GET["num2"]) ? $_GET["num2"] : '';
		if(is_numeric($pagina))
		{
			$inicio=(($pagina-1)*$registros);
		}
		else
		{
			$inicio=0;
			$pagina=1;
		}		
		$consulta=ejecutar($sql." LIMIT $inicio,$registros");
		$paginas=ceil($num_registros/$registros);
		if($num_registros==0){	$paginas=1; }
			$item = 0;
			while($fila=fetchArray($consulta))
			{
				$item++;
				?>
   				<tr>
   					<td height="25" ><div align="center"><?=($item -$registros ) + ($pagina * $registros)?></div></td>
   					<td><div class="cat_nombre" align="left"><?=stripcslashes($fila["nombre"])?></div></td>
   					<td>
   						<div align="center" class="Estilo_tipoestado<?=$fila["activo"]?>">
   							<?php
   								if($fila["activo"]=="1") {echo "Active";}
								else {echo "Inactive";}
							?>        
         				</div>
         			</td>
     				<td>       
       					<div align="center" >
       						<a href="page-builder-group-n.php?i=<?=$fila["idx"];?>" title="Edit Group" class="btn btn-primary">
       							<i class="fa fa-edit"></i>
       						</a>
       					</div>
       				</td>
     				<td>
     					<div align="center">
     						<a class="btn btn-danger" href="#" onClick="eliminar_registro_propio2('<?=$fila["idx"]?>','<?=$fila["nombre"]?>');">
     							<i class="fa fa-remove "></i>
     						</a>
     					</div>
     				</td>
   				</tr>
   				<?php
   			}  ?>
	</table>
</div>
<div id="pg">
    <?php
    	if($paginas > 1)
    	{
    		$envpag = "index.php?g=$g";    		
    		if($pagina>1) {echo "<a href='$envpag&num2=". ($pagina-1) ."'>Previous</a>"." ";}
			if($pagina<2)
			{
				$prev=isset($dataGlobal['label_prev']) ? $dataGlobal['label_prev'] : 'Previous';
				echo "<span class='disabled'>".$prev."</span>"." ";
				for($cont=1;$cont<=$paginas;$cont++)
				{
					if($cont==$pagina) {echo "<a class='current' href='$envpag&num2=". $cont." "."'>$cont</a> ";}
					else {echo "<a href='$envpag&num2=". $cont."'>$cont</a> ";}
				}
			}
		
			if($pagina<$paginas) {echo "<a href='$envpag&num2=". ($pagina+1) ."'>Next</a>"; }
			if($pagina>=$paginas) {
				$next=isset($dataGlobal['label_next']) ? $dataGlobal['label_next'] : 'Next';
				echo "<span class='disabled'>".$next."</span>";
			}		
		}
	?>
</div>