<?php
session_start();
date_default_timezone_set('America/New_York');
include "../cn/cnx.php";
//INICIO VAIDADORES
include('app/FormValidator.php');
include('app/ValidationRule.php');
$validator=new App_FormValidator();
//FIN VAIDADORES

require("phpmailer/class.phpmailer.php");
require("phpmailer/class.smtp.php");
$server= ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$server.= "://".$_SERVER['HTTP_HOST'];
$host= $_SERVER['HTTP_HOST'];
$host_basename= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$urlDomain= $server.$host_basename; 
$ope = isset($_POST['ope']) ? $_POST['ope'] : '';
$fecreg = date("Y-m-d H:i:s");
$wips = getenv('REMOTE_ADDR');
if ($ope == "comment") {
    $wdescrip = isset($_POST['descrip']) ? trim($_POST['descrip']) : '';
    $id = isset($_POST['id']) ? trim($_POST['id']) : '';
    $slug = isset($_POST['slug']) ? trim($_POST['slug']) : '';
	//$wdescrip=trim($descrip);
	if(!empty($wdescrip))
	{
		$sql_0 = "insert into comments (article_id, descrip, user_id , fecreg, comment_status_id, comment_status_id_salvar, ips )
							  values ('$id', '$wdescrip', '".$_SESSION['user']['idx']."', '$fecreg', '1', '1', '$wips')"; 
		if (!$res_0 = mysql_query($sql_0, $link)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " . mysql_error();
			exit();
		}
		$idArt=mysql_insert_id();
		$t1="comments";
		$t2="usuarios";
		$t4="articles";
		$sql="select 
		$t1.*,
		$t2.idx as usu_idx, 
		$t2.username as usu_username,
		$t2.nombres as usu_nombres,
		$t2.apellidos as usu_apellidos,
		$t2.userlevel as usu_userlevel,
		$t4.nombre as art_nombre,
		$t4.slug as art_slug
		from
		$t1,$t2,$t4
		where
		$t1.idx = '".$idArt."' and
		$t1.user_id = $t2.idx and 
		$t1.article_id = $t4.idx
		order by $t1.fecreg desc ";
		$consulta=mysql_query($sql,$link);
	 	$atabla_comm = array();
		if ($fila_comm= mysql_fetch_assoc($consulta)) {
			$atabla_comm[0] = $fila_comm;
		}
		if ($_SERVER['HTTP_HOST'] <> 'localhost') {
			
			$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = mysql_query($sql_company,$link);
            $atabla_company = array();
            while ($fila_company = mysql_fetch_assoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }
			//Especificamos los datos y configuración del servidor
			$sqlPen="select * from comments  where comment_status_id='1'"; //Devuelve todos los Idiomas
			$consultaPen=mysql_query($sqlPen,$link);
			$numPen=mysql_num_rows($consultaPen);
	
			ob_start(); //Abrimos el buffer			
            include ("phpmaileremail/comment-new-email.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
			
			$mail = new PHPMailer();
			$mail->From = $atabla_comm[0]['usu_username'];
			$mail->FromName = $atabla_comm[0]['usu_nombres'].' '.$atabla_comm[0]['usu_apellidos'];
			$mail->Subject = 'Please moderate: "'.$atabla_comm[0]['art_nombre'].'"‏';
			$mail->AltBody = "";
			$mail->MsgHTML($email_text);
			//$mail->AddAttachment("adjunto.txt");
			//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
			$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
			$mail->IsHTML(true);
			$mail->CharSet = 'UTF-8';
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
				exit();
			} else {
				//echo "Message sent!";
			}
		}
		//header("Location: blog/".$slug."/#comment-".mysql_insert_id());
		$_SESSION['saved_comment'] = "Your comment has been sent and will be publishing within moments, appreciate your patience.";
		header("Location: ../blog/".$atabla_comm[0]['art_slug']);
		exit();
	}
	else
	{
		header("Location: ../comments-post.php");
		exit();
	}	
}
if ($ope == "comment_resp") {

    $comment_parent = isset($_POST['comment_parent']) ? trim($_POST['comment_parent']) : '';
    $id = isset($_POST['id']) ? trim($_POST['id']) : '';
    $slug = isset($_POST['slug']) ? trim($_POST['slug']) : '';
	$wdescrip=isset($_POST["descrip_".$comment_parent]) ? trim($_POST["descrip_".$comment_parent]) : '';;	
	if(!empty($wdescrip))
	{
		$sql_0 = "insert into comments (article_id, descrip, user_id, comment_parent, fecreg, comment_status_id, comment_status_id_salvar, ips)
							  values ('$id', '$wdescrip', '".$_SESSION['user']['idx']."', '$comment_parent', '$fecreg', '1', '1', '$wips')"; 
		if (!$res_0 = mysql_query($sql_0, $link)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " . mysql_error();
			exit();
		}
		
		
		$idArt=mysql_insert_id();
		$t1="comments";
		$t2="usuarios";
		$t4="articles";
		$sql="select 
		$t1.*,
		$t2.idx as usu_idx, 
		$t2.username as usu_username,
		$t2.nombres as usu_nombres,
		$t2.apellidos as usu_apellidos,
		$t2.userlevel as usu_userlevel,
		$t4.nombre as art_nombre,
		$t4.slug as art_slug
		from
		$t1,$t2,$t4
		where
		$t1.idx = '".$idArt."' and
		$t1.user_id = $t2.idx and 
		$t1.article_id = $t4.idx
		order by $t1.fecreg desc ";
		$consulta=mysql_query($sql,$link);
	 	$atabla_comm = array();
		if ($fila_comm= mysql_fetch_assoc($consulta)) {
			$atabla_comm[0] = $fila_comm;
		}
		if ($_SERVER['HTTP_HOST'] <> 'localhost') {
			
			$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = mysql_query($sql_company,$link);
            $atabla_company = array();
            while ($fila_company = mysql_fetch_assoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }
			//Especificamos los datos y configuración del servidor
			$sqlPen="select * from comments  where comment_status_id='1'"; //Devuelve todos los Idiomas
			$consultaPen=mysql_query($sqlPen,$link);
			$numPen=mysql_num_rows($consultaPen);
	
			ob_start(); //Abrimos el buffer			
            include ("phpmaileremail/comment-new-email.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
			
			$mail = new PHPMailer();
			$mail->From = $atabla_comm[0]['usu_username'];
			$mail->FromName = $atabla_comm[0]['usu_nombres'].' '.$atabla_comm[0]['usu_apellidos'];
			$mail->Subject = 'Please moderate: "'.$atabla_comm[0]['art_nombre'].'"‏';
			$mail->AltBody = "";
			$mail->MsgHTML($email_text);
			//$mail->AddAttachment("adjunto.txt");
			//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
			$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
			$mail->IsHTML(true);
			$mail->CharSet = 'UTF-8';
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
				exit();
			} else {
				//echo "Message sent!";
			}
		}
		
		
		$_SESSION['saved_comment'] = "Your comment has been sent and will be publishing within moments, appreciate your patience.";
		header("Location: ../blog/".$atabla_comm[0]['art_slug']);
		exit();
	}
	else
	{
		header("Location: ../comments-post.php");
		exit();
	}	
}
if ($ope == "write-review-login") {
    //echo "Ingrese";
    //exit();
	$wdescrip=trim($descrip);
	if(isset($_SESSION['user']['idx']) and !empty($wdescrip) )
	{
		$fecreg = date("Y-m-d H:i:s");
		$wips = getenv('REMOTE_ADDR');
		$sql_0 = "insert into rating_reviews (project, city, state, descrip, stars, user_id , fecreg, comment_status_id, comment_status_id_salvar, ips )
							  values ('$project', '$city', '$state', '$wdescrip', '$stars', '".$_SESSION['user']['idx']."', '$fecreg', '1', '1', '$wips')"; 
		if (!$res_0 = mysql_query($sql_0, $link)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " . mysql_error();
			exit();
		}
		$idArt=mysql_insert_id();
		
		$t1="rating_reviews";
		$t2="usuarios";
		$sql="
		select 
		rating_reviews.*,
		usuarios.idx as usu_idx, 
		usuarios.username as usu_username,
		usuarios.nombres as usu_nombres,
		usuarios.apellidos as usu_apellidos,
		usuarios.userlevel as usu_userlevel,
		usuarios.slug as usu_slug
		from 
		$t1,$t2
		where
		$t1.idx = '".$idArt."' and
		rating_reviews.user_id = usuarios.idx 
		order by rating_reviews.fecreg desc";

		$consulta=mysql_query($sql,$link);
	 	$atabla_comm = array();
		if ($fila_comm= mysql_fetch_assoc($consulta)) {
			$atabla_comm[0] = $fila_comm;
		}
		if ($_SERVER['HTTP_HOST'] <> 'localhost') {
			
			$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = mysql_query($sql_company,$link);
            $atabla_company = array();
            while ($fila_company = mysql_fetch_assoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }
			//Especificamos los datos y configuración del servidor
			$sqlPen="select * from rating_reviews where comment_status_id='1'"; //Devuelve todos los Idiomas
			$consultaPen=mysql_query($sqlPen,$link);
			$numPen=mysql_num_rows($consultaPen);
	
			ob_start(); //Abrimos el buffer			
            include ("app_email/rating-reviews-new.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
			
			$mail = new PHPMailer();
			$mail->From = $atabla_comm[0]['usu_username'];
			$mail->FromName = $atabla_comm[0]['usu_nombres'].' '.$atabla_comm[0]['usu_apellidos'];
			$mail->Subject = 'Please moderate: "'.$atabla_comm[0]['project'].'"‏';
			$mail->AltBody = "";
			$mail->MsgHTML($email_text);
			//$mail->AddAttachment("adjunto.txt");
			//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
			$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
			$mail->IsHTML(true);
			$mail->CharSet = 'UTF-8';
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
				exit();
			} else {
				//echo "Message sent!";
			}
		}
		//header("Location: blog/".$slug."/#comment-".mysql_insert_id());
		$_SESSION['saved_comment'] = "Your review has been sent and will be publishing within moments, appreciate your patience.";
		header("Location: rating-reviews.php");
		exit();
	}
	else
	{
		header("Location: comments-post.php");
		exit();
	}	
}
if ($ope == "login-write-review") {
		$_SESSION['user']['login'] = false;
		$_SESSION['value_array'] = $_POST;
        $usuarios = addslashes($username);
        $claves = md5(md5(addslashes($pwd)));
        $sql = "SELECT * FROM usuarios where activo='1' and username = '$usuarios' and claves = '$claves'  limit 0,1";
        $consulta = mysql_query($sql, $link);
		 if ($fila = mysql_fetch_array($consulta)) {
            //$this->usuario[]=$fila;
            if ($fila["verif_email"] == 1) {
				unset($_SESSION['user']);
                $_SESSION['user']['login'] = true;
                $_SESSION['user']['idx'] = $fila['idx'];
                $_SESSION['user']['username'] = $fila['username'];
                $_SESSION['user']['nombres'] = $fila['nombres'];
                $_SESSION['user']['apellidos'] = $fila['apellidos'];
                $_SESSION['user']['imagen'] = $fila['imagen'];
				header("Location: rating-reviews.php");
				exit();
			}
			else
			{
				unset($_SESSION['user']);
                $_SESSION['user']['idx_code'] = $fila['idx'];
                $_SESSION['user']['username_code'] = $fila['username'];
                $_SESSION['user']['nombres_code'] = $fila['nombres'];
                $_SESSION['user']['apellidos_code'] = $fila['apellidos'];
                header("Location: my-account-active-account.php");
                exit();
			}
		 }
		 else
		 {
			    $_SESSION['user']['login'] = false;
                $_SESSION['user']['error'] = "Invalid username or password.";
				header("Location: rating-reviews.php");
				exit();
		 }
}
if ($ope == "write-review") {

	$validator->addRule('nombres', 'Is required.', 'required');
	$validator->addRule('apellidos', 'Is required.', 'required');
	$validator->addRule('city', 'Is required.', 'required');
	$validator->addRule('state', 'Is required.', 'required');
	$validator->addRule('username', 'Is required.', 'required');
	$validator->addRule('username', 'Please enter a valid email address.', 'email');
	$validator->addRule('stars', 'Is required.', 'required');
	$validator->addRule('descrip', 'Is required.', 'required');
	$validator->addRule('descrip', 'Your Review must be longer than 2 characters', 'minlength', 2);
	//$validator->addRule('genero', 'Is required.', 'required');
	//--------------------------------------------------------------------------------------------------------------
	$validator->addEntries($_POST);
	$validator->validate();

	//--------------------------------------------------------------------------------------------------------------
	$entries = $validator->getEntries();
	foreach ($entries as $key => $value) {
		${$key} = $value;
	}

	//--------------------------------------------------------------------------------------------------------------
	if ($validator->foundErrors()) {
		$errors = $validator->getErrors();
		$_SESSION['errors']	=$errors;
		$_SESSION['entries']=$entries;
	    header("Location: ../rating-write-review.php");
		exit();
	}
	
	if (empty($errors)) { 
		//echo "Procesar Envio";
		$fecreg = date("Y-m-d H:i:s");
		$wips = getenv('REMOTE_ADDR');
		
		$nombres 	= trim($_POST['nombres']);
		$apellidos	= trim($_POST['apellidos']);
		$username	= trim($_POST['username']);
		$project	= trim($_POST['project']);
		$city		= trim($_POST['city']);
		$state		= trim($_POST['state']);
		$telephone	= trim($_POST['telephone']);
		$descrip	= trim($_POST['descrip']);
		$stars		= trim($_POST['stars']);

		$sql_0 = "insert into rating_reviews (nombres, apellidos, username, project, city, state, telephone, genero, descrip, stars,  fecreg, comment_status_id, comment_status_id_salvar, avatar, ips )
							  values ('$nombres', '$apellidos', '$username', '$project', '$city', '$state', '$telephone', '$genero', '$descrip', '$stars', '$fecreg', '1', '1', '$avatar', '$wips')"; 
		
		if (!$res_0 = mysql_query($sql_0, $link)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " . mysql_error();
			exit();
		}
		if ($_SERVER['HTTP_HOST'] <> 'localhost') {
			$idArt=mysql_insert_id();
			$t1="rating_reviews";
			$sql="
			select 
			rating_reviews.*
			from 
			$t1
			where
			$t1.idx = '".$idArt."' 
			order by rating_reviews.fecreg desc";
	
			$consulta=mysql_query($sql, $link);
			$atabla_comm = array();
			if ($fila_comm= mysql_fetch_assoc($consulta)) {
				$atabla_comm[0] = $fila_comm;
			}
		
			$sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = mysql_query($sql_company,$link);
            $atabla_company = array();
            while ($fila_company = mysql_fetch_assoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }
			//Especificamos los datos y configuración del servidor
			$sqlPen="select * from rating_reviews where comment_status_id='1'"; //Devuelve todos los Idiomas
			$consultaPen=mysql_query($sqlPen,$link);
			$numPen=mysql_num_rows($consultaPen);
	
			ob_start(); //Abrimos el buffer			
            include ("phpmaileremail/rating-reviews-new.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
			
			$mail = new PHPMailer();
			$mail->From = $atabla_comm[0]['username'];
			$mail->FromName = $atabla_comm[0]['nombres'].' '.$atabla_comm[0]['apellidos'];
			$mail->Subject = 'Please moderate: Review "'.$atabla_comm[0]['project'].'"‏';
			$mail->AltBody = "";
			$mail->MsgHTML($email_text);
			//$mail->AddAttachment("adjunto.txt");
			//$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
			$mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
			$mail->IsHTML(true);
			$mail->CharSet = 'UTF-8';
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
				exit();
			} else {
				//echo "Message sent!";
			}
		}
						
		$_SESSION['data_saved'] = "Your review has been sent and will be publishing within moments, appreciate your patience.";
		header("Location: ../reviews.php");
		exit();
	
	}	
}
if ($ope == "sendMailContactUs") {
	$validator->addRule('fullname', 'Is required.', 'required');
	$validator->addRule('address', 'Is required.', 'required');
	$validator->addRule('telephone', 'Is required.', 'required');
	$validator->addRule('email', 'Is required.', 'required');
	$validator->addRule('email', 'Please enter a valid email address.', 'email');
	$validator->addRule('captcha', 'Is required.', 'required');
	$validator->addRule('captcha', 'Verification code incorrect, please try again.', 'captcha');
	//$validator->addRule('genero', 'Is required.', 'required');
	//--------------------------------------------------------------------------------------------------------------
	$validator->addEntries($_POST);
	$validator->validate();

	//--------------------------------------------------------------------------------------------------------------
	$entries = $validator->getEntries();
	foreach ($entries as $key => $value) {
		${$key} = $value;
	}

	//--------------------------------------------------------------------------------------------------------------
	if ($validator->foundErrors()) {
		$errors = $validator->getErrors();
		$_SESSION['errors']	=$errors;
		$_SESSION['entries']=$entries;
	    header("Location: ../contact-us.php");
		exit();
	}	
	if (empty($errors)) { 
		//echo "Procesar Envio";
		$id 		= 1;
		$fullname 	= trim($_POST['fullname']);// si no existe poner Anonymous
		$address	= trim($_POST['address']);
		$city		= trim($_POST['city']);
		$state		= trim($_POST['state']);
		$zipcode	= trim($_POST['zipcode']);
		$telephone	= trim($_POST['telephone']);
		$email		= trim($_POST['email']);
		$comments	= trim($_POST['comments']);
		
		$texto="";
		$texto=$texto . "Contact from ".$_SERVER['HTTP_HOST']." ".date('M d, Y') . "\n";		
		$texto=$texto . "\n";
		$texto=$texto . "IP Address: " . getenv('REMOTE_ADDR') . "\n";
		$texto=$texto . "Full Name: " .$fullname.  "\n";//Default
		$texto=$texto . "Address: " .$address.  "\n";
		$texto=$texto . "City: " .$city.  "\n";
		$texto=$texto . "State: " .$state.  "\n";
		$texto=$texto . "Zip Code: " .$zipcode.  "\n";
		$texto=$texto . "Telephone: " .$telephone.  "\n";
		$texto=$texto . "Email Address: " .$email.  "\n";
		$texto=$texto . "Comments: " .$comments.  "\n";
		
		$wfecreg = date("Y-m-d H:i:s");
		$wips=getenv('REMOTE_ADDR');
		$wemail=$data['email'];
		$sql_record = "insert into formularios_record_list (formularios_id, nombre, email, phone, fecreg, descrip, ips) 
				value 
				('$id', '$fullname', '$email', '$telephone', '$wfecreg', '$texto', '$wips')";
		if (!$res_record = mysql_query($sql_record, $link)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " . mysql_error();
			exit();
		}
		
		$widx_form = $id;
		$waut_autoresponder = 0;
		if ($_SERVER['HTTP_HOST'] <> 'localhost' )
		{	
			$sql = "SELECT * FROM formularios where idx = '$widx_form' ";
			$res=mysql_query($sql, $link);
			if ($col=mysql_fetch_array($res))			
			{	
				$f = explode("/", $_SERVER['PHP_SELF']);
				$urlfin1 = "http://".$_SERVER['HTTP_HOST'];
				
				$waut_email  = $col['email']; 
				$waut_nombre = $col['autonombre']; 
				$waut_asunto = $col['autoasunto']; 
				$waut_descri = str_replace('/userfiles/',$urlfin1.'/userfiles/',$col['autodescrip']); 
				$waut_autoresponder = $col['autoresponder']; 
				
				//$remitente="Remitente<calderon_edsel@hotmail.com>";
				$remitente=$fullname."<".$email.">";
				$sheader="From:".$remitente."\nReply-To:".$remitente."\n";
				
				mail($col['email'], "Contact from ".$_SERVER['HTTP_HOST']." : ". $email , $texto, $sheader) or die(mysql_error());
				for($ifor=1; $ifor<=10; $ifor++ )
				{
					$wemail_sec = $col['email'.$ifor];
					if(!empty($wemail_sec))
					{
						$remitente=$fullname."<".$email.">";
						$sheader="From:".$remitente."\nReply-To:".$remitente."\n";
						mail($wemail_sec, "Contact from ".$_SERVER['HTTP_HOST']." : ".$email , $texto, $sheader) or die(mysql_error());
					}
				}
				//-------------------------------------------------------------------------------------------------
		
			}
				
			if ($waut_autoresponder == '1')
			{		
				$texto	=	$waut_descri;
				$wemail = 	$email; 
				$nombre_origen	= $waut_nombre;
				$email_origen	= $waut_email;
				//========= INICIO ENVIO MAIL ==============
	
				//-----------------------------------------------------------------// 
				$headers  = "From: $nombre_origen <$email_origen> \r\n"; 
				$headers .= "Return-Path: <$email_origen> \r\n"; 
				$headers .= "Reply-To: $email_origen \r\n"; 
				//$headers .= "Cc: $email_copia \r\n"; 
				//$headers .= "Bcc: $email_ocultos \r\n"; 
				$headers .= "X-Sender: $email_origen \r\n"; 
				//$headers .= "X-Mailer: [Habla software de noticias v.1.0] \r\n"; 
				//$headers .= "X-Priority: 3 \r\n"; 
				$headers .= "MIME-Version: 1.0 \r\n"; 
				$headers .= "Content-Transfer-Encoding: 7bit \r\n"; 
				//$headers .= "Disposition-Notification-To: \"$nombre_origen\" <$email_origen> \r\n"; 
				//-----------------------------------------------------------------// 
				$formato          = "html";
				if($formato == "html") 
				{
					$headers .= "Content-Type: text/html; charset=iso-8859-1 \r\n";  
				} 
				else 
				{ $headers .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";  
				} 
				if (@mail($wemail, $waut_asunto, $texto, $headers))  
				{ 
				//			echo "Su email a sido correctamente Enviado!";  
				}  
				//========= FIN ENVIO MAIL ==============
			
			// fin autoresponder	
			}
		}
		
	   header('location: ../contact-us-env.php');
	   exit();
	}	
}
?>