<?php require_once("header_index.php"); ?>
<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header "><?=isset($dataGlobal['titleLogin']) ? $dataGlobal['titleLogin'] : 'Login'?></div>
      <?php
				if(!empty($_SESSION['error_array'])){
					echo "<div align=center><strong><font size=\"2\" color=\"#ff0000\">".$_SESSION['error_array']."</font></strong></div><br>";
					unset($_SESSION['error_array']);
				}
			?>
      <div class="card-body">
        <form id="form1" name="form1" method="post" action="user.php">
          <div class="form-group">
            <label for="exampleInputEmail1"><?=isset($dataGlobal['labelUserLogin']) ? $dataGlobal['labelUserLogin'] : 'Username'?></label>
            <input type="text"  aria-describedby="Username"  name="usuarios" class="form-control" id="usuarios" >
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?=isset($dataGlobal['labelPassLogin']) ? $dataGlobal['labelPassLogin'] : 'Password'?></label>
            <input class="form-control" id="exampleInputPassword1" type="password" name="claves" >
          </div>
          <div class="form-group">
            <div class="form-check">
            	<!-- <label class="form-check-label">
                	<input class="form-check-input" type="checkbox"> Remember Password
            	</label> -->
            </div>
          </div>
          <input type="hidden" name="sublogin" value="1">
			<input class="btn btn-primary btn-block" type="submit" name="button" id="button" value="<?=isset($dataGlobal['botonLogin']) ? $dataGlobal['botonLogin'] : 'Enter'?>" style="cursor:pointer" />
        </form>
        	<?php
		if (isset($_SESSION['value_temp_logeo']) && $_SESSION['value_temp_logeo'] == 'NO' or  empty($_SESSION['value_temp_logeo']))
		{
			unset($_SESSION['value_temp_logeo']);
			unset($_SESSION['value_temp_usuarios']);
			unset($_SESSION['value_temp_remember']);
		}
	?>
        <div class="text-center">
        	<div class="infosx">
			</div>
          <a class="d-block small" href="forgot.php"><?=isset($dataGlobal['forgot']) ? $dataGlobal['forgot'] : 'Forgot Password?'?></a>
        </div>
      </div>
    </div>
  </div>

<?php include("footer_index.php");?>