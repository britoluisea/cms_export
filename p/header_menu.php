        <nav class="navbar navbar-default no-margin navbar-fixed-top" id="navTop">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand " style="width: 170px;">
                    <a class="navbar-brand" href="<?=URL_HOME_P?>" style="padding: 5px 15px;"><img src="<?=$logo_cms?>" alt="logo" class="logo" style="width:160px;"></a>
                </div>
                <!-- navbar-header-->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <?php if (isset($atabla_perm_menu['8']['idxpermisocategory']) && $atabla_perm_menu['8']['idxpermisocategory'] > 0) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                    <?=isset($dataGlobal['web_content']) ? $dataGlobal['web_content'] : 'Web Content'?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <?php if (isset($atabla_perm_menu['8001']['idxpermisocategory']) && $atabla_perm_menu['8001']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>web-content/pages.php"><?=isset($dataGlobal['web_content_pages']) ? $dataGlobal['web_content_pages'] : 'Pages'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['8002']['idxpermisocategory']) && $atabla_perm_menu['8002']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>web-content/boxes.php"><?=isset($dataGlobal['web_content_boxes']) ? $dataGlobal['web_content_boxes'] : 'Boxes'?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (isset($atabla_perm_menu['9']['idxpermisocategory']) && $atabla_perm_menu['9']['idxpermisocategory'] > 0) { ?>
                            <li class="dropdown"  >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?=isset($dataGlobal['web_tools']) ? $dataGlobal['web_tools'] : 'Web Tools'?> <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php if (isset($atabla_perm_menu['9012']['idxpermisocategory']) && $atabla_perm_menu['9012']['idxpermisocategory'] > 0) { ?>  
                                    <li ><a href="<?=SERVER_P?>slider/index.php"><?=isset($dataGlobal['slider']) ? $dataGlobal['slider'] : 'Slider'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['9005']['idxpermisocategory']) && $atabla_perm_menu['9005']['idxpermisocategory'] > 0) { ?>        
                                    <li ><a href="<?=SERVER_P?>gallery/index.php"><?=isset($dataGlobal['gallery']) ? $dataGlobal['gallery'] : 'Gallery'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['9008']['idxpermisocategory']) && $atabla_perm_menu['9008']['idxpermisocategory'] > 0) { ?>        
                                    <li ><a href="<?=SERVER_P?>form/index.php"><?=isset($dataGlobal['forms']) ? $dataGlobal['forms'] : 'Forms/Autoresponder'?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (isset($atabla_perm_menu['17']['idxpermisocategory']) && $atabla_perm_menu['17']['idxpermisocategory'] > 0) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                    <?=isset($dataGlobal['custom_tools']) ? $dataGlobal['custom_tools'] : 'Custom Tools'?> <b class="caret"></b> 
                                </a>
                                <ul class="dropdown-menu">
                                    <?php if (isset($atabla_perm_menu['17015']['idxpermisocategory']) && $atabla_perm_menu['17015']['idxpermisocategory'] > 0) { ?>        
                                    <li ><a href="<?=SERVER_P?>project/index.php"><?=isset($dataGlobal['projects']) ? $dataGlobal['projects'] : 'Projects'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17007']['idxpermisocategory']) && $atabla_perm_menu['17007']['idxpermisocategory'] > 0) { ?>                
                                    <!--li ><a href="<?=SERVER_P?>page-builder-content.php">Page Builder for Content</a></li-->
                                    <?php } ?>        
                                    <?php if (isset($atabla_perm_menu['17021']['idxpermisocategory']) && $atabla_perm_menu['17021']['idxpermisocategory'] > 0) { ?>                
                                    <!--li ><a href="<?=SERVER_P?>product-d-list.php">Projects - Category</a></li-->
                                    <?php } ?>          
                                    <?php if (isset($atabla_perm_menu['17014']['idxpermisocategory']) && $atabla_perm_menu['17014']['idxpermisocategory'] > 0) { ?>                        
                                    <!--li ><a href="<?=SERVER_P?>material/index.php">Material List</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17016']['idxpermisocategory']) && $atabla_perm_menu['17016']['idxpermisocategory'] > 0) { ?>                        
                                    <!--li><a href="<?=SERVER_P?>real-estate-list.php">Real Estate List</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17019']['idxpermisocategory']) && $atabla_perm_menu['17019']['idxpermisocategory'] > 0) { ?>
                                    <!--li ><a href="<?=SERVER_P?>product-unilock-list.php">Catalog Unilock</a></li-->                                
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17009']['idxpermisocategory']) && $atabla_perm_menu['17009']['idxpermisocategory'] > 0) { ?>
                                    <!--li ><a href="<?=SERVER_P?>product-a-list.php">Catalog Nicolock</a></li-->                                
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17020']['idxpermisocategory']) && $atabla_perm_menu['17020']['idxpermisocategory'] > 0) { ?>
                                    <!--li ><a href="<?=SERVER_P?>product-c-list.php">Catalog Stone Veneer</a></li-->
                                    <?php } ?>    
                                    <?php if (isset($atabla_perm_menu['17023']['idxpermisocategory']) && $atabla_perm_menu['17023']['idxpermisocategory'] > 0) { ?>
                                    <!--li ><a href="<?=SERVER_P?>ads-box.php">Ads Box</a></li-->
                                    <?php } ?>      
                                    <?php if (isset($atabla_perm_menu['17025']['idxpermisocategory']) && $atabla_perm_menu['17025']['idxpermisocategory'] > 0) { ?>   
                                    <!--li ><a href="<?=SERVER_P?>news.php">News</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['17026']['idxpermisocategory']) && $atabla_perm_menu['17026']['idxpermisocategory'] > 0) { ?>   
                                    <li ><a href="<?=SERVER_P?>review/index.php"><?=isset($dataGlobal['rating_reviews']) ? $dataGlobal['rating_reviews'] : 'Rating & Reviews'?></a></li>
                                    <?php } ?>  
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (isset($atabla_perm_menu['19']['idxpermisocategory']) && $atabla_perm_menu['19']['idxpermisocategory'] > 0) { ?>
                            <li  class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                    <?=isset($dataGlobal['blog']) ? $dataGlobal['blog'] : 'Blog'?> <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php if (isset($atabla_perm_menu['19001']['idxpermisocategory']) && $atabla_perm_menu['19001']['idxpermisocategory'] > 0) { ?>
                                        <li ><a href="<?=SERVER_P?>blog/articles.php"><?=isset($dataGlobal['post']) ? $dataGlobal['post'] : 'Post'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19002']['idxpermisocategory']) && $atabla_perm_menu['19002']['idxpermisocategory'] > 0) { ?>
                                        <li ><a href="<?=SERVER_P?>blog/comments/index.php"><?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments'?></a></li>
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19003']['idxpermisocategory']) && $atabla_perm_menu['19003']['idxpermisocategory'] > 0) { ?>
                                        <!--li ><a href="<?=SERVER_P?>article-widget.php">Widgets Sidebar</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19005']['idxpermisocategory']) && $atabla_perm_menu['19005']['idxpermisocategory'] > 0) { ?>
                                        <!--li ><a href="<?=SERVER_P?>articles-box-home.php">Widgets Content</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19006']['idxpermisocategory']) && $atabla_perm_menu['19006']['idxpermisocategory'] > 0) { ?>
                                        <!--li ><a href="#">Widgets Footer</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19007']['idxpermisocategory']) && $atabla_perm_menu['19007']['idxpermisocategory'] > 0) { ?>
                                        <!--li ><a href="<?=SERVER_P?>articles-widget-list.php">Widget List</a></li-->
                                    <?php } ?>
                                    <?php if (isset($atabla_perm_menu['19004']['idxpermisocategory']) && $atabla_perm_menu['19004']['idxpermisocategory'] > 0) { ?>
                                        <li ><a href="<?=SERVER_P?>blog/articles-setting.php"><?=isset($dataGlobal['blog_setting']) ? $dataGlobal['blog_setting'] : 'Blog Setting'?></a></li>
                                    <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if (isset($atabla_perm_menu['20']['idxpermisocategory']) && $atabla_perm_menu['20']['idxpermisocategory'] > 0) { ?>
                        <!--li  class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                Store <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <?php if (isset($atabla_perm_menu['20001']['idxpermisocategory']) && $atabla_perm_menu['20001']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-orders.php">Order List </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20002']['idxpermisocategory']) && $atabla_perm_menu['20002']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-catalog.php">Catalog List</a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20003']['idxpermisocategory']) && $atabla_perm_menu['20003']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-group.php">Group </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20004']['idxpermisocategory']) && $atabla_perm_menu['20004']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-category.php">Categories </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20005']['idxpermisocategory']) && $atabla_perm_menu['20005']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-manufacturers.php">Manufacturers </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20006']['idxpermisocategory']) && $atabla_perm_menu['20006']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-color.php">Color </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20007']['idxpermisocategory']) && $atabla_perm_menu['20007']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-size.php">Size  </a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20009']['idxpermisocategory']) && $atabla_perm_menu['20009']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-order-notification.php">Order Notification</a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20011']['idxpermisocategory']) && $atabla_perm_menu['20011']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-widget.php">Widget</a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['20010']['idxpermisocategory']) && $atabla_perm_menu['20010']['idxpermisocategory'] > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>store-tax.php">Setup</a></li>
                                <?php } ?>
                            </ul>
                        </li -->
                        <?php } ?>
                    <?php if (isset($atabla_perm_menu['10']['idxpermisocategory']) && $atabla_perm_menu['10']['idxpermisocategory'] > 0) { ?>
                        <li  class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                <?=isset($dataGlobal['marketing']) ? $dataGlobal['marketing'] : 'Marketing'?> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">

                                <?php if (isset($atabla_perm_menu['20012']['idxpermisocategory']) > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>marketing/menu-website.php"><?=isset($dataGlobal['menu_website']) ? $dataGlobal['menu_website'] : 'Menu Website'?></a></li> 
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['10001']['idxpermisocategory']) > 0) { ?>
                                    <li><a href="<?=SERVER_P?>marketing/universal-meta-tags.php"><?=isset($dataGlobal['universal_meta']) ? $dataGlobal['universal_meta'] : 'Universal Meta Tags'?></a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['10004']['idxpermisocategory']) > 0) { ?>
                                    <li><a href="<?=SERVER_P?>marketing/xml-sitemaps.php"><?=isset($dataGlobal['xml_site']) ? $dataGlobal['xml_site'] : 'XML Sitemaps'?></a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['10005']['idxpermisocategory']) > 0) { ?>
                                    <li><a href="<?=SERVER_P?>marketing/robot-files.php"><?=isset($dataGlobal['robot_file']) ? $dataGlobal['robot_file'] : 'Robot File'?></a></li>
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['10006']['idxpermisocategory']) > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>marketing/google-analytics.php"><?=isset($dataGlobal['google_analytics']) ? $dataGlobal['google_analytics'] : 'Google Analytics'?></a></li>        
                                <?php } ?>
                                <?php if (isset($atabla_perm_menu['10003']['idxpermisocategory']) > 0) { ?>
                                    <li ><a href="<?=SERVER_P?>builder/index.php"><?=isset($dataGlobal['page_builder_SEO']) ? $dataGlobal['page_builder_SEO'] : 'Page Builder SEO'?></a></li>          
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                   
                   </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?=isset($dataGlobal['my_account']) ? $dataGlobal['my_account'] : 'My Account'?> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <?php if (isset($_SESSION['value_admin_userlevel']) && $_SESSION['value_admin_userlevel'] == 998 or $_SESSION['value_admin_userlevel'] == 999) { ?>
                                <li><a href="<?=SERVER_P?>my-account/my-account.php"><?=isset($dataGlobal['account_info']) ? $dataGlobal['account_info'] : 'Account Info'?></a></li>
                                <?php } ?>
                                <?php if (isset($_SESSION['value_admin_userlevel']) && $_SESSION['value_admin_userlevel'] == 998 or $_SESSION['value_admin_userlevel'] == 999) { ?>
                                <li><a href="<?=SERVER_P?>users/account/index.php"><?=isset($dataGlobal['Users']) ? $dataGlobal['Users'] : 'Users'?></a></li>
                                <?php } ?>
                                <?php if (isset($_SESSION['value_admin_userlevel']) && $_SESSION['value_admin_userlevel'] == 998 or $_SESSION['value_admin_userlevel'] == 999) { ?>
                                <li><a href="<?=SERVER_P?>my-account/change-password.php"><?=isset($dataGlobal['changePass']) ? $dataGlobal['changePass'] : 'Change Password'?></a></li>
                                <?php } ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?=SERVER_P?>cerrar.php"><?=isset($dataGlobal['log_out']) ? $dataGlobal['log_out'] : 'Log Out'?></a></li>
                            </ul>
                        </li>
                        <?php if(($userlevel != '999' && $userlevel != '1000')){?>
                        <li class="dropdown dropdown-large">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <b class="caret"></b></a>
                            
                            <ul class="dropdown-menu dropdown-menu-large col-sm-5">
                                <li class="col-sm-6">
                                    <ul>
                                        <li class="dropdown-header"><?=isset($dataGlobal['web_content']) ? $dataGlobal['web_content'] : 'Web Content'?></li>
                                        <li><a href="<?=SERVER_P?>web-master/web-contents/index.php"><?=isset($dataGlobal['edit_web_content_page']) ? $dataGlobal['edit_web_content_page'] : 'Edit Web PAGE'?></a></li>
                                        <li><a href="<?=SERVER_P?>web-master/web-contents/box.php"><?=isset($dataGlobal['edit_web_content_box']) ? $dataGlobal['edit_web_content_box'] : 'Edit Web BOX'?></a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header"><?=isset($dataGlobal['account']) ? $dataGlobal['account'] : 'Account'?></li>
                                        <li><a href="<?=SERVER_P?>web-master/account/index.php"><?=isset($dataGlobal['webmaster_support']) ? $dataGlobal['webmaster_support'] : 'Webmaster - Support'?></a></li> 
                                        <li><a href="<?=SERVER_P?>web-master/account/privileges.php"><?=isset($dataGlobal['privileges']) ? $dataGlobal['privileges'] : 'Privileges'?></a></li>
                                        <li><a href="<?=SERVER_P?>web-master/account/inactive.php"><?=isset($dataGlobal['alls_privileges']) ? $dataGlobal['alls_privileges'] : 'Alls Privileges'?></a></li>
                                        <li><a href="<?=SERVER_P?>web-master/account/info.php"><?=isset($dataGlobal['account_info']) ? $dataGlobal['account_info'] : 'Account Info'?></a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-6">
                                    <ul>
                                        <li class="dropdown-header"><?=isset($dataGlobal['website']) ? $dataGlobal['website'] : 'Website'?></li>
                                        <li><a href="<?=SERVER_P?>web-master/website/index.php"><?=isset($dataGlobal['website_status']) ? $dataGlobal['website_status'] : 'Website Status'?></a></li>  
                                        <li><a href="<?=SERVER_P?>web-master/account/label.php"><?=isset($dataGlobal['white_label']) ? $dataGlobal['white_label'] : 'CMS Options'?></a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header"><?=isset($dataGlobal['template']) ? $dataGlobal['template'] : 'Template'?></li>
                                        <li><a href="<?=SERVER_P?>themes/index.php"><?=isset($dataGlobal['template_active']) ? $dataGlobal['template_active'] : 'Active Template'?></a></li> 
                                        <li><a href="<?=SERVER_P?>themes/install.php"><?=isset($dataGlobal['template_install']) ? $dataGlobal['template_install'] : 'Install Template'?></a></li> 
                                        <li class="divider"></li>
                                        <li class="dropdown-header"><?=isset($dataGlobal['lenguaje']) ? $dataGlobal['lenguaje'] : 'CMS Languaje'?></li>
                                        <li><a href="<?=SERVER_P?>web-master/language.php"><?=isset($dataGlobal['change_lenguaje']) ? $dataGlobal['change_lenguaje'] : 'Change Lenguaje'?></a></li> 
                                    </ul>
                                </li>
                            </ul>                    
                        </li>
                        <?php } ?>
                        
                        <li><a href="<?=SERVER?>" title="<?=isset($dataGlobal['visite_site']) ? $dataGlobal['visite_site'] : 'Visit Site Now!'?>"   target="_blank"><i class="fa fa-external-link"></i></a></li>
                    </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
            </div>
        </nav>