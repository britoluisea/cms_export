<?php
class App_Model_Usuarios
{
	protected $_name = 'usuarios';
	
	
	private $contenidoListar= array();
	private $contenidoListarPag= array();
	private $contenido;
	
	public function urls_amigables($url) {
    //$url = strtolower($url);
		$url = mb_convert_case($url, MB_CASE_LOWER, "UTF-8");
		$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
		$repl = array('a', 'e', 'i', 'o', 'u', 'n');
		$url = str_replace($find, $repl, $url);
		$find = array(' ', '&', '\r\n', '\n', '+');
		$url = str_replace($find, '-', $url);
		$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
		$repl = array('', '-', '');
		$url = preg_replace($find, $repl, $url);
		return $url;
	}
	public function add(array $data)
	{	
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/
		//exit();
		$tb = "usuarios";
		$tipousuario=$data['tipousuario'];
		$username=$data['username'];
		$claves=$data['claves'];
		$nombres=$data['nombres'];
		$apellidos=$data['apellidos'];
		$direccion=$data['direccion'];
		$ciudad=$data['ciudad'];
		$estado=$data['estado'];
		$zipcode=$data['zipcode'];
		$telefono=$data['telefono'];
		$activo=$data['activo'];
		
		$username = trim(strtolower($username));
		$wusername=explode('@',$username);
		$slug = $this->urls_amigables($wusername[0]);
		$wfec1 = date("Y-m-d h:m:s");
		$claves = md5(md5($claves));
		$sql_0 = "insert into $tb (
		 idxamusuario,username,claves,nombres,apellidos,direccion, ciudad, estado, zipcode, telefono, userlevel, fecreg, slug, activo)
		 values 
		 ('1','$username','$claves','$nombres','$apellidos','$direccion','$ciudad','$estado','$zipcode','$telefono','$tipousuario','$wfec1','$slug' ,'$activo')"; 
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            exit();
        }  
		header("Location: users.php");
   		exit();
	} 
	public function edit(array $data)
	{	
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/
		//exit();
		$tb = "usuarios";
		$i=$data['i'];
		$tipousuario=$data['tipousuario'];
		$username=$data['username'];
		$nombres=$data['nombres'];
		$apellidos=$data['apellidos'];
		$direccion=$data['direccion'];
		$ciudad=$data['ciudad'];
		$estado=$data['estado'];
		$zipcode=$data['zipcode'];
		$telefono=$data['telefono'];
		$activo=$data['activo'];
		
		$username = trim(strtolower($username));
		$wusername=explode('@',$username);
		$slug = $this->urls_amigables($wusername[0]);
		$wfec1 = date("Y-m-d h:m:s");
			
		$sql_0 = "update $tb set  
			userlevel = '$tipousuario',
			username = '$username',
			nombres = '$nombres',
			apellidos = '$apellidos',
			direccion = '$direccion',
			ciudad = '$ciudad',
			estado = '$estado',
			zipcode = '$zipcode',
			telefono = '$telefono',
			slug = '$slug',
			fecmod = '$wfec1',
			activo = '$activo'
		 	where
		 	idx = '$i' and idxamusuario='1' ";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            exit();
        }
		
		if ($data['chkpass'] == 1) {
			$claves=$data['claves'];
            $claves = md5(md5($claves));
			$sql_0 = "update $tb set  
			claves = '$claves'
		 	where
		 	idx = '$i' and idxamusuario='1' ";
			if (!$res_0 = ejecutar($sql_0)) {
				echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
				exit();
			}

        }
		header("Location: users.php");
   		exit();
	} 
}
?>