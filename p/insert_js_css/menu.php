<aside style=" height: 500px;width: 100%;">
   <div id="resize">&laquo;</div>
   <div id="resize2">&raquo;</div>
    <div id="lateral">
      <div id="navl1">
        <a href="index.php">
            <h2 class="text-uppercase">
                <?=isset($dataGlobal['Css & Js']) ? $dataGlobal['Css & Js'] : 'Css & Js'?>
            </h2>
        </a>
        <ul id="menua" class="genial_menu">
        <li class="menucat1">
            <ul class="submenu">
                <li >
                    <a onclick="css()" >
                        <span style="font-size:12px;">
                            <?=isset($dataGlobal['Style Css']) ? $dataGlobal['Style Css'] : 'Style Css'?>
                        </span>
                    </a>            
                </li>
            </ul>
        </li>
        <li class="menucat1">
            <ul class="submenu">
                <li >
                    <a href="index.php">
                        <?=isset($dataGlobal['Javascript']) ? $dataGlobal['Javascript'] : 'Javascript'?>
                    </a>
                </li>
            </ul>
        </li>
        </ul>
        <div class="sidebar-collapse" id="sidebar-collapse">
            <i class="fa fa-double-angle-left" ></i>
        </div>
      </div>
     </div>
     <script type="text/javascript">
            function css() {
                $("#b-ody").load("css.php");
                
            }
            function js() {
                $("#b-ody").load("js.php");
                
            }
         </script>
</aside>
