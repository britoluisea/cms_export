  <div class="row" style="margin-bottom: 30px" id="r-ow">
    <div class="col-md-12">
      <a onclick="form()" class="btn btn-small btn-primary">
        <i class="fa fa-plus"></i> 
          <?=isset($dataGlobal['add_new_css']) ? $dataGlobal['add_new_css'] : 'Add css'?>
      </a>
    </div>
  </div>
        <script type="text/javascript">
          function form() {
            $("#r-ow").css("display", "none");
            $("#bo-dy").load("Css-&-Js-form.php");
            setInterval(cambio, 1000);
          }
          function cambio() {
            var x = document.getElementById("key").value;
            if (x == 0) {
                  document.getElementById("key").value = "css";
                  clearInterval(x);
            }else{
                  if (x == 'js') {
                  document.getElementById("key").value = "css";
                  clearInterval(x);
                  }
                }
          }
        </script>
  <div id="bo-dy">
    <div class="t-head">
      <div >Item</div>
      <div >Name</div>
      <div >Key</div>
      <div >Value</div>
      <div >Orden</div>
      <div >Created</div>
      <div >Modified</div>
      <div >Edit</div>
      <div >Delete</div>
    </div>
    <?php
      session_start();
      include "../../cn/cnx.php";
      include "../system/functions.php";
      $sql="SELECT * FROM tools_customize where clave = 'css'";
      $consulta=ejecutar($sql);
      $num_reg=numRows($consulta);
      echo '<tbody id="banner_tbody">';
      if($num_reg > 0){
        for ($i = 0; $i < $num_reg; $i++) {
          $data=fetchArray($consulta);
          echo '<div class="t-body">
          <div id="idx'.$i.'">'.$data[0].'</div>
          <div>'.$data[1].'</div>
          <div>'.$data[2].'</div>
          <div>'.stripslashes($data[3]).'</div>
          <div>'.$data[4].'</div>
          <div>'.$data[5].'</div>
          <div>'.$data[6].'</div>
          <div >
            <a onclick="edit()" class="btn btn-small btn-primary"><i class="fa fa-edit"></i> Edit</a>
          </div>
          <script type="text/javascript">
            function edit() {
              var edit = "'.$data[0].'";
              window.location.href = window.location.href + "?edi=" + edit;
            }
          </script>
          <div>
            <a onclick="dele()" class="btn btn-small btn-danger" style="padding:6px 15px;">
              <i class="fa fa-trash"></i> Delete
            </a>
          </div>
          <script type="text/javascript">
            function dele() {
              var dele = "'.$data[0].'";
              window.location.href = window.location.href + "?del=" + dele;
            }
          </script> 
    </div>
          ';}
      }else{ ?>
        <div class="t-body">
           <div style="width: 100%; text-align: center;"><?=isset($dataGlobal['loading']) ? $dataGlobal['loading'] : 'Loading'?>...
           </div>
        </div >
        <?php } ?>
    </div>