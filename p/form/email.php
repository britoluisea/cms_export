<?php 
require_once(dirname(__FILE__).'/../header_box.php'); 
include("email_f.php"); 
	if($Result1 == true){
    $success =isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
    echo'<script>$(function() {
    window.parent.alerValid("Success", "'.$success.'", "6000");
    window.parent.closeModal_hide();
		});</script>';
	}
?>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td  valign="top" style="padding: 10px;">
      <form method="post" name="form1" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data">
        <table width="100%" class="height100" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td>
              <div class="h4">
                <?=isset($dataGlobal['email_fwd']) ? '[ '.$dataGlobal['email_fwd'].' ]' : '[ Email fwd  ]'?>
                <?php if(empty($_GET['i'])){ ?>
                  <input type="hidden" name="MM_insert" value="form1">
                <?php }else{?>
                  <input type="hidden" name="MM_update" value="form1">
                  <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
                <?php }?>
              </div>
            </td>
            <td>
              <table border="0" align="right" cellpadding="5" cellspacing="0">
                <tr>
                  <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'save'?>" /></td>
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table class="" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
              <section>
                <div id="contenido2">
                    <div class="col-xs-6">
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 1</label>
                          <input type="text" id="email1" class="form-control" name="email1" value="<?php echo $row_u['email1']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 2</label>
                          <input type="text" id="email2" class="form-control" name="email2" value="<?php echo $row_u['email2']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 3</label>
                          <input type="text" id="email3" class="form-control" name="email3" value="<?php echo $row_u['email3']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 4</label>
                          <input type="text" id="email4" class="form-control" name="email4" value="<?php echo $row_u['email4']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 5</label>
                          <input type="text" id="email5" class="form-control" name="email5" value="<?php echo $row_u['email5']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 6</label>
                          <input type="text" id="email6" class="form-control" name="email6" value="<?php echo $row_u['email6']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 7</label>
                          <input type="text" id="email7" class="form-control" name="email7" value="<?php echo $row_u['email7']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 8</label>
                          <input type="text" id="email8" class="form-control" name="email8" value="<?php echo $row_u['email8']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 9</label>
                          <input type="text" id="email9" class="form-control" name="email9" value="<?php echo $row_u['email9']; ?>">
                        </div>
                        <div class="form-group">
                          <label for=""><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> 10</label>
                          <input type="text" id="email10" class="form-control" name="email10" value="<?php echo $row_u['email10']; ?>">
                        </div>
                    </div>
                </div>
              </section>
            </td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<script type="text/javascript">
  var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email1");
  var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "email2");
  var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "email3");
  var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "email4");
  var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5", "email5");
  var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6", "email6");
  var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7", "email7");
  var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8", "email8");
  var sprytextfield9 = new Spry.Widget.ValidationTextField("sprytextfield9", "email9");
  var sprytextfield10 = new Spry.Widget.ValidationTextField("sprytextfield10", "email10");
</script>  
<?php include(dirname(__FILE__)."/../foot.php"); ?>