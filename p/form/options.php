<?php
  require_once('name_table_bd.php');
  $query_c = "SELECT * FROM $tb_a  ORDER BY nombre ASC";
  $c = ejecutar($query_c);
  $totalRows_c = numRows($c);
?>
  <div class="estilotabla">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="10"><?=isset($dataGlobal['label_item']) ?  $dataGlobal['label_item'] : 'Item'?></th>
        <th align="left"><?=isset($dataGlobal['form_name']) ?  $dataGlobal['form_name'] : 'Form Name'?></th>
        <th ><?=isset($dataGlobal['email_receiver']) ?  $dataGlobal['email_receiver'] : 'Email Receiver'?></th>
        <th ><?=isset($dataGlobal['email_fwd']) ?  $dataGlobal['email_fwd'] : 'Email Fwd'?></th>
        <th width="150"><?=isset($dataGlobal['autoresponder']) ?  $dataGlobal['autoresponder'] : 'Autoresponder'?></th>
        <th width="10" ><?=isset($dataGlobal['label_edit']) ?  $dataGlobal['label_edit'] : 'Edit'?></th>
      </tr>
      <?php if ($totalRows_c == 0) 
      {?>
        <tr>
          <th class="text-uppercase" align="center" style="color:#FF0000"><?=isset($dataGlobal['label_no_record']) ?  $dataGlobal['label_no_record'] : 'No Record'?></th>
        </tr> <?php 
      }
      if ($totalRows_c > 0)
      {
        $i = 1;
        while ($row_c = fetchAssoc($c))
        { ?>
          <tr>
            <td align="center">
              <?php echo $i; ?>
            </td>
            <td align="left">
                <?php echo $row_c['nombre']; ?>
              </td>
              <td align="left">
                <?php echo $row_c['email']; ?>
              </td>
              <td>
                <div align="center">
                  <button type="button" class="btn btn-warning bmd-modalButton" data-toggle="modal" data-bmdsrc="email.php?i=<?=$row_c["idx"];?>" data-bmdwidth="740px" data-bmdheight="400px" data-target="#iframeModal" data-scrolling="si" data-bmdvideofullscreen="true">
                    <i class="fa fa-envelope"></i> 
                  </button>
                </div>
              </td>
              <td align="center"> 
                <span>
                  <?php
                  if($row_c["autoresponder"]=="1")
                    echo '<span class="Estilo_tipoestado1">Yes</span>';
                  else
                    echo "No";
                  ?>
                </span>
              </td>
              <td align="center"> 
                <button type="button" class="btn btn-info bmd-modalButton" data-toggle="modal" data-bmdsrc="e.php?i=<?=$row_c["idx"];?>" data-bmdwidth="770px" data-bmdheight="480px" data-target="#iframeModal" data-scrolling="si" data-bmdvideofullscreen="true">
                  <i class="fa fa-edit"></i> <?=isset($dataGlobal['label_edit']) ?  $dataGlobal['label_edit'] : 'Edit'?>
                </button>
              </td>
            </tr>
            <?php
            $i++;
          }; 
        } 
      ?>
    </table>
  </div>

        
