<?php
require_once('name_table_bd.php');
if(empty($i))
{
  $sql = "SELECT * FROM formularios where activo='1' order by idx limit 0,1";
  $res=ejecutar($sql);
  if ($col=fetchArray($res)) { $i=$col['idx']; }
 }
?>    
<table width="100%" border="0" cellspacing="4" cellpadding="4">
  <tr>
    <td>
      <?php
        $sql_1="select * from ".$tb_a." where idx='$i' "; //Devuelve todos los Idiomas
        $consulta_1=ejecutar($sql_1);
        $atabla=array();
        while($fila_1=fetchAssoc($consulta_1)) {  $atabla=$fila_1; }
        $sql="select * from ".$tb_c." where formularios_id='$i' order by fecreg desc";
        $consulta=ejecutar($sql);
        $num_reg=numRows($consulta);
      ?>
      <div style="font-size:20px; font-weight:bold; color:#2679b5"><?=isset($dataGlobal['record_list']) ?  $dataGlobal['record_list'] : 'Record List'?> <?=$atabla["nombre"]?>:</div>
    </td>
    <td>
      <div align="right">
        <?php if($num_reg==0){?>
          <a  class="btn disabled btn-small btn-success "> <i class="fa fa-file-excel-o"></i>  <?=isset($dataGlobal['export_excel']) ?  $dataGlobal['export_excel'] : 'Export Excel'?></a> 
          <?php }else{ ?>
            <a href="form-autoresponder-export.php?i=<?=$i?>" class="btn btn-small btn-success "> <i class="fa fa-file-excel-o"></i> <?=isset($dataGlobal['export_excel']) ?  $dataGlobal['export_excel'] : 'Export Excel'?>
            </a>
        <?php } ?>
      </div>
    </td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th width="50" ><div align="center">#</div></th>
      <th ><?=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name'?></th>
      <th ><?=isset($dataGlobal['label_email']) ?  $dataGlobal['label_email'] : 'Email'?></th>
      <th align="left"><?=isset($dataGlobal['label_date']) ?  $dataGlobal['label_date'] : 'Date'?></th>
      <th align="left"><?=isset($dataGlobal['label_time']) ?  $dataGlobal['label_time'] : 'Time'?></th>
      <th align="left"><?=isset($dataGlobal['label_ip']) ?  $dataGlobal['label_ip'] : 'Ip'?></th>
      <th width="80" align="center"><?=isset($dataGlobal['label_view']) ?  $dataGlobal['label_view'] : 'View'?></th>
    </tr>
    </thead>
    <tbody>
    <?php
      $pages = new Paginator;
      $pages->items_total = $num_reg;
      $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
      $pages->paginate(20);
      $item = 0;
      $consulta=ejecutar($sql.$pages->limit);
      $num_reg=numRows($consulta);
      if($num_reg == 0)
      { ?>
        <td colspan="7" ><div align="center" style="color:#F00">No Record</div></td>
        <?php
      }
      else
      {
        while($fila=fetchArray($consulta))
        {  
          $item++; ?> 
          <tr>
            <td valign="top" align="center"><?=$item + $pages->low?></td>
            <td valign="top" align="left"><?= $fila['nombre']?></td>
            <td valign="top" align="left"><?= $fila['email']?></td>
            <td valign="top" align="left"><?= strftime('%b %d, %Y', strtotime($fila['fecreg'])) ?></td>
            <td valign="top" align="left"><?= strftime('%I:%M:%S %p', strtotime($fila['fecreg'])) ?></td>
            <td valign="top" align="left"> <?=$fila["ips"]?></td>
            <td valign="top" align="center">
              <button type="button" class="btn btn-primary bmd-modalButton" data-toggle="modal" data-bmdsrc="form-autoresponder-list-n.php?id=<?=$fila["idx"]?>" data-bmdwidth="640px" data-bmdheight="380px" data-target="#iframeModal" data-scrolling="si" data-bmdvideofullscreen="true">
                <i class="icon-eye-open"></i> 
                <?=isset($dataGlobal['label_view']) ?  $dataGlobal['label_view'] : 'View'?>
              </button>
            </td>
          </tr>
          <?php
        }
      }
    ?>
    </tbody>
    <tfoot>
      <tr>
      <th width="50" ><div align="center">#</div></th>
      <th ><?=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name'?></th>
      <th ><?=isset($dataGlobal['label_email']) ?  $dataGlobal['label_email'] : 'Email'?></th>
      <th align="left"><?=isset($dataGlobal['label_date']) ?  $dataGlobal['label_date'] : 'Date'?></th>
      <th align="left"><?=isset($dataGlobal['label_time']) ?  $dataGlobal['label_time'] : 'Time'?></th>
      <th align="left"><?=isset($dataGlobal['label_ip']) ?  $dataGlobal['label_ip'] : 'Ip'?></th>
      <th width="80" align="center"><?=isset($dataGlobal['label_view']) ?  $dataGlobal['label_view'] : 'View'?></th>
      </tr>
    </tfoot>
  </table>
  <?php 
  if ($pages->num_pages > 1)  
  { ?> 
    <div align="center" style="padding:8px;">
      <?php echo $pages->display_pages(); ?> 
      <?php //echo $pages->display_jump_menu()?> 
      <?php //echo $pages->display_items_per_page()?> 
    </div> 
    <?php  
  } ?>