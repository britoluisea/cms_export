<?php 
  require_once(dirname(__FILE__).'/../header_box.php'); 
  include("e_f.php"); 
  if($Result1 == true){
    $success =isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
    echo'<script>$(function() {
    window.parent.alerValid("Success", "'.$success.'", "6000");
    window.parent.closeModal();
		});</script>';
	}
?>
<script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<table width="90%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td valign="top" style="padding:10px;">
      <form method="post" name="form1" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data">
        <table width="100%" class="height100" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td>
              <div class="h4">
                  <?=isset($dataGlobal['form_setting']) ? '[ '.$dataGlobal['form_setting'].' ]' : '[ Form Setting ]'?>
                <?php if(empty($_GET['i'])){ ?>
                  <input type="hidden" name="MM_insert" value="form1">
                <?php }else{?>
                  <input type="hidden" name="MM_update" value="form1">
                  <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
                <?php }?>
              </div>
            </td>
            <td>
              <table border="0" align="right" cellpadding="5" cellspacing="0">
                <tr>
                  <td>
                    <input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'save'?>" />
                  </td>
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table class="height100" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
              <section>
                <div id="contenido">
                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:0px solid #D8D8D8;">
                    <tr>
                      <td> 
                        <table width="100%" border="0" align="center">
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          <tr>                          
                          <tr>
                            <td><?=isset($dataGlobal['autoresponder']) ? $dataGlobal['autoresponder'] : 'Autoresponder'?></td>
                            <td>&nbsp;</td>
                            <td>
                              <select name="autoresponder" style="width:150px">
                                <option value="1" <?php if($row_u["autoresponder"]=="1"){echo "selected='selected'";}?>>
                                  <?=isset($dataGlobal['label_yes']) ? $dataGlobal['label_yes'] : 'Yes'?>
                                </option>
                                <option value="0" <?php if($row_u["autoresponder"]=="0"){echo "selected='selected'";}?>>
                                  <?=isset($dataGlobal['label_no']) ? $dataGlobal['label_no'] : 'Not'?>
                                </option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><strong><?=isset($dataGlobal['email_receiver']) ? $dataGlobal['email_receiver'] : '"Email Receiver'?></strong></td>
                            <td>&nbsp;</td>
                            <td>
                              <span id="sprytextfield1">
                                <input name="email" type="text" id="email" value="<?php echo $row_u["email"]; ?>" size="51" />
                                <br />
                                <span class="textfieldInvalidFormatMsg"><?=isset($dataGlobal['invalid_format']) ? $dataGlobal['invalid_format'] : 'Invalid Format'?>.</span>
                              </span>
                            </td>
                          </tr>
                          <tr>
                            <td width="110"><?=isset($dataGlobal['label_name']) ? $dataGlobal['label_name'] : 'Name'?></td>
                            <td width="10">&nbsp;</td>
                            <td>
                              <input name="nombre" type="text" value="<?php echo $row_u["nombre"]; ?>" size="51" />
                            </td>
                          </tr>
                          <tr>
                            <td width="110"><?=isset($dataGlobal['label_subject']) ? $dataGlobal['label_subject'] : 'Subject'?></td>
                            <td width="10">&nbsp;</td>
                            <td>
                              <input name="autoasunto" type="text" value="<?php echo $row_u["autoasunto"]; ?>" size="51" />
                            </td>
                          </tr>
                          <tr>
                            <td><?=isset($dataGlobal['label_message']) ? $dataGlobal['label_message'] : 'Message'?></td>
                            <td>&nbsp;</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td colspan="3">
                              <textarea name="autodescrip" id="FCKeditor1" ><?=$row_u["autodescrip"]?></textarea>
                              <script type="text/javascript">
                                var newCKEdit = CKEDITOR.replace('FCKeditor1',{width:'90%', height:'500',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
                                CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
                              </script>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>             
                </div>
              </section>
            </td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<script type="text/javascript">
  var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
  var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none");
</script>  
<?php include(dirname(__FILE__)."/../foot.php"); ?>