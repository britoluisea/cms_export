<?php include(dirname(__FILE__)."/../header.php");?>
<div class="container">
	<div id="tabs"><br><br>
		<!-- Nav tabs -->
		<?php include("menu.php");?>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="Bannerl" class="tab-pane active" id="tabs-1" style="padding-top: 20px;">		
				<?php include("form.php"); ?>
			</div>
			<div role="Setting" class="tab-pane" id="tabs-2" style="padding-top: 20px;">
				<?php include("options.php"); ?>			
			</div>
			<div role="Recatpcha" class="tab-pane" id="tabs-3" style="padding-top: 20px;">
				<?php include("formRecaptcha.php"); ?>
			</div>
		</div>
	</div>
</div>
<?php include(dirname(__FILE__)."/../footer.php"); ?>