<?php 
session_start();
include dirname(__FILE__)."/../../../varRutas.php";
include dirname(__FILE__)."/../../../cn/cnx.php";
include dirname(__FILE__)."/../../system/languages/languages.php";
include dirname(__FILE__)."/../../../app/FormValidator.php";
include dirname(__FILE__)."/../../../app/ValidationRule.php";
$validator = new App_FormValidator();
$urlDomain = SERVER;

$is_required=isset($dataGlobal['is_required']) ?  $dataGlobal['is_required'] : 'Is required.';
$valid_emial=isset($dataGlobal['valid_emial']) ?  $dataGlobal['valid_emial'] : 'Please enter a valid email address.';
$msj_captcha=isset($dataGlobal['msj_captcha']) ?  $dataGlobal['msj_captcha'] : 'Verification incorrect, please try again';
$msj_request=isset($dataGlobal['msj_request']) ?  $dataGlobal['msj_request'] : 'Request a Free Estimate from';
$free_estimate=isset($dataGlobal['free_estimate']) ?  $dataGlobal['free_estimate'] : 'Free Estimate from';
$ip_address=isset($dataGlobal['ip_address']) ?  $dataGlobal['ip_address'] : 'IP Address';
$label_name=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name';
$label_telephone=isset($dataGlobal['telephone']) ?  $dataGlobal['telephone'] : 'Telephone';
$label_address=isset($dataGlobal['address']) ?  $dataGlobal['address'] : 'Address';
$label_email=isset($dataGlobal['label_email']) ?  $dataGlobal['label_email'] : 'Email';
$label_comments=isset($dataGlobal['comments']) ?  $dataGlobal['comments'] : 'Comments';
$id = 1;
$sql = "SELECT * FROM formularios where idx = '".$id."' ";
$res = ejecutar($sql);
$k = fetchArray($res);
$validRecaptcha = $k['recaptcha'];
$validator->addRule("fullname", $is_required, "required");
$validator->addRule("email", $is_required, "required");
$validator->addRule("email", $valid_emial, "email");
$validator->addRule("telephone", $is_required, "required");
$validator->addRule("address", $is_required, "required");
$validator->addRule("comments", $is_required, "required");
$validator->addRule("g-recaptcha-response", $is_required, "required");
if(!$validRecaptcha){$validator->addRule("g-recaptcha-response", $msj_captcha, "g-recaptcha-response");}
$validator->addEntries($_POST);
$validator->validate();
$entries = $validator->getEntries();
foreach ($entries as $key => $value) {
	${$key} = $value;
}
if ($validator->foundErrors()) {
	$errors = $validator->getErrors();
	$_SESSION["errors"] = $errors;
	$_SESSION["entries"] = $entries;
	header("location: ".$urlDomain.$_POST["uri"]);
}
if (empty($errors)) 
{
	if($validRecaptcha==1 && $k['key_secret']!='')
	{
		// Google reCaptcha secret key
		$secretKey  = $k['key_secret'];
	    // Get verify response data
	    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['g-recaptcha-response']);
	    $responseData = json_decode($verifyResponse);
	    if(!$responseData->success){
	    	$_SESSION["errors"]['g-recaptcha-response'] =$msj_captcha;
			$_SESSION["entries"] = $entries;
	    	//var_dump($responseData); exit;
	    	header("location: ".SERVER.$_POST["uri"]);
	    }
	}
	$fullname = trim($_POST["fullname"]); 
	$telephone = trim($_POST["telephone"]);
	$email = trim($_POST["email"]);
	$direccion = trim($_POST["address"]);
	$comments = trim($_POST["comments"]);
	$texto = "";	
	$texto = $texto . $msj_request." " . $urlDomain . " " . date("M d, Y") . "<br>";
	$texto = $texto . "<br>";	
	$texto = $texto ."<strong>".$ip_address.":</strong> " . getenv("REMOTE_ADDR") . "<br>";
	$texto = $texto ."<strong>".$label_name.":</strong> " . $fullname . "<br>";
	$texto = $texto ."<strong>".$label_telephone.":</strong> " . $telephone . "<br>";
	$texto = $texto ."<strong>".$label_address.":</strong> " . $direccion . "<br>";
	$texto = $texto ."<strong>".$label_email.":</strong> " . $email . "<br>";
	$texto = $texto ."<strong>".$label_comments.":</strong> " . $comments . "<br>";
	$wfecreg = date("Y-m-d H:i:s");
	$wips = getenv("REMOTE_ADDR");
	$sql_record = "insert into formularios_record_list (formularios_id, nombre, email, phone, fecreg, descrip, ips) 
			value 
			('".$id."', '".addslashes($fullname)."', '".$email."', '".$telephone."', '".$wfecreg."', '".addslashes($texto)."', '".$wips."')";
	if (!$res_record = ejecutar($sql_record)) {
		echo $sql_record." <br>Error al insertar y/o modificar tabla ";		
	}
	$waut_autoresponder = 0;
	if ($host <> "localhost") {
		$res = ejecutar($sql);
		if(!$res){echo'error de ejecutar = '.$sql;}
		if ($col = fetchArray($res)) {
			$waut_email = $col["email"];
			$email_origen =$waut_email;
			$waut_nombre = $col["autonombre"];
			$waut_asunto = $col["autoasunto"];
			$waut_descri = str_replace("/userfiles/", $urlDomain . "/userfiles/", $col["autodescrip"]);
			$waut_autoresponder = $col["autoresponder"];
			$remitente = $fullname . "<" . $email . ">";
			$headers = "From: " . $remitente . "\r\n";
			$headers .= "Reply-To: ". $email_origen . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$message = '<html><body>';
			$message .= $texto;
			$message .= '</body></html>';			
			$enviando_para =$col["email"];
			for ($ifor = 1; $ifor <= 10; $ifor++)
			{
				$wemail_sec = $col["email" . $ifor];
				if (!empty($wemail_sec)){$enviando_para.=','.$wemail_sec;}
			}
			if(!mail($enviando_para, $free_estimate." " . $urlDomain . " : " . $email, $message, $headers))
			{
				//echo'<pre>';print_r($_SESSION); echo'</pre>';
			    echo'fallo el envio de mail ';
			    echo'para = '.$enviando_para.'<br>';
			    echo'correo de envio = '.$email.'<br>';
			    echo'texto =<br>'.$texto;
			}
		}
		if ($waut_autoresponder == "1") {
			$texto = $waut_descri;
			$wemail = $email;
			$nombre_origen = $waut_nombre;
			$email_origen = $waut_email;
			$headers = "From: $nombre_origen <$email_origen> \r\n";
			$headers .= "Return-Path: <$email_origen> \r\n";
			$headers .= "Reply-To: $email_origen \r\n";
			$headers .= "X-Sender: $email_origen \r\n";
			$headers .= "MIME-Version: 1.0 \r\n";
			$headers .= "Content-Transfer-Encoding: 7bit \r\n";
			$formato = "html";
			if ($formato == "html") {
				$headers .= "Content-Type: text/html; charset=iso-8859-1 \r\n";
			} else {
				$headers .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
			}
			if (@mail($wemail, $waut_asunto, $texto, $headers)) {
				//			echo "Su email a sido correctamente Enviado!";  
			}
		}
	}
	$_SESSION['msj_form']=1;	
   	header("location: ".SERVER.$_POST["uri"]);
}

?>

