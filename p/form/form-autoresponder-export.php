<?php
 	date_default_timezone_set('America/New_York');
	include "../../varRutas.php";
	include "../../cn/cnx.php";
	require_once('name_table_bd.php');
	require_once('../system/function.php'); 
	require_once('../system/functions.php');  
	include('../'.LIB_P."PHPExcel/Classes/PHPExcel.php");
	include('../'.LIB_P."PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
	$i=isset($_GET['i']) ? $_GET['i'] : '';
	$objPHPExcel = new PHPExcel();
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
    $objPHPExcel = $objReader->load('../'.LIB_P."PHPExcel/plantilla_form_autoresponder.xlsx");
    $objPHPExcel->setActiveSheetIndex(0);
	$sql_1="select * from $tb_a where idx='$i' "; //Devuelve todos los Idiomas
	$consulta_1=ejecutar($sql_1);
	$atabla=array();
	while($fila_1=fetchAssoc($consulta_1))
	{
		$atabla=$fila_1;
	}
	$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Record List '.$atabla['nombre']);
	$sql="select * from $tb_c where formularios_id='$i' order by fecreg desc";
	$consulta=ejecutar($sql);
	$num_reg=numRows($consulta);
	$item=4;
	while($fila=fetchArray($consulta))
	{   
		$item++;
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$item, $fila['nombre']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$item, $fila['email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$item, $fila['phone']);
	}
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="record-list-'.urls_amigables($atabla['nombre']).'.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;  
?>