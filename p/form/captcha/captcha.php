<?php
session_start();
$string = '';
$digito1=rand(1,4);
$digito2=rand(1,3);
$suma=$digito1+$digito2; 
$string .= $digito1;
$string .= '+';
$string .= $digito2;
$string .= ' = ';
$_SESSION['captcha'] = $suma; //store the captcha
$width  = 150;
$height = 60;
$dir =dirname(__FILE__). '/fonts/';
$image = imagecreatetruecolor($width , $height); //custom image size
$font = "arial.ttf"; // custom font style
$color = imagecolorallocate($image, 0, 0, 0); // custom color
$white = imagecolorallocate($image, 237, 237, 237); // custom background color
imagefilledrectangle($image,0,0, $width, $height,$white);
$angulo = 0;
imagettftext ($image, 30, $angulo, 10, 40, $color, $dir.$font, $string);
function wave_region($img, $x, $y, $width, $height,$amplitude = 4.5,$period = 30) 
{
	$mult = 2;
	$img2 = imagecreatetruecolor($width * $mult, $height * $mult);
	imagecopyresampled ($img2,$img,0,0,$x,$y,$width * $mult,$height * $mult,$width, $height);
	
	$k = rand(-60,60);
	for ($i = 0;$i < ($width * $mult);$i += 2) {
    	imagecopy($img2,$img2,
              $x + $i - 2,$y + sin($k+$i / $period) * $amplitude,
              $x + $i,$y,
              2,($height * $mult));
	}
	imagecopyresampled ($img,$img2,$x,$y,0,0,$width, $height,$width * $mult,$height * $mult);
	imagedestroy($img2);
}
wave_region($image, 0, 0, $width, $height, mt_rand(7, 22), mt_rand(25,40));
header("Content-type: image/png");
imagepng($image);
imagedestroy($image);
?>