<?php
session_start();
date_default_timezone_set('America/New_York');
/* ------------------Cambiando Zona Horaria---------------------------- */
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
}
else
{
	$dir = "../tmp/"; 
	/*chdir ($dir);  ///comentado para evitar vaciar todo los archivos
	$dh=opendir('.');
	while ($file = readdir ($dh)) {
	   if ($file != "." && $file != "..") {
	        @unlink($file);
	    }
	}
	  closedir($dh);*/
	 //vaciar solo los archivo de la sesion actual
	if(isset($_SESSION['img_banner']))
	{
		$original = $_SESSION['img_banner'];
    	$optima= substr($original, 0,-4);
    	$optima=$optima.'_optima.jpg';
		@unlink($dir.$original);//original
		@unlink($dir.$optima); // optima resize
		@unlink($dir."preview" .$original); // preview crop
		//echo'eliminando '.$dir."preview" .$_SESSION['img_banner2'];
		unset($_SESSION['img_banner']);
	}
}