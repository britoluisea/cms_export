<?php
$config = file_get_contents(dirname(__FILE__)."/config.json");
$data = json_decode($config, true);
if(!is_file(dirname(__FILE__)."/".$data['file']))
{echo'error de idiomas ';exit;}
else
{
	$file_idioma = file_get_contents(dirname(__FILE__)."/".$data['file']);
	$dataGlobal = json_decode($file_idioma, true);
	$dataGlobal['lang']=substr($data['file'], 0,-5);
	$file_custom = dirname(__FILE__)."/".$dataGlobal['lang']."_custom.json";
	if(file_exists($file_custom))
	{
		if($en_custom = file_get_contents($file_custom))
		{
			$object = json_decode($en_custom);
			$ob_array = get_object_vars($object);
			foreach ($ob_array as $key => $value) {
				if($value!=''){
					foreach ($dataGlobal as $k => $v){
						if($k==$key){$dataGlobal[$k]=$value;}
					}
				}
			}
		}
	}
	//echo'<pre>';print_r($dataGlobal);echo'</pre>'; exit;
} 
?>