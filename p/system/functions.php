<?php
require_once 'phpthumb-latest/ThumbLib.inc.php';
@ini_set("memory_limit", "3000M");
@ini_set( 'upload_max_size' , '3000M' );
@ini_set( 'post_max_size', '3000M');
@ini_set( 'max_execution_time', '30000' );
@set_time_limit(0);
function crear_imagen_Slider_banner($wruta1, $wruta2, $nombre_foto, $quality,  $wancho, $walto, $wanchosmall, $waltosmall, $relativo) {
    set_time_limit(0);
    $error=0;
    $proporcional=1;
    $foto2= substr($nombre_foto, 0,-4);
    $foto2=$foto2.'_optima.jpg';
    //slider - banner
    //if(!cortes_custom($wruta1, $wruta1, $nombre_foto, $foto2, $quality, $wancho, $walto, $proporcional))
    //{$error=1;}
    $thumb = PhpThumbFactory::create($wruta1.$nombre_foto);
    $thumb->adaptiveResize($wancho,$walto);
    $thumb->setOptions($options = array('jpegQuality'=>$quality));
    $thumb->save($wruta1.$foto2);
    if($relativo==0)
    {
        $thumb = PhpThumbFactory::create($wruta1.$foto2);
        //$thumb->resize($wancho, $walto);
        //$thumb->adaptiveResize($wancho, $walto);
        $thumb->cropFromCenter($wancho, $walto);
        //$thumb->setOptions($options = array('jpegQuality'=>$quality));
        $thumb->save($wruta2. "/slider/".$nombre_foto);
        if(!is_file($wruta2. "/slider/".$nombre_foto)){$error=2;}
    }
    //small	
    if(!cortes_custom($wruta2. "/slider/", $wruta2. "/small/", $nombre_foto, $nombre_foto, $quality, $wanchosmall, $waltosmall, $proporcional))
    {$error=3;}
    return $error;
}

function cortes_custom($ruta_img, $destino, $foto, $foto2, $quality, $width, $height, $relativo)
{
    $im     = imagecreatefromjpeg($ruta_img.$foto);// obtenemos la img
    $old_x=imageSX($im); // obteniendo los valores real de la img
    $old_y=imageSY($im); // obteniendo los valores real de la img

    $new_w=(int)($width); // asignamos el valor width nuevo
    if($relativo==0) // es igual a 0 es xq el alto se corta segun el valor de heigth
    {
        $img_w=(int)($width);
        $img_h=(int)($height);
    }
    else // sino es xq el alto sera relativo y se calcula segun proposciones
    {
        if (($new_w<=0) or ($new_w>$old_x)) 
        { 
            // validamos q el width tenga un valor positivo y q 
            //no sea  mayor q el width original de la img
            $new_w=$old_x;
        }
        $new_h=($old_x*($new_w/$old_x));

        if ($old_x > $old_y) { // si la imagen es panoramica
            $img_w=$new_w;
            $img_h=$old_y*($new_w/$old_x);
        }
        if ($old_x < $old_y) { // si la img es vertical 
            $img_w=$old_x*($new_w/$old_y);
            $img_h=$new_h;
        }
        if ($old_x == $old_y) {// si la img es cuadrada no calculamos
            $img_w=$new_w;
            $img_h=$new_h;
        }
    }
    $create=ImageCreateTrueColor($img_w,$img_h); //creamos la img 
    imagecopyresized($create,$im,0,0,0,0,$img_w,$img_h,$old_x,$old_y);// redimensionamos la img 
    $exportar=imagejpeg($create, $destino.$foto2, $quality);// exportamos la img y le damos el destino nombre del jpg y la Calidad a exportar
    imagedestroy($create);// liberamos la memoria
    return $exportar;
}

function urls_amigables($url) {
    //$url = strtolower($url);
    $url = mb_convert_case($url, MB_CASE_LOWER, "UTF-8");
    $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
    $repl = array('a', 'e', 'i', 'o', 'u', 'n');
    $url = str_replace($find, $repl, $url);
    $find = array(' ', '&', '\r\n', '\n', '+');
    $url = str_replace($find, '-', $url);
    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    $url = preg_replace($find, $repl, $url);
    return $url;
}

function eliminar_directorio($dir){
    $result = false;
    if ($handle = opendir("$dir")){
        $result = true;
        while ((($file=readdir($handle))!==false) && ($result)){
            if ($file!='.' && $file!='..'){
                if (is_dir("$dir/$file")){
                    $result = eliminar_directorio("$dir/$file");
                }
                else
                {
                    $result = unlink("$dir/$file");
                }
            }
        }
        closedir($handle);
        if ($result){ $result = rmdir($dir);}
    }
    return $result;
}
function idiomaCustom($file_origin, $file_custom)
{
    $arr = array();
    $file = file_get_contents($file_origin);
    $object = json_decode($file);
    $ob_array = get_object_vars($object);
    $json=$ob_array;
    sort($json, 2);
    foreach ($json as $key => $value) {
        if($value!=''){
            foreach ($ob_array as $k => $v){
                if($value==$v){$arr[$k]=$v;}
            }
        }
    }
    if(file_exists($file_custom))
    {
        $custom = file_get_contents($file_custom);
        $object = json_decode($custom);
        $ob_array = get_object_vars($object);
        foreach ($ob_array as $key => $value) {
            if($value!=''){
                foreach ($arr as $k => $v){
                    if($k==$key){$arr[$k]=$value;}
                }
            }
        }
    }
    return $arr;
}

?>