<?php
  function addBtnIcons($idEditor){
    include "languages/languages.php";
?>

        <style>
          .select2-container--default .select2-results__option--highlighted[aria-selected] {
              background-color: rgba(88, 151, 251, 0.47);
          }
          .select2-container--default .select2-results__option--highlighted[aria-selected],
          .select2-container--default .select2-results__option[aria-selected=true], 
          .select2-container--default .select2-selection--single .select2-selection__rendered{
            color: #000!important;
            font-weight:600;
          }
          .select2-container--default .select2-selection--single{border-radius: 0px;}
          span.select2-selection.select2-selection--single {
            height: 33px;
            border:1px solid #000;
          }
          ul#select2-botones-results li.select2-results__option,
          ul#select2-iconos-results li.select2-results__option {
            display: inline-block;
          }
          .addPlus{background-color:#2b579a;border-radius: 0px; border:1px solid #000;}
          .addPlus:hover{cursor: pointer;}
          #align-icon{border:1px solid #000; height: 33px;border-radius: 0px; padding:3px 5px;}
          .mb-0{margin-bottom: 0px;}
        </style>
        <div class="col-sm-3" style="padding: 3px 10px 0px 10px ;">
          <div style="font-size:18px; color:#2679b5">
            <strong><?=isset($dataGlobal['add_buttons']) ?  $dataGlobal['add_buttons'] : 'Add buttons'?></strong>
          </div>
          <div class="form-group mb-0">
            <label class="sr-only" for="search"><?=isset($dataGlobal['add_btn']) ?  $dataGlobal['add_btn'] : 'Add btn'?></label>
            <div class="input-group">
              <div class="input-group-addon addPlus" onclick="addBtn('<?=$idEditor?>')" >
                <i class="fa fa-plus" style="color:#fff;"></i>
              </div>
              <select id="botones" class="form-control" style="width: 100%;"></select>
            </div>
          </div>
        </div>
          
        <div class="col-sm-6" style="padding: 3px 10px 0px 10px;">    
          <div style="font-size:18px; color:#2679b5">
              <strong><?=isset($dataGlobal['add_icons']) ?  $dataGlobal['add_icons'] : 'Add Icons'?></strong>
          </div>      
          <div class="form-group mb-0">
            <label class="sr-only" for="search"><?=isset($dataGlobal['add_btn']) ?  $dataGlobal['add_btn'] : 'Add btn'?></label>
            <div class="input-group">
              <div class="input-group-addon addPlus" onclick="addIcons('<?=$idEditor?>')" >
                <i class="fa fa-plus" style="color:#fff;"></i>
              </div>
              <select id="iconos" class="form-control" style="width: 100%;"></select>
            </div>
          </div>
        </div>
        <div class="col-sm-3" style="padding: 3px 10px 0px 10px;">          
          <div style="font-size:18px; color:#2679b5">
              <strong><?=isset($dataGlobal['add_btn_icon']) ?  $dataGlobal['add_btn_icon'] : 'Add btn with icon'?></strong>
          </div> 
            <div class="form-group mb-0">
              <label class="sr-only" for="search"><?=isset($dataGlobal['add_btn']) ?  $dataGlobal['add_btn'] : 'Add btn'?></label>
              <div class="input-group">
                <div class="input-group-addon addPlus" onclick="addBtnIcons('<?=$idEditor?>')" >
                  <i class="fa fa-plus" style="color:#fff;"></i>
                </div>
                <select id="align-icon" class="form-control" style="width: 100%;">
                  <option value="left"><?=isset($dataGlobal['icon_left']) ?  $dataGlobal['icon_left'] : 'Icon on the left'?></option>
                  <option value="right"><?=isset($dataGlobal['icon_right']) ?  $dataGlobal['icon_right'] : 'Icon on the right'?></option>
                </select>
              </div>
            </div>
        </div>
        <script>
            $(document).ready(function()
            {
              var btns=[
                {
                  id: 0, 
                  text:'<a href="<?=SERVER?>" class="btn btn-default" style="padding: 3px 6px;">Default</a>', 
                  title: 'default'
                },{
                  id: 1, 
                  text:'<a href="<?=SERVER?>" class="btn btn-primary" style="padding: 3px 6px;">Primary</a>', 
                  title: 'primary'
                },{
                  id: 2, 
                  text:'<a href="<?=SERVER?>" class="btn btn-info" style="padding: 3px 6px;">Info</a>', 
                  title: 'info'
                },{
                  id: 3, 
                  text:'<a href="<?=SERVER?>" class="btn btn-warning" style="padding: 3px 6px;">Warning</a>', 
                  title: 'warning'
                },{
                  id: 4, 
                  text:'<a href="<?=SERVER?>" class="btn btn-success" style="padding: 3px 6px;">Success</a>', 
                  title: 'success'
                },{
                  id: 5, 
                  text:'<a href="<?=SERVER?>" class="btn btn-danger" style="padding: 3px 6px;">Danger</a>', 
                  title: 'danger'
                }
              ];
              $("#botones").select2({
                  data: btns,
                  escapeMarkup: function(markup) {
                    return markup;
                  }
                });
              $.get('<?=SERVER_P?>system/iconos.php', function(data)
              {
                var icons = JSON.parse(data);       
                $("#iconos").select2({
                  data: icons,
                  escapeMarkup: function(markup) {
                    return markup;
                  }
                });
              });
            });
        </script>
<?php } ?>