<?php
session_start();
date_default_timezone_set('America/New_York');
/* ------------------Cambiando Zona Horaria---------------------------- */
if (empty($_SESSION['value_admin_idx']) or $_SESSION['var_sec_admin'] <> true or empty($_SESSION['var_sec_admin'])) {
    header("Location: ../index.php");
}
else
{
	$dir =  "../../imgcms/headers/video/"; 
	chdir ($dir);  ///vaciar carpeta de video
	$dh=opendir('.');
	while ($file = readdir ($dh)) {
	   if ($file != "." && $file != "..") {
	        @unlink($file);
	    }
	}
	closedir($dh);
	unset($_SESSION['video']);
}