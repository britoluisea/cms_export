<?php 
include("list_f.php"); 
  if($Result1 == true){
    $success =isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
    $label_success=isset($dataGlobal['label_success']) ? $dataGlobal['label_success'] : 'Success';
    echo'<script>$(function() {
    alerValid("'.$label_success.'", "'.$success.'", "6000");
    window.location="index.php";});</script>';
  }
?>
<script src="<?=PLUGINS_P?>ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?=PLUGINS_P?>ckfinder/ckfinder.js" type="text/javascript"></script>
<table class="height100" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%" valign="top" class="">
<?php include '../menu.php'; ?>
</td>
    <td width="100%" valign="top"><section>
    <div id="contenido">
<form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="form1" >
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td style="padding: 20px;">
<div class="navipage">
  <?=isset($dataGlobal['website_status']) ?  "[ ".$dataGlobal['website_status']." ]" : '[ Website Status ]'?>
<?php if(empty($_GET['i'])){ ?>
<input type="hidden" name="MM_insert" value="form1">
<?php }else{ ?>
  <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
<?php }?>
    </div>
  </td>
    <td>
    <table border="0" align="right" cellpadding="5" cellspacing="0">
      <tr>
        <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ?  $dataGlobal['btn_save'] : 'Save'?>" /></td>
        <td><a class="btn btn-small btn-default" href="index.php"><?=isset($dataGlobal['btn_cancel']) ?  $dataGlobal['btn_cancel'] : 'Cancel'?></a></td>
        </tr>
    </table>
       </td>
  </tr>
</table>
 <table width="100%" border="0" align="center">
 <tr>
   <td height="60">
    <table border="0" cellpadding="3" cellspacing="0" >
     <tr>
        <td><?=isset($dataGlobal['label_status']) ?  $dataGlobal['label_status'] : 'Status'?>: </td>
        <td valign="top">
          <select name="activowebsite" style="width:200px" class="form-control">
            <option value="1" <?php if($row_u["activowebsite"]=="1"){ echo "selected='selected'";}?> >
              <?=isset($dataGlobal['label_online']) ?  $dataGlobal['label_online'] : 'Online'?>
            </option>
            <option value="0" <?php if($row_u["activowebsite"]=="0"){ echo "selected='selected'";}?> >
              <?=isset($dataGlobal['label_offline']) ?  $dataGlobal['label_offline'] : 'Offline'?>
            </option>
            <option value="2" <?php if($row_u["activowebsite"]=="2"){ echo "selected='selected'";}?>>
              <?=isset($dataGlobal['label_under_maintenance']) ?  $dataGlobal['label_under_maintenance'] : 'Under Maintenance'?>
            </option>
          </select>
        </td>
      </tr>
    </table>
  </td>
 </tr>
 <tr>
   <td width="612" valign="top">
     <textarea name="descriwebsite" id="FCKeditor1" ><?=$row_u["descriwebsite"]?></textarea>
     <script type="text/javascript">
        var newCKEdit = CKEDITOR.replace('FCKeditor1',{height:'550',allowedContent:true,language: '<?=isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en'?>'});
        CKFinder.setupCKEditor(newCKEdit, '<?=PLUGINS_P?>ckfinder/');
        </script>
     </td>
 </tr>
 </table>
</form>
</div>
  </section>
</td>
  </tr>
</table>        
<?php include(dirname(__FILE__)."/../../footer.php");?>
