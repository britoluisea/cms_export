<?php include("list_c.php");?>
<!-- modal confirmar eliminar registro -->
<?php include("modal.php");?>
<!-- modal confirmar eliminar registro -->
<table class="height100" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%" valign="top" class="">
 <?php include_once ('../menu.php'); ?>
</td>
    <td width="100%" valign="top"><section>
    <div id="contenido">
      <table style="margin-bottom:12px" width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td width="50%" bgcolor="">
  <a href="f.php"  class='btn btn-small btn-success'>
    <strong><span>+</span> <?=isset($dataGlobal['add_page']) ?  $dataGlobal['add_page'] : 'Add Page'?> </strong></a>
   </td>
    <td width="50%" >
       </td>
  </tr>
</table>
    <div class="estilotabla">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="60" scope="col"><?=isset($dataGlobal['label_item']) ?  $dataGlobal['label_item'] : 'Item'?></th>
    <th width="60" scope="col"><?=isset($dataGlobal['label_id']) ?  $dataGlobal['label_id'] : 'Id'?></th>
    <th align="left" scope="col"><?=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name'?></th>
    <th width="21%" align="left" scope="col"><?=isset($dataGlobal['label_web_marketing']) ?  $dataGlobal['label_web_marketing'] : 'Web Marketing'?></th>
    <th width="14%" scope="col"><?=isset($dataGlobal['label_status']) ?  $dataGlobal['label_status'] : 'Status'?></th>
    <th width="60" scope="col"><?=isset($dataGlobal['label_edit']) ?  $dataGlobal['label_edit'] : 'Edit'?></th>
    <th width="60" scope="col"><?=isset($dataGlobal['label_del']) ?  $dataGlobal['label_del'] : 'Delete'?></th>
  </tr>
<?php if ($totalRows_c == 0) { // Show if recordset empty ?>
    <tr>
      <th colspan="100"><div class="text-uppercase" align="center" style="color:#FF0000"><?=isset($dataGlobal['label_no_record']) ?  $dataGlobal['label_no_record'] : 'NO RECORD'?></div></th>
    </tr>
<?php } // Show if recordset empty ?>
<?php if ($totalRows_c > 0) { // Show if recordset not empty ?>
  <?php  
  $i = 1;
  do { ?>
       <tr>
          <td>
            <div align="center">
              <?php echo $i; ?>
            </div>
      </td>
          <td>
            <div align="center">
              <?php echo $row_c['id']; ?>
            </div>
      </td>
          <td>
            <div align="left">
              <?php echo $row_c['titulo']; ?>
            </div>
      </td>
      <td>
            <div align="left">
    <div class="taleft">
     <?php      
    if($row_c["usamarketing"]=="5"){
      $usamarketing=isset($dataGlobal['universal_metas']) ?  $dataGlobal['universal_metas'] : 'Universal Metas';
      echo "<label class='btn btn-small btn-info'>".$usamarketing."</label>";
    }
    if($row_c["usamarketing"]=="1"){
      $usamarketing=isset($dataGlobal['custom_metas']) ?  $dataGlobal['custom_metas'] : 'Custom Metas';
      echo "<label class='btn btn-small btn-success'>".$usamarketing."</label>";
    }
    if($row_c["usamarketing"]=="0"){
      $usamarketing=isset($dataGlobal['web_content_boxes']) ?  $dataGlobal['web_content_boxes'] : 'Boxes';
      echo "<label class='btn btn-small btn-primary'>".$usamarketing."</label>";
    }
    ?>        
    </div>
        </div>   
      </td>
<td>
<div align="center" class="Estilo_tipoestado">
    <?php
    switch ($row_c['activo']) {
    case ($row_c['activo'] = 1):
        $active=isset($dataGlobal['label_active']) ?  $dataGlobal['label_active'] : 'Active';
        echo "<label class='btn btn-small btn-success'>".$active."</label>";
        break;
    case ($row_c['activo'] = 0):
        $inactive=isset($dataGlobal['label_inactive']) ?  $dataGlobal['label_inactive'] : 'Inactive';
        echo "<label class='btn btn-small btn-danger'>".$inactive."</label>";
        break;
  }
?>  
</div>
</td>
          <td><div align="center"> 
          <a href="f.php?i=<?php echo $row_c['id']; ?>" title="Edit" class='btn btn-small btn-primary'><i class="fa fa-edit"></i></a>
          </div></td>
          <td> 
            <?php if($row_c['auto']==0){?>
              <a data-target="#ventana1" data-href="delete.php?i=<?php echo $row_c['id']; ?>" class="btn btn-small btn-danger" href="#" data-toggle="modal" data-book-id="<?php echo $row_c['titulo']; ?>?" >
                <i class="fa fa-trash-o bigger-120"></i>
              </a>
            <?php } ?>
          </td> </tr>
    <?php
    $i++;
   } while ($row_c = fetchAssoc($c)); ?>
  <?php } // Show if recordset not empty ?>
      </table>
    </div> 
      <div id="pg">  
<!-- paginador-->
<?php include(dirname(__FILE__)."/../../system/paginador.php");?>
<!-- paginador-->   
              </div>
    </div>
  </section>
</td>
  </tr>
</table>
<?php include(dirname(__FILE__)."/../../footer.php");?>
