<?php include("list_f.php"); ?>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link  href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td width="1%" valign="top">
      <?php include_once ('../menu.php'); ?>
    </td>
    <td style="vertical-align: sub;padding-top: 20px;">
      <form method="post" name="form1" action="<?=$formSend?>" enctype="multipart/form-data">
        <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td>
              <div class="navipage">
                <?php if(empty($_GET['i'])){ ?>
                  <?=isset($dataGlobal['add_page']) ?  "[ ".$dataGlobal['add_page']." ]" : '[ Add Page ]'?> 
                  <input type="hidden" name="MM_insert" value="form1">
                  <input type="hidden" name="fecha" value="<?=$fecha?>">
                <?php }else{ ?>
                  <?=isset($dataGlobal['edit_web_content_page']) ?  "[ ".$dataGlobal['edit_web_content_page']." ]" : '[ Edit Page ]'?> 
                  <input type="hidden" name="MM_update" value="form1">
                  <input type="hidden" name="id" value="<?php echo $row_u['id']; ?>">
                  <input type="hidden" name="idx_router" value="<?php echo $row_u['idx_router']; ?>">
                <?php }?>
                <input type="hidden" name="tipo" value="page">
              </div>
            </td>
            <td>
              <table border="0" align="right" cellpadding="5" cellspacing="0">
                <tr>
                  <td>
                    <input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ?  $dataGlobal['btn_save'] : 'Save'?>" />
                  </td>
                  <td>
                    <a class="btn btn-small btn-default" href="index.php">
                      <?=isset($dataGlobal['btn_cancel']) ?  $dataGlobal['btn_cancel'] : 'Cancel'?>
                    </a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="96%" border="0" align="center">
          <tr>
            <td colspan="3">
              <strong class="text-uppercase">
                <?=isset($dataGlobal['data_access']) ?  $dataGlobal['data_access'] : 'Data Access'?>
              </strong>
            </td>
            <td width="5">&nbsp;</td>
            <td><strong><!--HOURS OF OPERATIONS--></strong></td>
          </tr>
          <tr>
            <tr>
              <td><?=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name'?></td>
              <td><span class="asterisco">*</span></td>
              <td><span id="sprytextfield1">
                <input name="nombre" type="text" value="<?php echo $row_u['titulo']; ?>" size="80" />
              </span>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr> 
          <tr>
            <td><?=isset($dataGlobal['label_web_marketing']) ?  $dataGlobal['label_web_marketing'] : 'Web Marketing'?></td>
            <td>&nbsp;</td>
            <td align="left">
              <select name="usamarketing" style="width:200px">
                <option value="5" <?php if($row_u["usamarketing"]=="5"){echo "selected='selected'";}?>>
                  <?=isset($dataGlobal['universal_metas']) ?  $dataGlobal['universal_metas'] : 'Universal Metas'?>
                </option>
                <option value="1" <?php if($row_u["usamarketing"]=="1"){echo "selected='selected'";}?>>
                  <?=isset($dataGlobal['custom_metas']) ?  $dataGlobal['custom_metas'] : 'Custom Metas'?>
                </option>
              </select>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr> 
          <tr>
            <td valign="top"><?=isset($dataGlobal['label_layout']) ?  $dataGlobal['label_layout'] : 'Layout'?></td>
            <td><span class="asterisco">*</span></td>
            <td align="left">
              <span id="sprytextfield2">
                <select name="file">
                  <?php
                    $layout = 'default.php';
                    if(isset($row_u['file']) && $row_u['file']!=''){$layout=$row_u['file'];}
                    $dir =  dirname(__FILE__)."/../../../".$_SESSION['THEME']."template-pages/"; 
                    chdir ($dir);  ///carpeta de pages 
                    $dh=opendir('.');
                    while ($file = readdir ($dh)) 
                    {
                      if ($file != "." && $file != "..") 
                      { ?>
                        <option value="<?=$file?>" <?php if($layout==$file) { echo "selected='selected'"; } ?>>
                          <?=strtoupper(substr($file, 0,-4))?>
                        </option>
                        <?php 
                      }
                    }
                    closedir($dh);
                  ?>
                  <option value="index.php" <?php if($layout=='index.php') { echo "selected='selected'"; } ?>>
                    <?=isset($dataGlobal['label_home']) ? strtoupper($dataGlobal['label_home']) : 'HOME'?>
                  </option>
                </select>
              </span>
              <br />
              <em>( <?=isset($dataGlobal['label_exmple']) ?  $dataGlobal['label_exmple'] : 'Example'?>: 
                <strong class="text-uppercase">
                  <?=isset($dataGlobal['label_default']) ?  $dataGlobal['label_default'] : 'Default'?>
                </strong> )
              </em>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr><td>&nbsp;</td></tr> 
          <tr>
            <td>Status</td>
            <td>&nbsp;</td>
            <td>
              <select name="activo" style="width:150px">
                <option value="1" <?php if($row_u['activo'] == 1){echo "selected='selected'";} ?>>
                  <?=isset($dataGlobal['label_active']) ?  $dataGlobal['label_active'] : 'Active'?>
                </option>
                <?php if(empty($_GET['i'])){ ?>
                  <option value="0">Inactive</option>
                <?php }else{?>
                  <option value="0" <?php if($row_u['activo'] == 0){echo "selected='selected'";} ?>>
                    <?=isset($dataGlobal['label_inactive']) ?  $dataGlobal['label_inactive'] : 'Inactive'?>
                  </option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="3"><hr /></td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<script type="text/javascript">
  var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
  var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none");
</script>  
<?php include(dirname(__FILE__)."/../../footer.php"); ?>