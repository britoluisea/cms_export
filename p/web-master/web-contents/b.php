<?php include("list_f.php"); ?>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td width="1%" valign="top">
 <?php include_once ('../menu.php'); ?>
    </td>
    <td style="vertical-align: sub;padding-top: 20px;">
<form method="post" name="form1" action="<?=$formSend?>"  >
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td>
<div class="navipage">
<?php if(empty($_GET['i'])){ ?>
<?=isset($dataGlobal['add_box']) ?  "[ ".$dataGlobal['add_box']." ]" : '[ Add Box ]'?>
<input type="hidden" name="MM_insert" value="form1">
<input type="hidden" name="fecha" value="<?=$fecha?>">
<?php }else{ ?>
  <?=isset($dataGlobal['edit_web_content_box']) ? "[ ".$dataGlobal['edit_web_content_box']." ]" : '[ Edit Box ]'?>
  <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="id" value="<?php echo $row_u['id']; ?>">
<?php }?>
    <input type="hidden" name="tipo" value="box">
    </div>
  </td>
    <td>
    <table border="0" align="right" cellpadding="5" cellspacing="0">
      <tr>
        <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ?  $dataGlobal['btn_save'] : 'Save'?>" /></td>
        <td><a class="btn btn-default" href="box.php"><?=isset($dataGlobal['btn_cancel']) ?  $dataGlobal['btn_cancel'] : 'Cancel'?></a></td>
        </tr>
    </table>
       </td>
  </tr>
</table>
    <table width="96%" border="0" align="center">
    <tr>
      <td colspan="3"><strong class="text-uppercase"><?=isset($dataGlobal['data_access']) ?  $dataGlobal['data_access'] : 'Data Access'?></strong></td>
      <td width="5">&nbsp;</td>
      <td><strong><!--HOURS OF OPERATIONS--></strong></td>
      </tr>
    <tr>
    <tr>
      <td><?=isset($dataGlobal['label_name']) ?  $dataGlobal['label_name'] : 'Name'?></td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="nombre" type="text" value="<?php echo $row_u['titulo']; ?>" size="80" />
      </span>
</td>
    </tr>
     <tr><td>&nbsp;</td></tr> 
      <tr>
      <td><?=isset($dataGlobal['label_web_marketing']) ?  $dataGlobal['label_web_marketing'] : 'Web Marketing'?></td>
      <td>&nbsp;</td>
      <td align="left">
    <label><?=isset($dataGlobal['web_content_boxes']) ?  $dataGlobal['web_content_boxes'] : 'Boxes'?></label>
   <input type="hidden" value="0" name="usamarketing">
      </td>
    </tr>
   <tr><td>&nbsp;</td></tr> 
    <tr>
      <td><?=isset($dataGlobal['label_status']) ?  $dataGlobal['label_status'] : 'Status'?></td>
      <td>&nbsp;</td>
      <td>
      <select name="activo" style="width:150px">
<option value="1" <?php if($row_u['activo'] == 1){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_active']) ?  $dataGlobal['label_active'] : 'Active'?></option>
<?php if(empty($_GET['i'])){ ?>
<option value="0">Inactive</option>
<?php }else{?>
<option value="0" <?php if($row_u['activo'] == 0){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_inactive']) ?  $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php } ?>
    </select>
    </td>
    </tr>
    <tr>
      <td colspan="3"><hr /></td>
      </tr>
</table>
</form>
    </td>
  </tr>
</table>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none");
</script>  
<?php include(dirname(__FILE__)."/../../footer.php"); ?>