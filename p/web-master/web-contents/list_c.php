<?php
include(dirname(__FILE__)."/../../header.php");
include(dirname(__FILE__)."/../../system/url.php");
require_once('name_table_bd.php'); 
//paginador
$x1 = SERVER.dirname($_SERVER['PHP_SELF'])."/index.php"; //definir doc
//paginador 
$currentPage = $_SERVER["PHP_SELF"];
$maxRows_c = 20;
$pageNum_c = 0;
if (isset($_GET['pageNum_c'])) { $pageNum_c = $_GET['pageNum_c']; }
if(!$pageNum_c)
{ 
  $pageNum_c = 1;
  $startRow_c = 0;
}
else
{
  $startRow_c = ($pageNum_c  -1) * $maxRows_c;
}
$query_c = "SELECT * FROM $tb_a WHERE tipo = 'page' AND theme_idx='".$_SESSION['THEME_IDX']."' ORDER BY titulo ASC";
//echo $query_c;
$query_limit_c = sprintf("%s LIMIT %d, %d", $query_c, $startRow_c, $maxRows_c);
$c = ejecutar($query_limit_c);
$row_c = fetchAssoc($c);

if (isset($_GET['totalRows_c'])) {
  $totalRows_c = $_GET['totalRows_c'];
} else {
  $all_c = ejecutar($query_c);
  $totalRows_c = numRows($all_c);
}
$totalPages_c = ceil($totalRows_c/$maxRows_c);

$queryString_c = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_c") == false && 
        stristr($param, "totalRows_c") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_c = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_c = sprintf("&totalRows_c=%d%s", $totalRows_c, $queryString_c);
?>