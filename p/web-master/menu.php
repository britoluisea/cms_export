<aside>
   <div id="resize">&laquo;</div>
   <div id="resize2">&raquo;</div>
    <div id="lateral">
    	<div id="navl1">
    		<h2>
    			<?=isset($dataGlobal['label_webmaster_ops']) ? $dataGlobal['label_webmaster_ops'] : 'Webmaster\'s Options'?>
    		</h2>
    		<ul id="menua" class="genial_menu">
    			<li id="menucat1" class="menucat1">
    				<a class="expanded">
    					<?=isset($dataGlobal['web_content']) ? $dataGlobal['web_content'] : 'Web Content'?>
    				</a>
    				<ul>
    					<li>
    						<a href="<?=SERVER_P?>web-master/web-contents/index.php">
    							<?=isset($dataGlobal['edit_web_content_page']) ? $dataGlobal['edit_web_content_page'] : 'Edit Page'?>
    						</a>
    					</li> 
    					<li>
    						<a href="<?=SERVER_P?>web-master/web-contents/box.php">
    							<?=isset($dataGlobal['edit_web_content_box']) ? $dataGlobal['edit_web_content_box'] : 'Edit Box'?>
    						</a>
    					</li>
    				</ul>
    			</li>
    			<li id="menucat1" class="menucat1">
    				<a class="expanded">
    					<?=isset($dataGlobal['account']) ? $dataGlobal['account'] : 'Account'?>
    				</a>
    				<ul>
    					<li>
    						<a href="<?=SERVER_P?>web-master/account/index.php">
    							<?=isset($dataGlobal['webmaster_support']) ? $dataGlobal['webmaster_support'] : 'Webmaster - Support'?>
    						</a>
    					</li> 
    					<li>
    						<a href="<?=SERVER_P?>web-master/account/privileges.php">
    							<?=isset($dataGlobal['privileges']) ? $dataGlobal['privileges'] : 'Privileges'?>
    						</a>
    					</li>
    					<li>
    						<a href="<?=SERVER_P?>web-master/account/info.php">
    							<?=isset($dataGlobal['account_info']) ? $dataGlobal['account_info'] : 'Account Info'?>
    						</a>
    					</li>
    				</ul>
    			</li> 
    			<li id="menucat1" class="menucat1">
    				<a class="expanded">
    					<?=isset($dataGlobal['template']) ? $dataGlobal['template'] : 'Template'?>
    				</a>
    				<ul>
    					<li>
    						<a href="<?=SERVER_P?>themes/index.php">
    							<?=isset($dataGlobal['template_active']) ? $dataGlobal['template_active'] : 'Active Template'?>
    						</a>
    					</li> 
    					<li>
    						<a href="<?=SERVER_P?>themes/install.php">
    							<?=isset($dataGlobal['template_install']) ? $dataGlobal['template_install'] : 'Install Template'?>
    						</a>
    					</li>
    				</ul>
    			</li>
    			<li id="menucat1" class="menucat1">
    				<a class="expanded">
    					<?=isset($dataGlobal['website']) ? $dataGlobal['website'] : 'Website'?>
    				</a>
    				<ul>
    					<li>
    						<a href="<?=SERVER_P?>web-master/website/index.php">
    							<?=isset($dataGlobal['website_status']) ? $dataGlobal['website_status'] : 'Website Status'?>
    						</a>
    					</li> 
    					<li>
    						<a href="<?=SERVER_P?>web-master/account/label.php">
    							<?=isset($dataGlobal['white_label']) ? $dataGlobal['white_label'] : 'CMS Options'?>
    						</a>
    					</li>
    				</ul>
    			</li>
    			<li id="menucat1" class="menucat1">
    				<a class="expanded">
    					<?=isset($dataGlobal['change_lenguaje']) ? $dataGlobal['change_lenguaje'] : 'Change Lenguaje'?>
    				</a>
    				<ul>
    					<li>
    						<a href="<?=SERVER_P?>web-master/language.php">
    							<?=isset($dataGlobal['change_lenguaje']) ? $dataGlobal['change_lenguaje'] : 'Change Lenguaje'?>
    						</a>
    					</li> 
    				</ul>
    			</li>
    		</ul>
    	</div>
    </div>
</aside>
 
