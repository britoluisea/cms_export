<?php 
  require_once(dirname(__FILE__).'/../../header.php'); 
  include("list_f.php"); 
  if($Result1 == true){    
    $success =isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
    echo'<script>
      $(function() {
        alerValid("Success", "'.$success.'", "1000");
        setTimeout(function(){ location.href="index.php"; }, 1000 );
      });
    </script>';
  }
?>
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="<?=PLUGINS_P?>SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script>  
function actpass()
{	
	if (document.getElementById("chkpass").checked == true)
	{
		document.getElementById("div_pass2").style.display="block";
		document.getElementById("div_pass1").style.display="none";	
		document.getElementById("claves").focus();
			

	}else
	{
		document.getElementById("div_pass2").style.display="none";
		document.getElementById("div_pass1").style.display="block";
	}	
}

</script>
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td width="1%" valign="top">
 <?php include_once ('../menu.php'); ?>
    </td>
    <td style="vertical-align: sub;padding-top: 20px;">
<form method="post" name="form1"  enctype="multipart/form-data">
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td>
<div class="navipage">
<?php if(empty($_GET['i'])){echo "[ New Account Webmaster - Support ] "; ?>
<input type="hidden" name="MM_insert" value="form1">
<input type="hidden" name="fecha" value="<?=$fecha?>">
<?php }else{echo "[ Edit Account Webmaster - Support ]"; ?>
  <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="idx" value="<?php echo $row_u['idx']; ?>">
<?php }?>
    </div>
	</td>
    <td>
    <table border="0" align="right" cellpadding="5" cellspacing="0">
      <tr>
        <td><input type="submit" class="btn btn-small btn-success" value="<?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?>" /></td>
        <td><a class="btn btn-small btn-default" href="index.php"><?=isset($dataGlobal['btn_cancel']) ? $dataGlobal['btn_cancel'] : 'Cancel'?></a></td>
        </tr>
    </table>
       </td>
  </tr>
</table>



    <table width="96%" border="0" align="center">
    <tr>
      <td colspan="3"><strong class="text-uppercase"><?=isset($dataGlobal['data_access']) ? $dataGlobal['data_access'] : 'Data Access'?></strong></td>
      <td width="5">&nbsp;</td>
      <td><strong><!--HOURS OF OPERATIONS--></strong></td>
      </tr>
    <tr>
      <td width="80"><?=isset($dataGlobal['account_type']) ? $dataGlobal['account_type'] : 'Account Type'?></td>
      <td><span class="asterisco">*</span></td>
      <td>
      <span id="spryselect1">
      <select name="userlevel" style="width:150px">
        <option value=""> &#8212; Select &#8212; </option>
        <option value="997"
<?php if($row_u["userlevel"]=="997"){echo "selected='selected'";}?>>Support</option>
        <option value="998" 
<?php if($row_u["userlevel"]=="998"){echo "selected='selected'";}?>>Webmaster</option>
      </select>
      </span>
      </td>
</tr>
    <tr>
      <td><?=isset($dataGlobal['email_user']) ? $dataGlobal['email_user'] : 'Email (User)'?> </td>
      <td><span class="asterisco">*</span></td>
      <td><span id="sprytextfield1">
        <input name="username" type="text" value="<?=$row_u["username"]?>" size="25" />
        <span class="textfieldInvalidFormatMsg"><?=isset($dataGlobal['invalid_format']) ? $dataGlobal['invalid_format'] : 'Invalid Format'?>.</span></span></td>
      <td>&nbsp;</td>
      </tr>

      <?php if(empty($_GET['i'])){	?>
    <tr>
      <td><?=isset($dataGlobal['labelPassLogin']) ? $dataGlobal['labelPassLogin'] : 'Password'?></td>
      <td><span class="asterisco">*</span></td>
      <td>
      <span id="sprytextfield2">
        <input name="claves" type="password" id="claves" size="15" />  
	<span class="textfieldRequiredMsg"><?=isset($dataGlobal['is_required']) ? $dataGlobal['is_required'] : 'Is required'?>.</span></span>
<script type="text/javascript">
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
</script> 
      </td>
      <td>&nbsp;</td>
      </tr>
<?php }else{ ?>
    <tr>
      <td height="35"><?=isset($dataGlobal['labelPassLogin']) ? $dataGlobal['labelPassLogin'] : 'Password'?></td>
      <td><input name="chkpass" type="checkbox" id="chkpass" onclick="actpass('S')" value="1" /></td>
      <td>
      <div id="div_pass1">
        ******
      </div>
      <div id="div_pass2" style=" <?='display:none'?>">
      <input name="claves" type="password" id="claves" class="caja_texto" value="" size="10" />
      </div>
</td>
      <td>&nbsp;</td>
      </tr>
    <?php 	}	?>

    <tr>
      <td><?=isset($dataGlobal['label_status']) ? $dataGlobal['label_status'] : 'Status'?></td>
      <td>&nbsp;</td>
      <td>
  	  <select name="activo" style="width:150px">
<option value="1" <?php if($row_u['activo'] == 1){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_active']) ? $dataGlobal['label_active'] : 'Active'?></option>
<?php if(empty($_GET['i'])){ ?>
<option value="0">Inactive</option>
<?php }else{?>
<option value="0" <?php if($row_u['activo'] == 0){echo "selected='selected'";} ?>><?=isset($dataGlobal['label_inactive']) ? $dataGlobal['label_inactive'] : 'Inactive'?></option>
<?php } ?>
	  </select>
	  </td>
    </tr>
    <tr>
      <td colspan="3"></td>
      </tr>
	  
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td colspan="3"><strong class="text-uppercase"><?=isset($dataGlobal['label_information']) ? $dataGlobal['label_information'] : 'Information'?></strong></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['label_company']) ? $dataGlobal['label_company'] : 'Company'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="compania" type="text" value="<?=$row_u["compania"]?>" size="25" />
      </td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['first_name']) ? $dataGlobal['first_name'] : 'First Name'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="nombres" type="text" value="<?=$row_u["nombres"]?>" size="25" />
      </td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['last_name']) ? $dataGlobal['last_name'] : 'Last Name'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="apellidos" type="text" value="<?=$row_u["apellidos"]?>" size="25" />
      </td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['address']) ? $dataGlobal['address'] : 'Address'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="direccion" type="text" value="<?=$row_u["direccion"]?>" size="25" />
		</td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['address']) ? $dataGlobal['address'] : 'Address'?> 2</td>
      <td>&nbsp;</td>
      <td><input name="direccion2" type="text" value="<?=$row_u["direccion2"]?>" size="25" /></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="ciudad" type="text" value="<?=$row_u["ciudad"]?>" size="25" />
      <span class="textfieldRequiredMsg"><?=isset($dataGlobal['is_required']) ? $dataGlobal['is_required'] : 'Is required'?>.</span></span></td>
      <td>&nbsp;</td>
      </tr>
	 <tr>
      <td><?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="estado" type="text" value="<?=$row_u["estado"]?>" size="25" />
      </td>
      <td>&nbsp;</td>
      </tr>    
 <tr>
      <td><?=isset($dataGlobal['code_zip']) ? $dataGlobal['code_zip'] : 'Zip Code'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="zipcode" type="text" value="<?=$row_u["zipcode"]?>" size="10" />      </td>
      <td>&nbsp;</td>
      </tr>        
    <tr>
      <td><?=isset($dataGlobal['telephone']) ? $dataGlobal['telephone'] : 'Telephone'?></td>
      <td>&nbsp;</td>
      <td>
        <input name="telefono" type="text" value="<?=$row_u["telefono"]?>" size="25" />
      </td>
      <td>&nbsp;</td>
      </tr>
 <tr>
   <td><?=isset($dataGlobal['cell_tlf']) ? $dataGlobal['cell_tlf'] : 'Cell Phone'?></td>
   <td>&nbsp;</td>
   <td>
     <input name="celular" type="text" value="<?=$row_u["celular"]?>" size="25" />	</td>
   <td>&nbsp;</td>
   </tr>
 
 <tr>
   <td valign="top">&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   </tr>


</table>

</form>

    </td>
  </tr>
</table>

  <script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
</script>  

<?php include(dirname(__FILE__)."/../../footer.php"); ?>