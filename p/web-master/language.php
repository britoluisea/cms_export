<?php 
	require_once(dirname(__FILE__).'/../header.php'); 
	require_once dirname(__FILE__).'/../system/functions.php';
	$file_config = dirname(__FILE__).'/../system/languages/config.json';
	$selected = 'selected="selected"';
	if(isset($_POST['change']) && $_POST['change'] == 1){
    $success =isset($dataGlobal['msj_success_js']) ? $dataGlobal['msj_success_js'] : 'Successful process';
    $label_success=isset($dataGlobal['label_success']) ? $dataGlobal['label_success'] : 'Success';
    $fp = fopen($file_config, "w");
	fputs($fp, '{"leng":"'.$_POST['lenguaje'].'", "file":"'.$_POST['lenguaje'].'.json"}');
	fclose($fp);
		echo'<script>$(function() {
		alerValid("'.$label_success.'", "'.$success.'", "6000");
		window.location=window.location;});</script>';
	}
	else
	{
		$f = file_get_contents($file_config);
		$d = json_decode($config, true);
	}
?>

  <table width="100%" border="0" align="center">
    <tr>
      <td width="274" valign="top"> <?php include 'menu.php'; ?></td>
      <td width="20" valign="top">&nbsp;</td>
      <td valign="top">
      	<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Tabla_2">
	        <tr>
	          <td>
				<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
				  <tr>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <tr>
				    <td>
					 <h2 class="h2" style="color:#006699"><strong><?=isset($dataGlobal['lenguaje']) ? $dataGlobal['lenguaje'] : 'CMS Languaje'?></strong></h2><br />
				    </td>
				    <td>&nbsp;</td>
				  </tr>
				</table>
	          </td>
	        </tr>
	        <tr>
	          <td>
	          	<form method="post">
		          	<label for=""><?=isset($dataGlobal['change_lenguaje']) ? $dataGlobal['change_lenguaje'] : 'Change Lenguaje'?></label>
		          	<select name="lenguaje" class="form-control" style="width:300px">
		          		<option value="en" <?php if(isset($d['leng']) && $d['leng']=='en'){echo $selected;}?>><?=isset($dataGlobal['leg_en']) ? $dataGlobal['leg_en'] : 'English'?></option>
		          		<option value="es" <?php if(isset($d['leng']) && $d['leng']=='es'){echo $selected;}?>><?=isset($dataGlobal['leg_es']) ? $dataGlobal['leg_es'] : 'Spanish'?></option>
		          	</select>
		          	<div cms-cols="col c100" style="padding-left: 0px;">
		          		<input type="hidden" name="change" value="1">
		          		<button class="btn  btn-success"><i class="fa fa-save"></i> <?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
		          	</div>
				</form>          
	          </td>
	        </tr>
	      </table>
	      <table align="center" border="0" width="90%" >
	      	<tr>
	      		<th style="width:40%"><?=isset($dataGlobal['leg_en']) ? $dataGlobal['leg_en'] : 'Ingles<'?></th>
	      		<th style="width:10%;text-align: right; padding-right: 1%" align="right">
	      			<a class="btn btn-samll btn-primary" id="en_custom" style="padding: 2px 4px;border-radius: 3px;"><i class="fa fa-edit"></i></a>
	      		</th>
	      		<th style="width:40%"><?=isset($dataGlobal['leg_es']) ? $dataGlobal['leg_es'] : 'Español'?></th>
	      		<th style="width:10%;text-align: right; padding-right: 1%" align="right">
	      			<a class="btn btn-samll btn-primary" id="es_custom" style="padding: 2px 4px;border-radius: 3px;"><i class="fa fa-edit"></i></a>
	      		</th>
	      	</tr>
	      	<tr>
	      		<td colspan="2">
	      			<input type="text" id="buscar_en" class="form-control" style="width: 98%" placeholder="<?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search'] : 'Search'?>">
	      		</td>
	      		<td colspan="2">
	      			<input type="text" id="buscar_es" class="form-control" style="width: 98%" placeholder="<?=isset($dataGlobal['label_search']) ? $dataGlobal['label_search']: 'Search'?>">
	      		</td>
	      	</tr>
	      	<tr>
	      		<td valign="top" colspan="2">
	      			<select id="lang_en" size="5" class="selectMulti" style="width: 98%; height: 350px;">
	      				<?php
	      					$file_origin = "../system/languages/en.json";
							$file_custom = "../system/languages/en_custom.json";
							$arr=idiomaCustom($file_origin, $file_custom);
							foreach ($arr as $key => $value){
	      						if($value!=''){ ?>
	      							<option value="<?=$key?>"><?=$value?></option>
	      							<?php		
	      						}
	      					}
	      				?>
	      			</select>
	      		</td>
	      		<td valign="top" colspan="2">
	      			<select id="lang_es" size="5" class="selectMulti" style="width: 98%; height: 350px;">
	      				<?php
	      					$file_origin = "../system/languages/es.json";
							$file_custom = "../system/languages/es_custom.json";
							$arr=idiomaCustom($file_origin, $file_custom);
							foreach ($arr as $key => $value){
	      						if($value!=''){ ?>
	      							<option value="<?=$key?>"><?=$value?></option>
	      							<?php		
	      						}
	      					}
	      				?>
	      			</select>
	      		</td>
	      	</tr>
	      </table>
	  	</td>
    </tr>
  </table>
  <div class="modal fade" id="modal-id">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  			</div>
  			<div class="modal-body">
  				<textarea id="textEdit" class="form-control"></textarea>
  				<input type="hidden" id="key">
  			</div>
  			<div class="modal-footer">
  				<button type="button" id="restore" class="btn btn-default" ><?=isset($dataGlobal['label_restore']) ? $dataGlobal['label_restore'] : 'Restore'?></button>
  				<button type="button" id="save" class="btn btn-primary"><?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
  			</div>
  		</div>
  	</div>
  </div>
  <div class="modal fade" id="modal-idES">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  			</div>
  			<div class="modal-body">
  				<textarea id="textEditES" class="form-control"></textarea>
  				<input type="hidden" id="keyES">
  			</div>
  			<div class="modal-footer">
  				<button type="button" id="restoreES" class="btn btn-default" ><?=isset($dataGlobal['label_restore']) ? $dataGlobal['label_restore'] : 'Restore'?></button>
  				<button type="button" id="saveES" class="btn btn-primary"><?=isset($dataGlobal['btn_save']) ? $dataGlobal['btn_save'] : 'Save'?></button>
  			</div>
  		</div>
  	</div>
  </div>
  <style>
  	.selectMulti option {
  		white-space: pre-line;
  		padding: 5px;
    	height: auto;
    	border-bottom: 1px solid #ccc;
  	}
  </style>
  <script>
  	var buscar = function(input, select){
  		if(input.value.length>0){
  			for (var n = 0; n < select.options.length; n++) {
				select.options[n].style.display="none";
				let origin = input.value.toLowerCase();
				let text = select.options[n].text.toLowerCase();
				if(text.indexOf(origin) != -1)
				{
					select.options[n].style.display="block";
				}
			}
  		}
  		else
  		{
  			for (var n = 0; n < select.options.length; n++) {
				select.options[n].style.display="block";
			}
  		}
  	}
  	var buscar_en = document.querySelector("#buscar_en");
  	var lang_en = document.querySelector("#lang_en");
  	buscar_en.addEventListener('keyup', function(e){
  		buscar(buscar_en, lang_en);
  	});
  	var buscar_es = document.querySelector("#buscar_es");
  	var lang_es = document.querySelector("#lang_es");  	
  	buscar_es.addEventListener('keyup', function(e){
  		buscar(buscar_es, lang_es);
  	});
  	$('#en_custom').click(function(e) {
  		e.preventDefault();
  		var select= lang_en;
  		var selectedOption = select.options[select.selectedIndex];
    	if(typeof selectedOption != "undefined")
    	{
    		$("#textEdit").val(selectedOption.text);
    		$("#key").val(selectedOption.value);
    		$('#modal-id').modal('show'); // abrir
    		//console.log(selectedOption.value + ': ' + selectedOption.text);	
    	}
  	});
  	$('#save').click(function(event) {  		
    	let val = $("#textEdit").val();
    	let key= $("#key").val();
  		$.post('../system/languages/langSave.php', {'key': key, 'val': val, 'lang':'en', 'ope':'add'}, function(data, textStatus, xhr) {
  			let select= lang_en;  	;
  			let selectedOption = select.options[select.selectedIndex];
  			selectedOption.text=data;
  			$('#modal-id').modal('hide');
  			$("#textEdit").val('');
			$("#key").val('');
  		});
  	});
  	$('#restore').click(function(event) {  		
    	let val = $("#textEdit").val();
    	let key= $("#key").val();
  		$.post('../system/languages/langSave.php', {'key': key, 'val': val, 'lang':'en', 'ope':'del'}, function(data, textStatus, xhr) {
  			let select= lang_en;  	;
  			let selectedOption = select.options[select.selectedIndex];
  			selectedOption.text=data;
  			$('#modal-id').modal('hide');
  			$("#textEdit").val('');
			$("#key").val('');
  		});
  	});
  	$('#es_custom').click(function(e) {
  		e.preventDefault();
  		var select= lang_es;
  		var selectedOption = select.options[select.selectedIndex];
    	if(typeof selectedOption != "undefined")
    	{
    		$("#textEditES").val(selectedOption.text);
    		$("#keyES").val(selectedOption.value);
    		$('#modal-idES').modal('show'); // abrir
    		//console.log(selectedOption.value + ': ' + selectedOption.text);	
    	}
  	});
  	$('#saveES').click(function(event) {  		
    	let val = $("#textEditES").val();
    	let key= $("#keyES").val();
  		$.post('../system/languages/langSave.php', {'key': key, 'val': val, 'lang':'es', 'ope':'add'}, function(data, textStatus, xhr) {
  			let select= lang_es;  	;
  			let selectedOption = select.options[select.selectedIndex];
  			selectedOption.text=data;
  			$('#modal-idES').modal('hide');
  			$("#textEditES").val('');
			$("#keyES").val('');
  		});
  	});
  	$('#restoreES').click(function(event) {  		
    	let val = $("#textEditES").val();
    	let key= $("#keyES").val();
  		$.post('../system/languages/langSave.php', {'key': key, 'val': val, 'lang':'es', 'ope':'del'}, function(data, textStatus, xhr) {
  			let select= lang_es;  	;
  			let selectedOption = select.options[select.selectedIndex];
  			selectedOption.text=data;
  			$('#modal-idES').modal('hide');
  			$("#textEditES").val('');
			$("#keyES").val('');
  		});
  	});
  </script>
<?php include(dirname(__FILE__)."/../footer.php");?>
