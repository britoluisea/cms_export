<?php include(dirname(__FILE__)."/header_before.php");?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- 
		comentando propiedad responsive
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
    	-->
        <link rel="icon" href="<?=IMG_CMS?>system/icon-cms.png" type="image/png" sizes="16x16">
        <title>CMS</title>
        <meta name="author" content="marketea.com">
        <meta name = "robots" content = "noindex, nofollow">
        <link rel="stylesheet" href="<?=CSS_P?>website.css">
        <link href="<?=CSS_P?>bootstrap.min.css" rel="stylesheet">
        <link href="<?=CSS_P?>font-awesome.min.css" rel="stylesheet">
        <link href="<?=CSS_P?>style.css" rel="stylesheet" type="text/css">
        <link href="<?=CSS_P?>simple-sidebar.css" rel="stylesheet">
        <link href="<?=CSS_P?>SmartNotification.css" rel="stylesheet">
        <link href="<?=CSS_P?>select2.min.css" rel="stylesheet">
        <link href="<?=CSS_P?>customize.css" rel="stylesheet">
        <!-- jQuery -->
        <script>
            var server = '<?=SERVER?>';
            var server_p = '<?=SERVER_P?>';
            var plugins_p = '<?=PLUGINS_P?>';
            var importCssTheme = "<?=SERVER.$_SESSION['CSS_THEME']?>import.css";
        </script>
        <script src="<?=JS_P?>jquery.js"></script>
        <script src="<?=JS_P?>SmartNotification.min.js"></script> 
        <script src="<?=JS_P?>notifications.js"></script> 
        <script src="<?=JS_P?>select2.min.js"></script> 
        <script src='<?=JS_P?>jquery-sortable.js'></script>
    </head>