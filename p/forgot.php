<?php include("header_index.php"); ?>
<?php
$usuarios = isset($_POST['usuarios']) ? $_POST['usuarios'] : '';
$usuarios = str_replace("/", "", $_POST['usuarios']);
if (!empty($usuarios)) 
{
    $_SESSION['correo_enviado'] = 'NO';
    $usuarios = addslashes($_POST['usuarios']);
    $sql3 = "SELECT * FROM usuarios where activo=1 and username = '$usuarios'" . " limit 0,1";
    $res3 = ejecutar($sql3);
    if ($col3 = fetchArray($res3)) 
    {
        $sql4 = "delete from usuariosreset where username = '$usuarios' ";
        $res4 = ejecutar($sql4);
        $token = rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999);
        $sql4 = "insert into usuariosreset (username , token    , fecreg , fecmod , activo) values ( '$usuarios' , '$token', now(), now(), 1)";
        $res4 = ejecutar($sql4);
        $sql_1 = "select * from usuarioscompany where idx = '1' "; //Devuelve todos los Idiomas
        $consulta_1 = ejecutar($sql_1);
        $atabla = array();
        while ($fila_1 = fetchAssoc($consulta_1)) { $atabla['en'] = $fila_1; }
        ob_start(); //Abrimos el buffer
        ?>
        <?=isset($dataGlobal['msjForgot']) ? $dataGlobal['msjForgot'] : 'Please click on the url below to create a new password.'?><br /> <br />
        <a href="http://<?=SERVER_P ?>/p/forgot-password.php?token=<?= $token ?>"><?= SERVER_P ?>forgot-password.php?token=<?= $token ?></a><?php
        $texto_email = ob_get_contents(); //Vaciamos el buffer
        ob_end_clean();
        define('HTTP_SERVER', $_SERVER['HTTP_HOST']);
        define('DIR_WS_IMAGES', 'images/');
        define('DIR_WS_FUNCTIONS', 'functions/');
        define('DIR_WS_CLASSES', 'classes/');
        define('DIR_WS_LANGUAGES', 'languages/');
        // ************ inicio tabla configuracion email
        define('EMAIL_TRANSPORT', 'sendmail'); // 'sendmail' or 'smtp'
        define('EMAIL_LINEFEED', 'LF'); // 'LF' OR 'CRLF'
        define('EMAIL_USE_HTML', 'true'); //'true' OR 'false'
        //define('ENTRY_EMAIL_ADDRESS_CHECK', 'false'); // REVISAR
        define('SEND_EMAILS', 'true'); //'true' or 'false'
        // charset for web pages and emails
        define('CHARSET', 'iso-8859-1');
        //  define('CHARSET', 'utf-8');
        // email classes
        include_once(DIR_WS_FUNCTIONS . 'func_email.php');
        include_once(DIR_WS_CLASSES . 'mime.php');
        include_once(DIR_WS_CLASSES . 'email.php');
        // ************ fin tabla configuracion email
        $email_text_subject = isset($dataGlobal['forgot_text_subject']) ? $dataGlobal['forgot_text_subject'] : 'Forgot Password';
        $store_owner = ''; // COLOCAR SIEMPRE BLANCO
        $store_owner_email_address = $atabla['en']["sendercompany"] . '<' . $atabla['en']["senderemail"] . '>';
        $wemail = $usuarios;
        if ($_SERVER['HTTP_HOST'] <> 'localhost')
            tep_mail($wnombre . ' ' . $wapellido, $wemail, $email_text_subject, $texto_email, $store_owner, $store_owner_email_address);
        $_SESSION['correo_enviado'] = 'SI';

        //echo'<script>location.href="'.SERVER_P.'forgot.php";</script>';
    }
    else 
    {
        $_SESSION['error_array'] = isset($dataGlobal['msjErrorLogin']) ? $dataGlobal['msjErrorLogin'] : "Invalid username or password.";
        //echo'<script>location.href="'.SERVER_P.'forgot.php";</script>';
    }
}
?>

<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header "><?=isset($dataGlobal['titleForgot']) ? $dataGlobal['titleForgot'] : 'Forgot Password'?></div>
<?php
            if (isset($_SESSION['correo_enviado']) && $_SESSION['correo_enviado'] == 'SI') 
            {
                ?>
                <div align="justify" style="padding:10px;">
                    <p><?=isset($dataGlobal['instructionForgot']) ? $dataGlobal['instructionForgot'] : 'We have sent you an email with instruction on how to create your new password.  If you dont see our email, check your spam folder.  If you encounter any issues, contact your website administrator.'?></p>
                    <p align="center"><br />
                        <a href="<?=URL_INDEX?>"><?=isset($dataGlobal['volverLogin']) ? $dataGlobal['volverLogin'] : 'LOGIN'?></a>
                    </p></div>
                <?php
                    unset($_SESSION['correo_enviado']);
            }
            else 
            {
                if (isset($_SESSION['correo_enviado']) && $_SESSION['correo_enviado'] == 'NO') 
                {
                    ?>  <div align="center" style="color:#F00;"><?=isset($dataGlobal['forgetEmailInvalid']) ? $dataGlobal['forgetEmailInvalid'] : 'Invalid username.'?><p></p></div> <?php
                    unset($_SESSION['correo_enviado']);
                }
            }

            if(isset($_SESSION['error_array']) && !empty($_SESSION['error_array'])){
                echo "<div align=center><strong><font size=\"2\" color=\"#ff0000\">".$_SESSION['error_array']."</font></strong></div><br>";
                unset($_SESSION['error_array']);
            }
            ?>
      <div class="card-body">
        <form id="form1" name="form1" method="post" action="" >
          <div class="form-group">
            <label for="exampleInputEmail1"><?=isset($dataGlobal['labelForgotUser']) ? $dataGlobal['labelForgotUser'] : 'Username'?></label>
            <input type="text"  aria-describedby="Username"  name="usuarios" class="form-control" id="usuarios" >
          </div>
          <input type="hidden" name="sublogin" value="1">
            <input class="btn btn-primary btn-block" type="submit" name="button" id="button" value="<?=isset($dataGlobal['botonForgot']) ? $dataGlobal['botonForgot'] : 'Recover Now'?>" style="cursor:pointer" />
        </form>
        <div class="text-center">
            <div class="infosx">
            </div>
          <a href="<?=URL_INDEX_P?>"><?=isset($dataGlobal['volverLogin']) ? $dataGlobal['volverLogin'] : 'Login'?></a>
        </div>
      </div>
    </div>
  </div>
<?php include("footer_index.php"); ?>