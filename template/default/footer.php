</div>
        <!-- CONTENT END -->
        <!-- FOOTER START -->
        <footer class="site-footer footer-dark">
            <!-- FOOTER BLOCKES START -->
            <div class="footer-top overlay-wraper">
                <div class="overlay-main"></div>
                <div class="container">
                    <div class="row">
                        <!-- USEFUL LINKS -->
                        <div class="col-md-3 col-sm-6">
                            <div class="widget widget_services">
                                <h4 class="widget-title">Useful links</h4>
                                <ul>
                                   <?php include "nav_foot.php"; ?>
                                </ul>
                            </div>
                            <div class="widget widget_social_inks">

                                
                                <ul class="social-icons social-square social-darkest">
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-facebook"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-twitter"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-linkedin"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-rss"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-youtube"></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="fa fa-instagram"></a>
                                    </li>
                                </ul>
                                
                            </div>
                        </div>
                        <!-- Widget Facebook -->
                        <div class="col-md-4 col-sm-6">
                            <div class="widget recent-posts-entry-date">
                                <h4 class="widget-title">Widget FB</h4>
                                <div class="widget-post-bx">
                                    <iframe src="inc/_iframe.php" class="WGiframe" style="width: 100%; max-width:300px; height:350px; border:0px" scrolling="no"  data-box="66"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <!-- SOCIAL LINKS -->
                            <div class="widget ">
                                <h4 class="widget-title">Widget Youtube</h4>
                                <iframe src="inc/_iframe.php" class="WGiframe" style="width: 100%; height:350px;border:0px" scrolling="no"  data-box="67"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6  p-tb20">
                            <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-travel"></span>
                                </div>
                                <div class="icon-content">
                                    <h5 class="wt-tilte text-uppercase m-b0">Address</h5>
                                    <p>291 Main St Milford, MA 01757</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6  p-tb20 ">
                            <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix ">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-smartphone-1"></span>
                                </div>
                                <div class="icon-content">
                                    <h5 class="wt-tilte text-uppercase m-b0">Phone</h5>
                                    <p class="m-b0">(888) 778-2541</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 p-tb20">
                            <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-email"></span>
                                </div>
                                <div class="icon-content">
                                    <h5 class="wt-tilte text-uppercase m-b0">Email</h5>
                                    <p class="m-b0">info@MarketeaContractors.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom overlay-wraper">
                <div class="overlay-main"></div>
                <div class="container p-t30">
                    <div class="row">
                        <div class="text-center">
                            <span class="copyrights-text">© 2019 Your Company. All Rights Reserved. </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->
        <!-- SCROLL TOP BUTTON -->
        <button class="scroltop">
            <span class=" iconmoon-house relative" id="btn-vibrate"></span>Top
        </button>
<?php include "foot.php"; ?>