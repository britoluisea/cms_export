    </div>
    <!-- JAVASCRIPT  FILES ========================================= -->
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>jquery.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>bootstrap-select.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>jquery.bootstrap-touchspin.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>waypoints.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>counterup.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>waypoints-sticky.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>stellar.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>scrolla.min.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>custom.js"></script>
    <script type="text/javascript" src="<?=SERVER.JS_THEME?>shortcode.js"></script>
    <?php include _INC."_includes.php"; ?>
</body>
</html>