<?php include "header.php"; ?>
            <!-- ABOUT COMPANY SECTION START -->
            <div class="section-full p-tb100 bg-gray">
                <div class="container">
                    <div class="section-head text-center">
                        <?php $i= $url_request['id_router']; include _INC."_content.php"; ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6 col-xs-100pc">
                            <div class="about-com-pic-2">
                                <img src="<?=IMG_EDITOR?>about-pic4.jpg" alt="" />
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="about-types row">
                                <div class="col-md-6 col-sm-6 m-b30">
                                    <div class="wt-icon-box-wraper bx-style-1 p-a30 center bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><img src="<?=IMG_EDITOR?>icon/home.png" alt=""></span>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase">Building</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod .</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 m-b30">
                                    <div class="wt-icon-box-wraper bx-style-1 p-a30 center bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><img src="<?=IMG_EDITOR?>icon/paint-brush.png" alt=""></span>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase">Renovation</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod .</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 m-b30">
                                    <div class="wt-icon-box-wraper bx-style-1 p-a30 center bg-white">
                                        <div class="icon-lg text-primary  m-b20">
                                            <span class="icon-cell text-primary"><img src="<?=IMG_EDITOR?>icon/jackhammer.png" alt=""></span>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase">Digging</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod .</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 m-b30">
                                    <div class="wt-icon-box-wraper bx-style-1 p-a30 center bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><img src="<?=IMG_EDITOR?>icon/window.png" alt=""></span>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase">interior</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod .</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ABOUT COMPANY SECTION END -->
            <!-- SERVICES START  -->
            <div class="section-full bg-white p-t80 p-b50 scale-bg-top scale-bg-bottom">
                <div class="container">
                    <!-- IMAGE CAROUSEL START -->
                    <!-- TITLE START -->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Our Services</h2>
                        <div class="wt-separator-outer">
                            <div class="wt-separator style-square">
                                <span class="separator-left bg-primary"></span>
                                <span class="separator-right bg-primary"></span>
                            </div>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <!-- TITLE END -->
                    <!-- CAROUSEL -->
                    <div class="section-content">
                        <div class="owl-carousel Home-services-carousel owl-btn-vertical-center">
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic1.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Shopping Mall</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic2.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Hospital Building</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic3.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Garden House</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic1.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Shopping Mall</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic2.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Hospital Building</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="wt-box bg-white">
                                    <div class="wt-media">
                                        <a href="javascript:void(0);"><img src="<?=IMG_EDITOR?>our-work/pic3.jpg" alt=""></a>
                                    </div>
                                    <div class="wt-info p-tb30">
                                        <h4 class="wt-title m-t0 m-b5"><a href="javascript:void(0);">Garden House</a></h4>
                                        <p>Lorem ipsum dolor consectetur adipiscing Fusce varius euismod lacus eget feugiat rorem ipsum dolor consectetur Fusce varius . </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SERVICES CONTENT END -->
            <!-- SECTION CONTENT START -->
            <div class="section-full  p-tb80 bg-no-repeat bg-cover " style="background-image:url(<?=IMG_EDITOR?>background/bg5.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6  clearfix">
                            <div class="wt-box graph-tabing">
                                
                                <iframe src="inc/_iframe.php" class="WGiframe" style="width: 100%; height: 543px; border:0px" scrolling="no"  data-box="68"></iframe>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wt-box graph-part-right text-white">
                                <strong class="text-uppercase title-first display-block">Construction</strong>
                                <span class="text-uppercase text-primary display-block title-second">What are you looking for?</span>
                                <p><strong>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                                     type and scrambled it to make a type specimen book.</strong></p>
                                <ul class="list-check-circle primary">
                                    <li>Simply dummy text of the Lorem Ipsum is printing and type setting. </li>
                                    <li>Dummy text of the printing and typesetting industry. Text of the printing</li>
                                    <li>And typesetting industry Lorem Ipsum has been. Ipsum has been the </li>
                                    <li>Industry's standard dummy since the 1500s, </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SECTION CONTENT  END -->
            <!-- LATEST PROJECT SECTION START -->
            <div class="section-full bg-white p-tb80 scale-bg-top scale-bg-bottom">
                <div class="container">
                    <div class="section-head text-center">
                        <h2 class="text-uppercase ">Latest Projects</h2>
                        <div class="wt-separator-outer">
                            <div class="wt-separator style-square">
                                <span class="separator-left bg-primary"></span>
                                <span class="separator-right bg-primary"></span>
                            </div>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="portfolio-wrap mfp-gallery no-col-gap">
                            <!-- COLUMNS 1 -->
                            <div class="masonry-item cat-1 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic1.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic1.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="masonry-item cat-2 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow wt-img-effect zoom">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic2.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic2.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="masonry-item cat-3 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic3.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic3.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 4 -->
                            <div class="masonry-item cat-4 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic4.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic4.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 5 -->
                            <div class="masonry-item cat-5 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic5.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic5.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 6 -->
                            <div class="masonry-item cat-4 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="wt-gallery-bx">
                                    <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom-slow">
                                        <a href="javascript:void(0);">
                                            <img src="<?=IMG_EDITOR?>latest-projects/pic6.jpg" alt="">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-icon">
                                                <a href="javascript:void(0);">
                                                    <i class="fa fa-external-link wt-icon-box-xs"></i>
                                                </a>
                                                <a href="<?=IMG_EDITOR?>gallery/pic6.jpg" class="mfp-link">
                                                    <i class="fa fa-arrows-alt wt-icon-box-xs"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- LATEST PROJECT SECTION END -->
            <!-- COMPANY DETAIL SECTION START -->
            <div class="section-full  p-t50 p-b10 overlay-wraper bg-repeat" data-stellar-background-ratio="0.5" style="background-image:url(<?=IMG_EDITOR?>switcher/switcher-patterns/large/pt2.jpg)">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="section-content satisfaction mb-5">
                        <div class="row">
                            <div class="text-center">
                                <h2><span>100%</span> Satisfaction Guaranteed!</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- COMPANY DETAIL SECTION End -->
            <!-- WHY CHOOSE US SECTION START  -->
            <div class="section-full bg-gray p-t80 p-b120 bg-no-repeat bg-left-center" style="background-image:url(<?=IMG_EDITOR?>background/why-choose-pic-2.png);">
                <div class="container">
                    <!-- TITLE START-->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Why Choose us</h2>
                        <div class="wt-separator-outer">
                            <div class="wt-separator style-square">
                                <span class="separator-left bg-primary"></span>
                                <span class="separator-right bg-primary"></span>
                            </div>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <!-- TITLE END-->
                    <div class="section-content no-col-gap">
                        <div class="row">
                            <!-- COLUMNS 1 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/engineer.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">BEST QUALITY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/light-bulb.png" alt=""></a>
                                    </div>
                                    <div class="icon-content ">
                                        <h5 class="wt-tilte text-uppercase">INTEGRITY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/compass.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">STRATEGY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 4 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/helmet.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">SAFETY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 5 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/brickwall.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">COMMUNITY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 6 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="<?=IMG_EDITOR?>icon/no-entry.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">SUSTAINABILITY</h5>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesg indtrysum has been the Ipsum dummy of the printing indus .</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- WHY CHOOSE US SECTION END  -->
            <!-- TESTIMONIAL SECTION START -->
            <div class="section-full p-t80 p-b50 overlay-wraper bg-parallax" data-stellar-background-ratio="0.5" style="background-image:url(<?=IMG_EDITOR?>background/bg9.jpg)">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <?php include(_INC."rating_reviews_widget.php");?>
                </div>
            </div>
            <!-- TESTIMONIAL SECTION START -->
            <!-- LATEST BLOG SECTION START -->
            <div class="section-full latest-blog bg-gray p-t80 p-b50 " id="blog">
                <div class="container">
                    <?php include _INC."_blog_widget_list.php"; ?>
                </div>
            </div>
            <!-- LATEST BLOG SECTION END -->
            <!-- GOOGLE MAP START -->
            <iframe src="inc/_iframe.php" class="WGiframe" style="width: 100%; height: 450px; border:0px" scrolling="no"  data-box="65"></iframe>
            <!-- GOOGLE MAP SECTION END -->            
            <!-- OUR CLIENT SLIDER START -->
            <div class="section-full overlay-wraper bg-cover bg-repeat-x bg-primary" style="background-image:url(<?=IMG_EDITOR?>background/bg7.png)">
                <div class="container">
                    <!-- IMAGE CAROUSEL START -->
                    <div class="section-content">
                        <div class="owl-carousel home4-logo-carousel">
                            <!-- COLUMNS 1 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w1.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w2.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w3.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 4 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w4.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 5 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w5.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 6 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w6.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 7 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w1.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 8 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w2.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 9 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w3.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 10 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w4.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 11 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w5.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 12 -->
                            <div class="item">
                                <div class="ow-client-logo">
                                    <div class="client-logo wt-img-effect on-color">
                                        <a href="#"><img src="<?=IMG_EDITOR?>client-logo/w6.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- IMAGE CAROUSEL START -->
                </div>
            </div>
            <!-- OUR CLIENT SLIDER END -->
<?php include "footer.php"; ?>  