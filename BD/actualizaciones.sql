{
	"table": {
		"name": "articles_options",
		"add": {
			"comments_blog": "INT(1) NOT NULL AFTER twitter_like;",
			"color_text_btn": "VARCHAR(20) NOT NULL AFTER comments_blog;",
			"fondo_btn": "VARCHAR(20) NOT NULL AFTER color_text_btn;",
			"color_text_descrip": "VARCHAR(20) NOT NULL AFTER fondo_btn;"
		},
		"update": {
			"articles_options": "SET comments_blog = '0', color_text_btn = '#ffffff', fondo_btn = '#2e6da4', color_text_descrip = '#555555' WHERE articles_options.idx = 1;"
		}
	},
	"table2":{
		"name": "articles_widget_list",
		"add": {
			"layout": "VARCHAR(200) NOT NULL AFTER show_descrip;"
		},
		"update": {
			"articles_widget_list": "SET box_name = 'LATEST BLOG POST', background_color = '#f30000', text_color = '#ffffff', num_col_page = '1', num_reg_limit = '4', sort = '1', show_date = '1', show_cover = '1', show_descrip = '1', layout = 'articles-list.php' WHERE articles_widget_list.idx = 1;"
		}
	}
}