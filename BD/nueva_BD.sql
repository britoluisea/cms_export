-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2019 a las 08:02:04
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `idx_usuario` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `imagen_show` int(1) NOT NULL,
  `detail` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `detail_home` mediumtext NOT NULL,
  `descrip` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descrip_share` mediumtext NOT NULL,
  `fecreg` datetime NOT NULL,
  `conteo` int(11) NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_category`
--

CREATE TABLE `articles_category` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `keywords` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `description` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `item` int(11) NOT NULL,
  `fecreg` datetime NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `no_eli` int(1) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_category`
--

INSERT INTO `articles_category` (`idx`, `nombre`, `title`, `keywords`, `description`, `item`, `fecreg`, `slug`, `no_eli`, `activo`) VALUES
(1, 'Uncategorized', '', '', '', 1, '2014-07-09 12:44:46', 'uncategorized', 1, 1),
(2, 'test', '', '', '', 2, '2019-06-20 00:40:03', 'test', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_detail`
--

CREATE TABLE `articles_detail` (
  `idx` int(11) NOT NULL,
  `idx_articles` int(11) NOT NULL,
  `idx_articles_tag` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_detail_category`
--

CREATE TABLE `articles_detail_category` (
  `idx` int(11) NOT NULL,
  `idx_articles` int(11) NOT NULL,
  `idx_articles_category` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_options`
--

CREATE TABLE `articles_options` (
  `idx` int(11) NOT NULL,
  `width` varchar(35) NOT NULL,
  `height` varchar(35) NOT NULL,
  `quality` int(11) NOT NULL,
  `relativo` int(11) NOT NULL,
  `num_reg_page` int(11) NOT NULL,
  `num_reg_page_cms` int(11) NOT NULL,
  `face_share` int(1) NOT NULL,
  `twitter_share` int(1) NOT NULL,
  `face_like` int(11) NOT NULL,
  `face_comment` int(11) NOT NULL,
  `twitter_like` int(11) NOT NULL,
  `comments_blog` int(1) NOT NULL,
  `color_text_btn` varchar(20) NOT NULL,
  `fondo_btn` varchar(20) NOT NULL,
  `color_text_descrip` varchar(20) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_options`
--

INSERT INTO `articles_options` (`idx`, `width`, `height`, `quality`, `relativo`, `num_reg_page`, `num_reg_page_cms`, `face_share`, `twitter_share`, `face_like`, `face_comment`, `twitter_like`, `comments_blog`, `color_text_btn`, `fondo_btn`, `color_text_descrip`, `activo`) VALUES
(1, '800', '450', 50, 0, 30, 100, 1, 1, 1, 1, 1, 0, '#ffffff', '#2e6da4', '#555555', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_options_home`
--

CREATE TABLE `articles_options_home` (
  `idx` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `type_0_num_reg_limit` int(11) NOT NULL,
  `type_0_background_color` varchar(10) NOT NULL,
  `type_0_text_color` varchar(10) NOT NULL,
  `type_0_face_like` int(1) NOT NULL,
  `type_0_twitter_like` int(1) NOT NULL,
  `type_0_google_like` int(1) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_options_home`
--

INSERT INTO `articles_options_home` (`idx`, `type`, `type_0_num_reg_limit`, `type_0_background_color`, `type_0_text_color`, `type_0_face_like`, `type_0_twitter_like`, `type_0_google_like`, `activo`) VALUES
(1, 0, 4, '#2c3086', '#ffffff', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_tag`
--

CREATE TABLE `articles_tag` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descrip` mediumtext NOT NULL,
  `item` int(11) NOT NULL,
  `fecreg` datetime NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_tag`
--

INSERT INTO `articles_tag` (`idx`, `nombre`, `descrip`, `item`, `fecreg`, `slug`, `activo`) VALUES
(1, 'examples', '', 3, '2017-11-03 10:08:20', 'examples', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_widget`
--

CREATE TABLE `articles_widget` (
  `idx` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `post_number` int(11) NOT NULL,
  `show_date` int(11) NOT NULL,
  `dis_drop` int(1) NOT NULL,
  `show_count` int(1) NOT NULL,
  `descrip` mediumtext NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_widget_list`
--

CREATE TABLE `articles_widget_list` (
  `idx` int(11) NOT NULL,
  `box_name` varchar(200) NOT NULL,
  `background_color` varchar(10) NOT NULL,
  `text_color` varchar(10) NOT NULL,
  `num_col_page` int(11) NOT NULL,
  `num_reg_limit` int(11) NOT NULL,
  `sort` int(1) NOT NULL,
  `show_date` int(1) NOT NULL,
  `show_cover` int(1) NOT NULL,
  `show_descrip` int(1) NOT NULL,
  `layout` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_widget_list`
--

INSERT INTO `articles_widget_list` (`idx`, `box_name`, `background_color`, `text_color`, `num_col_page`, `num_reg_limit`, `sort`, `show_date`, `show_cover`, `show_descrip`, `layout`, `activo`) VALUES
(1, 'LATEST BLOG POST', '#f30000', '#ffffff', 1, 4, 1, 1, 1, 1, 'articles-list.php', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_widget_list_category`
--

CREATE TABLE `articles_widget_list_category` (
  `news_widget_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_widget_list_category`
--

INSERT INTO `articles_widget_list_category` (`news_widget_id`, `category_id`, `item`, `activo`) VALUES
(1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_widget_orden`
--

CREATE TABLE `articles_widget_orden` (
  `idx` int(11) NOT NULL,
  `idx_widget` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_widget_tipo`
--

CREATE TABLE `articles_widget_tipo` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articles_widget_tipo`
--

INSERT INTO `articles_widget_tipo` (`idx`, `nombre`, `activo`) VALUES
(1, 'Recent Post', 1),
(2, 'Category', 1),
(3, 'Plain Box', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_session`
--

CREATE TABLE `auth_session` (
  `id` int(11) NOT NULL,
  `idx_user` int(11) NOT NULL,
  `auth` int(11) NOT NULL,
  `storage` varchar(5000) CHARACTER SET utf8 NOT NULL,
  `fe` datetime NOT NULL,
  `fs` datetime NOT NULL,
  `ip` varchar(20) NOT NULL,
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_session`
--

INSERT INTO `auth_session` (`id`, `idx_user`, `auth`, `storage`, `fe`, `fs`, `ip`, `del`) VALUES
(89, 183, 1, '{\"var_sec_admin\":true,\"value_admin_idx\":\"183\",\"value_admin_idxamusuario\":\"1\",\"value_admin_idxusuariossp\":\"0\",\"value_admin_usuarios\":\"support@amwebpro.com\",\"value_admin_userlevel\":\"998\",\"value_admin_nomcar\":\"imgcms\",\"value_temp_usuarios\":\"\",\"value_temp_remember\":\"\",\"value_admin_nombres\":\"Soporte\",\"value_admin_apellidos\":\"\"}', '2019-10-13 21:36:24', '2019-10-14 09:36:24', '::1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backup_tabla`
--

CREATE TABLE `backup_tabla` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannershome`
--

CREATE TABLE `bannershome` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre_align` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mostrar_nombre` int(11) NOT NULL,
  `descrip` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `mostrar_descrip` int(11) NOT NULL,
  `imagen` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `page` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bannershome`
--

INSERT INTO `bannershome` (`idx`, `item`, `nombre`, `nombre_align`, `mostrar_nombre`, `descrip`, `mostrar_descrip`, `imagen`, `slug`, `page`, `activo`) VALUES
(44, 44, 'Increasing sales of your Local Business!', 'center', 1, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>', 1, 'img_557_692_874_601.jpg', 'increasing-sales-of-your-local-business', 0, 1),
(45, 45, 'Ten Questions With Chris Ross', 'center', 1, '<p><br></p>', 1, 'img_808_134_239_764.jpg', 'ten-questions-with-chris-ross', 0, 1),
(46, 46, 'Home > About Us', 'center', 1, '<p><br></p>', 1, 'img_535_376_731_121.jpg', 'home->-about-us', 5, 1),
(47, 47, 'Home > Services', 'center', 1, '<p style=\"text-align: center;\"><span style=\"font-size:22px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</span></p>', 1, 'img_877_698_923_711.jpg', 'home->-services', 6, 1),
(48, 48, 'Blog', 'center', 1, '<p><br></p>', 1, 'img_714_928_753_795.jpg', 'blog', 64, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannershome_options`
--

CREATE TABLE `bannershome_options` (
  `idx` int(11) NOT NULL,
  `width` varchar(35) NOT NULL,
  `height` varchar(35) NOT NULL,
  `relativo` int(11) NOT NULL,
  `quality` int(75) NOT NULL,
  `weight` int(11) NOT NULL,
  `overlay` varchar(11) NOT NULL,
  `speed` int(11) NOT NULL,
  `direction` varchar(20) NOT NULL,
  `arrows` int(11) NOT NULL,
  `effect` varchar(20) NOT NULL,
  `ico_nav` int(11) NOT NULL,
  `template_slider` varchar(100) NOT NULL,
  `width_inner` int(11) NOT NULL,
  `height_inner` int(11) NOT NULL,
  `quality_inner` int(11) NOT NULL,
  `weight_inner` int(11) NOT NULL,
  `overlay_inner` varchar(11) NOT NULL,
  `template_inner` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bannershome_options`
--

INSERT INTO `bannershome_options` (`idx`, `width`, `height`, `relativo`, `quality`, `weight`, `overlay`, `speed`, `direction`, `arrows`, `effect`, `ico_nav`, `template_slider`, `width_inner`, `height_inner`, `quality_inner`, `weight_inner`, `overlay_inner`, `template_inner`, `activo`) VALUES
(1, '1360', '650', 0, 50, 20, '0.2', 8, 'horizontal', 0, 'slide', 1, 'default.php', 1500, 400, 30, 10, '0.4', 'default.php', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bannershome_video`
--

CREATE TABLE `bannershome_video` (
  `id` int(11) NOT NULL,
  `autoplay` int(11) NOT NULL,
  `muted` int(11) NOT NULL,
  `poster` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `overlay` varchar(10) NOT NULL,
  `table_ipad` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `descrip_video` mediumtext NOT NULL,
  `height_poster` int(11) NOT NULL,
  `width_poster` int(11) NOT NULL,
  `quality_poster` int(11) NOT NULL,
  `weight_poster` int(11) NOT NULL,
  `weight_video` int(11) NOT NULL,
  `template_video` varchar(100) NOT NULL,
  `activo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bannershome_video`
--

INSERT INTO `bannershome_video` (`id`, `autoplay`, `muted`, `poster`, `video`, `overlay`, `table_ipad`, `mobile`, `descrip_video`, `height_poster`, `width_poster`, `quality_poster`, `weight_poster`, `weight_video`, `template_video`, `activo`) VALUES
(1, 0, 0, 'img_859_848_921_785.jpg', 'video.mp4', '0.4', 1, 0, '<h2 style=\"text-align: center;\"><span style=\"font-size:48px;\">Affordable Metal </span><span style=\"font-size:48px;\">Roofing Always!</span></h2><p style=\"text-align: center;\"><span style=\"font-size:26px;\">Request a consultation and receive <span style=\"color:#FF0000;\"></span><span style=\"color:#ff3333;\"><br><ins><strong><big>$2,000</big></strong></ins></span><span style=\"color:#FF0000;\"> </span><span style=\"color:#FF0000;\"></span>OFF<br>on Residential new metal roof replacement</span></p><p style=\"text-align: center;\"><a data-cke-saved-href=\"#\" href=\"#\" class=\"btn btn-primary btn-lg\" style=\"background-color: #5cb85c; border-color: #4cae4c;\">Get Started <i class=\"fa fa-angle-double-right\">u200b</i></a></p>', 700, 1500, 50, 10, 35, 'default.php', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `idx` int(11) UNSIGNED NOT NULL,
  `article_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `comment_status_id` int(11) NOT NULL,
  `comment_status_id_salvar` int(11) NOT NULL,
  `descrip` mediumtext NOT NULL,
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count_like` int(11) NOT NULL,
  `count_unlike` int(11) NOT NULL,
  `fecreg` datetime NOT NULL,
  `ips` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments_like_log`
--

CREATE TABLE `comments_like_log` (
  `idx` int(11) UNSIGNED NOT NULL,
  `comment_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `tipo_me_gusta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments_status`
--

CREATE TABLE `comments_status` (
  `idx` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comments_status`
--

INSERT INTO `comments_status` (`idx`, `nombre`) VALUES
(1, 'Pending'),
(2, 'Approve'),
(4, 'Trash'),
(3, 'Spam');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detgaleriaslista`
--

CREATE TABLE `detgaleriaslista` (
  `idx` int(11) NOT NULL,
  `idgalerias` int(11) DEFAULT 0,
  `nombre` varchar(100) NOT NULL,
  `descripcion` mediumtext NOT NULL,
  `imagen` varchar(1000) NOT NULL DEFAULT 'blanco.jpg',
  `imagenmini` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `clave` varchar(50) NOT NULL DEFAULT '',
  `idfoto` int(11) NOT NULL DEFAULT 0,
  `nombrep` text DEFAULT NULL,
  `clavep` varchar(50) DEFAULT NULL,
  `idfotop` int(11) DEFAULT NULL,
  `ver` tinyint(1) NOT NULL DEFAULT 1,
  `item` int(11) NOT NULL DEFAULT 0,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detgaleriaslista`
--

INSERT INTO `detgaleriaslista` (`idx`, `idgalerias`, `nombre`, `descripcion`, `imagen`, `imagenmini`, `clave`, `idfoto`, `nombrep`, `clavep`, `idfotop`, `ver`, `item`, `activo`) VALUES
(341, 1, 'slidegg.jpg', ' ', 'slidegg.jpg', 'blanco.jpg', '', 0, NULL, NULL, NULL, 1, 341, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detgalerias_project_lista`
--

CREATE TABLE `detgalerias_project_lista` (
  `idx` int(11) NOT NULL,
  `idgalerias` int(11) DEFAULT 0,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `imagenmini` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `clave` varchar(50) NOT NULL DEFAULT '',
  `idfoto` int(11) NOT NULL DEFAULT 0,
  `nombrep` text DEFAULT NULL,
  `clavep` varchar(50) DEFAULT NULL,
  `idfotop` int(11) DEFAULT NULL,
  `ver` tinyint(1) NOT NULL DEFAULT 1,
  `item` int(11) NOT NULL DEFAULT 0,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fonts`
--

CREATE TABLE `fonts` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fonts`
--

INSERT INTO `fonts` (`idx`, `nombre`, `activo`) VALUES
(1, 'Arial', 1),
(2, 'Arial Black', 1),
(3, 'Comic Sans MS', 1),
(4, 'Courier New', 1),
(5, 'Georgia', 1),
(6, 'Lucida Console', 1),
(7, 'Lucida Sans Unicode', 1),
(8, 'MS Serif', 1),
(9, 'Palatino Linotype', 1),
(10, 'Tahoma', 1),
(11, 'Times New Roman', 1),
(12, 'Trebuchet MS', 1),
(13, 'Verdana', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularios`
--

CREATE TABLE `formularios` (
  `idx` int(1) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email1` varchar(100) NOT NULL,
  `email2` varchar(100) NOT NULL,
  `email3` varchar(100) NOT NULL,
  `email4` varchar(100) NOT NULL,
  `email5` varchar(100) NOT NULL,
  `email6` varchar(100) NOT NULL,
  `email7` varchar(100) NOT NULL,
  `email8` varchar(100) NOT NULL,
  `email9` varchar(100) NOT NULL,
  `email10` varchar(100) NOT NULL,
  `autoresponder` int(1) NOT NULL,
  `autonombre` varchar(50) NOT NULL,
  `autoasunto` varchar(100) NOT NULL,
  `autodescrip` mediumtext NOT NULL,
  `key_website` varchar(200) DEFAULT NULL,
  `key_secret` varchar(200) DEFAULT NULL,
  `recaptcha` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formularios`
--

INSERT INTO `formularios` (`idx`, `nombre`, `email`, `email1`, `email2`, `email3`, `email4`, `email5`, `email6`, `email7`, `email8`, `email9`, `email10`, `autoresponder`, `autonombre`, `autoasunto`, `autodescrip`, `key_website`, `key_secret`, `recaptcha`, `activo`) VALUES
(1, 'Luis Brito', 'britoluisea@gmail.com', '', '', '', '', '', '', '', '', '', '', 0, 'Cmsb', 'Test Subject', '<p>&nbsp;Test Message</p>\r\n', '6LchDkgUAAAAADaj5FmZqluObDqfCc_cfDKC34ip', '6LchDkgUAAAAAKJBkYIxFAEUlyKOIj-p8R_4qzp5', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularios_record_list`
--

CREATE TABLE `formularios_record_list` (
  `idx` int(1) NOT NULL,
  `formularios_id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `phone` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fecreg` datetime NOT NULL,
  `descrip` mediumtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ips` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_group`
--

CREATE TABLE `galerias_group` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_group`
--

INSERT INTO `galerias_group` (`idx`, `nombre`, `activo`) VALUES
(1, 'PRINCIPAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_lista`
--

CREATE TABLE `galerias_lista` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `imagen` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `nomcar` varchar(35) NOT NULL,
  `usamarketing` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `keywords` mediumtext DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `slug` varchar(250) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_lista`
--

INSERT INTO `galerias_lista` (`idx`, `item`, `nombre`, `descripcion`, `imagen`, `nomcar`, `usamarketing`, `title`, `keywords`, `description`, `slug`, `activo`) VALUES
(1, 3, 'Gallery 1', 'prueba de album', 'ico_818_421_376_991.jpg', '754599831822', 0, '', '', '', 'Gallery-1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_lista_options`
--

CREATE TABLE `galerias_lista_options` (
  `idx` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `modelgallery` varchar(20) NOT NULL,
  `tipogallery` varchar(250) NOT NULL,
  `title` varchar(5000) NOT NULL,
  `numregweb` int(11) NOT NULL DEFAULT 0,
  `numcolweb` int(11) DEFAULT 0,
  `numregfacebookweb` int(11) NOT NULL DEFAULT 1,
  `modelgalleryfc` varchar(20) NOT NULL,
  `model1width` int(11) NOT NULL,
  `model1height` int(11) NOT NULL,
  `model2widthimage` int(11) NOT NULL,
  `model1width_fc` int(11) NOT NULL,
  `model1height_fc` int(11) NOT NULL,
  `model2widthimage_fc` int(11) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `facelike` int(11) NOT NULL,
  `facecomments` int(11) NOT NULL,
  `back_col` varchar(10) NOT NULL,
  `text_col` varchar(10) NOT NULL,
  `num_reg_widg` int(11) NOT NULL,
  `name_sh` int(11) NOT NULL DEFAULT 1,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_lista_options`
--

INSERT INTO `galerias_lista_options` (`idx`, `width`, `height`, `modelgallery`, `tipogallery`, `title`, `numregweb`, `numcolweb`, `numregfacebookweb`, `modelgalleryfc`, `model1width`, `model1height`, `model2widthimage`, `model1width_fc`, `model1height_fc`, `model2widthimage_fc`, `imagen`, `facelike`, `facecomments`, `back_col`, `text_col`, `num_reg_widg`, `name_sh`, `activo`) VALUES
(1, 450, 350, 'model2', 'gallery1.php', '<h2 style=\"text-align: center;\">Our Gallery</h2>\r\n', 40, 0, 5, 'model1', 980, 450, 200, 820, 300, 100, '', 0, 0, '#003366', '#ffffff', 3, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_options`
--

CREATE TABLE `galerias_options` (
  `idx` int(11) NOT NULL,
  `width` varchar(35) NOT NULL,
  `widths` varchar(100) NOT NULL,
  `heights` varchar(100) NOT NULL,
  `widthl` varchar(100) NOT NULL,
  `cut` varchar(100) NOT NULL,
  `quality` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_options`
--

INSERT INTO `galerias_options` (`idx`, `width`, `widths`, `heights`, `widthl`, `cut`, `quality`, `activo`) VALUES
(1, '450', '100', '100', '1200', 'adaptiveResize', '50', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_project_category`
--

CREATE TABLE `galerias_project_category` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_project_group`
--

CREATE TABLE `galerias_project_group` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_project_group`
--

INSERT INTO `galerias_project_group` (`idx`, `nombre`, `activo`) VALUES
(1, 'PRINCIPAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_project_lista`
--

CREATE TABLE `galerias_project_lista` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `idxgrupo` int(11) NOT NULL,
  `idclientes` int(11) NOT NULL DEFAULT 0,
  `idcategorias` int(11) DEFAULT 0,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text NOT NULL,
  `fulldescripcion` mediumtext NOT NULL,
  `nomxml` varchar(35) NOT NULL,
  `imagen` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `imagenmini` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `descripaudio` text NOT NULL,
  `audio` varchar(35) NOT NULL,
  `activaraudio` int(1) NOT NULL,
  `activarpanzoom` varchar(5) NOT NULL DEFAULT 'On',
  `ordenes` char(1) NOT NULL DEFAULT 'A',
  `clave` varchar(50) NOT NULL DEFAULT '',
  `nomcar` varchar(35) NOT NULL,
  `idfoto` int(11) NOT NULL DEFAULT 0,
  `nombrep` text DEFAULT NULL,
  `clavep` varchar(50) DEFAULT NULL,
  `idfotop` int(11) DEFAULT NULL,
  `ver` tinyint(1) NOT NULL DEFAULT 1,
  `location` varchar(250) NOT NULL,
  `fecha` varchar(250) NOT NULL,
  `projectstatus` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 0,
  `estado` int(1) NOT NULL DEFAULT 1,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_project_lista_fc_options`
--

CREATE TABLE `galerias_project_lista_fc_options` (
  `idx` int(11) NOT NULL,
  `modelgallery` varchar(20) NOT NULL,
  `tipogallery` varchar(25) NOT NULL,
  `numregweb` int(11) NOT NULL DEFAULT 0,
  `numcolweb` int(11) DEFAULT 0,
  `numregfacebookweb` int(11) NOT NULL DEFAULT 1,
  `modelgalleryfc` varchar(20) NOT NULL,
  `model1width` int(11) NOT NULL,
  `model1height` int(11) NOT NULL,
  `model2widthimage` int(11) NOT NULL,
  `model1width_fc` int(11) NOT NULL,
  `model1height_fc` int(11) NOT NULL,
  `model2widthimage_fc` int(11) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `facelike` int(11) NOT NULL,
  `facecomments` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_project_lista_fc_options`
--

INSERT INTO `galerias_project_lista_fc_options` (`idx`, `modelgallery`, `tipogallery`, `numregweb`, `numcolweb`, `numregfacebookweb`, `modelgalleryfc`, `model1width`, `model1height`, `model2widthimage`, `model1width_fc`, `model1height_fc`, `model2widthimage_fc`, `imagen`, `facelike`, `facecomments`, `activo`) VALUES
(1, 'model2', 'thumbnail', 20, 4, 5, 'model2', 800, 500, 100, 820, 300, 120, 'img_263_227_811_871.jpg', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galerias_project_lista_options`
--

CREATE TABLE `galerias_project_lista_options` (
  `idx` int(11) NOT NULL,
  `modelgallery` varchar(20) NOT NULL,
  `tipogallery` varchar(25) NOT NULL,
  `numregweb` int(11) NOT NULL DEFAULT 0,
  `numcolweb` int(11) DEFAULT 0,
  `numregfacebookweb` int(11) NOT NULL DEFAULT 1,
  `modelgalleryfc` varchar(20) NOT NULL,
  `model1width` int(11) NOT NULL,
  `model1height` int(11) NOT NULL,
  `model2widthimage` int(11) NOT NULL,
  `model1width_fc` int(11) NOT NULL,
  `model1height_fc` int(11) NOT NULL,
  `model2widthimage_fc` int(11) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `facelike` int(11) NOT NULL,
  `facecomments` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galerias_project_lista_options`
--

INSERT INTO `galerias_project_lista_options` (`idx`, `modelgallery`, `tipogallery`, `numregweb`, `numcolweb`, `numregfacebookweb`, `modelgalleryfc`, `model1width`, `model1height`, `model2widthimage`, `model1width_fc`, `model1height_fc`, `model2widthimage_fc`, `imagen`, `facelike`, `facecomments`, `activo`) VALUES
(1, 'model2', 'thumbnail', 20, 4, 5, 'model2', 800, 500, 100, 820, 300, 120, 'img_263_227_811_871.jpg', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gift`
--

CREATE TABLE `gift` (
  `idx` int(11) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `provider` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `bought` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gift_n`
--

CREATE TABLE `gift_n` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gift_n`
--

INSERT INTO `gift_n` (`idx`, `name`, `status`) VALUES
(1, 'Received, Thanks!!', 1),
(2, 'Still Need: 1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gift_w`
--

CREATE TABLE `gift_w` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gift_w`
--

INSERT INTO `gift_w` (`idx`, `name`, `code`, `status`) VALUES
(1, '100', '<?php  include(\"gift-r.php\"); ?>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgcut`
--

CREATE TABLE `imgcut` (
  `idx` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `height` varchar(100) DEFAULT NULL,
  `width` varchar(100) DEFAULT NULL,
  `tsize` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imgcut`
--

INSERT INTO `imgcut` (`idx`, `name`, `height`, `width`, `tsize`, `item`, `status`) VALUES
(1, '1', '100', '100', 'small', '1', 1),
(2, '2', '350', '450', ' thumbnail', '2', 1),
(3, '3', '900', '1200', ' large', '3', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgcuts`
--

CREATE TABLE `imgcuts` (
  `idx` int(11) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `status` int(11) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imgcuts`
--

INSERT INTO `imgcuts` (`idx`, `name`, `status`, `item`) VALUES
(2, 'adaptiveResize\n', 1, 2),
(3, 'resize\n', 1, 3),
(6, 'cropFromCenter\n', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgext`
--

CREATE TABLE `imgext` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imgext`
--

INSERT INTO `imgext` (`idx`, `name`, `status`) VALUES
(37, 'dd', 1),
(3, 'jpg', 1),
(4, 'gif', 1),
(5, 'psd', 1),
(6, 'swf', 1),
(7, 'tiff', 1),
(8, 'jb2', 1),
(9, 'xbm', 1),
(10, 'jpx', 1),
(11, 'wbmp', 1),
(12, 'bmp', 1),
(13, 'iff', 1),
(14, 'jpc', 1),
(15, 'swc', 1),
(16, 'jp2', 1),
(28, 'png', 1),
(33, 'jpeg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgquality`
--

CREATE TABLE `imgquality` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imgquality`
--

INSERT INTO `imgquality` (`idx`, `name`, `status`) VALUES
(1, '50', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgsize`
--

CREATE TABLE `imgsize` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imgsize`
--

INSERT INTO `imgsize` (`idx`, `name`, `status`) VALUES
(4, '15000000', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgtsize`
--

CREATE TABLE `imgtsize` (
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imgtsize`
--

INSERT INTO `imgtsize` (`idx`, `name`, `status`) VALUES
(1, 'thumbnail', 1),
(2, 'small', 1),
(3, 'large', 1),
(4, 'big', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_cover_page`
--

CREATE TABLE `img_cover_page` (
  `id` int(11) NOT NULL,
  `id_page` int(11) NOT NULL,
  `img` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `img_cover_page`
--

INSERT INTO `img_cover_page` (`id`, `id_page`, `img`) VALUES
(3, 1, 'imgcms/img_cover_page/img_cover_page_1_9lLEQXuiL0Mo.jpg'),
(4, 2, 'imgcms/img_cover_page/img_cover_page_2_boxcn8sA6NDc.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informativo`
--

CREATE TABLE `informativo` (
  `id` int(10) NOT NULL,
  `idx` int(11) NOT NULL,
  `theme_idx` int(10) NOT NULL DEFAULT 1,
  `tipo` varchar(4) NOT NULL DEFAULT 'box',
  `auto` int(11) NOT NULL DEFAULT 0,
  `titulo` varchar(50) NOT NULL DEFAULT 'sin-titulo',
  `descri` mediumtext NOT NULL,
  `usamarketing` int(1) NOT NULL,
  `idx_router` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `keywords` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `fecha` datetime NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informativo`
--

INSERT INTO `informativo` (`id`, `idx`, `theme_idx`, `tipo`, `auto`, `titulo`, `descri`, `usamarketing`, `idx_router`, `title`, `keywords`, `description`, `fecha`, `orden`, `activo`) VALUES
(1, 1, 1, 'page', 1, 'Home', '<h1>Home default</h1>\r\n\r\n<p>Under construction</p>\r\n', 5, 1, 'Home', 'Home', 'Home', '0000-00-00 00:00:00', 0, 1),
(7, 2, 1, 'page', 0, 'About', '<h2>About</h2><p>Under construction</p>', 5, 2, 'About', '', '', '2019-10-14 01:39:12', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informativoprotected`
--

CREATE TABLE `informativoprotected` (
  `id` int(10) NOT NULL,
  `tipo` varchar(4) NOT NULL DEFAULT 'espa',
  `titulo` varchar(50) NOT NULL DEFAULT '',
  `descri` mediumtext NOT NULL,
  `enlace` varchar(100) NOT NULL DEFAULT '',
  `fecha` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `orden` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informativoprotected`
--

INSERT INTO `informativoprotected` (`id`, `tipo`, `titulo`, `descri`, `enlace`, `fecha`, `orden`, `estado`, `item`, `activo`) VALUES
(4, 'espa', 'Test P. Page', '<p>&nbsp;Under</p>', '', '0000-00-00 00:00:00', 0, 1, 4, 1),
(5, 'espa', 'Test Protected Page II', '<p>&nbsp;Under Construction</p>', '', '0000-00-00 00:00:00', 0, 1, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE `login_attempts` (
  `ip` varchar(20) DEFAULT NULL,
  `attempts` int(11) DEFAULT 0,
  `lastlogin` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login_attempts`
--

INSERT INTO `login_attempts` (`ip`, `attempts`, `lastlogin`) VALUES
('190.202.161.139', 1, '2016-08-05 09:47:51'),
('190.76.250.72', 2, '2016-12-20 09:37:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(100) NOT NULL,
  `id_content` int(11) NOT NULL,
  `id_router` int(11) NOT NULL,
  `idx_menu` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `theme_idx` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `name`, `url`, `id_content`, `id_router`, `idx_menu`, `nivel`, `item`, `theme_idx`) VALUES
(3, 'Home', 'home', 1, 1, 1, 0, 1, 1),
(5, 'About Us', 'about-us', 2, 2, 1, 0, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_name`
--

CREATE TABLE `menu_name` (
  `id` int(11) NOT NULL,
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `theme_idx` int(11) NOT NULL DEFAULT 1,
  `type` int(11) NOT NULL,
  `json` varchar(50000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_name`
--

INSERT INTO `menu_name` (`id`, `idx`, `name`, `theme_idx`, `type`, `json`) VALUES
(1, 1, 'Main (Principal)', 1, 1, '[[]]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_type`
--

CREATE TABLE `menu_type` (
  `idx` int(11) NOT NULL,
  `name_type` varchar(100) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_type`
--

INSERT INTO `menu_type` (`idx`, `name_type`, `active`) VALUES
(1, 'Main Menu', 0),
(2, 'Lateral Menu', 0),
(3, 'Foot Menu', 0),
(4, 'Custom Menu', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meta_page`
--

CREATE TABLE `meta_page` (
  `id` int(11) NOT NULL,
  `id_page` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `meta_page`
--

INSERT INTO `meta_page` (`id`, `id_page`, `active`) VALUES
(1, 5, 0),
(2, 1, 1),
(3, 6, 0),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_builder`
--

CREATE TABLE `page_builder` (
  `idx` int(11) NOT NULL,
  `idx_grupo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `keywords` mediumtext NOT NULL,
  `descrip1` mediumtext NOT NULL,
  `descrip2` mediumtext NOT NULL,
  `ciudad` varchar(35) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `estado` varchar(35) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `page_builder`
--

INSERT INTO `page_builder` (`idx`, `idx_grupo`, `nombre`, `keywords`, `descrip1`, `descrip2`, `ciudad`, `slug`, `estado`, `activo`) VALUES
(11, 1, 'demo', 'demo prueba ', 'demo prueba', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'San Felipe', 'test-group-san-felipe-yaracuy', 'Yaracuy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_builder_seo_option`
--

CREATE TABLE `page_builder_seo_option` (
  `idx` int(11) NOT NULL,
  `showpagebuilder` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `page_builder_seo_option`
--

INSERT INTO `page_builder_seo_option` (`idx`, `showpagebuilder`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_content_builder`
--

CREATE TABLE `page_content_builder` (
  `idx` int(11) NOT NULL,
  `idx_grupo` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `keywords` mediumtext NOT NULL,
  `descrip1` mediumtext NOT NULL,
  `descrip2` mediumtext NOT NULL,
  `ciudad` varchar(300) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `estado` varchar(300) NOT NULL,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_content_group`
--

CREATE TABLE `page_content_group` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descrip` mediumtext NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `page_content_group`
--

INSERT INTO `page_content_group` (`idx`, `item`, `nombre`, `descrip`, `activo`) VALUES
(3, 3, 'Our main menu', 'Click on the links below to see the menu\r\n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_group`
--

CREATE TABLE `page_group` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `page_group`
--

INSERT INTO `page_group` (`idx`, `nombre`, `activo`) VALUES
(1, 'Test Group', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_category`
--

CREATE TABLE `project_category` (
  `idx` int(11) NOT NULL,
  `idx_group` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `nomcar` varchar(100) NOT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `usamarketing` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `descriptiona` varchar(1000) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `item` int(11) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_category`
--

INSERT INTO `project_category` (`idx`, `idx_group`, `nombre`, `imagen`, `nomcar`, `descripcion`, `usamarketing`, `title`, `keywords`, `descriptiona`, `slug`, `item`, `activo`) VALUES
(23, 1, 'MASONRY', 'ico_324_461_111_651.jpg', '299791392689', '', 0, '', '', '', 'masonry', 23, 1),
(21, 1, 'Finish Carpentry', 'ico_147_494_330_476.jpg', '499582479462', NULL, 0, NULL, NULL, NULL, 'finish-carpentry', 21, 1),
(22, 2, 'Exterior Painting', 'ico_548_875_268_776.jpg', '209861347853', '', 0, '', '', '', 'exterior-painting', 22, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_detalle`
--

CREATE TABLE `project_detalle` (
  `idx` int(11) NOT NULL,
  `idgalerias` int(11) DEFAULT 0,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(1000) NOT NULL DEFAULT 'blanco.jpg',
  `imagenmini` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `clave` varchar(50) NOT NULL DEFAULT '',
  `idfoto` int(11) NOT NULL DEFAULT 0,
  `nombrep` text DEFAULT NULL,
  `clavep` varchar(50) DEFAULT NULL,
  `idfotop` int(11) DEFAULT NULL,
  `ver` tinyint(1) NOT NULL DEFAULT 1,
  `item` int(11) NOT NULL DEFAULT 0,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_group`
--

CREATE TABLE `project_group` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_group`
--

INSERT INTO `project_group` (`idx`, `item`, `nombre`, `imagen`, `activo`) VALUES
(1, 1, 'commercial', '', 1),
(2, 3, 'residential', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_lista`
--

CREATE TABLE `project_lista` (
  `idx` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `idxgrupo` int(11) NOT NULL,
  `idcategorias` int(11) DEFAULT 0,
  `nombre` varchar(150) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `descriptiona` varchar(1000) DEFAULT NULL,
  `fulldescripcion` mediumtext DEFAULT NULL,
  `imagen` varchar(35) NOT NULL DEFAULT 'blanco.jpg',
  `nomcar` varchar(35) NOT NULL,
  `location` varchar(250) DEFAULT NULL,
  `fecha` varchar(250) DEFAULT NULL,
  `projectstatus` int(11) NOT NULL,
  `usamarketing` int(11) NOT NULL,
  `title` varchar(300) DEFAULT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `slug` varchar(300) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_lista`
--

INSERT INTO `project_lista` (`idx`, `item`, `idxgrupo`, `idcategorias`, `nombre`, `descripcion`, `descriptiona`, `fulldescripcion`, `imagen`, `nomcar`, `location`, `fecha`, `projectstatus`, `usamarketing`, `title`, `keywords`, `slug`, `activo`) VALUES
(12, 12, 2, 21, 'd', '', '', '', 'ico_984_955_817_746.jpg', '324207312336', 'd', 'd', 1, 0, '', '', 'd', 1),
(13, 13, 2, 22, 'New Cabinet project', '', '', '', 'ico_208_351_600_823.jpg', '318676285100', '', '', 1, 0, '', '', 'new-cabinet-project', 1),
(14, 14, 1, 23, 'PATIO REMODELING', '', '', '', 'ico_818_297_153_922.jpg', '255787719905', 'DORCHESTER', 'November 2017', 1, 0, '', '', 'patio-remodeling', 1),
(16, 16, 2, 23, 'demo', 'd', '', '', '0', '678359328125', 'd', 'f', 1, 0, '', '', 'demo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_lista_options`
--

CREATE TABLE `project_lista_options` (
  `idx` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `modelgallery` varchar(20) NOT NULL,
  `tipoprojects` varchar(250) NOT NULL,
  `title` varchar(5000) NOT NULL,
  `numregweb` int(11) NOT NULL DEFAULT 0,
  `numcolweb` int(11) DEFAULT 0,
  `numregfacebookweb` int(11) NOT NULL DEFAULT 1,
  `modelgalleryfc` varchar(20) NOT NULL,
  `model1width` int(11) NOT NULL,
  `model1height` int(11) NOT NULL,
  `model2widthimage` int(11) NOT NULL,
  `model1width_fc` int(11) NOT NULL,
  `model1height_fc` int(11) NOT NULL,
  `model2widthimage_fc` int(11) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `facelike` int(11) NOT NULL,
  `facecomments` int(11) NOT NULL,
  `back_col` varchar(10) NOT NULL,
  `text_col` varchar(10) NOT NULL,
  `num_reg_widg` int(11) NOT NULL,
  `name_sh` int(11) NOT NULL DEFAULT 1,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_lista_options`
--

INSERT INTO `project_lista_options` (`idx`, `width`, `height`, `modelgallery`, `tipoprojects`, `title`, `numregweb`, `numcolweb`, `numregfacebookweb`, `modelgalleryfc`, `model1width`, `model1height`, `model2widthimage`, `model1width_fc`, `model1height_fc`, `model2widthimage_fc`, `imagen`, `facelike`, `facecomments`, `back_col`, `text_col`, `num_reg_widg`, `name_sh`, `activo`) VALUES
(1, 450, 350, 'model2', 'projects-home.php', '<h2 style=\"text-align: center;\">Our Projects</h2>\r\n', 50, 0, 5, 'model1', 980, 450, 280, 820, 300, 100, '', 0, 0, '', '', 3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_options`
--

CREATE TABLE `project_options` (
  `idx` int(11) NOT NULL,
  `width` varchar(35) NOT NULL,
  `widths` varchar(100) NOT NULL,
  `heights` varchar(100) NOT NULL,
  `widthl` varchar(100) NOT NULL,
  `cut` varchar(100) NOT NULL,
  `quality` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_options`
--

INSERT INTO `project_options` (`idx`, `width`, `widths`, `heights`, `widthl`, `cut`, `quality`, `activo`) VALUES
(1, '450', '100', '100', '1200', 'adaptiveResize', '60', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_status`
--

CREATE TABLE `project_status` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `project_status`
--

INSERT INTO `project_status` (`idx`, `nombre`, `activo`) VALUES
(1, 'Luis', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews`
--

CREATE TABLE `rating_reviews` (
  `idx` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_status_id` int(11) NOT NULL,
  `comment_status_id_salvar` int(11) NOT NULL,
  `idx_gallery` int(11) DEFAULT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `project` varchar(250) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `genero` int(1) NOT NULL,
  `descrip` mediumtext NOT NULL,
  `stars` decimal(18,2) NOT NULL,
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count_like` int(11) NOT NULL,
  `count_unlike` int(11) NOT NULL,
  `fecreg` datetime NOT NULL,
  `avatar` int(1) NOT NULL,
  `ips` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rating_reviews`
--

INSERT INTO `rating_reviews` (`idx`, `user_id`, `comment_status_id`, `comment_status_id_salvar`, `idx_gallery`, `nombres`, `apellidos`, `username`, `project`, `city`, `state`, `telephone`, `genero`, `descrip`, `stars`, `comment_parent`, `count_like`, `count_unlike`, `fecreg`, `avatar`, `ips`) VALUES
(1, 0, 4, 2, 0, 'Jon', 'Gutierrez', 'support@amwebpro.com', 'Awesome service!!!', 'Milford', 'MA', '', 1, 'The men were quick to answer emails/phone calls, and they were prompt with their work. They arrived and completed the work quickly and efficiently. Overall, great experience!\r\nThe men were quick to answer emails/phone calls, and they were prompt with their work. They arrived and completed the work quickly and efficiently. Overall, great experience!\r\nThe men were quick to answer emails/phone calls, and they were prompt with their work. They arrived and completed the work quickly and efficiently. Overall, great experience!', '5.00', 0, 1, 1, '2016-01-01 00:00:00', 1, '127.0.0.1'),
(2, 0, 2, 2, NULL, 'Omar', 'Calderon', 'omar@amwebpro.com', 'Great service by LCT Roofing very happy!', 'Lima', 'Li', '', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '5.00', 0, 1, 1, '2016-01-01 00:00:00', 7, '179.7.67.167'),
(5, 0, 2, 2, NULL, 'Omar', 'Calderon', 'omar@amwebpro.com', 'Great service by LCT Roofing very happy!', 'Lima', 'Li', '', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '5.00', 0, 1, 1, '2016-01-01 00:00:00', 7, '179.7.67.167'),
(26, 0, 1, 1, NULL, 'Luis', 'Brito', 'britoluisea@gmail.com', 'demo', 'San Felipe', 'Yaracuy', '', 1, 'demo demo', '3.50', 0, 0, 0, '2019-10-14 03:01:42', 1, '::1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_avatar`
--

CREATE TABLE `rating_reviews_avatar` (
  `idx` int(11) UNSIGNED NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rating_reviews_avatar`
--

INSERT INTO `rating_reviews_avatar` (`idx`, `imagen`, `activo`) VALUES
(1, 'avatar.jpg', 1),
(2, 'avatar-1.jpg', 1),
(3, 'avatar-2.jpg', 1),
(4, 'avatar-3.jpg', 1),
(5, 'avatar-4.jpg', 1),
(6, 'avatar-5.jpg', 1),
(7, 'avatar-6.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_options`
--

CREATE TABLE `rating_reviews_options` (
  `idx` int(11) NOT NULL,
  `header_title` varchar(250) NOT NULL,
  `header_business_name` varchar(250) NOT NULL,
  `assign_page` int(11) NOT NULL,
  `write_review` int(11) NOT NULL,
  `header_review_message` mediumtext NOT NULL,
  `num_reg_page` int(11) NOT NULL,
  `counter_show` int(1) NOT NULL,
  `field_project` varchar(200) NOT NULL,
  `field_project_show` int(1) NOT NULL,
  `descrip_form` mediumtext NOT NULL,
  `show_date` int(1) NOT NULL,
  `email` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating_reviews_options`
--

INSERT INTO `rating_reviews_options` (`idx`, `header_title`, `header_business_name`, `assign_page`, `write_review`, `header_review_message`, `num_reg_page`, `counter_show`, `field_project`, `field_project_show`, `descrip_form`, `show_date`, `email`, `activo`) VALUES
(1, 'Ratings & Reviews ', '', 2, 17, '', 20, 1, '', 0, '<h1 style=\"margin-top:0px;\">Write a Review</h1>\r\n', 1, 'support@incabox.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_status`
--

CREATE TABLE `rating_reviews_status` (
  `idx` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rating_reviews_status`
--

INSERT INTO `rating_reviews_status` (`idx`, `nombre`) VALUES
(1, 'Pending'),
(2, 'Approve'),
(4, 'Trash'),
(3, 'Spam');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_template`
--

CREATE TABLE `rating_reviews_template` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `descrip_home` mediumtext NOT NULL,
  `descrip_sidebar` mediumtext NOT NULL,
  `descrip_footer` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating_reviews_template`
--

INSERT INTO `rating_reviews_template` (`idx`, `nombre`, `descrip_home`, `descrip_sidebar`, `descrip_footer`) VALUES
(1, '', '', '<h1>Side Bar</h1>\r\n\r\n<p>Under Construction</p>\r\n', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_widget`
--

CREATE TABLE `rating_reviews_widget` (
  `idx` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `protocolo` varchar(10) NOT NULL,
  `web` varchar(200) NOT NULL,
  `destino` varchar(10) NOT NULL,
  `business_name` varchar(250) NOT NULL,
  `num_reg_limit` int(11) NOT NULL,
  `show_hide` int(1) NOT NULL,
  `num_car` decimal(18,0) NOT NULL,
  `descrip` mediumtext NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating_reviews_widget`
--

INSERT INTO `rating_reviews_widget` (`idx`, `title`, `protocolo`, `web`, `destino`, `business_name`, `num_reg_limit`, `show_hide`, `num_car`, `descrip`, `activo`) VALUES
(1, 'Ratings & Reviews', 'http', '', '_blank', '', 4, 1, '200', '<p><strong>Real feedback from real homeowners.</strong></p>\r\n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_widget_c`
--

CREATE TABLE `rating_reviews_widget_c` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `num_col` int(11) NOT NULL,
  `num_reg` int(11) NOT NULL,
  `background_color` varchar(10) NOT NULL,
  `text_color` varchar(10) NOT NULL,
  `num_car` decimal(18,0) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating_reviews_widget_c`
--

INSERT INTO `rating_reviews_widget_c` (`idx`, `nombre`, `num_col`, `num_reg`, `background_color`, `text_color`, `num_car`, `activo`) VALUES
(1, 'What our clients say', 2, 10, '', '#ffffff', '200', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating_reviews_widget_scroll`
--

CREATE TABLE `rating_reviews_widget_scroll` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `pausa` int(11) NOT NULL,
  `velocidad` int(11) NOT NULL,
  `opcdireccion` varchar(25) NOT NULL,
  `aleatorio` varchar(10) NOT NULL,
  `control` varchar(10) NOT NULL,
  `ancho` int(11) NOT NULL,
  `alto` int(11) NOT NULL,
  `num_car` decimal(18,0) NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating_reviews_widget_scroll`
--

INSERT INTO `rating_reviews_widget_scroll` (`idx`, `nombre`, `pausa`, `velocidad`, `opcdireccion`, `aleatorio`, `control`, `ancho`, `alto`, `num_car`, `activo`) VALUES
(1, 'Scroll', 8, 2, 'vertical', 'false', 'false', 360, 200, '200', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redirect`
--

CREATE TABLE `redirect` (
  `idx` int(1) NOT NULL,
  `type` int(11) NOT NULL,
  `urlpath` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `router`
--

CREATE TABLE `router` (
  `id` int(11) NOT NULL,
  `idx` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `file` varchar(100) NOT NULL,
  `access` int(11) NOT NULL,
  `theme_idx` int(11) NOT NULL DEFAULT 1,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `router`
--

INSERT INTO `router` (`id`, `idx`, `url`, `file`, `access`, `theme_idx`, `active`) VALUES
(1, 1, 'home', 'index.php', 0, 1, 1),
(7, 2, 'about', 'default.php', 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templateweb`
--

CREATE TABLE `templateweb` (
  `idx` int(11) NOT NULL,
  `templogo` varchar(35) NOT NULL,
  `tempdescrip` mediumtext NOT NULL,
  `protocolo` varchar(25) NOT NULL,
  `web` varchar(150) NOT NULL,
  `destino` varchar(25) NOT NULL,
  `mostrarimagen` int(11) NOT NULL,
  `repetirimagen` varchar(35) NOT NULL,
  `alinear` varchar(35) NOT NULL,
  `imgfondo` varchar(35) NOT NULL,
  `colorfondo` varchar(7) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `templateweb`
--

INSERT INTO `templateweb` (`idx`, `templogo`, `tempdescrip`, `protocolo`, `web`, `destino`, `mostrarimagen`, `repetirimagen`, `alinear`, `imgfondo`, `colorfondo`, `activo`, `fecreg`, `fecmod`) VALUES
(1, 'img_161_819_675_232.png', '', 'http://', 'wwwww', '_blank', 1, 'repeat', 'left', 'img_733_109_460_351.jpg', '#000000', 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `idx` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `f_update` datetime NOT NULL,
  `act` int(11) NOT NULL,
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `themes`
--

INSERT INTO `themes` (`id`, `idx`, `name`, `active`, `f_update`, `act`, `del`) VALUES
(1, 1, 'default', 1, '2019-10-14 03:51:12', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubigeoestadousa`
--

CREATE TABLE `ubigeoestadousa` (
  `codigo` varchar(2) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `activo` int(11) NOT NULL,
  `noeliminar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ubigeoestadousa`
--

INSERT INTO `ubigeoestadousa` (`codigo`, `nombre`, `activo`, `noeliminar`) VALUES
('AK', 'Alaska', 1, 0),
('AL', 'Alabama', 1, 0),
('AR', 'Arkansas', 1, 0),
('AZ', 'Arizona', 1, 0),
('CA', 'California', 1, 0),
('CO', 'Colorado', 1, 0),
('CT', 'Connecticut', 1, 0),
('DC', 'Distrito de Columbia', 1, 0),
('DE', 'Delaware', 1, 0),
('FL', 'Florida', 1, 0),
('GA', 'Georgia', 1, 0),
('HI', 'Hawaii', 1, 0),
('IA', 'Iowa', 1, 0),
('ID', 'Idaho', 1, 0),
('IL', 'Illinois', 1, 0),
('IN', 'Indiana', 1, 0),
('KS', 'Kansas', 1, 0),
('KY', 'Kentucky', 1, 0),
('LA', 'Louisiana', 1, 0),
('MA', 'Massachusetts', 1, 0),
('MD', 'Maryland', 1, 0),
('ME', 'Maine', 1, 0),
('MI', 'Michigan', 1, 0),
('MN', 'Minnesota', 1, 0),
('MO', 'Missouri', 1, 0),
('MS', 'Mississippi', 1, 0),
('MT', 'Montana', 1, 0),
('NC', 'North Carolina', 1, 0),
('ND', 'North Dakota', 1, 0),
('NE', 'Nebraska', 1, 0),
('NH', 'New Hampshire', 1, 0),
('NJ', 'New Jersey', 1, 0),
('NM', 'New Mexico', 1, 0),
('NV', 'Nevada', 1, 0),
('NY', 'New York', 1, 0),
('OH', 'Ohio', 1, 0),
('OK', 'Oklahoma', 1, 0),
('OR', 'Oregon', 1, 0),
('PA', 'Pennsylvania', 1, 0),
('RI', 'Rhode Island', 1, 0),
('SC', 'South Carolina', 1, 0),
('SD', 'South Dakota', 1, 0),
('TN', 'Tennessee', 1, 0),
('TX', 'Texas', 1, 0),
('UT', 'Utah', 1, 0),
('VA', 'Virginia', 1, 0),
('VT', 'Vermont', 1, 0),
('WA', 'Washington', 1, 0),
('WI', 'Wisconsin', 1, 0),
('WV', 'West Virginia', 1, 0),
('WY', 'Wyoming', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariomenu_permiso`
--

CREATE TABLE `usuariomenu_permiso` (
  `idxamusuario` int(11) NOT NULL,
  `idxusuariosec` int(11) NOT NULL,
  `idxpermisocategory` int(11) NOT NULL,
  `fecreg` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuariomenu_permiso`
--

INSERT INTO `usuariomenu_permiso` (`idxamusuario`, `idxusuariosec`, `idxpermisocategory`, `fecreg`) VALUES
(183, 1, 17, '2019-06-19 11:23:17'),
(183, 1, 17026, '2019-06-19 11:23:17'),
(183, 1, 17015, '2019-06-19 11:23:17'),
(183, 1, 13, '2019-06-19 11:23:17'),
(183, 1, 13001, '2019-06-19 11:23:17'),
(183, 1, 10, '2019-06-19 11:23:17'),
(183, 1, 20012, '2019-06-19 11:23:17'),
(183, 1, 10003, '2019-06-19 11:23:17'),
(183, 1, 10006, '2019-06-19 11:23:17'),
(183, 1, 10005, '2019-06-19 11:23:17'),
(183, 1, 10004, '2019-06-19 11:23:17'),
(183, 1, 10001, '2019-06-19 11:23:17'),
(183, 1, 9, '2019-06-19 11:23:17'),
(183, 1, 9008, '2019-06-19 11:23:17'),
(183, 1, 9005, '2019-06-19 11:23:17'),
(183, 1, 9012, '2019-06-19 11:23:17'),
(183, 1, 8, '2019-06-19 11:23:17'),
(183, 1, 8002, '2019-06-19 11:23:17'),
(183, 1, 8001, '2019-06-19 11:23:17'),
(183, 1, 1, '2019-06-19 11:23:17'),
(183, 1, 1003, '2019-06-19 11:23:17'),
(183, 1, 1002, '2019-06-19 11:23:17'),
(183, 1, 1001, '2019-06-19 11:23:17'),
(183, 1, 19001, '2019-06-19 11:23:17'),
(183, 1, 19002, '2019-06-19 11:23:17'),
(183, 1, 19007, '2019-06-19 11:23:17'),
(183, 1, 19004, '2019-06-19 11:23:17'),
(183, 1, 19, '2019-06-19 11:23:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariomenu_permiso_category`
--

CREATE TABLE `usuariomenu_permiso_category` (
  `idx` int(11) NOT NULL,
  `idxnivel0` int(11) NOT NULL,
  `idxnivel1` int(11) NOT NULL,
  `idxnivel2` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descrip` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NOT NULL,
  `activousersec` int(1) NOT NULL,
  `activousersec_tipo2` int(1) NOT NULL,
  `url` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuariomenu_permiso_category`
--

INSERT INTO `usuariomenu_permiso_category` (`idx`, `idxnivel0`, `idxnivel1`, `idxnivel2`, `nombre`, `descrip`, `orden`, `activo`, `status`, `activousersec`, `activousersec_tipo2`, `url`) VALUES
(1, 1, 0, 1, 'Home', 'Es el Menu Home', 0, 1, 0, 1, 0, ''),
(2, 2, 0, 2, 'General', 'Es el menu General', 0, 1, 0, 1, 0, ''),
(2002, 2, 2, 2002, 'Page Builder SEO', '', 5, 1, 0, 1, 0, ''),
(2004, 2, 4, 2004, 'Preferences', '', 5, 1, 0, 1, 0, ''),
(13001, 13, 1, 13001, 'Preferences', '', 0, 1, 1, 1, 0, 'preferences-gen-logo.php,preferences-gen-wm-support-login-options.php,redirect.php,white-label.php,pref-links.php,preferences-gen-wm-website-status.php,preferences-gen-wm-account-info.php,preferences-gen-wm-service-account-info.php,preferences-gen-form.php,preferences-gen-autoresponder-n.php'),
(11001, 11, 1, 11001, 'CMS Users', '', 2, 1, 0, 1, 0, 'users.php,users-n.php,users-privileges.php'),
(10001, 10, 1, 10001, 'Universal Meta Tags', '', 1, 1, 1, 1, 0, 'universal-meta-tags.php'),
(8001, 8, 1, 8001, 'Pages', '', 0, 1, 1, 1, 0, 'web-content.php'),
(1003, 1, 3, 1003, 'Preferences', '', 3, 1, 1, 1, 0, ''),
(1002, 1, 2, 1002, 'Gallery', '', 2, 1, 1, 1, 0, ''),
(1001, 1, 1, 1001, 'Web Content', '', 1, 1, 1, 1, 0, ''),
(8, 8, 0, 8, 'Web Content', '', 0, 1, 0, 1, 0, ''),
(9, 9, 0, 9, 'Web Tools', '', 0, 1, 0, 1, 0, ''),
(10, 10, 0, 10, 'Marketing', '', 0, 1, 0, 1, 0, ''),
(11, 11, 0, 11, 'Users', '', 0, 1, 0, 1, 0, ''),
(12, 12, 0, 12, 'Links', '', 0, 1, 0, 1, 0, ''),
(13, 13, 0, 13, 'Setting', '', 0, 1, 0, 1, 0, ''),
(15, 15, 0, 15, 'Products', '', 0, 1, 0, 1, 0, ''),
(15001, 15, 1, 15001, 'Products', '', 0, 1, 0, 1, 0, ''),
(17, 17, 0, 17, 'Custom Tools', '', 0, 1, 0, 1, 0, ''),
(17001, 17, 1, 17001, 'Protected Pages', '', 0, 0, 0, 1, 0, ''),
(17002, 17, 2, 17002, 'User Protected Pages', '', 1, 0, 0, 1, 0, ''),
(18, 18, 0, 18, 'Facebook', '', 0, 1, 0, 1, 0, ''),
(18001, 18, 1, 18001, 'Facebook', '', 0, 1, 0, 1, 0, ''),
(2005, 2, 5, 2005, 'Page Builder Content', '', 5, 1, 0, 1, 0, ''),
(17003, 17, 3, 17003, 'PP Login', '', 2, 0, 0, 1, 0, ''),
(8002, 8, 2, 8002, 'Boxes', '', 2, 1, 1, 1, 0, 'web-content-boxes.php'),
(9005, 9, 5, 9005, 'Gallery List', '', 7, 1, 1, 1, 0, 'gallery-list.php,gallery-list-n.php,gallery-list-img.php,gallery-list-options.php,gallery-fc-code.php,gallery-fc-option.php,gallery-list-img-resize.php,gallery-widget.php'),
(17007, 17, 7, 17007, 'PB for Content', '', 2, 1, 0, 1, 0, 'page-builder-content.php,page-builder-content-n.php,page-builder-group-content.php'),
(9008, 9, 8, 9008, 'Forms / Autoresponder', '', 8, 1, 1, 1, 0, 'form-autoresponder.php,autoresponder-n.php,form-autoresponder-list.php,form-autoresponder-list-n.php,form.php'),
(17009, 17, 9, 17009, 'Catalog Nicolock', '', 13, 0, 0, 1, 0, ''),
(10003, 10, 3, 10003, 'Page Builder SEO', '', 5, 1, 1, 1, 0, 'page-builder.php,page-builder-n.php,page-builder-group.php,page-builder-option.php'),
(13002, 13, 2, 13002, 'Advanced Options', '', 2, 0, 0, 0, 0, 'white-label.php,pref-links.php,preferences-gen-wm-website-status.php,preferences-gen-wm-account-info.php,preferences-gen-wm-service-account-info.php,preferences-gen-form.php,preferences-gen-autoresponder-n.php'),
(17011, 17, 11, 17011, 'Form for Specials', '', 11, 0, 0, 1, 0, ''),
(17013, 17, 13, 17013, 'Playlist', '', 8, 0, 0, 1, 0, ''),
(17014, 17, 14, 17014, 'Material List', '', 5, 1, 0, 1, 0, 'material-list.php,material-list-n.php,material-category.php,material-pattern.php,material-option.php,material-fc-code.php,material-facebook-option.php,material-list-img.php'),
(17015, 17, 15, 17015, 'Projects', '', 1, 1, 1, 1, 0, 'projects-list.php,projects-list-n.php,projects-category-list.php,projects-list-options.php,projects-fc-code.php,projects-fc-options.php'),
(17016, 17, 16, 17016, 'Real Estate List', '', 6, 1, 0, 1, 0, 'real-estate-list.php,real-estate-list-n.php,real-estate-category.php,real-estate-category-n.php,real-estate-list-img.php,real-estate-city.php,real-estate-city-n.php,real-estate-agent-inf.php,real-estate-agent-inf-n.php,real-estate-side.php,real-estate-option.php,real-estate-status.php,real-estate-list-img-resize.php,real-estate-widget-slider.php,real-estate-state.php,real-estate-widget-thumbnail-list.php,real-estate-widget-thumbnail-list-n.php'),
(10004, 10, 4, 10004, 'XML Sitemaps', '', 2, 1, 1, 1, 0, 'xml-sitemaps.php'),
(10005, 10, 5, 10005, 'Robot File', '', 3, 1, 1, 1, 0, 'robot-files.php'),
(10006, 10, 6, 10006, 'Google Analytics', '', 4, 1, 1, 1, 0, 'google-analytics.php'),
(17019, 17, 19, 17019, 'Catalog Unilock', '', 12, 0, 0, 1, 0, ''),
(17020, 17, 20, 17020, 'Catalog Stone Veneer', '', 13, 0, 0, 1, 0, ''),
(17021, 17, 21, 17021, 'Projects - Category', '', 3, 1, 0, 1, 0, 'product-d-list.php,product-d-list-n.php,product-d-group.php,product-d-category.php,product-d-category-n.php,product-d-status.php,product-d-options.php,product-d-fc-code.php,product-d-fc-options.php,product-d-list-img.php'),
(17023, 17, 23, 17023, 'Ads Box', '', 23, 1, 0, 1, 0, 'ads-box.php,ads-box-group.php'),
(17024, 17, 24, 17024, 'Blog', '', 24, 0, 0, 1, 0, ''),
(17025, 17, 25, 17025, 'News', '', 25, 1, 0, 1, 0, 'news.php,news-n.php,news-category.php,news-category-n.php,news-tags.php,news-tags-n.php,news-options.php,news-widget-scroll.php,news-widget.php,news-widget-n.php,news-widget-list.php'),
(19, 19, 0, 19, 'Blog', '', 0, 1, 0, 1, 1, ''),
(19001, 19, 1, 19001, 'Post', '', 1, 1, 1, 1, 1, 'articles.php,articles-n.php,articles-category.php,articles-tags.php,articles-tags-n.php'),
(19002, 19, 2, 19002, 'Comments', '', 2, 1, 1, 1, 1, 'comments.php,comments-n.php'),
(19003, 19, 3, 19003, 'Widgets Sidebar', '', 3, 1, 0, 1, 1, 'article-widget.php,article-widget-recent-post.php,article-widget-recent-post-n.php,article-widget-category.php,article-widget-category-n.php,article-widget-plain-box.php,article-widget-plain-box-n.php,article-widget-search.php,article-widget-search-n.php'),
(19004, 19, 4, 19004, 'Blog Setting', '', 999, 1, 1, 1, 1, 'articles-post-list.php,articles-post.php'),
(19005, 19, 5, 19005, 'Widgets Content', '', 4, 1, 0, 1, 1, 'articles-box-home.php'),
(19006, 19, 6, 19006, 'Widgets Footer', '', 5, 0, 0, 1, 1, ''),
(11002, 11, 2, 11002, 'Subscriber List', '', 1, 1, 0, 1, 0, 'subscribers.php,subscribers-n.php'),
(17026, 17, 26, 17026, 'Rating & Reviews', '', 26, 1, 1, 1, 0, 'rating-reviews.php,rating-reviews-options.php,rating-reviews-home.php,rating-reviews-sidebar.php,rating-reviews-footer.php,rating-reviews-n.php,rating-reviews-widget.php,rating-reviews-widget-scroll.php,rating-reviews-options-form.php,rating-reviews-widget-c.php,rating-notification.php'),
(20, 20, 0, 20, 'Store', '', 0, 1, 0, 1, 0, ''),
(20001, 20, 1, 20001, 'Order List', '', 1, 1, 0, 1, 0, 'store-orders.php,store-orders-n.php'),
(20002, 20, 2, 20002, 'Catalog List', '', 2, 1, 0, 1, 0, 'store-catalog.php,store-catalog-n.php,store-catalog-photo.php'),
(20003, 20, 3, 20003, 'Group', '', 3, 0, 0, 1, 0, 'store-group.php'),
(20004, 20, 4, 20004, 'Category', '', 4, 1, 0, 1, 0, 'store-category.php'),
(20005, 20, 5, 20005, 'Manufacturers', '', 5, 1, 0, 1, 0, 'store-manufacturers.php,store-manufacturers-n.php'),
(20006, 20, 6, 20006, 'Color', '', 6, 1, 0, 1, 0, 'store-color.php'),
(20007, 20, 7, 20007, 'Size', '', 7, 1, 0, 1, 0, 'store-size.php'),
(20008, 20, 8, 20007, 'Shipping', '', 8, 0, 0, 1, 0, ''),
(20009, 20, 9, 20009, 'Order Notification', '', 9, 0, 0, 1, 0, 'store-order-notification.php,store-template-notification.php'),
(20010, 20, 10, 20010, 'Setup', '', 11, 1, 0, 1, 0, 'store-tax.php,store-shipping.php,store-shipping-n.php,store-setting.php,store-options.php,store-order-notification.php,store-template-notification.php,store-paypal.php'),
(9012, 9, 12, 9012, 'Banner 2', '', 5, 1, 1, 1, 0, 'banner-home-f.php,banner-home-f-n.php,banner-home-f-options.php,banner-home-f-code.php'),
(19007, 19, 7, 19007, 'Widgets List', '', 6, 1, 1, 1, 1, 'articles-widget-list.php'),
(20011, 20, 11, 20011, 'Widget', '', 10, 1, 0, 1, 0, 'store-widget.php,store-widget-n.php,store-widget-delete.php'),
(20012, 10, 3, 20012, 'SEO Url Menu', '', 5, 1, 1, 1, 0, 'seo-url.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `idxusuariossp` int(11) NOT NULL,
  `idx_user_category` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `claves` varchar(100) CHARACTER SET latin1 NOT NULL,
  `compania` varchar(60) DEFAULT NULL,
  `nombres` char(35) CHARACTER SET latin1 DEFAULT NULL,
  `apellidos` varchar(35) CHARACTER SET latin1 DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `direccion2` varchar(60) DEFAULT NULL,
  `ciudad` varchar(60) DEFAULT NULL,
  `id_estado` varchar(2) DEFAULT NULL,
  `estado` varchar(60) DEFAULT NULL,
  `zipcode` varchar(25) DEFAULT NULL,
  `telefono` varchar(35) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `celular` varchar(35) DEFAULT NULL,
  `alloweduser` int(11) NOT NULL,
  `allowedclient` int(11) NOT NULL,
  `fechacreado` date NOT NULL,
  `nomcar` varchar(25) NOT NULL DEFAULT 'imgcms',
  `logo` varchar(35) NOT NULL,
  `userlevel` int(11) NOT NULL DEFAULT 1,
  `observ` mediumtext NOT NULL,
  `horaope` int(11) NOT NULL DEFAULT 1,
  `h1a` int(11) NOT NULL,
  `h1b` int(11) NOT NULL,
  `cl1` int(11) NOT NULL,
  `h2a` int(11) NOT NULL,
  `h2b` int(11) NOT NULL,
  `cl2` int(11) NOT NULL,
  `h3a` int(11) NOT NULL,
  `h3b` int(11) NOT NULL,
  `cl3` int(11) NOT NULL,
  `h4a` int(11) NOT NULL,
  `h4b` int(11) NOT NULL,
  `cl4` int(11) NOT NULL,
  `h5a` int(11) NOT NULL,
  `h5b` int(11) NOT NULL,
  `cl5` int(11) NOT NULL,
  `h6a` int(11) NOT NULL,
  `h6b` int(11) NOT NULL,
  `cl6` int(11) NOT NULL,
  `h7a` int(11) NOT NULL,
  `h7b` int(11) NOT NULL,
  `cl7` int(11) NOT NULL,
  `imagen` varchar(35) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `genero` int(1) NOT NULL DEFAULT 1,
  `anio` int(11) NOT NULL,
  `verif_email` int(1) NOT NULL,
  `ips` varchar(25) NOT NULL,
  `avatar` int(1) NOT NULL,
  `sub_vip` int(1) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idx`, `idxamusuario`, `idxusuariossp`, `idx_user_category`, `username`, `claves`, `compania`, `nombres`, `apellidos`, `direccion`, `direccion2`, `ciudad`, `id_estado`, `estado`, `zipcode`, `telefono`, `fax`, `celular`, `alloweduser`, `allowedclient`, `fechacreado`, `nomcar`, `logo`, `userlevel`, `observ`, `horaope`, `h1a`, `h1b`, `cl1`, `h2a`, `h2b`, `cl2`, `h3a`, `h3b`, `cl3`, `h4a`, `h4b`, `cl4`, `h5a`, `h5b`, `cl5`, `h6a`, `h6b`, `cl6`, `h7a`, `h7b`, `cl7`, `imagen`, `slug`, `genero`, `anio`, `verif_email`, `ips`, `avatar`, `sub_vip`, `activo`, `fecreg`, `fecmod`) VALUES
(1, 0, 1, 3, 'alex@amwebpro.com', '550e1bafe077ff0b0b67f4e32f29d751', 'Test New', 'Test', 'New cms', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00', 'imgcms', '', 999, '', 1, 1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 'alex', 1, 0, 1, '127.0.0.1', 0, 0, 1, '0000-00-00', '2014-07-31'),
(181, 1, 0, 0, 'alex@incabox.com', 'b10c1360e52102725ad7b5b8317261c5', 'Marketea Inc', 'Alex', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00', 'imgcms', '', 998, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '', 'alex', 1, 0, 1, '127.0.0.1', 0, 0, 1, '2011-06-15', '0000-00-00'),
(182, 1, 0, 0, 'support@incabox.com', '74be16979710d4c4e7c6647856088456', 'Marketea Inc', 'Support', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, 0, 0, '0000-00-00', 'imgcms', '', 998, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '', 'support', 1, 0, 1, '127.0.0.1', 0, 0, 1, '0000-00-00', '0000-00-00'),
(183, 1, 0, 0, 'support@amwebpro.com', 'b10c1360e52102725ad7b5b8317261c5', 'Marketea Inc', 'Soporte', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00', 'imgcms', '', 998, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '', 'support2', 1, 0, 1, '127.0.0.1', 0, 0, 1, '0000-00-00', '0000-00-00'),
(184, 1, 0, 0, 'support@marketea.com', 'd7b92cecd9c7c39c912f9e9b866f17d7', 'Marketea Inc', 'Marketea', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00', 'imgcms', '', 998, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '', 'support2', 1, 0, 1, '127.0.0.1', 0, 0, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioscompany`
--

CREATE TABLE `usuarioscompany` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `idx_user_category` int(11) NOT NULL,
  `username` varchar(60) CHARACTER SET latin1 NOT NULL,
  `claves` varchar(35) CHARACTER SET latin1 NOT NULL,
  `compania` varchar(60) NOT NULL,
  `nombrecontacto` varchar(100) NOT NULL,
  `nombres` char(35) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(35) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `direccion2` varchar(60) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `estado` varchar(60) NOT NULL,
  `zipcode` varchar(25) NOT NULL,
  `telefono` varchar(35) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `celular` varchar(35) NOT NULL,
  `alloweduser` int(11) NOT NULL,
  `fechacreado` date NOT NULL,
  `logo` varchar(35) NOT NULL,
  `usarlogopropio` int(1) NOT NULL DEFAULT 1,
  `logopropio` varchar(35) NOT NULL,
  `fondoinvoice` varchar(35) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `definvoiceterm` varchar(5) NOT NULL,
  `defrepapayment` int(11) NOT NULL,
  `defsalesreceiptterm` varchar(5) NOT NULL,
  `numcontract` int(11) NOT NULL,
  `numinvoice` int(11) NOT NULL,
  `numestimate` int(11) NOT NULL,
  `numreceivepayment` int(11) NOT NULL,
  `numsalesreceipt` int(11) NOT NULL,
  `sendemailinvoice` mediumtext NOT NULL,
  `sendemailestimate` mediumtext NOT NULL,
  `sendemailcontract` mediumtext NOT NULL,
  `sendemailsalesreceipt` mediumtext NOT NULL,
  `activowebsite` int(1) NOT NULL,
  `descriwebsite` mediumtext CHARACTER SET latin1 NOT NULL,
  `accountinfoservice` mediumtext NOT NULL,
  `activeaccountinfoservice` int(1) NOT NULL,
  `loginattempts` int(11) NOT NULL,
  `logintimeunblock` int(11) NOT NULL,
  `gallerymostrarmobile` int(1) NOT NULL,
  `gallerynumfotos` int(11) NOT NULL,
  `colorheader` varchar(25) NOT NULL,
  `white_label_image` varchar(50) NOT NULL,
  `white_label_footer` mediumtext NOT NULL,
  `descrilinks` mediumtext NOT NULL,
  `describackupinformation` mediumtext NOT NULL,
  `sendercompany` varchar(100) NOT NULL,
  `senderemail` varchar(100) NOT NULL,
  `accountname` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarioscompany`
--

INSERT INTO `usuarioscompany` (`idx`, `idxamusuario`, `idx_user_category`, `username`, `claves`, `compania`, `nombrecontacto`, `nombres`, `apellidos`, `direccion`, `direccion2`, `ciudad`, `estado`, `zipcode`, `telefono`, `fax`, `celular`, `alloweduser`, `fechacreado`, `logo`, `usarlogopropio`, `logopropio`, `fondoinvoice`, `tax`, `definvoiceterm`, `defrepapayment`, `defsalesreceiptterm`, `numcontract`, `numinvoice`, `numestimate`, `numreceivepayment`, `numsalesreceipt`, `sendemailinvoice`, `sendemailestimate`, `sendemailcontract`, `sendemailsalesreceipt`, `activowebsite`, `descriwebsite`, `accountinfoservice`, `activeaccountinfoservice`, `loginattempts`, `logintimeunblock`, `gallerymostrarmobile`, `gallerynumfotos`, `colorheader`, `white_label_image`, `white_label_footer`, `descrilinks`, `describackupinformation`, `sendercompany`, `senderemail`, `accountname`, `activo`, `fecreg`, `fecmod`) VALUES
(1, 0, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '2011-02-18', '', 0, '', '', '6.25', '', 0, '', 0, 0, 0, 0, 0, '', '', '', '', 1, '<p style=\"text-align: center;\"><img alt=\"coming soon\" src=\"/imgcms/editor/images/icon-cms.png\" style=\"width: 58px; height: 42px;\" /></p>\r\n\r\n<h1 style=\"text-align: center;\">Under Construction</h1>\r\n\r\n<p style=\"text-align: center;\">Our website is currently under scheduled maintenance. Come back soon. Thank you for your patience.</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n', '', 1, 12, 10, 0, 3000, '#0F0F0F', 'ico_872_959_876.jpg', '<p style=\"text-align: center;\">By <a href=\"http://marketea.com\" target=\"_black\">Marketea</a> &copy; 2018 All right reserved.</p>\r\n', '', '', 'Marketea Inc', 'support@marketea.com', 'Cms Ingles', 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioscompanysp`
--

CREATE TABLE `usuarioscompanysp` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `idx_user_category` int(11) NOT NULL,
  `username` varchar(60) CHARACTER SET latin1 NOT NULL,
  `claves` varchar(35) CHARACTER SET latin1 NOT NULL,
  `compania` varchar(60) NOT NULL,
  `nombrecontacto` varchar(100) NOT NULL,
  `nombres` char(35) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(35) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `direccion2` varchar(60) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `estado` varchar(60) NOT NULL,
  `zipcode` varchar(25) NOT NULL,
  `telefono` varchar(35) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `celular` varchar(35) NOT NULL,
  `alloweduser` int(11) NOT NULL,
  `fechacreado` date NOT NULL,
  `logo` varchar(35) NOT NULL,
  `usarlogopropio` int(1) NOT NULL DEFAULT 1,
  `logopropio` varchar(35) NOT NULL,
  `fondoinvoice` varchar(35) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `definvoiceterm` varchar(5) NOT NULL,
  `defrepapayment` int(11) NOT NULL,
  `defsalesreceiptterm` varchar(5) NOT NULL,
  `numcontract` int(11) NOT NULL,
  `numinvoice` int(11) NOT NULL,
  `numestimate` int(11) NOT NULL,
  `numreceivepayment` int(11) NOT NULL,
  `numsalesreceipt` int(11) NOT NULL,
  `sendemailinvoice` mediumtext NOT NULL,
  `sendemailestimate` mediumtext NOT NULL,
  `sendemailcontract` mediumtext NOT NULL,
  `sendemailsalesreceipt` mediumtext NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosec_permiso`
--

CREATE TABLE `usuariosec_permiso` (
  `idxamusuario` int(11) NOT NULL,
  `idxusuariosec` int(11) NOT NULL,
  `idxpermisocategory` int(11) NOT NULL,
  `fecreg` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosinfo`
--

CREATE TABLE `usuariosinfo` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `idxusuariossp` int(11) NOT NULL,
  `idx_user_category` int(11) NOT NULL,
  `username` varchar(60) CHARACTER SET latin1 NOT NULL,
  `claves` varchar(35) CHARACTER SET latin1 NOT NULL,
  `compania` varchar(60) NOT NULL,
  `nombres` char(35) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(35) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `direccion2` varchar(60) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `estado` varchar(60) NOT NULL,
  `zipcode` varchar(25) NOT NULL,
  `telefono` varchar(35) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `celular` varchar(35) NOT NULL,
  `alloweduser` int(11) NOT NULL,
  `allowedclient` int(11) NOT NULL,
  `fechacreado` date NOT NULL,
  `nomcar` varchar(25) NOT NULL,
  `logo` varchar(35) NOT NULL,
  `userlevel` int(11) NOT NULL DEFAULT 1,
  `observ` mediumtext NOT NULL,
  `horaope` int(11) NOT NULL,
  `h1a` int(11) NOT NULL,
  `h1b` int(11) NOT NULL,
  `cl1` int(11) NOT NULL,
  `h2a` int(11) NOT NULL,
  `h2b` int(11) NOT NULL,
  `cl2` int(11) NOT NULL,
  `h3a` int(11) NOT NULL,
  `h3b` int(11) NOT NULL,
  `cl3` int(11) NOT NULL,
  `h4a` int(11) NOT NULL,
  `h4b` int(11) NOT NULL,
  `cl4` int(11) NOT NULL,
  `h5a` int(11) NOT NULL,
  `h5b` int(11) NOT NULL,
  `cl5` int(11) NOT NULL,
  `h6a` int(11) NOT NULL,
  `h6b` int(11) NOT NULL,
  `cl6` int(11) NOT NULL,
  `h7a` int(11) NOT NULL,
  `h7b` int(11) NOT NULL,
  `cl7` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuariosinfo`
--

INSERT INTO `usuariosinfo` (`idx`, `idxamusuario`, `idxusuariossp`, `idx_user_category`, `username`, `claves`, `compania`, `nombres`, `apellidos`, `direccion`, `direccion2`, `ciudad`, `estado`, `zipcode`, `telefono`, `fax`, `celular`, `alloweduser`, `allowedclient`, `fechacreado`, `nomcar`, `logo`, `userlevel`, `observ`, `horaope`, `h1a`, `h1b`, `cl1`, `h2a`, `h2b`, `cl2`, `h3a`, `h3b`, `cl3`, `h4a`, `h4b`, `cl4`, `h5a`, `h5b`, `cl5`, `h6a`, `h6b`, `cl6`, `h7a`, `h7b`, `cl7`, `activo`, `fecreg`, `fecmod`) VALUES
(1, 1, 0, 0, 'Test@test.com', 'b06e6202d9f5a6b039eb94997d3c9b4b', 'AMW', 'Mr Bean', 'Marle', 'Usa', '', 'MASS', 'MA', '0000', '000000000', '', '000000000', 0, 0, '0000-00-00', 'imgcms', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosinfo_permiso`
--

CREATE TABLE `usuariosinfo_permiso` (
  `idxusuariosec` int(11) NOT NULL,
  `idxpermisocategory` int(11) NOT NULL,
  `fecreg` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosreset`
--

CREATE TABLE `usuariosreset` (
  `idx` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `token` varchar(35) DEFAULT NULL,
  `fecreg` datetime DEFAULT NULL,
  `fecmod` datetime DEFAULT '0000-00-00 00:00:00',
  `activo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuariosreset`
--

INSERT INTO `usuariosreset` (`idx`, `username`, `token`, `fecreg`, `fecmod`, `activo`) VALUES
(4, 'alex@incabox.com', '56675426605665196041214841121915', '2017-09-27 15:23:07', '2017-09-27 15:23:07', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariossp`
--

CREATE TABLE `usuariossp` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `idx_user_category` int(11) NOT NULL,
  `username` varchar(60) CHARACTER SET latin1 NOT NULL,
  `claves` varchar(35) CHARACTER SET latin1 NOT NULL,
  `compania` varchar(60) NOT NULL,
  `nombres` char(35) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(35) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `direccion2` varchar(60) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `estado` varchar(60) NOT NULL,
  `zipcode` varchar(25) NOT NULL,
  `telefono` varchar(35) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `celular` varchar(35) NOT NULL,
  `alloweduser` int(11) NOT NULL,
  `allowedclient` int(11) NOT NULL,
  `allowedaccounts` int(11) NOT NULL,
  `fechacreado` date NOT NULL,
  `nomcar` varchar(35) NOT NULL,
  `logo` varchar(35) NOT NULL,
  `userlevel` int(11) NOT NULL DEFAULT 1,
  `observ` mediumtext NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `fecreg` date NOT NULL,
  `fecmod` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_tipo`
--

CREATE TABLE `usuarios_tipo` (
  `idx` int(11) NOT NULL,
  `nombre` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios_tipo`
--

INSERT INTO `usuarios_tipo` (`idx`, `nombre`) VALUES
(1, 'Administrator'),
(2, 'Author'),
(3, 'Subscriber'),
(998, 'Webmaster'),
(999, 'Account'),
(997, 'support');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_verify`
--

CREATE TABLE `usuarios_verify` (
  `idx` int(11) NOT NULL,
  `idxamusuario` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(35) NOT NULL,
  `ips` varchar(25) NOT NULL,
  `fecreg` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `webmarketing`
--

CREATE TABLE `webmarketing` (
  `idx` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `keywords` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `googleanalyticscode` mediumtext NOT NULL,
  `sitemap_xml` varchar(100) NOT NULL,
  `sitemap_html` varchar(100) NOT NULL,
  `robot_file_txt` varchar(100) NOT NULL,
  `bing_xml` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `webmarketing`
--

INSERT INTO `webmarketing` (`idx`, `title`, `keywords`, `description`, `googleanalyticscode`, `sitemap_xml`, `sitemap_html`, `robot_file_txt`, `bing_xml`) VALUES
(1, 'Welcome to Our Website2', 'Welcome to Our Website3', 'Welcome to Our Website4', '', '', '', 'robots.txt', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `slug` (`slug`) USING BTREE;

--
-- Indices de la tabla `articles_category`
--
ALTER TABLE `articles_category`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `articles_detail`
--
ALTER TABLE `articles_detail`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_detail_category`
--
ALTER TABLE `articles_detail_category`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_options`
--
ALTER TABLE `articles_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_options_home`
--
ALTER TABLE `articles_options_home`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_tag`
--
ALTER TABLE `articles_tag`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `articles_widget`
--
ALTER TABLE `articles_widget`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_widget_list`
--
ALTER TABLE `articles_widget_list`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_widget_orden`
--
ALTER TABLE `articles_widget_orden`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `articles_widget_tipo`
--
ALTER TABLE `articles_widget_tipo`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `auth_session`
--
ALTER TABLE `auth_session`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `backup_tabla`
--
ALTER TABLE `backup_tabla`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `bannershome`
--
ALTER TABLE `bannershome`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `bannershome_options`
--
ALTER TABLE `bannershome_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `bannershome_video`
--
ALTER TABLE `bannershome_video`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `comment_post_ID` (`article_id`),
  ADD KEY `comment_parent` (`comment_parent`);

--
-- Indices de la tabla `comments_like_log`
--
ALTER TABLE `comments_like_log`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `comment_post_ID` (`comment_id`);

--
-- Indices de la tabla `comments_status`
--
ALTER TABLE `comments_status`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `detgaleriaslista`
--
ALTER TABLE `detgaleriaslista`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `detgalerias_project_lista`
--
ALTER TABLE `detgalerias_project_lista`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `fonts`
--
ALTER TABLE `fonts`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `formularios`
--
ALTER TABLE `formularios`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `formularios_record_list`
--
ALTER TABLE `formularios_record_list`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_group`
--
ALTER TABLE `galerias_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_lista`
--
ALTER TABLE `galerias_lista`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_lista_options`
--
ALTER TABLE `galerias_lista_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_options`
--
ALTER TABLE `galerias_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_project_category`
--
ALTER TABLE `galerias_project_category`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_project_group`
--
ALTER TABLE `galerias_project_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_project_lista`
--
ALTER TABLE `galerias_project_lista`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_project_lista_fc_options`
--
ALTER TABLE `galerias_project_lista_fc_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `galerias_project_lista_options`
--
ALTER TABLE `galerias_project_lista_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `gift_n`
--
ALTER TABLE `gift_n`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `gift_w`
--
ALTER TABLE `gift_w`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgcut`
--
ALTER TABLE `imgcut`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgcuts`
--
ALTER TABLE `imgcuts`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgext`
--
ALTER TABLE `imgext`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgquality`
--
ALTER TABLE `imgquality`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgsize`
--
ALTER TABLE `imgsize`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `imgtsize`
--
ALTER TABLE `imgtsize`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `img_cover_page`
--
ALTER TABLE `img_cover_page`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informativo`
--
ALTER TABLE `informativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informativoprotected`
--
ALTER TABLE `informativoprotected`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access` (`idx_menu`),
  ADD KEY `uri` (`id_router`),
  ADD KEY `theme_idx` (`theme_idx`);

--
-- Indices de la tabla `menu_name`
--
ALTER TABLE `menu_name`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_type`
--
ALTER TABLE `menu_type`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `meta_page`
--
ALTER TABLE `meta_page`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `page_builder`
--
ALTER TABLE `page_builder`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `page_builder_seo_option`
--
ALTER TABLE `page_builder_seo_option`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `page_content_builder`
--
ALTER TABLE `page_content_builder`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `slug` (`slug`);

--
-- Indices de la tabla `page_content_group`
--
ALTER TABLE `page_content_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `page_group`
--
ALTER TABLE `page_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_detalle`
--
ALTER TABLE `project_detalle`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_group`
--
ALTER TABLE `project_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_lista`
--
ALTER TABLE `project_lista`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_lista_options`
--
ALTER TABLE `project_lista_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_options`
--
ALTER TABLE `project_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews`
--
ALTER TABLE `rating_reviews`
  ADD PRIMARY KEY (`idx`),
  ADD KEY `comment_parent` (`comment_parent`);

--
-- Indices de la tabla `rating_reviews_avatar`
--
ALTER TABLE `rating_reviews_avatar`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_options`
--
ALTER TABLE `rating_reviews_options`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_status`
--
ALTER TABLE `rating_reviews_status`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_template`
--
ALTER TABLE `rating_reviews_template`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_widget`
--
ALTER TABLE `rating_reviews_widget`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_widget_c`
--
ALTER TABLE `rating_reviews_widget_c`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `rating_reviews_widget_scroll`
--
ALTER TABLE `rating_reviews_widget_scroll`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `redirect`
--
ALTER TABLE `redirect`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `router`
--
ALTER TABLE `router`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `templateweb`
--
ALTER TABLE `templateweb`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx` (`idx`),
  ADD KEY `name` (`name`);

--
-- Indices de la tabla `ubigeoestadousa`
--
ALTER TABLE `ubigeoestadousa`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `usuariomenu_permiso`
--
ALTER TABLE `usuariomenu_permiso`
  ADD PRIMARY KEY (`idxamusuario`,`idxusuariosec`,`idxpermisocategory`);

--
-- Indices de la tabla `usuariomenu_permiso_category`
--
ALTER TABLE `usuariomenu_permiso_category`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `usuarios` (`username`);

--
-- Indices de la tabla `usuarioscompany`
--
ALTER TABLE `usuarioscompany`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `usuarioscompanysp`
--
ALTER TABLE `usuarioscompanysp`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `usuariosec_permiso`
--
ALTER TABLE `usuariosec_permiso`
  ADD PRIMARY KEY (`idxamusuario`,`idxusuariosec`,`idxpermisocategory`);

--
-- Indices de la tabla `usuariosinfo`
--
ALTER TABLE `usuariosinfo`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `usuarios` (`username`);

--
-- Indices de la tabla `usuariosinfo_permiso`
--
ALTER TABLE `usuariosinfo_permiso`
  ADD PRIMARY KEY (`idxusuariosec`,`idxpermisocategory`);

--
-- Indices de la tabla `usuariosreset`
--
ALTER TABLE `usuariosreset`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `usuariossp`
--
ALTER TABLE `usuariossp`
  ADD PRIMARY KEY (`idx`),
  ADD UNIQUE KEY `usuarios` (`username`);

--
-- Indices de la tabla `usuarios_tipo`
--
ALTER TABLE `usuarios_tipo`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `usuarios_verify`
--
ALTER TABLE `usuarios_verify`
  ADD PRIMARY KEY (`idx`);

--
-- Indices de la tabla `webmarketing`
--
ALTER TABLE `webmarketing`
  ADD PRIMARY KEY (`idx`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles_category`
--
ALTER TABLE `articles_category`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articles_detail`
--
ALTER TABLE `articles_detail`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `articles_detail_category`
--
ALTER TABLE `articles_detail_category`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `articles_options`
--
ALTER TABLE `articles_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articles_options_home`
--
ALTER TABLE `articles_options_home`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articles_tag`
--
ALTER TABLE `articles_tag`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `articles_widget`
--
ALTER TABLE `articles_widget`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `articles_widget_list`
--
ALTER TABLE `articles_widget_list`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `articles_widget_orden`
--
ALTER TABLE `articles_widget_orden`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `articles_widget_tipo`
--
ALTER TABLE `articles_widget_tipo`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `auth_session`
--
ALTER TABLE `auth_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `backup_tabla`
--
ALTER TABLE `backup_tabla`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `bannershome`
--
ALTER TABLE `bannershome`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `bannershome_options`
--
ALTER TABLE `bannershome_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `comments_like_log`
--
ALTER TABLE `comments_like_log`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comments_status`
--
ALTER TABLE `comments_status`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `detgaleriaslista`
--
ALTER TABLE `detgaleriaslista`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;

--
-- AUTO_INCREMENT de la tabla `detgalerias_project_lista`
--
ALTER TABLE `detgalerias_project_lista`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fonts`
--
ALTER TABLE `fonts`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `formularios`
--
ALTER TABLE `formularios`
  MODIFY `idx` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `formularios_record_list`
--
ALTER TABLE `formularios_record_list`
  MODIFY `idx` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galerias_group`
--
ALTER TABLE `galerias_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galerias_lista`
--
ALTER TABLE `galerias_lista`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `galerias_lista_options`
--
ALTER TABLE `galerias_lista_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galerias_options`
--
ALTER TABLE `galerias_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galerias_project_category`
--
ALTER TABLE `galerias_project_category`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `galerias_project_group`
--
ALTER TABLE `galerias_project_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galerias_project_lista`
--
ALTER TABLE `galerias_project_lista`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galerias_project_lista_fc_options`
--
ALTER TABLE `galerias_project_lista_fc_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `galerias_project_lista_options`
--
ALTER TABLE `galerias_project_lista_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gift`
--
ALTER TABLE `gift`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gift_n`
--
ALTER TABLE `gift_n`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `gift_w`
--
ALTER TABLE `gift_w`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `imgcut`
--
ALTER TABLE `imgcut`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `imgcuts`
--
ALTER TABLE `imgcuts`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `imgext`
--
ALTER TABLE `imgext`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `imgquality`
--
ALTER TABLE `imgquality`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `imgsize`
--
ALTER TABLE `imgsize`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `imgtsize`
--
ALTER TABLE `imgtsize`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `img_cover_page`
--
ALTER TABLE `img_cover_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `informativo`
--
ALTER TABLE `informativo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `informativoprotected`
--
ALTER TABLE `informativoprotected`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `menu_name`
--
ALTER TABLE `menu_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu_type`
--
ALTER TABLE `menu_type`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `meta_page`
--
ALTER TABLE `meta_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `page_builder`
--
ALTER TABLE `page_builder`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `page_builder_seo_option`
--
ALTER TABLE `page_builder_seo_option`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `page_content_builder`
--
ALTER TABLE `page_content_builder`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `page_content_group`
--
ALTER TABLE `page_content_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `page_group`
--
ALTER TABLE `page_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `project_category`
--
ALTER TABLE `project_category`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `project_detalle`
--
ALTER TABLE `project_detalle`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT de la tabla `project_group`
--
ALTER TABLE `project_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `project_lista`
--
ALTER TABLE `project_lista`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `project_lista_options`
--
ALTER TABLE `project_lista_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `project_options`
--
ALTER TABLE `project_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `project_status`
--
ALTER TABLE `project_status`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rating_reviews`
--
ALTER TABLE `rating_reviews`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_avatar`
--
ALTER TABLE `rating_reviews_avatar`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_options`
--
ALTER TABLE `rating_reviews_options`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_status`
--
ALTER TABLE `rating_reviews_status`
  MODIFY `idx` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_template`
--
ALTER TABLE `rating_reviews_template`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_widget`
--
ALTER TABLE `rating_reviews_widget`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_widget_c`
--
ALTER TABLE `rating_reviews_widget_c`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rating_reviews_widget_scroll`
--
ALTER TABLE `rating_reviews_widget_scroll`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `redirect`
--
ALTER TABLE `redirect`
  MODIFY `idx` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `router`
--
ALTER TABLE `router`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `templateweb`
--
ALTER TABLE `templateweb`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuariomenu_permiso_category`
--
ALTER TABLE `usuariomenu_permiso_category`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20013;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT de la tabla `usuarioscompany`
--
ALTER TABLE `usuarioscompany`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarioscompanysp`
--
ALTER TABLE `usuarioscompanysp`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuariosinfo`
--
ALTER TABLE `usuariosinfo`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuariosreset`
--
ALTER TABLE `usuariosreset`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuariossp`
--
ALTER TABLE `usuariossp`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios_tipo`
--
ALTER TABLE `usuarios_tipo`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT de la tabla `usuarios_verify`
--
ALTER TABLE `usuarios_verify`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
