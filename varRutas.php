<?php
//------------------------------------------------------------------------------------------------
// declarando datos permanentes
define('OS', strtoupper(substr(PHP_OS, 0, 3))); // separador de directorios
define('DS', DIRECTORY_SEPARATOR); // separador de directorios
define('ROOT_PATH', realpath(dirname(__FILE__)) );
define('ADMIN_CMS', 'p/'); // nombre de la carpeta del administrador
define('LOCAL', 'localhost'); // nombre del servidor local
define('IP_LOCAL', '::1'); // Ip del servidor local
define('VHOST', 'cms.local'); // nombre del servidor virtual
define('IP_VHOST', '127.0.0.1'); // Ip del servidor virtual
define('IP', $_SERVER['REMOTE_ADDR']);//obteniendo ip del servidor
define('VERSION', '5.27');//obteniendo ip del servidor
//------------------------------------------------------------------------------------------------
//validacion de protocolo
$server= ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$host= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : LOCAL;
//------------------------------------------------------------------------------------------------
//obteniendo ruta path de la carpeta raiz de sistema
$separarDir = explode(DS , ROOT_PATH); // separando los directorios
$dirBase = '/'; // definiendo raiz 
$dirLocal='/'.$separarDir[count($separarDir)-1].'/'; // obteniendo el directorio raiz
if(IP == IP_LOCAL && $host == LOCAL ){$dirBase=$dirLocal;} // asignando directorio 
//if(IP==IP_VHOST && $host==VHOST ){$dirBase=$dirLocal;} //descomentar si usa sub directorios como dominio.com/dir
//------------------------------------------------------------------------------------------------
//obteniendo URL
$len_uri=  strlen($dirBase) ;
if(isset($_GET['url'])){$uri = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);}
else{$uri = substr($_SERVER["REQUEST_URI"], $len_uri);}
define('URL', trim($uri, "/"));	
//------------------------------------------------------------------------------------------------
// archivos de enrutamiento
define("REQUEST", "request.php");
define("ROUTER", "router.php");
//------------------------------------------------------------------------------------------------
//definiendo datos de control de rutas
$server.= "://".$host; //construyendo url 
$urlDomain= $server; //otra variable de url 
$preview=0; // variable de control para visualizar website en status off
define('DIR_PATH', $dirBase); // carpeta raiz 
define('SERVER', $server.DIR_PATH); // url base
define('SERVER_P', SERVER.ADMIN_CMS); // url con carpeta del admin
define('URL_INDEX', DIR_PATH.'index.php'); // ubicacion del index
define('FILE_PHP', basename($_SERVER['PHP_SELF'])); // nombre del archivo php q se esta ejecutando
define('IMG_CMS', DIR_PATH.'imgcms/'); // imagenes cms
define('USERFILES', SERVER.'userfiles/'); // directorio raiz de archivos 
define('IMG', USERFILES.'images/'); // imagenes web
define('JS', USERFILES.'js/'); // archivos js 
define('CSS', USERFILES.'css/');// archivos css
define('PLUGINS', SERVER.'plugins/');
define('MOBILE_DETECT', 'plugins/Mobile_Detect.php');// detectar dispositivos moviles desde PHP
define('_INC', 'inc/');// carpeta inc
define('CORE_ROUTER', 'core/router.php');// validador de rutas
define('CORE_REQUEST', 'core/request.php');// enrutador
//------------------------------------------------------------------------------------------------
//directorios de admin
define('URL_INDEX_P', SERVER_P.'index.php');
define('URL_HOME_P', SERVER_P.'home.php');
define('PLUGINS_P', SERVER_P.'plugins/');
define('LIB_P', 'plugins/');
define('LAYOUT', SERVER_P.'customize/');
define('IMG_P', SERVER_P.'img/');
define('IMGAGES_P', LAYOUT.'images/');
define('JS_P', LAYOUT.'js/');
define('CSS_P', LAYOUT.'css/');
define('READ_THEMES', ADMIN_CMS.'themes/themes.php');
define('TEMPLATE', 'template/');
define('LANGUAGES', ADMIN_CMS.'system/languages/languages.php');
