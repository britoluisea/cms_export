<?php include('header.php') ?>
<?php include('prot-logeo.php') ?>
<?php include('app/model/Usuarios.php'); ?>
<?php include('app/FormValidator.php'); ?>
<?php include('app/ValidationRule.php'); ?>
<?php
$validator = new App_FormValidator();
$mUsuarios = new App_Model_Usuarios();
?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validator->addRule('pwd', 'Is required.', 'required');
    $validator->addRule('username', 'Is required.', 'required');
    $validator->addRule('username', 'Please enter a valid email address.', 'email');
    //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');
    //**********************************************************************************************************
    $validator->addEntries($_POST);
    $validator->validate();

    //**********************************************************************************************************
    $entries = $validator->getEntries();
    foreach ($entries as $key => $value) {
        ${$key} = $value;
    }

    //**********************************************************************************************************
    if ($validator->foundErrors()) {
        $errors = $validator->getErrors();
    }
    if (empty($errors)) :
        $mUsuarios->login($entries);
    endif;
    //**********************************************************************************************************
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($_SESSION['user']['saved'])): ?>
                <div  class="alert alert-success" align="center"><?= $_SESSION['user']['saved'] ?></div>
            <?php endif; ?>
            <?php unset($_SESSION['user']['saved']); ?>

            <?php if (isset($_SESSION['user']['error_correo'])): ?>
                <div  class="alert alert-danger" align="center"><?= $_SESSION['user']['error_correo'] ?></div>
            <?php endif; ?>
            <?php unset($_SESSION['user']['error_correo']); ?>	
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="col-md-4 column">
                <form enctype="application/x-www-form-urlencoded" action="" method="post" name="myform" id="myform">  

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title fontb">Returning Customer </h3>
                        </div>
                        <div class="panel-body">
                            <?php if (isset($_SESSION['user']['error'])): ?>
                                <div  class="alert alert-danger"><?= $_SESSION['user']['error'] ?></div>
                            <?php endif; ?>
                            <?php unset($_SESSION['user']['error']); ?>
                            <div class="form-group">
                                <label for="username">Email address:</label>
                                <input name="username" type="text" class="form-control" id="username" placeholder="Email" value="<?= $username ?>" />
                                <div class="text-danger"><?= $errors['username'] ?></div>  
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input name="pwd" type="password" class="form-control" id="pwd" placeholder="Password" />
                                <div class="text-danger"><?= $errors['pwd'] ?></div>
                            </div>
                            <div class="form-group">
                                Forgot your password? <a href="forgot.php">Click here</a>
                            </div>

                        </div>
                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-primary">Log In</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 column">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title fontb">New Customer </h3>
                    </div>
                    <div class="panel-body">
                        <p>New customers can create an account so that you can shop faster in the future and keep track of orders you have made in the past. <br><br>
                            Also special offers and promotions will be available only for registered users.</p>
                        <div style="height: 60px;"></div>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="register.php" class="btn btn-default">Register</a>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php include('footer.php') ?>