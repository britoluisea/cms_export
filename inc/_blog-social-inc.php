<?php
  $wurl=$url_article;
  $wimg=SERVER."/imgcms/articles/thumbs/".$fila["imagen"];
?>
  <?php
    if($atabla_opt['twitter_share']==1 || $atabla_opt['face_share']==1) 
    { ?>
      <li class="social sp-mobile">
      </li>
      <li class="social" >
        <i class="fa fa-share-alt fa-lg"></i>
      </li>
      <?php 
    }
    if($atabla_opt['twitter_share']==1)
    { ?>
      <li class="social">
        <a href="javascript:void(0)" class="social-icon social-tw" onclick="popupBlogList('http://twitter.com/share?url=<?=$wurl?>&text=<?=$fila["nombre"]?>')">
          <i class="fa fa-twitter fa-lg"></i>
        </a>
      </li>
      <?php 
    }
    if($atabla_opt['face_share']==1)
    { ?>
      <li class="social">
        <a href="javascript:void(0)" class="social-icon social-fb" onclick="popupBlogList('https://www.facebook.com/sharer/sharer.php?p[title]=<?=$fila["nombre"]?>&p[url]=<?=$wurl?>')">
          <i class="fa fa-facebook fa-lg"></i>
        </a>
      </li>
      <?php 
    }
  ?>
<script>
  function popupBlogList(url)
  {
  	window.open(url, "","width=515,height=430,location=0,menubar=0,scrollbars=1,status=1,resizable=0")
  }
</script>