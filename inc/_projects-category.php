	<?php 
	include dirname(__FILE__).'/../'.$_SESSION['THEME'].'header.php';
	require_once('paginator.class.php');
	require_once ("app/model/ProductDCategory.php");
	require_once ("app/model/ProductDLista.php");
	require_once ("app/model/ProductDListaDet.php");
	$s="SELECT * FROM project_category WHERE slug ='".$uriRule['sub'][1]."' ";
	$q = ejecutar($s);
	$f= fetchAssoc($q);
	$id=$f['idx'];	
	$mProductDCategory 	= new App_Model_ProductDCategory();
	$mProductDLista 	= new App_Model_ProductDLista();
	$mProductDListaDet 	= new App_Model_ProductDListaDet();
	$rowCat=$mProductDCategory->getById($id);
	$filas = $mProductDLista->getListar($id);
	$numReg = count($filas);
	$pages = new Paginator();
	$pages->items_total = $numReg;
	$pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
	$pages->paginate(12); //cuantos debe de mostrar por pagina
	$filasPag = $mProductDLista->getListarPagination($id, $pages->limit);
	$numRegPag = count($filasPag);
	?>
	<link rel="stylesheet" href="<?=PLUGINS?>project-category/css/style.css">
	<br>
	<div class="container">
		<div class="row">
			<div class="" cms-cols="col c-2 cx-12">
				<?php include('_projects_lateral.php'); ?>
			</div>
			<div class="" cms-cols="col c-10 cx-12">
				<div class="category-child">
					<div class="row">
						<div class="" cms-cols="col c-9 cx-12">
							<div class="cat_nombre"><?=$rowCat['nombre']?></div><br>
						</div>
		    			<div class="" cms-cols="col c-3 cx-12"> 
			              	<div style="display:none;"> 
			                  <form id="form1" name="form1" method="post" action="" class="form-horizontal">
			                      <div class="form-group">
			                        <label class="" cms-cols="col c-5 cx-12"><?=isset($dataGlobal['sort_by']) ? $dataGlobal['sort_by'] : 'Sort by' ?>:</label>
			                        <div class="" cms-cols="col c-7 cx-12">
			                          <select name="projectstatus" class="form-control input-sm" id="projectstatus" onChange='location="<?=SERVER.'projects/'.$uriRule['sub'][1]?>&sb="+document.getElementById("projectstatus").value+""' >
			                            <option value=""><?=isset($dataGlobal['label_all']) ? $dataGlobal['label_all'] : 'All' ?></option>
						                    <?php
						                    $sql="select * from project_status where activo = 1 order by nombre";
						                    $consulta=ejecutar($sql);
						                    while($fila=fetchArray($consulta))
						                    {
						                    ?>
					                     <option value="<?=$fila["idx"]?>" <?php if(isset($sb) && $fila["idx"]==$sb){  echo "selected='selected'";}?>><?=$fila["nombre"]?> </option> <?php } ?></select>
					                        </div>
			                      </div>
			                	</form>
			               	</div>
						</div>
					</div>
		            <?php 
		            	if ($numRegPag == 0) 
		            	{ ?>
		                  <div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?>.</div>
		              		<?php 
		          		}
		          		else
		          		{ ?>
							<?php 
							//print_r($filasPag);
							foreach ($filasPag as $item) 
							{ ?>
								<div class="" cms-cols="col c-4 cs-6 cx-12">
									<div class="thumb-details">
										<div class="caption post-content">
											<a  href="<?=SERVER?>projects/<?=$rowCat['slug']?>/<?=$item['slug']?>">
												<span><?= $item['nombre'];?> </span>
											</a>
										</div>
										<a class="thumb-cat" href="<?=SERVER?>projects/<?=$rowCat['slug']?>/<?=$item['slug']?>" >
											<img class="img-responsive" src="<?=SERVER?>imgcms/product_d/<?=$item['nomcar']?>/thumbs/<?=$item['imagen']?>" alt="<?= $item['nombre'];?>">
										</a>
										<div class="detail">
											<ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
												<li style="width:48%; padding-left: 5px !important;">
													<strong><?= $item['location'];?></strong>
												</li>
			                                    <!-- li style="list-style: none">|</li>
			                                    <li><strong>Status:	<?= $item['status_nombre'];?></strong></li>
			                                    <li style="list-style: none">|</li>
			                                    <li><strong>Date: <?= $item['fecha'];?></strong></li -->
			                                    <li style="width:48%;text-align: right;">
			                                    	<a href="<?=SERVER?>projects/<?=$rowCat['slug']?>/<?=$item['slug']?>" class="btn btn-default  " rel="tooltip" title="Photos" style=" padding: 3px 5px;">
			                                    		<span class="fa fa-photo  " >
			                                    			<strong style="font-weight: 100;">
			                                    				<?=isset($dataGlobal['see_this_idea']) ? $dataGlobal['see_this_idea'] : 'See this idea' ?>
			                                    			</strong>
			                                    		</span>
			                                    	</a>
			                                    </li> 
			                                </ul>
			                                <div class="fnt-lighter">
			                                	<span class="description">
			                                		<?= nl2br($item['descripcion']);?>
			                                	</span>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
		                    	<?php 
		                	} ?>
							<?php if($pages->num_pages > 1) 
							{ ?>       
		                    	<div align="center" style="padding:8px;">
		                    		<div class="btn-group" role="group">
		                    			<?php echo $pages->display_pages(); ?>
		                    		</div> 
		                    	</div>
		                    	<?php 
		                    } 
		            	}; 
		            ?>
		        </div>
	        </div>
	    </div>
	</div>
	<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'footer.php'; ?>