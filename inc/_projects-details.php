<?php 
include dirname(__FILE__).'/../'.$_SESSION['THEME'].'header.php';
require_once('paginator.class.php');
require_once ("app/model/ProductDLista.php");
require_once ("app/model/ProductDListaDet.php");
require_once ("app/model/ProductDOptions.php");
//echo' url = '.$uriRule['sub'][2];
$s="SELECT * FROM project_lista WHERE slug ='".$uriRule['sub'][2]."' ";
$q = ejecutar($s);
$f= fetchAssoc($q);
$id=$f['idx'];	
$mProductDLista 	= new App_Model_ProductDLista();
$mProductDListaDet 	= new App_Model_ProductDListaDet();
$mProductDOptions 	= new App_Model_ProductDOptions();
$row = $mProductDLista->getById($id);
$rowOpt=$mProductDOptions->getById(1);
//print_r($row);
?>
<link rel="stylesheet" href="<?=PLUGINS?>project-category/css/style.css">
<style>
.request, .request:hover, .request:active, .request:focus {
    background: #378702;
    color: #fff;
    border-radius: 0px;
    font-size: 20px;
    display: block;
    padding: 10px 14px;
    font-weight: 100;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    text-decoration: none;
    cursor: pointer;
    width: 200px;
    height: 80px;
    white-space: inherit;
    margin: 40px auto;
    border-radius: 4px;
    font-family: Arial;
}
.galleria-theme-folio .galleria-plus span {
    display: none; /*nombre de img en galeria*/
}
</style>
<br>
<div class="container">
	<div class="row">                      
		<div class="row">
			<div cms-cols="col c-8 cx-12" class="">
				<img class="img-responsive" src="<?=SERVER?>imgcms/product_d/<?=$row['nomcar']?>/large/<?=$row['imagen']?>" alt="<?= $row['nombre'];?>" style="width:100%;">
			</div>
			<div cms-cols="col c-4 cx-12" class="">
				<div cms-cols="col c-12 cx-12" class="cat_nombre">
					<h2><?=$row['nombre']?><br>
						<small><?=$row['location']?></small>
					</h2>
				</div>
				<div cms-cols="col c-12 cx-12"><?=$row["fulldescripcion"]?></div>
			</div>
		</div>
		<br><br><br><br>
		<div class="row">
			<div cms-cols="col c-12 cx-12">
				<?php 
					if($rowOpt['modelgallery']=='model1'):
						include("_projects_model_1.php");
					endif;
					if($rowOpt['modelgallery']=='model2'):
						include("_projects_model_2.php");
					endif;?>
			</div>
		</div>
		<br><br><br><br>
	</div>
</div>
<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'footer.php'; ?>