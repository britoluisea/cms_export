<?php 
require_once dirname(__FILE__)."/../app/model/RatingReviews.php";
require_once dirname(__FILE__)."/../app/FormValidator.php";
require_once dirname(__FILE__)."/../app/ValidationRule.php";
$validator=new App_FormValidator();
$mRatingReviews=new App_Model_RatingReviews();
$entries['avatar']=1;//default
$is_required =isset($dataGlobal['is_required']) ? $dataGlobal['is_required'] : 'Is required';
$min_chart_6 =isset($dataGlobal['min_chart_6']) ? $dataGlobal['min_chart_6'] : 'Your Review must be longer than 6 characters';
$error_email =isset($dataGlobal['valid_emial']) ? $dataGlobal['valid_emial'] : 'Please enter a valid email address';
$msj_captcha=isset($dataGlobal['msj_captcha']) ?  $dataGlobal['msj_captcha'] : 'Verification code incorrect, please try again';


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $validator->addRule('nombres', $is_required, 'required');
  $validator->addRule('apellidos', $is_required, 'required');
  $validator->addRule('city', $is_required, 'required');
  $validator->addRule('state', $is_required, 'required');
  $validator->addRule('descrip', $is_required, 'required');
  $validator->addRule('descrip', $min_chart_6, 'minlength', 6);
  $validator->addRule('stars', $is_required, 'required');
  $validator->addRule('username', $is_required, 'required');
  $validator->addRule('username', $error_email, 'email');
  $validator->addRule("captcha", $is_required, "required");
  $validator->addRule("captcha", $msj_captcha, "captcha");
  $validator->addEntries($_POST);
  $validator->validate();
  $entries = $validator->getEntries();
  foreach ($entries as $key => $value) {
    ${$key} = $value;
  }
  if ($validator->foundErrors()) {
    $errors = $validator->getErrors();
  }
  if (empty($errors)) : 
    $mRatingReviews->add($_POST);
    unset($entries);
  endif;
  //if(isset($_SESSION['data_saved']))
  //{echo'<script>window.parent.closeModal_reload();</script>';}
}
$lib_review=1;
$star_write=1;
?>
<style>
  body{opacity: 1 !important;}
  input[type="radio"], input[type="radio"]:checked {opacity: 0.3;}
  input[type="radio"]:hover, input[type="radio"]:checked {opacity: 1;}
  input[type="radio"] {
    min-height: 20px;
    /*position: absolute;
    width: 45px;
    height: 45px;
    left: 3px;
    top: -3px;
    opacity: 0.1;
    background: #000;*/
  }
</style>
<?php
  $sql_1="select * from rating_reviews_template where idx='1' ";
  $consulta_1=ejecutar($sql_1);
  $atabla=array();
  while($fila_1=fetchAssoc($consulta_1))
  {   $atabla=$fila_1;}
  $sql_2="select descrip_form from rating_reviews_options where idx='1' ";
  $consulta_2=ejecutar($sql_2);
  $atabla_opt_rat=array();
  while($fila_2=fetchAssoc($consulta_2))
  {   $atabla_opt_rat=$fila_2;  }
?>
<div class="cont-g-rating-write-review" style="padding:20px; padding-top: 0px;">
  <div class="row">
    <div cms-cols="col c-12 cs-12 cx-12">
      <?=$atabla_opt_rat['descrip_form'];?>
          <!--Star messages-->
          <?php if (isset($_SESSION['data_saved'])): ?>
            <div class="alert alert-success"><?= $_SESSION['data_saved'] ?></div>
          <?php unset($_SESSION['data_saved']); ?>
          <?php endif; ?>
          <!--End messages-->
    </div>
  </div>
  <form id="form-write-review" name="form1" method="post" action="">
    <div class="row">
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['first_name']) ? $dataGlobal['first_name'] : 'First Name'?><span class="asterisco">*</span></label>
        <input name="nombres" id="nombres"  class="form-nombres" type="text" value="<?=isset($entries['nombres']) ? $entries['nombres'] : ''?>"   >
        <?=isset($errors['nombres']) ? '<div class="error">'.$errors['nombres'].'</div>' : '' ?>
      </div> 
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['last_name']) ? $dataGlobal['last_name'] : 'Last Name'?><span class="asterisco">*</span></label>
        <input name="apellidos" id="apellidos"  class="form-apellidos" type="text" value="<?=isset($entries['apellidos']) ? $entries['apellidos'] : ''?>" >
        <?=isset($errors['apellidos']) ? '<div class="error">'.$errors['apellidos'].'</div>' : '' ?>
      </div>
    </div>
    <div class="row">   
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?><span class="asterisco">*</span></label>
        <input name="city" id="city"  class="form-city" type="text" value="<?=isset($entries['city']) ? $entries['city'] : ''?>">
        <?=isset($errors['city']) ? '<div class="error">'.$errors['city'].'</div>' : '' ?>
      </div> 
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?><span class="asterisco">*</span> </label>
        <input name="state" id="state"  class="form-state" type="text" value="<?=isset($entries['state']) ? $entries['state'] : ''?>">
        <?=isset($errors['state']) ? '<div class="error">'.$errors['state'].'</div>' : '' ?>
      </div>
    </div>
    <div class="row">   
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?><span class="asterisco">*</span></label>
        <input name="username" id="username"  class="form-username" type="text" value="<?=isset($entries['username']) ? $entries['username'] : ''?>">
        <?=isset($errors['username']) ? '<div class="error">'.$errors['username'].'</div>' : '' ?>
      </div>
      <div cms-cols="col c-6 cs-6 cx-12">
        <label><?=isset($dataGlobal['telephone']) ? $dataGlobal['telephone'] : 'Telephone'?></label>
        <input name="telephone" id="telephone"  class="form-telephone" type="text">
      </div>
    </div>
    <div class="row">   
      <div cms-cols="col c-5 cs-5 cx-12">
        <label><?=isset($dataGlobal['label_gender']) ? $dataGlobal['label_gender'] : 'Gender'?></label>
        <select name="genero" id="genero" class="form-genero"  >
          <option value="1" <?php if(isset($entries['genero']) && $entries['genero']=='1'): ?> selected="selected"<?php endif;?> ><?=isset($dataGlobal['label_male']) ? $dataGlobal['label_male'] : 'Male'?></option>
          <option value="0" <?php if(isset($entries['genero']) && $entries['genero']=='0'): ?> selected="selected"<?php endif;?>><?=isset($dataGlobal['label_famale']) ? $dataGlobal['label_famale'] : 'Female'?></option>
        </select> 
      </div>
      <div cms-cols="col c-7 cs-7 cx-12">
        <label cms-cols="col c100 cx100" style="padding: 0px;"><?=isset($dataGlobal['label_avatar']) ? $dataGlobal['label_avatar'] : 'Avatar'?></label>
        <div cms-cols="col c100 cx100" style="padding-top: 0px;">
          <?php
            $sql=" select *  from  rating_reviews_avatar where activo='1'"; 
            $consulta=ejecutar($sql);
            $num_reg=numRows($consulta);
            while($fila=fetchArray($consulta))
            {   
              ?>
              <div style="border:0px solid #000; display:inline-block; float: left; width: 50px; text-align: center; position: relative;">
                <div class="controls" align="center">
                  <label class="">
                    <input name="avatar" type="radio" value="<?=$fila['idx']?>" <?php if(isset($entries['avatar']) && $fila['idx']==$entries['avatar']){ ?> checked="checked" <?php }elseif($fila['idx']==1){?>checked="checked"<?php } ?> >
                    <span class="lbl"></span>
                    <div align="center">
                      <img  src="<?=SERVER?>imgcms/write-review/avatar/<?=$fila['imagen']?>" class="img-circle picture-wri-xy"/>
                    </div>
                  </label>
                </div>   
              </div>
              <?php 
            } 
          ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div cms-cols="col c-12 cs-12 cx-12">
        <label><?=isset($dataGlobal['rating_stars']) ? $dataGlobal['rating_stars'] : 'What is your overall rating of this business?'?><span class="asterisco">*</span></label>
        <div class="c-opacity-1" >
          <span id="hover-test" style="margin:0 0 0 5px; font-weight:bold; font-size:18px;">
            <?=!empty($entries['stars']) ? $entries['stars'] : "0" ?>
          </span>
          <input class="star-big-form {split:2} required" type="radio" name="stars" id="stars" value="0.5" <?php if(isset($entries['stars']) && $entries['stars']=='0.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="1.0" <?php if(isset($entries['stars']) && $entries['stars']=='1.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="1.5" <?php if(isset($entries['stars']) && $entries['stars']=='1.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="2.0" <?php if(isset($entries['stars']) && $entries['stars']=='2.0'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="2.5" <?php if(isset($entries['stars']) && $entries['stars']=='2.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="3.0" <?php if(isset($entries['stars']) && $entries['stars']=='3.0'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="3.5" <?php if(isset($entries['stars']) && $entries['stars']=='3.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="4.0" <?php if(isset($entries['stars']) && $entries['stars']=='4.0'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="4.5" <?php if(isset($entries['stars']) && $entries['stars']=='4.5'): ?> checked="checked" <?php endif;?>/>
          <input class="star-big-form {split:2}" type="radio" name="stars" id="stars" value="5.0" <?php if(isset($entries['stars']) && $entries['stars']=='5.0'): ?> checked="checked" <?php endif;?>/>
        </div>
        <div class="error clear">
          <?=isset($errors['stars']) ? '<div class="error">'.$errors['stars'].'</div>' : '' ?>
        </div>
      </div>
    </div>
    <div class="row">   
      <div  cms-cols="col c-12 cs-12 cx-12">
        <label><?=isset($dataGlobal['label_title']) ? $dataGlobal['label_title'] : 'Title'?></label>
        <input name="project" id="project"  class="form-project" type="text" value="<?=isset($entries['project']) ? $entries['project'] : ''?>">
      </div>
    </div>
    <div class="row">
      <div cms-cols="col c-12 cs-12 cx-12">
        <label><?=isset($dataGlobal['your_review']) ? $dataGlobal['your_review'] : 'Your Review'?><span class="asterisco">*</span></label>
        <textarea name="descrip" id="descrip" cols="45" rows="5" class="form-descrip"><?=isset($entries['descrip']) ? $entries['descrip'] : ''?></textarea>
        <?=isset($errors['descrip']) ? '<div class="error">'.$errors['descrip'].'</div>' : '' ?>
      </div>  
    </div>
    <div cms-cols="col c20 cx100">
      <img src="<?=SERVER_P?>form/captcha/captcha.php" alt="captcha" class="captcha-cms" />
    </div>
    <div cms-cols="col c80 cx100">
      <input type="text" name="captcha" required placeholder="<?=isset($dataGlobal['input_captcha']) ? $dataGlobal['input_captcha'] : 'Add the result of the sum'?>">
      <?=isset($errors['captcha']) ? '<div class="error">'.$errors['captcha'].'</div>' : '' ?>
    </div>
    <div class="text-left" cms-cols="col c-12 cs-12 cx-12">
      <button type="submit" class="btn-cms btn-yellow"><i class="fa fa-save"></i> <?=isset($dataGlobal['send_review']) ? $dataGlobal['send_review'] : 'Send Review'?> </button>
    </div>
  </form>
</div>