<?php 
$lib_blog_detail=1;
$wdisplay = ' display:none; '; 
$rowUsu = $mUsuarios->getById();
if (isset($_SESSION['user']['login']) && $_SESSION['user']['login'] == true){
    ?>
    <form id="form1" name="form1" method="post" action="<?=SERVER_P?>blog/comments/addComments.php">
        <table width="100%" border="0" cellspacing="2" cellpadding="2"  style="margin:0px;">
            <tr>
                <td width="80" valign="top">
                    <?php if ($rowUsu['avatar'] == 0): ?>
                        <div class="profile_picture">
                        <?php if ($rowUsu['imagen'] == ""): ?>
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <?php else : ?>
                                <?php if(is_file('imgcms/users/small/'.$rowUsu['imagen'])): ?>
                                    <img src="imgcms/users/small/<?= $rowUsu['imagen'] ?>">
                                <?php else : ?>
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <?php endif;?>
                        <?php endif; ?> 
                        </div>  
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 1): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-1.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 2): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-2.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 3): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-3.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 4): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-4.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 5): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-5.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($rowUsu['avatar'] == 6): ?>
                        <div class="profile_picture"><img  src="imgcms/users/default/avatar-6.jpg" /></div> 
                    <?php endif; ?>
                </td>
                <td valign="top">
                    <div class="com_title"><?= $rowUsu['nombres'] ?> <?= $rowUsu['apellidos'] ?></div>
                    <input name="id" type="hidden" id="id" value="<?= $fila[0]['idx'] ?>" />
                    <input name="slug" type="hidden" id="slug" value="<?= $fila[0]['slug'] ?>" />
                    <input name="ope" type="hidden" id="ope" value="comment" />
                    <textarea name="descrip" rows="4" id="descrip" placeholder="<?=isset($dataGlobal['write_comment']) ? $dataGlobal['write_comment'] : 'Write a comment' ?>..." class="campos_descrip"></textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="commentAdd" id="commentAdd" value="Post Comment"  class="btn-post-comm" /></td>
            </tr>
        </table>
    </form>
<?php }else{ ?>
    <div style="padding:4px;">
        <?=isset($dataGlobal['to_comment1']) ? $dataGlobal['to_comment1'] : 'To comment' ?>, <a href="#" id="login"  class="bmd-modalButton" data-toggle="modal" data-bmdSrc="<?=SERVER?>_user-login-p" data-bmdWidth="100%" data-bmdHeight="380px" data-scrolling="si" data-target="#myModal" title="Log in"><strong style="color:#ff9900"><?=isset($dataGlobal['log_in']) ? $dataGlobal['log_in'] : 'Log in' ?></strong></a> <?=isset($dataGlobal['to_comment2']) ? $dataGlobal['to_comment2'] : 'to your account. If you do not have an account' ?>, 
        <a href="#" id="register" title="Register" class="bmd-modalButton" data-toggle="modal" data-bmdSrc="<?=SERVER?>_user-register-p" data-bmdWidth="100%" data-bmdHeight="450px" data-scrolling="si" data-target="#myModal"><strong style="color:#ff9900"><?=isset($dataGlobal['label_register']) ? $dataGlobal['label_register'] : 'Register' ?></strong></a>.
    </div>
<?php 
}
$idPost = $fila[0]['idx'];
$rows = $mComments->getListar($idPost);
foreach ($rows as $item):
    ?>
    <div id="comment-<?= $item['idx'] ?>">
        <table width="100%" border="0" cellspacing="2" cellpadding="2"  style="margin:0px;">
            <tr>
                <td width="80" valign="top">
                    <?php if ($item['usu_avatar'] == 0): ?>
                        <div class="profile_picture">
                        <?php if (!isset($rowUsu['imagen']) || $rowUsu['imagen'] == ""): ?>
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <?php else : ?>
                                <?php if(is_file('imgcms/users/small/'.$rowUsu['imagen'])): ?>
                                    <img src="<?=SERVER?>imgcms/users/small/<?= $rowUsu['imagen'] ?>">
                                <?php else : ?>
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <?php endif;?>
                        <?php endif; ?> 
                        </div>  
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 1): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-1.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 2): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-2.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 3): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-3.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 4): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-4.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 5): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-5.jpg" /></div> 
                    <?php endif; ?>
                    <?php if ($item['usu_avatar'] == 6): ?>
                        <div class="profile_picture"><img  src="<?=SERVER?>imgcms/users/default/avatar-6.jpg" /></div> 
                    <?php endif; ?>
                </td>
                <td valign="top">
                    <div class="com_title"><?= $item['usu_nombres'] ?> <?= $item['usu_apellidos'] ?></div>
                    <div class="com_descrip">
                        <?= $item['descrip'] ?>
                    </div>
                    <div class="id_soc">
                        <span class="ico-date"><?= strftime('%b %d, %Y', strtotime($item['fecreg'])) ?> <?= strftime('%I:%M:%S %p', strtotime($item['fecreg'])) ?></span>
                        <span>|</span>
                        <span id="social_ico_<?= $item['idx'] ?>">
                        <?php
                        $id=$item['idx'];
                        $opc='';
                        include('_blog-detail-coment-like-unlike.php');
                        ?>  
                        </span>
                        <span>|</span>
                        <?php if (isset($_SESSION['user']['login']) && $_SESSION['user']['login'] == true): ?>
                        <span class="ico-reply">
                            <a href="#" data-id="<?= $item['idx'] ?>" class="btn_toggle"><i class="fa fa-reply"></i> <span><?=isset($dataGlobal['label_reply']) ? $dataGlobal['label_reply'] : 'Reply' ?></span></a>
                        </span>
                        <?php else: ?>  
                        <span class="ico-reply">
                            <a href="#" class="login"  ><i class="fa fa-reply"></i> <span><?=isset($dataGlobal['label_reply']) ? $dataGlobal['label_reply'] : 'Reply' ?></span></a>
                        </span>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
        </table>
        <!--Form Reply-->
        <div id="target_<?= $item['idx'] ?>" class="target" style=" <?= $wdisplay ?>">
            <div id="reply_<?= $item['idx'] ?>">
                <?php
                $idReply = $item['idx'];
                ?>
                <form id="form_reply" name="form_reply" method="post" class="form_reply" action="<?=SERVER_P?>blog/comments/addComments.php" data-id="<?= $item['idx'] ?>">
                    <table width="100%" border="0" cellspacing="2" cellpadding="2"  style="margin:0px;">
                        <tr>
                            <td width="80">&nbsp;</td>
                            <td width="80" valign="top">
                                <?php if ($rowUsu['avatar'] == 0): ?>
                                    <div class="profile_picture image-reply">
                                    <?php if ($rowUsu['imagen'] == ""): ?>
                                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <?php else : ?>
                                            <?php if(is_file('imgcms/users/small/'.$rowUsu['imagen'])): ?>
                                                <img src="<?=SERVER?>imgcms/users/small/<?= $rowUsu['imagen'] ?>">
                                            <?php else : ?>
                                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                            <?php endif;?>
                                    <?php endif; ?> 
                                    </div>  
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 1): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-1.jpg" /></div> 
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 2): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-2.jpg" /></div> 
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 3): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-3.jpg" /></div> 
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 4): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-4.jpg" /></div> 
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 5): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-5.jpg" /></div> 
                                <?php endif; ?>
                                <?php if ($rowUsu['avatar'] == 6): ?>
                                    <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-6.jpg" /></div> 
                                <?php endif; ?>
                            </td>
                            <td valign="top">
                                <div class="com_title"><?= $rowUsu['nombres'] ?> <?= $rowUsu['apellidos'] ?></div>
                                <input name="id" type="hidden" id="id" value="<?= $idPost ?>" />
                                <input name="comment_parent" type="hidden" id="comment_parent" value="<?= $idReply ?>" />
                                <input name="slug" type="hidden" id="slug" value="<?= $fila[0]['slug'] ?>" />
                                <input name="ope" type="hidden" id="ope" value="comment_resp" />
                                <textarea name="descrip_<?= $item['idx'] ?>"  rows="8" id="descrip_<?= $item['idx'] ?>" class="campos_descrip" placeholder="<?=isset($dataGlobal['write_comment']) ? $dataGlobal['write_comment'] : 'Write a comment' ?>..."></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="commentAddReply" id="commentAddReply" value="<?=isset($dataGlobal['post_comment']) ? $dataGlobal['post_comment'] : 'Post Comment' ?>"   class="btn-post-comm" /></td>
                        </tr>
                    </table>
                </form> 
            </div>       
        </div>
        <!--Fin Form Reply-->
        <!--Reply-->
        <?php
        //echo $idReply;
        $rowsReply = $mComments->getListarReply($idReply);
        foreach ($rowsReply as $itemReply):
        //echo $itemReply['idx'];
            ?>
            <table width="100%" border="0" cellspacing="2" cellpadding="2" style="margin:0px;">
                <tr>
                    <td width="80" valign="top">&nbsp;</td>
                    <td width="80" valign="top">
                        <?php if ($itemReply['usu_par_avatar'] == 0): ?>
                            <div class="profile_picture image-reply">
                                    <?php if ($itemReply['usu_par_imagen'] == ""): ?>
                                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    <?php else : ?>
                                            <?php if(is_file('imgcms/users/small/'.$itemReply['usu_par_imagen'])): ?>
                                                <img src="<?=SERVER?>imgcms/users/small/<?= $itemReply['usu_par_imagen'] ?>">
                                            <?php else : ?>
                                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                            <?php endif;?>
                                    <?php endif; ?> 
                            </div>  
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 1): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-1.jpg" /></div> 
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 2): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-2.jpg" /></div> 
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 3): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-3.jpg" /></div> 
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 4): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-4.jpg" /></div> 
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 5): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-5.jpg" /></div> 
                        <?php endif; ?>
                        <?php if ($itemReply['usu_par_avatar'] == 6): ?>
                            <div class="profile_picture image-reply"><img  src="<?=SERVER?>imgcms/users/default/avatar-6.jpg" /></div> 
                        <?php endif; ?>
                    </td>
                    <td valign="top">
                        <div class="com_title"><?= $itemReply['usu_par_nombres'] ?> <?= $itemReply['usu_par_apellidos'] ?></div>
                        <div class="com_descrip">
                            <?= $itemReply['descrip'] ?>
                        </div>
                        <div class="id_soc">
                        <span class="ico-date"><?= strftime('%b %d, %Y', strtotime($itemReply['fecreg'])) ?> <?= strftime('%I:%M:%S %p', strtotime($itemReply['fecreg'])) ?></span>
                        <span>|</span>
                        <span id="social_ico_<?= $itemReply['idx'] ?>">
                        <?php
                        $id=$itemReply['idx'];
                        $opc='';
                        include('_blog-detail-coment-like-unlike.php');
                        ?>  
                        </span>
                    </div>
                    </td>
                </tr>
            </table>    
    <?php endforeach; ?>
        <!--Fin Reply-->
    </div>
<?php endforeach; ?>
<hr style="background-color:#cccccc; border:0; height: 1px;">

