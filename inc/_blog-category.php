<?php 
  include dirname(__FILE__).'/../'.$_SESSION['THEME'].'header.php';
  require_once('app/pag/Paginator.php');
  $sql_opt="SELECT * FROM articles_options where idx='1' limit 0,1";
  $consulta_opt=ejecutar($sql_opt);
  $atabla_opt=array();
  if($fila_opt=fetchAssoc($consulta_opt))
  {
    $atabla_opt=$fila_opt;  
  }
  $slug= $uriRule['sub'][2];
  if (empty($slug)) 
  {
    echo'<script>location.href="'.SERVER.'blog"</script>';
  }
  else
  {
    $t="articles_category";
    $sql_cat="SELECT * FROM $t where slug = '$slug' and activo='1' limit 0,1";  
    //echo $sql_cat; exit;
    $consulta_cat=ejecutar($sql_cat);
    $atabla_cat=array();
    $idx_cat = '';
    if($fila_cat=fetchAssoc($consulta_cat))
    {
      $atabla_cat=$fila_cat;   
      $idx_cat = $atabla_cat['idx'];
    }
    $t1="articles_detail_category"; 
    $t2="articles";
    $t3="usuarios";
    $sql="SELECT $t2.*,
    $t1.idx_articles_category as t1_idx_articles_category,
    $t3.idx as usu_idx, 
    $t3.username as usu_username,
    $t3.nombres as usu_nombres,
    $t3.apellidos as usu_apellidos,
    $t3.userlevel as usu_userlevel,
    $t3.slug as usu_slug
    FROM
    $t1,$t2,$t3
    where
    $t1.idx_articles_category  = '".$idx_cat."' and
    $t1.idx_articles=$t2.idx and
    $t2.activo='1' and
    $t2.idx_usuario = $t3.idx
    order by $t2.fecreg desc 
    ";  
    $consulta=ejecutar($sql);
    $num_reg=numRows($consulta);
    $pages = new App_Pag_Paginator();
    $pages->items_total = $num_reg;
    $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
    $pages->paginate($atabla_opt['num_reg_page'], SERVER.'blog/category');   //cuantos debe de mostrar por pagina
    $consulta=ejecutar($sql.$pages->limit);
    //echo$sql;
    ?>
    <link href="<?=PLUGINS?>articles/css/estilos.css" rel="stylesheet" type="text/css" />
    <style>
      .blog-date{background:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
      .link-date a, .link-date a:hover{color:<?=$atabla_opt['fondo_btn']?>}
      .contenidointerno .blogtitle a, .contenidointerno .blogtitle a:hover, .blog-detail, .blog-detail p, .blog-detail ul, .blog-detail li{color:<?=$atabla_opt['color_text_descrip']?>;}
      .btn-read-more-article, .btn-read-more-article:hover{background:<?=$atabla_opt['fondo_btn']?>; border-color:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
    </style>
    <div class="container" >
      <div class="contenidointerno"> 
        <!--Blog content here-->
        <?php
          if($num_reg==0)
          { ?>
            <div align="center" style="color:#F00">
              <?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?>.
            </div>
            <?php 
          }
          else
          { ?>
            <div class="caption_title">
              <h2>
              <?=isset($dataGlobal['label_category']) ? $dataGlobal['label_category'] : 'Category' ?>: <?=$atabla_cat['nombre']?> 
              </h2> 
              </div>
            <?php 
            while($fila=fetchArray($consulta))
            {
              $url_article=SERVER.'blog/'. $fila['slug'] ; ?>
              <div class="post">
                <div cms-cols="col c10 cs20 cx20 " class="col-date">
                  <div class="blog-date">
                    <div class="head-date"><i class="fa fa-calendar fa-2x"></i></div>
                    <div class="body-date" >
                      <?= strftime('%d', strtotime($fila['fecreg'])) ?>
                    </div>
                    <div class="foot-date">
                      <?= strftime('%b - %Y', strtotime($fila['fecreg'])) ?> 
                    </div>
                  </div>
                  <div class="link-date" >
                    <a href="<?=$url_article?>">                  
                      <i class="fa fa-link"></i>
                    </a>
                  </div>
                </div>
                <div cms-cols="col c90 cs80 cx80 " class="article-post">                
                    <div class="thumbnail-blog">
                      <div class="blogtitle">
                        <h2><a href="<?=$url_article?>"><?= $fila['nombre'] ?></a></h2>
                        <div class="date-mobile">
                          <i class="fa fa-calendar"></i>  <?= strftime('%d / %b / %Y', strtotime($fila['fecreg'])) ?> 
                        </div>
                      </div>
                      <?php 
                        if($fila['imagen_show']==1)
                        {   
                          if(!empty($fila['imagen']))
                          { 
                            $wimg=SERVER."imgcms/articles/thumbs/".$fila['imagen'];  ?>
                            <div class="post-img">
                              <a href="<?=$url_article?>"><img src="<?php echo $wimg ?>" alt="<?= $fila['nombre'] ?>"  /></a>
                            </div>  
                            <?php 
                          }
                        } 
                      ?>
                      <div class="caption">      
                        <div class="blog-metas">
                          <ul>
                            <li>
                              <i class="fa fa-user"></i>  
                              <?=isset($dataGlobal['label_by']) ? $dataGlobal['label_by'] : 'By' ?>:
                              <?php $urlAutor=SERVER."blog/author/".$fila["usu_slug"];?>
                              <?php if($fila["usu_nombres"] == "" and $fila["usu_apellidos"]==""):?>
                                <a href="<?=$urlAutor?>"><?= $fila["usu_username"]?></a>
                              <?php else:?>
                                <a href="<?=$urlAutor?>">
                                  <?= $fila["usu_nombres"]?> <?=$fila["usu_apellidos"]?>
                                </a>
                              <?php endif;?>                                
                            </li>
                            <?php if(isset($fila_opt['comments_blog']) && $fila_opt['comments_blog']==1) { ?>
                              <li class="comments">
                                <?php 
                                  $sql_5 = "SELECT idx FROM comments where article_id = '".$fila["idx"]."' and comment_status_id='2' ";
                                  $res_5=ejecutar($sql_5);
                                  $regNum=numRows($res_5);     
                                ?>
                                <?=$regNum?> 
                                <i class="fa fa-comments"></i> 
                                <?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?>
                              </li>
                            <?php } ?>
                            <?php include("_blog-social-inc.php"); ?>
                          </ul>
                        </div>
                        <div class="blog-detail"><?= $fila['detail'] ?></div>
                        <div class="blog-btn">
                          <a href="<?= $url_article ?>" class="btn-cms btn-blue btn-read-more-article" >
                            <?=isset($dataGlobal['read_more']) ? $dataGlobal['read_more'] : 'Read More' ?>  | 
                            <i class="fa fa-arrow-right"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <hr class="_hr_b">
              <?php
            }
          } 
        ?>
        <!--Blog content ends here-->
        <!--Pagination-->
        <?php if ($pages->num_pages > 1) :?>
          <div align="center"><?=$pages->display_pages();?></div> 
        <?php endif; ?> 
      </div>
      <?php include('_blog-sidebar.php');?>
    </div>
    <?php 
  }
  include dirname(__FILE__).'/../'.$_SESSION['THEME'].'footer.php'; 
?>