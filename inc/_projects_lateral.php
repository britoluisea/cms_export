<?php 
require_once ("app/model/ProductDGroup.php"); 
require_once ("app/model/ProductDCategory.php"); 
$mProductDGroup = new App_Model_ProductDGroup();
$mProductDCategory = new App_Model_ProductDCategory();
$rowPriItem=$mProductDGroup->getPrimerItem();
$filasLateral = $mProductDGroup->getListarLateral();
$alias = isset($dataGlobal['url_projects']) ? $dataGlobal['url_projects'] : 'projects';
if($detect->isMobile())
{ ?>
    <div class="panel panel-default hidden-md hidden-sm hidden-lg menu-projects">
        <div class="panel-heading">
            <h3 class="panel-title text-uppercase"><?=isset($dataGlobal['label_menu']) ? $dataGlobal['label_menu'] : 'Menu' ?></h3>
        </div>
        <div class="panel-body">
            <select name="group" id="group" class="form-control">
                <option value="" class="text-uppercase"><?=isset($dataGlobal['_select_']) ? $dataGlobal['_select_'] : '- Select -' ?></option>
                <?php foreach ($filasLateral as $item) { ?>
                    <optgroup label="<?= $item['nombre'] ?>">
                        <?php
                        $rowCatMenu = $mProductDCategory->getByIdGrupo($item['idx']);
                        ?>
                        <?php foreach ($rowCatMenu as $item2) { ?>
                            <option value="<?=SERVER.$alias?>/<?=$item2['slug']?>" <?php if (isset($id) && $item2['idx'] == $id){ ?> selected="selected" <?php } ?>><?= $item2['nombre'] ?></option>
                        <?php } ?>
                    </optgroup>
                <?php } ?>
            </select>
            <script>
                document.getElementById("group").onchange = function () {
                    if (this.selectedIndex !== 0) {
                        window.location.href = this.value;
                    }
                };
            </script>
        </div>
    </div>
    <?php 
}
if(!$detect->isMobile())
{ ?>
    <div class="hidden-xs">
        <div class="panel panel-default">
            <?php
            $cont = 0;
            foreach ($filasLateral as $item){
            $cont++;
            ?>
            <div class="panel-heading">
                <h3 class="panel-title"><?= strtoupper($item['nombre']) ?></h3>
            </div>
            <div class="list-group">  
                    <?php
                        $rowCatMenu = $mProductDCategory->getByIdGrupo($item['idx']);
                        //print_r($rowCatMenu);
                        foreach ($rowCatMenu as $item2) { 
                         ?>                   
                             <a href="<?=SERVER.$alias?>/<?=$item2['slug']?>" class="list-group-item">
                                <?= $item2['nombre'] ?>  
                            </a>                                                 
                      <?php } ?>
            </div>
            <?php } ?> 
        </div>
    </div>
    <?php 
}?>
