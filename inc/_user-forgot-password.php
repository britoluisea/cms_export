<?php 
include('header.php')  
if (isset($_SESSION['user']) && (!empty($_SESSION['user']['login']) or $_SESSION['user']['login'] == true)) {
        header("Location: my-account.php");
        exit();
}
?>
<?php include('app/model/Usuarios.php'); ?>
<?php include('app/model/UsuariosReset.php'); ?>
<?php include('app/FormValidator.php'); ?>
<?php include('app/ValidationRule.php'); ?>
<?php
$validator = new App_FormValidator();
$mUsuarios = new App_Model_Usuarios();
$mUsuariosReset = new App_Model_UsuariosReset();
?>
<?php
$_SESSION['token_id'] = "";
$_SESSION['token_email'] = "";
$token = addslashes($_GET['token']);
$mUsuariosReset->getByIdToken($token);
?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validator->addRule('pwd2', 'Is required.', 'required');
    $validator->addRule('pwd3', 'Is required.', 'required');
    $validator->addRule('pwd3', 'Passwords do not match.', 'verifyPass');

    //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');
    //**********************************************************************************************************
    $validator->addEntries($_POST);
    $validator->validate();

    //**********************************************************************************************************
    $entries = $validator->getEntries();
    foreach ($entries as $key => $value) {
        ${$key} = $value;
    }

    //**********************************************************************************************************
    if ($validator->foundErrors()) {
        $errors = $validator->getErrors();
    }
    /* echo "<pre>";
      print_r($errors);
      echo "</pre>"; */
    if (empty($errors)) :
        $mUsuarios->forgotPasswordChange($entries, $_SESSION['token_id'], $_SESSION['token_email']);
    endif;
    //**********************************************************************************************************
}
?>



<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-4">
            <?php if ($_SESSION['correo_enviado'] == 'SI'): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title fontb"> Retrieve Password </h3>
                    </div>
                    <div class="panel-body">
                        <div align="justify">
                            <p>We have sent you an email with instruction on how to create your new password.  If you dont see our email, check your spam folder.  If you encounter any issues, contact your website administrator.</p>
                            <p align="center"><br />
                                <a href="login.php">LOGIN</a>
                            </p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>  
            <?php else: ?>   
                <form enctype="application/x-www-form-urlencoded" action="" method="post" name="myform" id="myform"> 
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title fontb"> Retrieve Password </h3>
                        </div>


                        <div class="panel-body">
                            <?php if ($_SESSION['correo_enviado'] == 'NO') { ?>
                                <div align="center" class="alert alert-danger" >Invalid username.</div>
                            <?php } ?>

                            <p>
                                Email: <strong><?= $_SESSION['token_email'] ?></strong>
                            </p>

                            <div class="form-group">
                                <input name="pwd2" type="password" class="form-control" id="pwd2" placeholder="Password"  />
                                <div class="text-danger"><?= $errors['pwd2'] ?></div>
                            </div>
                            <div class="form-group">   
                                <input name="pwd3" type="password" class="form-control" id="pwd3" placeholder="Confirm Password"  />
                                <div class="text-danger"><?= $errors['pwd3'] ?></div>


                            </div>



                        </div>
                        <div class="panel-footer text-right">
                            <a href="login.php" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>Back to login</a>
                            <button type="submit" class="btn btn-primary"> Enter </button>
                        </div>

                    </div>
                </form>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php include('footer.php') ?>