<?php
  include dirname(__FILE__).'/../'.$_SESSION['THEME'].'head.php';
  if (isset($_SESSION['user']) && (!empty($_SESSION['user']['login']) or $_SESSION['user']['login'] == true)) 
  {
    header("Location: my-account.php");
    exit();
  }
  include('app/model/Usuarios.php');
  include('app/FormValidator.php');
  include('app/ValidationRule.php');
  $validator=new App_FormValidator();
  $mUsuarios=new App_Model_Usuarios();
  $genero=1;
  if ($_SERVER['REQUEST_METHOD'] == 'POST') 
  { 
    $validator->addRule('pwd', 'Is required.', 'required'); 
    $validator->addRule('username', 'Is required.', 'required');
    $validator->addRule('username', 'Please enter a valid email address.', 'email');
    $validator->addRule('nombres', 'Is required.', 'required');
    $validator->addRule('apellidos', 'Is required.', 'required');
    //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');
    //**********************************************************************************************************
    $validator->addEntries($_POST);
    $validator->validate();
    //**********************************************************************************************************
    $entries = $validator->getEntries();
    foreach ($entries as $key => $value) {
      ${$key} = $value;
    }
    //**********************************************************************************************************
    if ($validator->foundErrors()) {
      $errors = $validator->getErrors();
    }
    if (empty($errors)) : 
       $mUsuarios->add($entries,'1');
    endif;
    echo'<script>window.parent.closeModal_reload();</script>';
    //**********************************************************************************************************
  }
?>
<style>
  input[type="radio"]:checked+span{ font-weight: bold; }
</style>
<form action="" method="post" name="form1" id="form1">
  <div class="modalcread modalcread-edit">
    <div class="modal-header ">
      <div class="edit"> <h3>New User Registration</h3></div>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
              <label class="sr-only" for="nombres">Name</label>
              <div class="input-group">
                <input type="text" name="nombres" id="nombres" value="<?=isset($nombres) ? $nombres : ''?>" placeholder="Name" autocomplete="off" class="form-control">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
              </div>
              <div class="help-inline"><?=isset($errors['nombres']) ? $errors['nombres'] : '' ?></div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label class="sr-only" for="apellidos">Last Name</label>
              <div class="input-group">
                <input type="text" name="apellidos" id="apellidos" value="<?=isset($apellidos) ? $apellidos : ''?>" placeholder="Last Name" autocomplete="off" class="form-control">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
              </div>
              <div class="help-inline"><?=isset($errors['apellidos']) ? $errors['apellidos'] : '' ?></div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label class="sr-only" for="username">Email</label>
              <div class="input-group">
                <input type="text" name="username" id="username" value="<?=isset($username) ? $username : ''?>" placeholder="Email" autocomplete="off" class="form-control">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
              </div>
              <div class="help-inline"><?=isset($errors['username']) ? $errors['username'] : '' ?></div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label class="sr-only" for="pwd">Password</label>
              <div class="input-group">
                <input type="password" name="pwd" id="pwd" value="<?=isset($password) ? $password : ''?>" placeholder="Password" autocomplete="off" class="form-control">
                <div class="input-group-addon"><i class="fa fa-lock"></i></div>
              </div>
              <div class="help-inline"><?=isset($errors['pwd']) ? $errors['pwd'] : '' ?></div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <label class="radio-inline">
                <input name="genero" type="radio" value="1"  <?php if(isset($genero) && $genero==1){ ?> checked="CHECKED" <?php }?> ><span class="lbl"> Male</span>
              </label>
              <label class="radio-inline">
                <input name="genero" type="radio" value="0" <?php if(isset($genero) && $genero==0){ ?> checked="CHECKED" <?php }?> ><span class="lbl"> Female</span>
              </label>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
              <select name="anio" id="anio" class="form-control">
                <option value="" >Select Year</option>
                <?php  for($ifor=date('Y')-12;$ifor>=1900;$ifor--) { ?>
                  <option value="<?=$ifor?>" 
                    <?php if(isset($anio) && $anio==$ifor){?>selected="selected"<?php } ?>>
                    <?=$ifor?>
                  </option>
                <?php } ?>
              </select>
            </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="<?=SERVER?>_user-login-p"  class="btn btn-small btn-default" style="float: left;"> &laquo; Back to login</a> 
      <button name="button" type="submit" class="btn btn-small btn-success" id="button">Register <i class="fa fa-arrow-right"></i></button>
    </div>
  </div>
</form>
<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'foot.php'; ?>