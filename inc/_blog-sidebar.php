<aside class="sidebar" >
<?php
  $sql="SELECT 
  articles_widget.*,
  articles_widget_orden.idx as arti_wid_ite_idx,
  articles_widget_orden.idx_widget as arti_wid_ite_idx_widget
  FROM
  articles_widget,articles_widget_orden
  where
  articles_widget.idx = articles_widget_orden.idx_widget and
  articles_widget.activo='1'
  order by articles_widget_orden.idx";
  $consulta=ejecutar($sql);
  while($fila=fetchArray($consulta))
  {
    $wtipo=$fila['tipo'];
    //Tipo 1
    if($wtipo==1)
    { ?>
      <div class="box">
        <p><?=$fila['nombre']?></p>
           <ul>    
        <?php
            $sql_tipo1="SELECT * FROM articles where activo ='1' order by fecreg desc limit 0,".$fila['post_number'];
            $consulta_tipo1=ejecutar($sql_tipo1);
            while($fila_tipo1=fetchArray($consulta_tipo1))
            {
            ?>
                            <li>
                                  <a href='<?=SERVER?>blog/<?=$fila_tipo1['slug']?>'><span><?=$fila_tipo1['nombre']?></span></a><br> 
                                  <span style="color:#ff9900;"><?php if($fila['show_date']==1){?>
                                  <?= strftime('%B %d, %Y', strtotime($fila_tipo1['fecreg'])) ?>
                                  <?=strftime('%I:%M %p', strtotime($fila_tipo1["fecreg"]));?></span>
                                  <?php  } ?> 
                            </li>
              <?php  } ?> 
           </ul>   
      </div>
      <?php
    }
    //Tipo 2-->
    if($wtipo==2)
    {?>
      <div class="box">
        <p><?=$fila['nombre']?></p>
        <?php  
          if($fila['dis_drop']==0 and $fila['show_count']==0)
          {?>
            <!--Listado-->
            <ul>
              <?php
                $sql_tipo1="SELECT * FROM articles_category where activo='1' order by item desc";
                $consulta_tipo1=ejecutar($sql_tipo1);
                while($fila_tipo1=fetchArray($consulta_tipo1))
                {
                ?>
                  <li><a href='<?=SERVER?>blog/category/<?=$fila_tipo1['slug']?>'><?=$fila_tipo1['nombre']?></a></li>  
                <?php     
                }
                ?> 
            </ul> 
            <?php 
          }
          elseif($fila['dis_drop']==1 and $fila['show_count']==0)
          { ?> 
            <!--Select-->
            <form name="form_category" id="form_category">
              <select name="slugCat" id="slugCat">
                <option value="">- Select Category -</option>
                <?php
                  $sql_tipo1="SELECT * FROM articles_category where activo='1' order by item desc";
                  $consulta_tipo1=ejecutar($sql_tipo1);
                  while($fila_tipo1=fetchArray($consulta_tipo1))
                  {
                  ?>
                  <option value="<?=$fila_tipo1['slug']?>" <?php if($fila_tipo1['slug']==$_GET['slug']):?> selected="selected" <?php endif; ?>><?=$fila_tipo1['nombre']?></option>
                    <?php     
                  }
                ?>
              </select>
            </form>
            <?php 
          }
          elseif($fila['dis_drop']==0 and $fila['show_count']==1)
          { ?>
              <!--Listado + Count-->
             <ul>
                    <?php
              $sql_tipo1="SELECT * FROM articles_category where activo='1' order by item desc";
              $consulta_tipo1=ejecutar($sql_tipo1);
              while($fila_tipo1=fetchArray($consulta_tipo1))
              {
                  $num_row="";
                  $t1="articles_detail_category"; 
                  $t2="articles";
                  $sql_row="SELECT $t2.*,
                  $t1.idx_articles_category as t1_idx_articles_category 
                  FROM
                  $t1,$t2
                  where
                  $t1.idx_articles_category  = '".$fila_tipo1['idx']."' and
                  $t1.idx_articles=$t2.idx and
                  $t2.activo='1'
                  order by $t2.fecreg desc 
                  ";
                  $res_row =ejecutar($sql_row);
                  $num_row=numRows($res_row);
                  $wnum_row = "(".$num_row.")";           
                 ?>
                 <li><a href='<?=$urlCanonical?>'><span><?=$fila_tipo1['nombre']?> <?=$wnum_row?></span></a></li> 
                <?php     
              }
             ?> 
               
             </ul>  
              <?php 
          }
          elseif($fila['dis_drop']==1 and $fila['show_count']==1)
          { ?> 
            <!-- Select + Count-->
            <form name="form_category" id="form_category">
             <select name="slugCat" id="slugCat">
              <option value="">- Select Category -</option>
              <?php
                    $sql_tipo1="SELECT * FROM articles_category where activo='1' order by item desc";
                    $consulta_tipo1=ejecutar($sql_tipo1);
                    while($fila_tipo1=fetchArray($consulta_tipo1))
                    {
                      ?>
                      <?php 
                      $num_row="";
                      $t1="articles_detail_category"; 
                      $t2="articles";
                      $sql_row="SELECT $t2.*,
                      $t1.idx_articles_category as t1_idx_articles_category 
                      FROM
                      $t1,$t2
                      where
                      $t1.idx_articles_category  = '".$fila_tipo1['idx']."' and
                      $t1.idx_articles=$t2.idx and
                      $t2.activo='1'
                      order by $t2.fecreg desc 
                      ";
                      $res_row =ejecutar($sql_row);
                      $num_row=numRows($res_row);
                      $wnum_row = "(".$num_row.")";;            
                      ?>
                      <option value="<?=$fila_tipo1['slug']?>" <?php if($fila_tipo1['slug']==$_GET['slug']):?> selected="selected" <?php endif; ?>><?=$fila_tipo1['nombre']?> <?=$wnum_row?></option>
                      <?php     
                    }
                    ?>      
               </select>
            </form>
            <?php 
          }
        ?>
        <script type="text/javascript">
          $( "#slugCat").change(function() {
            location.href = "<?=SERVER?>blog/category/"+$( "#slugCat").val();
          });
        </script>
      </div>
      <?php
    }
    //Tipo 3
    if($wtipo==3)
    {?>
      <div class="box">
      <div><p><strong><?=$fila['nombre']?></strong></p></div>
      <div style="color:#444;font-weight:normal;"><?=$fila['descrip']?></div>
      </div>
      <?php
    }
    //Tipo 4
    if($wtipo==4)
    {?>
      <div class="box">
      <div><p><strong><?=$fila['nombre']?></strong></p></div>
      <form name="form_search" id="form_search"  method="get" action="blog">
          <input type="text" name="search" id="search" placeholder="Search..." value="<?=$search?>">
          <button type="submit" style="cursor:pointer;color:#FFF" ><?=isset($dataGlobal['label_go']) ? $dataGlobal['label_go'] : 'Go' ?></button>
          </form>
      </div>
      <?php
    }
  }
?>
</aside>