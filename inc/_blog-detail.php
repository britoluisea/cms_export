<?php 
  include dirname(__FILE__).'/../'.$_SESSION['THEME'].'header.php';
  include ("app/model/Usuarios.php"); 
  include ("app/model/Comments.php"); 
  include ("app/model/ArticlesCategory.php"); 
  include ("app/model/Articles.php"); 
  include ("app/model/ArticlesOptions.php");
  include ("app/model/ArticlesDetail.php");
  include ("app/model/ArticlesDetailCategory.php"); 
  $wdisplay = ' display:none; ';
  $mUsuarios=new App_Model_Usuarios();
  $mComments=new App_Model_Comments();
  $mArticles=new App_Model_Articles();
  $mArticlesOptions=new App_Model_ArticlesOptions();
  $mArticlesDetail=new App_Model_ArticlesDetail();
  $mArticlesDetailCategory=new App_Model_ArticlesDetailCategory();
  $slug=$uriRule['sub'][1];
  $fila=$mArticles->getByIdUsuario($slug);
  $numreg=is_array($fila) ?  count($fila) : 0;
  $rowOptions=$mArticlesOptions->getById(1);
  $rowArtDet=$mArticlesDetail->getListarTag($fila[0]['idx']);
  $num_rowArtDet=count($rowArtDet);
  $rowArtDetCat=$mArticlesDetailCategory->getListarPostedIn($fila[0]['idx']);
  $num_rowArtDetCat=count($rowArtDetCat);
  $sql_0="update articles set conteo=conteo+1 where idx ='".$fila[0]['idx']."'";
  if (!$res_0 = ejecutar($sql_0)) {
    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
    exit();
  }
?>
<div class="container">
  <div class="contenidointerno blog-details">  
  <?php 
    if($numreg!=0)
    {?>
      <link href="<?=PLUGINS?>articles/css/estilos.css" rel="stylesheet" type="text/css" />
      <style>
        .blog-date{background:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
        .link-date a, .link-date a:hover{color:<?=$atabla_opt['fondo_btn']?>}
        .contenidointerno .blogtitle a, .contenidointerno .blogtitle a:hover, .blog-detail, .blog-detail p, .blog-detail ul, .blog-detail li{color:<?=$atabla_opt['color_text_descrip']?>;}
        .btn-read-more-article, .btn-read-more-article:hover{background:<?=$atabla_opt['fondo_btn']?>; border-color:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
      </style>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1"; fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
      <!-- twitter. -->
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
      <!-- Place this tag after the last +1 button tag. -->  
      <div class="post">
        <div class="blogtitle">
          <h1><?= $fila[0]['nombre'] ?></h1>
          <div class="date-mobile" style="display: block;">
            <i class="fa fa-calendar"></i>  <?= strftime('%d / %b / %Y', strtotime($fila[0]['fecreg'])) ?> 
          </div>
        </div>
        <div cms-cols="col c100 cs100 cx100 " class="article-post">                
            <div class="thumbnail-blog">                            
              <div class="caption">      
                <div class="blog-metas">
                  <ul>
                    <li>
                      <i class="fa fa-user"></i>  
                      <?=isset($dataGlobal['label_by']) ? $dataGlobal['label_by'] : 'By' ?>:
                      <?php $urlAutor=SERVER."blog/author/".$fila[0]["usu_slug"];?>
                      <?php if($fila[0]["usu_nombres"] == "" and $fila[0]["usu_apellidos"]==""):?>
                        <a href="<?=$urlAutor?>"><?= $fila[0]["usu_username"]?></a>
                      <?php else:?>
                        <a href="<?=$urlAutor?>">
                          <?= $fila[0]["usu_nombres"]?> <?=$fila[0]["usu_apellidos"]?>
                        </a>
                      <?php endif;?>                                
                    </li>
                    <?php if(isset($rowOptions[0]['comments_blog']) && $rowOptions[0]['comments_blog']==1) {?>
                    <li class="comments">
                      <?php 
                        $sql_5 = "SELECT idx FROM comments where article_id = '".$fila[0]["idx"]."' and comment_status_id='2' ";
                        $res_5=ejecutar($sql_5);
                        $regNum=numRows($res_5);     
                      ?>
                      <?=$regNum?> 
                      <i class="fa fa-comments"></i> 
                      <?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?>
                    </li>
                    <?php
                    } 
                    $atabla_opt['twitter_share']=$rowOptions[0]['twitter_share'];
                    $atabla_opt['face_share']=$rowOptions[0]['face_share'];
                    $url_article=$urlCanonical;
                    $fila["imagen"]=$fila[0]["imagen"];
                    $fila["nombre"]=$fila[0]['nombre'];
                    include("_blog-social-inc.php"); 
                    if($rowOptions[0]['twitter_like']==1 || $rowOptions[0]['face_like']==1)
                    { ?>
                      <li class="social" style="margin:0px  20px;">
                        <i class="fa fa-ellipsis-v fa-lg"></i>
                      </li>
                      <?php
                    }if($rowOptions[0]['twitter_like']==1)
                    { ?>
                        <li class="social">
                          <div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?=$urlCanonical?>">Tweet</a>
                          </div>
                        </li>
                      <?php 
                    }
                    if($rowOptions[0]['face_like']==1)
                    { ?>
                        <li class="social">
                          <div  class="fb-like" data-href="<?=$urlCanonical?>" data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>
                        </li>
                      <?php 
                    }?>
                  </ul>
                </div>
                <div class="blog-detail"><?= $fila[0]['descrip'] ?></div>
                <div class="blog-categories">
                  <?php 
                    if($num_rowArtDetCat != 0)
                    { ?>
                      <h4><?=isset($dataGlobal['posted_in']) ? $dataGlobal['posted_in'] : 'Posted in' ?></h4>
                      <div class="categories">
                        <ul> 
                          <?php 
                            foreach ($rowArtDetCat as $item)
                            { ?>
                              <li>
                                <a href="<?=SERVER?>blog/category/<?=$item["tag_slug"]?>">
                                  <?=$item["tag_nombre"]?>
                                </a>
                              </li>
                              <?php 
                            }
                          ?>
                        </ul>
                      </div>
                      <?php 
                    }
                  ?>                       
                </div>
                <div class="blog-tags">
                  <?php 
                    if($num_rowArtDet != 0)
                    { ?>
                      <h4><?=isset($dataGlobal['label_tags']) ? $dataGlobal['label_tags'] : 'Tags' ?></h4></h4>
                      <div class="categories">
                        <ul> 
                          <?php 
                            foreach ($rowArtDet as $item)
                            { ?>
                              <li>
                                <a href="<?=SERVER?>blog/tag/<?=$item["tag_slug"]?>">
                                  <?=$item["tag_nombre"]?>
                                </a>
                              </li>
                              <?php 
                            }
                          ?>
                        </ul>
                      </div>
                      <?php 
                    }
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="comentarios">
      <hr />
      <?php if($rowOptions[0]['comments_blog']==1 || $rowOptions[0]['face_comment']==1) { ?>
      <div class="caption-comments">
          <span class="ico-comm"></span>
          <?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?>:
      </div>
      <?php } ?>
        <div class="menu-comentarios">
          <?php if($rowOptions[0]['comments_blog']==1 && $rowOptions[0]['face_comment']==1) { ?>
          <ul class="menu-comments">
            <li onclick="cm_blog(this);"><?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?> <?=isset($dataGlobal['blog']) ? $dataGlobal['blog'] : 'Blog'?> </li> 
            <li  onclick="cm_face(this);"><?=isset($dataGlobal['facebook_comments']) ? $dataGlobal['facebook_comments'] : 'Facebook Comments'?></li> 
          </ul>
          <?php } ?>
        </div>
        <div class="content-comentarios">
          <?php 
            if($rowOptions[0]['comments_blog']==1)
            {
              if(isset($_SESSION['saved_comment']))
              { ?>
                <div class="art_comment"><?=$_SESSION['saved_comment']?></div>
                <?php 
                unset($_SESSION['saved_comment']);
              }              
            }
            if($rowOptions[0]['face_comment']==1)
            { ?>
              <style>iframe.fb_iframe_widget_lift {width: 100% !important;}</style>
              <div class="comentarios-face " >
                  <div class="fb-comments" data-href="<?=$urlCanonical?>" data-width="100%" data-numposts="5"></div>
              </div>
              <?php 
            }
            if($rowOptions[0]['comments_blog']==1)
            { ?>
              <div class="comentarios-blog" style="<?php if($rowOptions[0]['face_comment']==1){echo'display: none;';}?>">
                <?php include('_blog-detail-coment-inc.php'); ?>
              </div>
              <?php 
            } 
              if($rowOptions[0]['comments_blog']==1 && $rowOptions[0]['face_comment']==1) 
              { ?>
                <script>
                  var verBlog = document.querySelector(".comentarios-blog");
                  var verFace = document.querySelector(".comentarios-face");
                  var cm_blog = function(el)
                  {                  
                    $(verBlog).show('slow');
                    $(verFace).hide('slow');
                  }
                  var cm_face = function(el)
                  {
                    $(verBlog).hide('slow');
                    $(verFace).show('slow');
                  }
                </script>  
                <?php
              } 
          ?>
        </div>
      </div>
      <?php 
    }
  ?>
  </div>
</div>
<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'footer.php'; ?>