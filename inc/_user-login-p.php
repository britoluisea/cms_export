<?php
    include dirname(__FILE__).'/../'.$_SESSION['THEME'].'head.php';
    if (isset($_SESSION['user']) && (!empty($_SESSION['user']['login']) or $_SESSION['user']['login'] == true)) 
    {
        header("Location: my-account.php");
        exit();
    }
    include('app/model/Usuarios.php'); 
    include('app/FormValidator.php'); 
    include('app/ValidationRule.php'); 
    $validator = new App_FormValidator();
    $mUsuarios = new App_Model_Usuarios();
    if ($_SERVER['REQUEST_METHOD'] == 'POST') 
    {        
        $validator->addRule('pwd', 'Is required.', 'required');
        $validator->addRule('username', 'Is required.', 'required');
        $validator->addRule('username', 'Please enter a valid email address.', 'email');
        //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');
        $validator->addEntries($_POST);
        $validator->validate();
        $entries = $validator->getEntries();
        foreach ($entries as $key => $value){${$key} = $value;}
        if ($validator->foundErrors()) {$errors = $validator->getErrors();}
        if (empty($errors)){$mUsuarios->login($entries,'1');}
        echo'<script>window.parent.closeModal_reload();</script>';
    }
?>
<form enctype="application/x-www-form-urlencoded" action="" method="post" name="myform" id="myform">  
    <div class="modalcread modalcread-edit">  
                <div class="modal-header ">
                    <div class="edit"> <h3>Please Enter Your Information</h3></div>
                </div>
                <div class="modal-body">
                <?php if(isset($_SESSION['user']['saved'])): ?>
                    <div  class="alert  alert-success" align="center"><?=$_SESSION['user']['saved']?></div>
                     <?php unset($_SESSION['user']['saved']);   ?>
                <?php endif;?>
                <?php if(isset($_SESSION['user']['error_correo'])): ?>
                    <div  class="alert  alert-error" align="center"><?=$_SESSION['user']['error_correo']?></div>
                     <?php unset($_SESSION['user']['error_correo']);    ?>  
                <?php endif;?>
                    <div class="row-fluid">
                        <fieldset>
                            <div class="form-group">
                                <label class="sr-only" for="username">Email</label>
                                <div class="input-group">
                                    <input type="text" name="username" id="username" value="<?=isset($username) ? $username : ''?>" placeholder="Email" autocomplete="off" class="form-control">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                </div>
                                <div class="help-inline"><?=isset($errors['username']) ? $errors['username'] : '' ?></div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="pwd">Password</label>
                                <div class="input-group">
                                    <input type="password" name="pwd" id="pwd"  placeholder="Password"  class="form-control">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                </div>
                                <div class="help-inline"><?=isset($errors['pwd']) ? $errors['pwd'] : '' ?></div>
                                <?php if (isset($_SESSION['user']['error'])): ?>
                                    <div  class="alert alert-error"><?= $_SESSION['user']['error'] ?></div>
                                    <?php unset($_SESSION['user']['error']); ?>
                                <?php endif; ?>
                            </div>
                            <div class="space"></div>
                        </fieldset>
                    </div>
                <div align="right" class="class_register"><a href="<?=SERVER?>_user-register-p" >I want to register &raquo;</a></div>
                <div align="right" class="class_register"><a href="<?=SERVER?>_user-forgot-p" >I forgot my password &raquo;</a></div>
                </div>
                <div class="modal-footer">
                    <button name="button" type="submit" class="btn btn-small btn-primary" id="button"><i class="fa fa-key"></i> Login</button>
                </div>
    </div>
</form>
<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'foot.php'; ?>        