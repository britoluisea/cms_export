<?php
session_start();
if(empty($_SESSION['user']['login']) or $_SESSION['user']['login']<> true )
{
	header("Location: login.php");
	exit();	
}
?>
<?php
unset($_SESSION['user']);
unset($_SESSION['value_array']);
//session_destroy();
header("Location: rating-reviews.php");
?>