<?php
	$page_default=dirname(__FILE__).'/../template/'.$_SESSION['THEME'].'/template-pages/default.php';
	if(is_file($request_file))
	{
		//echo $request_file .' aqui';
		include_once $request_file; 
	}
	elseif(is_file($page_default) && $url_request['request_file'] != 'error404.php')
	{
		//echo $page_default.' default';
		include_once $page_default;
	}
	else
	{ 
	    include_once dirname(__FILE__).'/error404.php'; 
	}