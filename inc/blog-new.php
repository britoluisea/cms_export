<?php include('header.php') ?>
<?php require_once('app/pag/Paginator.php'); ?>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="articles/css/estilos.css" rel="stylesheet" type="text/css" />

<div class="contenedor_interno">
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
<div class="contenidointerno">
 <div class="categ_title">Blog</div>   
<?php
$sql_opt="SELECT * FROM articles_options where idx='1' limit 0,1";
$consulta_opt=ejecutar($sql_opt);
$atabla_opt=array();
if($fila_opt=fetchAssoc($consulta_opt))
{
	$atabla_opt=$fila_opt;	
}

$t="articles,usuarios";	
$sql="select 
articles.*,
usuarios.idx as usu_idx, 
usuarios.username as usu_username,
usuarios.nombres as usu_nombres,
usuarios.apellidos as usu_apellidos,
usuarios.userlevel as usu_userlevel,
usuarios.slug as usu_slug
from
$t
where
articles.idx_usuario = usuarios.idx and 
articles.activo='1'
order by articles.fecreg desc ";
$consulta=ejecutar($sql);
$num_reg=numRows($consulta);
$pages = new App_Pag_Paginator();
$pages->items_total = $num_reg;
$pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
$pages->paginate($atabla_opt['num_reg_page'], 'blog'); 	//cuantos debe de mostrar por pagina

$consulta=ejecutar($sql.$pages->limit);
//echo $num_reg=numRows($consulta);
while($fila=fetchArray($consulta))
{
?>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100" valign="top">
    <div class="entry-date">
    <a href="blog/<?= $fila['slug'] ?>" title="<?= strftime('%b %d, %Y', strtotime($fila['fecreg'])) ?> <?= strftime('%I:%M:%S %p', strtotime($fila['fecreg'])) ?>">
    	<span class="entry-date-day">
          <?= strftime('%d', strtotime($fila['fecreg'])) ?>
        </span>
    	<span class="entry-date-month-year">
        <?= strftime('%b %Y', strtotime($fila['fecreg'])) ?>
        </span>
    </a>
    </div>

    </td>
    <td valign="top">
    <div class="entry-title"><a href="blog/<?= $fila['slug'] ?>"><?= $fila['nombre'] ?></a></div>
    <hr style="background-color:#cccccc; border:0; height: 1px;">
     <div>
     <span>
     <?php $urlAutor="blog/author/".$fila["usu_slug"];?>
        <?php if($fila["usu_nombres"] == "" and $fila["usu_apellidos"]==""):?>
        		<a href="<?=$urlAutor?>"><?= $fila["usu_username"]?></a>
        	<?php else:?>
            	<a href="<?=$urlAutor?>"><?= $fila["usu_nombres"]?> <?=$fila["usu_apellidos"]?></a>
		<?php endif;?>
     </span>
      <span>-</span> 
      <span><a href="javascript:void(0)">0 Comment</a></span>
     </div>                                   
    </td>
  </tr>
  <tr>
    <td  colspan="2" valign="top">
         <div><?= ($fila['detail']) ?></div>
                                   
    </td>
    </tr>
</table>

<br />
<br />
<br />
                            
<?php
}			
?>  
 <?php if ($pages->num_pages > 1) :?>
             <div align="center" style="padding:18px;"><?=$pages->display_pages();?> </div>
 <?php endif; ?>   
</div>
    </td>
    <td width="10" valign="top">&nbsp;</td>
    <td width="238" valign="top">
   <div> <?php include('article-sidebar.php');?></div>
    </td>
  </tr>
</table>
</div>

<?php include('footer.php')?>