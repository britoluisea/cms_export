<?php 
require_once "app/model/ProductDGroup.php";
require_once "app/model/ProductDCategory.php";
$mProductDGroup = new App_Model_ProductDGroup();
$mProductDCategory = new App_Model_ProductDCategory();
$listaGrupo = $mProductDGroup->getListarLateral();
require_once('paginator.class.php');
$pages = new Paginator();
$control_paginas=0;
$minPage= 6;
$id_control=0; //id con maximos item
$alias = isset($dataGlobal['url_projects']) ? $dataGlobal['url_projects'] : 'projects';
?>
    <link rel="stylesheet" href="<?=PLUGINS?>project-category/css/style.css">    
    <div class="" cms-cols="col c-3 cx-12"> <?php include('_projects_lateral.php'); ?> </div>
    <div class="" cms-cols="col c-9 cx-12">
        <?php foreach ($listaGrupo as $group) : ?>
            <div class="category-child">
                        <div class="group"><?= strtoupper($group['nombre']) ?></div>
                <?php
                require_once ("app/model/ProductDLista.php");
                require_once ("app/model/ProductDListaDet.php");
                $mProductDLista     = new App_Model_ProductDLista();
                $mProductDListaDet  = new App_Model_ProductDListaDet();
                $categorys = array();
                $categorys = $mProductDCategory->getByIdGrupo($group['idx']);    
                foreach ($categorys as $cat) 
                {                       
                    $id=$cat['idx'];
                    $rowCat=$mProductDCategory->getById($id);
                    $filas = $mProductDLista->getListar($id);
                    $numReg = count($filas);
                    //echo $id.'  <br>';
                    if($control_paginas < $numReg){$control_paginas=$numReg; $id_control=$id;}
                    $pages->items_total = $numReg;
                    $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
                    $pages->paginate($minPage); //cuantos debe de mostrar por pagina
                    $filasPags = array();
                    $filasPags = $mProductDLista->getListarPagination($id, $pages->limit);
                    //print_r($rowCat); echo '<br>';
                    $numRegPag = count($filasPags);
                    ?>
                    <div class="cat_nombre">
                        <h4 style=""><?=$rowCat['nombre']?></h4>
                    </div><?php
                        if ($numRegPag == 0) { ?> <div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?>.</div> <?php  }
                        else
                        { //print_r($filas); echo '<br>';
                                foreach ($filasPags as $item) 
                                { 
                                    if($item['idcategorias'] ==$id ) 
                                    {
                                        ?>
                                        <div cms-cols="col c-4 cs-6 cx-12">
                                            <div class="thumb-details">
                                                <div class="caption post-content">
                                                    <a  href="<?=SERVER.$alias?>/<?=$rowCat['slug']?>/<?=$item['slug']?>">
                                                        <span><?= $item['nombre'];?> </span>
                                                    </a>
                                                </div>
                                                <a class="thumb-cat" href="<?=SERVER.$alias?>/<?=$rowCat['slug']?>/<?=$item['slug']?>" >
                                                    <img class="img-responsive" src="<?=SERVER?>imgcms/product_d/<?=$item['nomcar']?>/thumbs/<?=$item['imagen']?>" alt="<?= $item['nombre'];?>">
                                                </a>
                                                <div class="detail">
                                                    <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                                        <li style="width:48%;     padding-left: 5px !important;"><strong><?= $item['location'];?></strong></li>
                                                        <li style="width:48%;text-align: right;">
                                                            <a href="<?=SERVER.$alias?>/<?=$rowCat['slug']?>/<?=$item['slug']?>" class="btn btn-default  " rel="tooltip" title="Photos" style="    padding: 3px 5px;">
                                                                <span class="fa fa-photo  " >
                                                                    <strong style="font-weight: 100;">
                                                                        <?=isset($dataGlobal['see_this_idea']) ? $dataGlobal['see_this_idea'] : 'See this idea' ?> 
                                                                    </strong>
                                                                </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="fnt-lighter">
                                                        <span class="description"><?= nl2br($item['descripcion']);?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } 
                                } ?>
                                <?php
                        } ?>
                        <div class="clear"></div>
                        <?php
                } ?>
            </div>
        <?php endforeach; ?>
        <?php 
        $pages = new Paginator();
        $pages->items_total = $control_paginas;
        $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
        $pages->paginate($minPage); //cuantos debe de mostrar por pagina
        $filasPag = $mProductDLista->getListarPagination($id_control, $pages->limit);
        $numRegPag = count($filasPag);
        if ($pages->num_pages > 1) 
        { ?>
            <div align="center" style="padding:8px;">
                <div class="btn-group" role="group">
                    <?php echo $pages->display_pages(); ?>
                </div>
            </div><?php
        } ?>
    </div>
            