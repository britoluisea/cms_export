<?php 
    //------------------------------------------------------------------------------------------------
    session_start();
    //------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
    <html>
        <head>
            <title>video</title>
            <style>
                #widgetVideo{                    
                    background-image:url('<?=$_SESSION['SERVER']?>imgcms/system/loading.gif');
                    background-size: auto;
                    background-position:center;
                    background-repeat:no-repeat;
                }
            </style>
        </head>
    <body style="padding:0px; margin:0px;">
        <video id="widgetVideo" loop <?php if(isset($_GET['muted']) && $_GET['muted']==0){echo' muted ';} ?>  style="width: 100%;">
            <source src="<?=$_SESSION['SERVER']?>imgcms/headers/video/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
        </video>
    </body>
</html>