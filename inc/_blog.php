<?php 
  include('app/pag/PaginatorBlog.php');
  $search = isset($_POST['search']) ? $_POST['search'] : '';
  $sql_opt="SELECT * FROM articles_options where idx='1' limit 0,1";
  $consulta_opt=ejecutar($sql_opt);
  $atabla_opt=array();
  if($fila_opt=fetchAssoc($consulta_opt))
  {
    $atabla_opt=$fila_opt;  
  }
  $t="articles,usuarios"; 
  $sql="select 
    articles.*,
    usuarios.idx as usu_idx, 
    usuarios.username as usu_username,
    usuarios.nombres as usu_nombres,
    usuarios.apellidos as usu_apellidos,
    usuarios.userlevel as usu_userlevel,
    usuarios.slug as usu_slug
    from
    $t
    where
    articles.idx_usuario = usuarios.idx and 
    articles.activo='1' and
    (articles.nombre like '%$search%')
    order by articles.fecreg desc ";
  $consulta=ejecutar($sql);
  $num_reg=numRows($consulta);
  $pages = new App_Pag_Paginator();
  $pages->items_total = $num_reg;
  $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
  $pages->paginate($atabla_opt['num_reg_page'], 'blog');  //cuantos debe de mostrar por pagina
  $consulta=ejecutar($sql.$pages->limit);
  //echo $num_reg=numRows($consulta);
?>
<link href="<?=PLUGINS?>articles/css/estilos.css" rel="stylesheet" type="text/css" />
<style>
  .blog-date{background:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
  .link-date a, .link-date a:hover{color:<?=$atabla_opt['fondo_btn']?>}
  .contenidointerno .blogtitle a, .contenidointerno .blogtitle a:hover, .blog-detail, .blog-detail p, .blog-detail ul, .blog-detail li{color:<?=$atabla_opt['color_text_descrip']?>;}
  .btn-read-more-article, .btn-read-more-article:hover{background:<?=$atabla_opt['fondo_btn']?>; border-color:<?=$atabla_opt['fondo_btn']?>; color:<?=$atabla_opt['color_text_btn']?>;}
</style>
<div class="container" >
  <div class="contenidointerno"> 
    <!--Blog content here-->
    <?php
      if($num_reg==0)
      { ?>
        <div align="center" style="color:#F00">
          <?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?>.
        </div>
        <?php 
      }
      else
      { 
        while($fila=fetchArray($consulta))
        {
          $url_article=SERVER.'blog/'. $fila['slug'] ; ?>
          <div class="post">
            <div cms-cols="col c10 cs20 cx20 " class="col-date">
              <div class="blog-date">
                <div class="head-date"><i class="fa fa-calendar fa-2x"></i></div>
                <div class="body-date" >
                  <?= strftime('%d', strtotime($fila['fecreg'])) ?>
                </div>
                <div class="foot-date">
                  <?= strftime('%b - %Y', strtotime($fila['fecreg'])) ?> 
                </div>
              </div>
              <div class="link-date" >
                <a href="<?=$url_article?>">                  
                  <i class="fa fa-link"></i>
                </a>
              </div>
            </div>
            <div cms-cols="col c90 cs80 cx80 " class="article-post">                
                <div class="thumbnail-blog">
                  <div class="blogtitle">
                    <h2><a href="<?=$url_article?>"><?= $fila['nombre'] ?></a></h2>
                    <div class="date-mobile">
                      <i class="fa fa-calendar"></i>  <?= strftime('%d / %b / %Y', strtotime($fila['fecreg'])) ?> 
                    </div>
                  </div>
                  <?php 
                    if($fila['imagen_show']==1)
                    {   
                      if(!empty($fila['imagen']))
                      { 
                        $wimg=SERVER."imgcms/articles/thumbs/".$fila['imagen'];  ?>
                        <div class="post-img">
                          <a href="<?=$url_article?>"><img src="<?php echo $wimg ?>" alt="<?= $fila['nombre'] ?>"  /></a>
                        </div>  
                        <?php 
                      }
                    } 
                  ?>
                  <div class="caption">      
                    <div class="blog-metas">
                      <ul>
                        <li>
                          <i class="fa fa-user"></i>  
                          <?=isset($dataGlobal['label_by']) ? $dataGlobal['label_by'] : 'By' ?>:
                          <?php $urlAutor=SERVER."blog/author/".$fila["usu_slug"];?>
                          <?php if($fila["usu_nombres"] == "" and $fila["usu_apellidos"]==""):?>
                            <a href="<?=$urlAutor?>"><?= $fila["usu_username"]?></a>
                          <?php else:?>
                            <a href="<?=$urlAutor?>">
                              <?= $fila["usu_nombres"]?> <?=$fila["usu_apellidos"]?>
                            </a>
                          <?php endif;?>                                
                        </li>
                        <?php if(isset($fila_opt['comments_blog']) && $fila_opt['comments_blog']==1) { ?>
                          <li class="comments">
                            <?php 
                              $sql_5 = "SELECT idx FROM comments where article_id = '".$fila["idx"]."' and comment_status_id='2' ";
                              $res_5=ejecutar($sql_5);
                              $regNum=numRows($res_5);     
                            ?>
                            <?=$regNum?> 
                            <i class="fa fa-comments"></i> 
                            <?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments' ?>
                          </li>
                        <?php } ?> 
                        <?php include("_blog-social-inc.php"); ?>
                      </ul>
                    </div>
                    <div class="blog-detail"><?= $fila['detail'] ?></div>
                    <div class="blog-btn">
                      <a href="<?= $url_article ?>" class="btn-cms btn-blue btn-read-more-article" >
                        <?=isset($dataGlobal['read_more']) ? $dataGlobal['read_more'] : 'Read More' ?>  | 
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <hr class="_hr_b">
          <?php
        }
      } 
    ?>
    <!--Blog content ends here-->
    <!--Pagination-->
    <?php if ($pages->num_pages > 1) :?>
      <div align="center"><?=$pages->display_pages();?></div> 
    <?php endif; ?> 
  </div>
  <?php include('_blog-sidebar.php');?>
</div>