<?php  include("insert_foot_css_js.php");?>

<div class="modal fade" id="iframeModal">
  <div class="modal-dialog">
    <div class="modal-content bmd-modalContent" style="border-radius: 5px;border: 7px solid;">
      <div class="modal-body">
        <div class="close-button">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 10px;right: 35px;z-index: 1;font-size: 30px;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="embed-responsive embed-responsive-16by9" id="embed-responsive-conter">
          <iframe class="embed-responsive-item" frameborder="0"></iframe>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php  if(isset($lib_swiper) && $lib_swiper == 1) { ?>
  <link href="<?=PLUGINS?>swiper/swiper.min.css"  rel="stylesheet" >
  <script src="<?=PLUGINS?>swiper/swiper.min.js"></script>
  <script>
      $(document).ready(function() {
        var s = document.querySelector('#sliderShow');
        if(s)
        {
          $(s).show();
          var autoplayVal= s.getAttribute('data-autoplay') ? s.getAttribute('data-autoplay') === "false" ? undefined : s.getAttribute('data-autoplay') : 5000; 
          var d = s.getAttribute('data-direction') ? s.getAttribute('data-direction') : "horizontal"; 
          var e = s.getAttribute('data-slide-effect') ? s.getAttribute('data-slide-effect') : "slide";
          var t = s.getAttribute('data-slide-speed') ? s.getAttribute('data-slide-speed') : 600;
          var swiper = new Swiper('#sliderShow', {      
            slidesPerView: 1,
            spaceBetween: 0,
            centeredSlides: true,
            autoHeight: true,
            loop: true,
            direction: d, // horizontal | vertical
            effect: e, // slide | fade | cube | flip | coverflow
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
              rotate: 50,
              stretch: 0,
              depth: 100,
              modifier: 1,
              slideShadows : true,
            },
            speed: 1000,
            cubeEffect: {shadow: true,slideShadows: true,shadowOffset: 20,shadowScale: 0.94,},
            autoplay: {delay: autoplayVal,disableOnInteraction: false,},
            pagination: {el: '.swiper-pagination',clickable: true,},
            navigation: {nextEl: '.swiper-button-next',prevEl: '.swiper-button-prev',},
          });
        }
    });
  </script>
<?php }if(isset($lib_review) && $lib_review == 1) { ?>
<script src='<?=PLUGINS?>star-rating/js/jquery.MetaData.js' type="text/javascript"></script>
<script src='<?=PLUGINS?>star-rating/js/jquery.rating.js' type="text/javascript"></script>
<link href='<?=PLUGINS?>star-rating/css/jquery.rating.css' type="text/css" rel="stylesheet"/>
<link href='<?=PLUGINS?>star-rating/css/style.css' type="text/css" rel="stylesheet"/>
<?php } if(isset($btnLike) && $btnLike == 1) { ?>
<script>
    $(".btnLike").click(function () {
        var id = $(this).data('id');
        //alert(id);
        //return false;
        $.ajax({
            type: "POST",
            url: "<?=SERVER._INC?>rating-reviews-like-unlike.php",
            //dataType: 'html',
            dataType: 'json',
            data: {
                id: id,
                opc: 'like',
            },
            beforeSend: function (objeto) {
                $('#btnLike_' + id).attr('disabled', 'disabled');
                //$('#btnLike_'+id).hide(); 
            },
            complete: function (objeto, exito) {
                if (exito == "success") {
                    //$('#loader').fadeIn('fast', function() {  $(this).remove()        });
                    $('#btnLike_' + id).removeAttr('disabled');
                    //$('#like-yes-'+id).show();
                }
            },
            success: function (data) {
                <?php if ($atabla_opt['counter_show'] == 1): ?>
                    $('#count_like_' + id).html(data.count_like);
                <?php else: ?>
                    $('#notificationLike_' + id).html('<span id="styleNotLike' + id + '" class="styleNotLike"></span>');
                    $('#styleNotLike' + id).hide(0).html('Data Saved.');
                    $('#styleNotLike' + id).slideDown('slow');
                    setTimeout(function () {
                        $('#styleNotLike' + id).slideUp('slow');
                    }, (4000));
                <?php endif; ?>
            },
        });
    });
    $(".btnUnlike").click(function () {
        var id = $(this).data('id');
        //alert(id);
        $.ajax({
            type: "POST",
            url: "<?=SERVER._INC?>rating-reviews-like-unlike.php",
            //dataType: 'html',
            dataType: 'json',
            data: {
                id: id,
                opc: 'unlike',
            },
            beforeSend: function (objeto) {
                $('#btnUnlike_' + id).attr('disabled', 'disabled');
                //$('#btnLike_'+id).hide(); 
            },
            complete: function (objeto, exito) {
                if (exito == "success") {
                    //$('#loader').fadeIn('fast', function() {  $(this).remove()        });
                    $('#btnUnlike_' + id).removeAttr('disabled');
                    //$('#like-yes-'+id).show();
                }
            },
            success: function (data) {
                <?php if ($atabla_opt['counter_show'] == 1): ?>
                    $('#count_unlike_' + id).html(data.count_unlike);
                <?php else: ?>
                    $('#notificationLike_' + id).html('<span id="styleNotUnlike' + id + '" class="styleNotUnlike"></span>');
                    $('#styleNotUnlike' + id).hide(0).html('Data Saved.');
                    $('#styleNotUnlike' + id).slideDown('slow');
                    setTimeout(function () {
                        $('#styleNotUnlike' + id).slideUp('slow');
                    }, (4000));
                <?php endif; ?>
            },
        });
    });
</script>    
<?php } if(isset($star_write) && $star_write == 1) { ?>
<script>
  $('.star-big-form').rating({
    callback: function(value){
      var tip = $('#hover-test');
      tip.html(value);
    },
  });
</script>
<?php } if(isset($gallery_model1) && $gallery_model1 == 1) { ?>
    <script src="<?=PLUGINS?>/gallery/galleria-1.4.2.min.js"></script>
    <script src="<?=PLUGINS?>/gallery/themes/azur/galleria.azur.min.js"></script>
     <script>
        Galleria.run('.galleria_<?=$gallery_model1_ID?>', {
        autoplay: true, // will move forward every 7 seconds
        showInfo: true,
        responsive: true,
        imageCrop: false,
        wait: true
    });
</script>
<?php } if(isset($gallery_model2) && $gallery_model2 == 1) { ?>
    <script src="<?=PLUGINS?>/gallery/galleria-1.4.2.min.js"></script> 
    <script src="<?=PLUGINS?>/gallery/themes/folio/galleria.folio.min.js"></script>   
        <script>
        Galleria.run('.galleria_<?=$gallery_model2_ID?>', {
            responsive: true,
            imageCrop: false,
        });
        </script>
<?php } if(isset($projetc_model1) && $projetc_model1 == 1) {?>
<script src="<?=PLUGINS?>project-category/galleria/galleria-1.4.2.min.js"></script>
    <script src="<?=PLUGINS?>project-category/galleria/themes/azur/galleria.azur.min.js"></script>
    <div class="content_model_<?=$projetc_model1_ID?>">
<script>
    Galleria.run('.galleria_<?=$projetc_model1_ID?>', {
      autoplay: true, // will move forward every 7 seconds
    showInfo: true,
    responsive: true,
    imageCrop: false,
    wait: true
     });
    </script>
<?php }if(isset($projetc_model2) && $projetc_model2 == 1) {?>
  <script src="<?=PLUGINS?>project-category/galleria/galleria-1.4.2.min.js"></script>
  <script src="<?=PLUGINS?>/project-category/galleria/themes/folio/galleria.folio.min.js"></script>     
  <div class="content_model_<?=$projetc_model2_ID?>">
  <script>
  Galleria.run('.galleria_<?=$projetc_model2_ID?>', {
      responsive: true,
      imageCrop: false,
  });
  </script> 
<?php } ?>  
<script>
  var alinkReload = function(alink, target){
    //debugger;
      if(alink!='#' && alink.includes('#') === false&& alink.includes('mailto:') === false && alink.includes('tel:') === false)
      {
        var valid = alink.indexOf('<?=SERVER?>');
            var s ='';
            var r ='';
            if(valid==-1)
            {
              if(alink[0]=='/')
              {
                  r = '<?=SERVER?>' + '<?=(isset($preview) && $preview==1) ? "preview" : ""?>' + alink;
              }
              else
              {
                  r =  alink;
              }
            }
            else
            {
              s = alink.split('<?=SERVER?>');
              if(alink[0]=='/')
              {
                  r = '<?=SERVER?>' + '<?=(isset($preview) && $preview==1) ? "preview" : ""?>' + s[1];
              }
              else
              {
                  r = '<?=SERVER?>' + '<?=(isset($preview) && $preview==1) ? "preview/" : ""?>' + s[1];
              }
            }
        console.log('nueva ruta = '+r);
            if(target!='')
            {window.open(r, target);}
            else{location.href= r;}       
      }
      else
      {
            if(alink.includes('#sliderShow') === false && alink.includes('#javascrip()') === false)
            {location.href= alink;}       
      }
    }
    var alinkPreview = function(alink){
      if(alink!='#' && alink.includes('#') === false && alink.includes('mailto:') === false && alink.includes('tel:') === false)
      {
        var s = alink.split('<?=SERVER?>');
        var r = '<?=SERVER?>' + '<?=(isset($preview) && $preview==1) ? "preview/" : ""?>' + s[1];
        console.log('nueva ruta = '+r);
        return r;
      }
      else
      {
            if(alink.includes('#sliderShow') === false && alink.includes('#javascrip()') === false )
            {location.href= alink;}   
      }
    }
  $(function() {
    $.fn.bmdIframe = function( options ) {
      var self = this;
      var settings = $.extend({
              classBtn: '.bmd-modalButton',
              defaultW: '640px',
              defaultH: '360px'
          }, options );
          $(settings.classBtn).on('click', function(e) {
            var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
            var dataStyle ={            
          'height': $(this).attr('data-bmdHeight') || settings.defaultH,
          'width': $(this).attr('data-bmdWidth') || settings.defaultW,
            };
        var widthModal = (parseInt(dataStyle.width.substr(-5, 3)) + parseInt(30)) +'px'; 
            if($(window).width() < 768 )
            {
              dataStyle.width='100%';
              widthModal='100%';
            }
            var src = alinkPreview(alink= $(this).attr('data-bmdSrc'));
            console.log(src);
            var dataAttr = {
          'src': src,
          'scrolling': $(this).attr('data-scrolling') || 'no',
        };
        // obteniendo wl width y le sumo 30 del padding para q el iframe se mantenga dentro del modal
        if(dataStyle.width.substr(-1) == '%')
        {
          var widthModal =dataStyle.width;
          $('#iframeModal .close-button').css('display', 'none');
          $('#iframeModal .modal-dialog').css({'width': widthModal, 'height': widthModal, 'margin': 0});
          $('#iframeModal .modal-content').css({'width': widthModal, 'height': widthModal});
        }
        else
        {
          var widthModal = parseInt(dataStyle.width.substr(-5, 3)) + parseInt(30); 
          widthModal = widthModal+'px'; 
          $('#iframeModal .modal-dialog').css('width', widthModal);
        }
        $('#embed-responsive-conter').css(dataStyle);
        if ( allowFullscreen ) {dataAttr.allowfullscreen = "";}
        // stampiamo i nostri dati nell'iframe
        //debugger;
        $(self).find("iframe").attr(dataAttr);
        $(self).find("iframe").css(dataStyle);
          });
          // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale Ã¨ chiusa
          this.on('hidden.bs.modal', function(){
            $(this).find('iframe').html("").attr("src", "");
          });
          return this;
      };
  });
  window.closeModal_reload = function(){ 
    $('#iframeModal').modal('hide'); 
    window.location.reload();
  };
  window.closeModal = function(){ 
    $('#iframeModal').modal('hide'); 
  };
  $(document).ready(function(){
      $("#iframeModal").bmdIframe();
      var WGiframe = document.querySelectorAll('.WGiframe');
      if(WGiframe.length > 0)
      {
        setTimeout(function()
        {
          for (var i = 0; i < WGiframe.length; i++) {
            let element = WGiframe[i];
            let idBox =  $(element).attr('data-box') ? parseInt($(element).attr('data-box')) : 0; 
            if((typeof  idBox === 'number') && idBox > 0 ) {            
              $(element).attr('src', '<?=SERVER._INC?>_widget_iframe.php?idbox='+idBox);            
            }
          }
        }, 2000);      
        setTimeout(function()
        {
          $('.WGiframe').contents().find('body').css('margin', '0px');
        }, 3000);
      }
        $('a').click(function(event) {
          event.preventDefault();
                var alink = $(this).attr('href');
                var target = $(this).attr('target') ? $(this).attr('target')  : '';
          var nolink = $(this).attr('data-nolink') ? $(this).attr('data-nolink')  : 'si';
                if(alink != '<?=SERVER?>#' && nolink != 'no')
                {
                  alinkReload(alink, target);          
                }
        });
        <?php 
        if(isset($preview) && $preview==1)
        {
            ?>            
        if($(".formPages").length > 0)
        {
        var formPageslink = $(".formPages").attr('action');
        var urlformPageslink = alinkPreview(formPageslink);
        $(".formPages").attr('action', urlformPageslink);
      }
       <?php 
    }
       ?>
  });
  $( document ).ready(function() { if($("[rel='tooltip']").length > 0){$("[rel='tooltip']").tooltip();}}); 
</script>
<?php if(isset($url_request['request_file']) && $url_request['request_file'] == 'index.php') {?>
<script>
  console.log("estas en <?=$url_request['request_file']?>");
  $(document).ready(function() 
  { 
    <?php if(isset($v['autoplay']) && $v['autoplay']==1){?>           
      if($('.iframe_video')){cargar_video(5000, 3000, play=1, a=<?=isset($a) ? $a : 0?>);}
    <?php }else{?> if($('.iframe_video')){cargar_video(5000, 3000, play=0, <?=isset($a) ? $a : 0?>);} <?php }?>
    $("#play").click(function(event){
      event.preventDefault();
      $(this).hide();
      $("#pause").show();
      $('.iframe_video').contents().find('#widgetVideo')[0].play();
        $('.iframe_video').show();
        $('.container-video').removeClass('bg-video');
    });
    $("#play2").click(function(event){
      event.preventDefault();
      $(this).hide();
      $("#pause").show();
      $('.iframe_video').contents().find('#widgetVideo')[0].play();
    });
    $("#pause").click(function(event){
      event.preventDefault();
      $(this).hide();
      $("#play2").show();
      $('.iframe_video').contents().find('#widgetVideo')[0].pause();
    });
  });     
  var cargar_video = function(f, v, p, a)
  {
    var mostrarVideo = 0;//mostrar 1 | ocultar 0
    var table_ipad = <?=$v['table_ipad']?>;
    var mobile = <?=$v['mobile']?>;
    if(window.innerWidth > 767 && window.innerWidth < 1190 && table_ipad==1){mostrarVideo=1}
    else if(window.innerWidth > 0 && window.innerWidth < 768 && mobile==1){mostrarVideo=1}
    else if(window.innerWidth > 1189 ){mostrarVideo=1}
    if(a==1 && mostrarVideo==1)
    {
      setTimeout(function()
      {
        var src = '<?=SERVER._INC?>_video_widget.php?muted=<?=isset($v["muted"]) ? $v["muted"] : 0 ?>';
        $('.iframe_video').attr('src', src);
      }, v);
      <?php if(isset($v['autoplay']) && $v['autoplay']==1){?>   
        setTimeout(function(){$('.container-video').removeClass('bg-video');}, f);
      <?php } ?>
      if(p==1)
      {
        setTimeout(function(){
          $('.iframe_video').contents().find('#widgetVideo')[0].play();
        }, 6000);
      }
      else
      {
        $('.iframe_video').hide();
      }
    }
    else{$('.iframe_video').hide();console.log('video desactivado para esta resolucion');}
  }//fin de cargar_video
</script>
<?php }if(isset($lib_blog_detail) && $lib_blog_detail == 1) { ?>
<script type="text/javascript">    
    $(document).ready(function()
    {        
        $.fn.bmdIframeBlog = function( options ) {
            var self = this;
            var settings = $.extend({
                classBtn: '.bmd-modalButton',
                defaultW: 100+'%',
                defaultH: 360+'px'
            }, options );
            $(settings.classBtn).on('click', function(e) {
              var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
              var src = alinkPreview(alink= $(this).attr('data-bmdSrc'));
            console.log(src);
              var dataVideo = {
                'src': src,
                'scrolling': $(this).attr('data-scrolling') || 'no',
                'frameborder': '0',
                'name': 'iframe_user',
              };
              var dataCss = {
                'height': $(this).attr('data-bmdHeight') || settings.defaultH,
                'width': $(this).attr('data-bmdWidth') || settings.defaultW
              };
              if ( allowFullscreen ) dataVideo.allowfullscreen = "";
              // stampiamo i nostri dati nell'iframe
              $("#contentIframe").css('height', dataCss.height);
              $(self).find("iframe").attr(dataVideo);
              $(self).find("iframe").css(dataCss);;
            });
            // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
            this.on('hidden.bs.modal', function(){
              $(this).find('iframe').html("").attr("src", "");
            });
            return this;
        };         
        $("#myModal").bmdIframeBlog();  
        $(".login").click(function(e) {
            e.preventDefault();
            //tb_show('', 'login-p.php?KeepThis=true&TB_iframe=false&width=450&height=330&modal=false');
            $("#login").trigger('click');
        });
        $(".btn_toggle").click(function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            $('#target_' + id).toggle();
            $('#descrip_' + id).focus();
        });
        $("#form1").submit(function() {
            value = $.trim($("#descrip").val());
            if (value.length < 1) {
                $("#descrip").focus();
                return false;
            }
        });
        $(".form_reply").submit(function() {
            var id = $(this).data('id');
            value = $.trim($("#descrip_" + id).val());
            if (value.length < 1) {
                $("#descrip_" + id).focus();
                return false;
            }
        });
    }); 
</script>
<script>
  //icono me gusta
  $(document).ready(function()
  {
    $("#like-yes-"<?=!empty($idx) ? '+'.$idx : ''?>).click(function(e) {
      e.preventDefault();
      var id = $(this).data('id');
      // alert(id);
      $.ajax({
        type: "POST",
        url: "_blog-detail-coment-like-unlike.php",
        dataType: 'html',
        //dataType: 'json',
        data: {
          id: id,
          opc: 'like',
        },
        beforeSend: function(objeto){
                $('#like-yes-'+id).attr('disabled', 'disabled').after('<img src="img/appc-wait-14x14.gif" id="loader" height="12px"/>').val("Post Comment");
          $('#like-yes-'+id).hide();  
            },
                complete: function(objeto, exito){
          if(exito=="success"){
            $('#loader').fadeIn('fast', function() {  $(this).remove()    });
            $('#like-yes-'+id).removeAttr('disabled');
            $('#like-yes-'+id).show();
          }
        },
        success: function(data) {
          //alert(data);
          //console.log(data);
          //$('#count_like_'+data.idx).text(data.count_like);
          //$('#count_unlike_'+data.idx).text(data.count_unlike);
          $('#social_ico_'+id).html(data);  
        },
      });
    });
    $("#like-no-"<?=!empty($idx) ? '+'.$idx : ''?>).click(function(e) {
      e.preventDefault();
      var id = $(this).data('id');
      //alert(id);
      $.ajax({
        type: "POST",
        url: "_blog-detail-coment-like-unlike.php",
        dataType: 'html',
        //dataType: 'json',
        data: {
          id: id,
          opc: 'unlike',
          },
        beforeSend: function(objeto){
                $('#like-no-'+id).attr('disabled', 'disabled').after('<img src="img/appc-wait-14x14.gif" id="loader" height="12px"/>').val("Post Comment");
          $('#like-no-'+id).hide(); 
            },
                complete: function(objeto, exito){
          if(exito=="success"){
            $('#loader').fadeIn('fast', function() {  $(this).remove()    });
            $('#like-no-'+id).removeAttr('disabled');
            $('#like-no-'+id).show();
          }
        },
        success: function(data) {
          //alert(data);
          //console.log(data);
          $('#social_ico_'+id).html(data);
          //$('#count_unlike_'+data.idx).text(data.count_unlike);
        },
      });
    });
  });
</script>
<div class="modal fade" id="myModal">
       <div class="modal-dialog">
           <div class="modal-content bmd-modalContent">
               <div class="modal-body">
                   <div class="close-button">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 30px">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <div class="embed-responsive " id="contentIframe">
                       <iframe class="embed-responsive-item" frameborder="0"></iframe>
                   </div>
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->     
<?php } //lib_blog_detail 
//--------------------------------------------------------------------------------------
  // aplicando bloqueo de mantenimiento
  if(!isset($_SESSION['value_admin_idx'])  && $wactivowebsite==2 && (!isset($preview) || $preview!=1))
  {
    require_once dirname(__FILE__)."/_maintenance.php"; // pantalla de mantenimiento programado
  }
?>