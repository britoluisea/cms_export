<style>
    .error {
        color:#e74c3c;
        font-size:12px;
    }
    .asterisco {
        color: #e74c3c;
        font-size: 12px;
    }
</style>
<?php
$fullname='';
$address='';
$email='';
$city='';
$state='';
$zipcode='';
$telephone='';
$comments='';
if(isset($_POST['autofilling'])){
$fullname=$_POST['fullname'];
$address=$_POST['address'];
$city=$_POST['city'];
$state=$_POST['state'];
$zipcode=$_POST['zipcode'];
$telephone=$_POST['telephone'];
$email=$_POST['email'];
$comments=$_POST['message'];
}
?>
<form d="form1" name="form1" action="p/form/send/contact-us.php" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Full Name <span class="asterisco">*</span> </label>
                <input autofocus type="text" name="fullname" id="fullname" value="<?=isset($_SESSION['entries']['fullname']) ? $_SESSION['entries']['fullname'] : '' ?><?=$fullname?>" class="form-control">
                <div class="error"><?=isset($_SESSION['errors']['fullname']) ? $_SESSION['errors']['fullname'] : '' ?></div>
            </div> 
        </div>
    </div>
</row>

<div class="row">
    <div class="col-xs-6 column">
        <div class="form-group">
            <label>Address <span class="asterisco">*</span> </label>
            <input type="text" name="address" id="address" 
            value="<?=isset($_SESSION['entries']['address']) ? $_SESSION['entries']['address'] : '' ?><?=$address?>" class="form-control">
            <div class="error"><?=isset($_SESSION['errors']['address']) ? $_SESSION['errors']['address'] : ''?></div>
        </div>
    </div>

    <div class="col-xs-6 column">
        <div class="form-group">
            <label>City</label>
            <input type="text" name="city" id="city" value="<?=isset($_SESSION['entries']['city']) ? $_SESSION['entries']['city'] : '' ?><?=$city?>" class="form-control">
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-6 column">
        <div class="form-group">
            <label>State</label>
            <input type="text" name="state" id="state" value="<?=isset($_SESSION['entries']['state']) ? $_SESSION['entries']['state'] : '' ?><?=$state?>" class="form-control" >
        </div>
    </div>

    <div class="col-xs-6 column">
        <div class="form-group">
            <label>Zip Code</label>
            <input  type="text" name="zipcode" id="zipcode"  value="<?=$_SESSION['entries']['zipcode']=isset($_SESSION['entries']['zipcode']) ? $_SESSION['entries']['zipcode'] : '' ?>" class="form-control">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6 column">
        <div class="form-group">
            <label>Telephone <span class="asterisco">*</span> </label>
            <input type="text" name="telephone" id="telephone" value="<?=isset($_SESSION['entries']['telephone']) ? $_SESSION['entries']['telephone'] : '' ?><?=$telephone?>" class="form-control">
            <div class="error"><?=isset($_SESSION['errors']['telephone']) ? $_SESSION['errors']['telephone'] : '' ?></div>
        </div>
    </div>

    <div class="col-xs-6 column">
        <div class="form-group">
            <label>Email <span class="asterisco">*</span> </label>
            <input type="text" name="email" id="email" value="<?=isset($_SESSION['entries']['email']) ? $_SESSION['entries']['email'] : '' ?><?=$email?>" class="form-control">
            <div class="error"><?=isset($_SESSION['errors']['email']) ? $_SESSION['errors']['email'] : '' ?></div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <label>Comments</label>
            <textarea name="comments" rows="3" id="comments" class="form-control"><?=isset($_SESSION['entries']['comments']) ? $_SESSION['entries']['comments'] : '' ?><?=$comments?></textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <label>Insert Security Code <span class="asterisco">*</span> </label>
            <input name="captcha" type="text" id="captcha" class="form-control">
            <div class="error"><?=isset($_SESSION['errors']['captcha']) ? $_SESSION['errors']['captcha'] : '' ?></div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            <img class="captcha" src="<?=SERVER._INC?>captcha/newCaptcha.php" alt="captcha-<?=$_SESSION['captcha']?>" id="captcha" />
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <input type="hidden"  name="ope" value="sendMailContactUs">
            <button type="submit" class="btn btn-primary">Send Message</button>
        </div>
    </div>
</div>
<!-- No Borrar-->
<?php unset($_SESSION['errors']) ?>
<?php unset($_SESSION['entries']) ?>
<!-- No Borrar-->
</form>