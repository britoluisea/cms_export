<?php
    include dirname(__FILE__).'/../'.$_SESSION['THEME'].'head.php';
    if (isset($_SESSION['user']) && (!empty($_SESSION['user']['login']) or $_SESSION['user']['login'] == true)) 
    {
    header("Location: my-account.php");
    exit();
    }
    include('app/model/Usuarios.php');
    include('app/FormValidator.php');
    include('app/ValidationRule.php');
    $validator=new App_FormValidator();
    $mUsuarios=new App_Model_Usuarios();
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $validator->addRule('username', 'Is required.', 'required');
        $validator->addRule('username', 'Please enter a valid email address.', 'email');
        //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');    
        $validator->addEntries($_POST);
        $validator->validate();    
        $entries = $validator->getEntries();
        foreach ($entries as $key => $value) { ${$key} = $value; }    
        if ($validator->foundErrors()){ $errors = $validator->getErrors();}
        if (empty($errors)){ $mUsuarios->forgotPassword($entries,'1'); };    
    }
?>
<form  action="" method="post" name="myform" id="myform">  
    <div class="modalcread modalcread-edit">  
        <div class="modal-header ">
            <div class="edit"> <h3>Retrieve Password</h3></div>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <?php
                    if (isset($_SESSION['correo_enviado']) && $_SESSION['correo_enviado'] == 'SI') 
                    { ?>
                        <div align="justify">
                            <p>We have sent you an email with instruction on how to create your new password.  If you dont see our email, check your spam folder.  If you encounter any issues, contact your website administrator.</p>
                            <p>&nbsp;</p>
                        </div> <?php
                    }
                    else
                    {
                        if (isset($_SESSION['correo_enviado']) && $_SESSION['correo_enviado'] == 'NO') 
                        { ?>
                            <div align="center" class="alert  alert-error" >Invalid username.</div>
                            <?php
                        } ?>
                        <div><p>Enter your email and to receive instructions: </p></div>
                        <div class="controls">
                            <input name="username" type="text" class="form-control" id="username" placeholder="Email" value="<?=isset($username) ? $username : '' ?>" />
                            <div class="help-inline"><?=isset($errors['username']) ? $errors['username'] : ''?></div>
                        </div> <?php
                    }
                    unset($_SESSION['correo_enviado']);
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?=SERVER?>_user-login-p" class="btn btn-small btn-default" style="float: left;">&laquo; Back to login</a>
            <button name="button" type="submit" class="btn btn-small btn-success" id="button">
                Send Me <i class="fa fa-arrow-right "></i>
            </button>
        </div>
    </div>
</form>
<?php include dirname(__FILE__).'/../'.$_SESSION['THEME'].'foot.php';?>