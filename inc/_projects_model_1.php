<?php
require_once ("app/model/ProductDLista.php");
require_once ("app/model/ProductDListaDet.php");
require_once ("app/model/ProductDOptions.php");

$mProductDLista 	= new App_Model_ProductDLista();
$mProductDListaDet 	= new App_Model_ProductDListaDet();
$mProductDOptions 	= new App_Model_ProductDOptions();

$row = $mProductDLista->getById($id);
$filas=$mProductDListaDet->getListar($id, $row['ordenes']);
$rowOpt=$mProductDOptions->getById(1);

$numReg= count($filas);
?>
<style>
.content_model_<?=$id?>{max-width:<?=$rowOpt["model1width"]?>px; margin:20px auto;}
.galleria_<?=$id?>{height:<?=$rowOpt["model1height"]?>px;}
@media (max-width: 480px) {
		.galleria_<?=$id?>{max-height:220px;}
	}
</style>

<?php if($numReg==0): ?>
<div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?> </div>    
<?php else:
    $projetc_model1=1; $projetc_model1_ID=$id;?>
    <div class="galleria_<?=$projetc_model1_ID?>">
    	<?php
        foreach ($filas as $item)			
        {
        ?>
        <a href="<?=SERVER?>imgcms/product_d/<?=$row["nomcar"]?>/large/<?=$item['imagen']?>">
            <img title="<?=$col_xml_2['nombre']?>"
            alt="<?=nl2br($col_xml_2['descripcion'])?>"
            src="<?=SERVER?>imgcms/product_d/<?=$row["nomcar"]?>/thumbs/<?=$item['imagen']?>">
        </a>
        <?php
        }
        ?>
    </div>
</div>
 <?php endif;?>

  