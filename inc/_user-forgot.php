<?php include('prot-logeo.php') ?>
<?php include('app/model/Usuarios.php'); ?>
<?php include('app/FormValidator.php'); ?>
<?php include('app/ValidationRule.php'); ?>
<?php
$validator = new App_FormValidator();
$mUsuarios = new App_Model_Usuarios();
?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validator->addRule('username', 'Is required.', 'required');
    $validator->addRule('username', 'Please enter a valid email address.', 'email');

    //$validator->addRule('auth_token', 'No token was provided to match against.', 'token', 'login');
    //**********************************************************************************************************
    $validator->addEntries($_POST);
    $validator->validate();

    //**********************************************************************************************************
    $entries = $validator->getEntries();
    foreach ($entries as $key => $value) {
        ${$key} = $value;
    }

    //**********************************************************************************************************
    if ($validator->foundErrors()) {
        $errors = $validator->getErrors();
    }
    /* echo "<pre>";
      print_r($errors);
      echo "</pre>"; */
    if (empty($errors)) :
        $mUsuarios->forgotPassword($entries);
    endif;
    //**********************************************************************************************************
}
?>
<?php include('header.php') ?>
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-4">
            <form enctype="application/x-www-form-urlencoded" action="" method="post" name="myform" id="myform"> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title fontb"> Retrieve Password </h3>
                    </div>


                    <div class="panel-body">

                        <div class="widget-main">

                            <?php
                            //echo "===>".$_SESSION['correo_enviado'];
                            if ($_SESSION['correo_enviado'] == 'SI') {
                                ?>
                                <div align="justify">
                                    <p>We have sent you an email with instruction on how to create your new password.  If you dont see our email, check your spam folder.  If you encounter any issues, contact your website administrator.</p>

                                    <p>&nbsp;</p>
                                </div>
                                <?php
                            } else {
                                if ($_SESSION['correo_enviado'] == 'NO') {
                                    ?>
                                    <div align="center" class="alert  alert-danger" >Invalid username.</div>
                                    <?php
                                }
                                ?>
                                <p>
                                    Enter your email and to receive instructions:
                                </p>


                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>

                                        <input name="username" type="text" class="form-control" id="email" placeholder="Email" value="<?= $username ?>">

                                    </div>
                                    <div class="text-danger"><?= $errors['username'] ?></div>
                                </div>
                              
                                <?php
                            }
                            unset($_SESSION['correo_enviado']);
                            ?>

                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="login.php" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>Back to login</a>
                        <button type="submit" class="btn btn-primary"> Send </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php') ?>