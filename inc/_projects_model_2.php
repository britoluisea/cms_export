<?php
require_once ("app/model/ProductDLista.php");
require_once ("app/model/ProductDListaDet.php");
require_once ("app/model/ProductDOptions.php");
$mProductDLista     = new App_Model_ProductDLista();
$mProductDListaDet  = new App_Model_ProductDListaDet();
$mProductDOptions   = new App_Model_ProductDOptions();
$row = $mProductDLista->getById($id);
$filas=$mProductDListaDet->getListar($id, $ordenes='');
$rowOpt=$mProductDOptions->getById(1);
$numReg= count($filas);
?>
<style>
.content_model_<?=$id?>{margin:0px;}
.galleria_<?=$id?>{ min-height: 100px !important; }
.galleria-thumbnails .galleria-image { width:<?=$rowOpt["model2widthimage"]?>px !important }
</style>
<?php if($numReg==0): ?>
         <div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_record']) ? $dataGlobal['label_no_record'] : 'No Record' ?> </div>  
<?php else:
    $projetc_model2=1; $projetc_model2_ID=$id; ?>
        <div class="galleria_<?=$projetc_model2_ID?>">
        <?php
        foreach ($filas as $item)           
        {
        ?>
        <a href="<?=SERVER?>imgcms/product_d/<?=$row["nomcar"]?>/large/<?=$item['imagen']?>">
            <img title="<?=$item['nombre']?>"
            alt="<?=nl2br($item['descripcion'])?>"
            src="<?=SERVER?>imgcms/product_d/<?=$row["nomcar"]?>/thumbs/<?=$item['imagen']?>">
        </a>
        <?php
        }
        ?>        
        </div>
    </div>
<?php endif;?>