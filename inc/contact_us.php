<?php
$sql = "SELECT * FROM formularios where idx = '1' ";
$res = ejecutar($sql);
$k = fetchArray($res);
$validRecaptcha = $k['recaptcha'];
$key_website = $k['key_website'];
$fullname='';
$address='';
$city='';
$state='';
$zipcode='';
$email='';
$telephone='';
$comments='';
if(isset($_POST['autofilling'])){
$fullname=isset($_POST['fullname']) ? $_POST['fullname'] : '';
$address=isset($_POST['address']) ? $_POST['address'] : '';
$city=isset($_POST['city']) ? $_POST['city'] : '';
$state=isset($_POST['state']) ? $_POST['state'] : '';
$zipcode=isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
$telephone=isset($_POST['telephone']) ? $_POST['telephone'] : '';
$email=isset($_POST['email']) ? $_POST['email'] : '';
$comments=isset($_POST['message']) ? $_POST['message'] : '';
$comments=isset($_POST['comments']) ? $_POST['comments'] : '';
}
if(isset($_SESSION['msj_form']) && $_SESSION['msj_form']==1)
{ ?>
    <div class="msj-response">
        <strong>
            <?=isset($dataGlobal['contacting_us']) ? $dataGlobal['contacting_us'] : 'Thank you for contacting us!'?></strong>
    </div>
    <br><br>
    <?php 
    unset($_SESSION['msj_form']);
} 
?>
<form id="form1" name="form1" action="<?=SERVER_P?>form/send/contact-us.php" method="post">
            <div cms-cols="col c100 cx100">
                <label><?=isset($dataGlobal['full_name']) ? $dataGlobal['full_name'] : 'Full Name'?> <span class="asterisco">*</span> </label>
                <input autofocus type="text" name="fullname" id="fullname" value="<?=isset($_SESSION['entries']['fullname']) ? $_SESSION['entries']['fullname'] : $fullname?>" class="form-fullname">
                <div class="error"><?=isset($_SESSION['errors']['fullname']) ? $_SESSION['errors']['fullname'] : '' ?></div>
            </div> 
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['address']) ? $dataGlobal['address'] : 'Address'?> <span class="asterisco">*</span> </label>
            <input type="text" name="address" id="address" value="<?=isset($_SESSION['entries']['address']) ? $_SESSION['entries']['address'] : $address?>" class="form-address">
            <div class="error"><?=isset($_SESSION['errors']['address']) ? $_SESSION['errors']['address'] : ''?></div>
        </div>
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['city']) ? $dataGlobal['city'] : 'City'?></label>
            <input type="text" name="city" id="city" value="<?=isset($_SESSION['entries']['city']) ? $_SESSION['entries']['city'] : $city?>" class="form-city">
        </div>
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['state']) ? $dataGlobal['state'] : 'State'?></label>
            <input type="text" name="state" id="state" value="<?=isset($_SESSION['entries']['state']) ? $_SESSION['entries']['state'] : $state?>" class="form-state" >
        </div>
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['code_zip']) ? $dataGlobal['code_zip'] : 'Zip Code'?></label>
            <input  type="text" name="zipcode" id="zipcode"  value="<?=isset($_SESSION['entries']['zipcode']) ? $_SESSION['entries']['zipcode'] : $zipcode?>" class="form-zipcode">
        </div>
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['telephone']) ? $dataGlobal['telephone'] : 'Telephone'?> <span class="asterisco">*</span> </label>
            <input type="text" name="telephone" id="telephone" value="<?=isset($_SESSION['entries']['telephone']) ? $_SESSION['entries']['telephone'] : $telephone?>" class="form-telephone">
            <div class="error"><?=isset($_SESSION['errors']['telephone']) ? $_SESSION['errors']['telephone'] : '' ?></div>
        </div>
        <div cms-cols="col c50 cx100">
            <label><?=isset($dataGlobal['label_email']) ? $dataGlobal['label_email'] : 'Email'?> <span class="asterisco">*</span> </label>
            <input type="text" name="email" id="email" value="<?=isset($_SESSION['entries']['email']) ? $_SESSION['entries']['email'] : $email?>" class="form-email">
            <div class="error"><?=isset($_SESSION['errors']['email']) ? $_SESSION['errors']['email'] : '' ?></div>
        </div>
        <div cms-cols="col c100 cx100">
            <label><?=isset($dataGlobal['comments']) ? $dataGlobal['comments'] : 'Comments'?></label>
            <textarea name="comments" rows="3" id="comments" class="form-comments"><?=isset($_SESSION['entries']['comments']) ? $_SESSION['entries']['comments'] : $comments?></textarea>
        </div>
        <?php 
            if($validRecaptcha==1 && $key_website!='')
            { ?>
                <div cms-cols="col c100 cx100">
                    <div id="recaptcha" class="g-recaptcha"></div>
                    <div class="error">
                        <?=isset($_SESSION['errors']['g-recaptcha-response']) ? $_SESSION['errors']['g-recaptcha-response'] : '' ?>
                    </div>
                </div>
                <?php 
            }
            else 
            {?>
                <div cms-cols="col c100 cx100">
                    <label><?=isset($dataGlobal['input_captcha']) ? $dataGlobal['input_captcha'] : 'Add the result of the sum'?> <span class="asterisco">*</span> </label>
                </div>
                <div cms-cols="col c20 cx100" >
                    <img src="<?=SERVER_P?>form/captcha/captcha.php" alt="captcha" class="captcha-cms" />
                </div>
                <div cms-cols="col c80 cx100">
                    <input name="g-recaptcha-response" type="text" id="captcha" class="form-captcha">
                    <div class="error">
                        <?=isset($_SESSION['errors']['g-recaptcha-response']) ? $_SESSION['errors']['g-recaptcha-response'] : '' ?>
                    </div>
                </div>
                <?php
            } 
        ?>
        <div cms-cols="col c50 cx100">
            <input type="hidden"  name="ope" value="sendMailContactUs">
            <input type="hidden"  name="uri" id="uri" value="<?=(isset($preview) && $preview==1) ? 'preview/': ''?><?=$url_request['request_url']?>">
            <button type="submit" id="submit" class="btn-cms"  ><?=isset($dataGlobal['btn_sent_message']) ? $dataGlobal['btn_sent_message'] : 'Send Message'?></button>
        </div>
<?php unset($_SESSION['errors']) ?>
<?php unset($_SESSION['entries']) ?>
</form>
<?php if($validRecaptcha==1 && $key_website!=''){ ?>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('recaptcha', {
                'sitekey' : '<?=$key_website?>'
            });
      };
     </script>
     <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
    </script>
<?php } ?>
