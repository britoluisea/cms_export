<?php include('paginator.class.php'); ?>
<style>
 .btnUnlike, .btnLike {padding: 1px 6px;}
</style>
<?php
    $sql_2 = "select * from rating_reviews_options where idx='1' ";
    $consulta_2 = ejecutar($sql_2);
    $atabla_opt = array();
    if ($fila_2 = fetchAssoc($consulta_2)) {$atabla_opt = $fila_2;}
    $lib_review=1;
    $btnLike=1;
    $sql_page="SELECT a.url FROM router AS a LEFT JOIN informativo AS b ON a.idx=b.idx_router WHERE b.id='".$atabla_opt['write_review']."' ";
    $ejecutar_page = ejecutar($sql_page); 
    if ($f = fetchAssoc($ejecutar_page)) {$page = $f['url'];}          
                    
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $atabla_opt['header_title']; ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div cms-cols="col c-7 cs-7 cx-12" >
                <div class="c-profile-company-name">
                    <?= $atabla_opt['header_business_name']; ?>
                </div>
                <?php
                    $sql_3 = "SELECT SUM(stars) as total_stars, COUNT(stars) AS count_stars  FROM rating_reviews where comment_status_id='2'";
                    $consulta_3 = ejecutar($sql_3);
                    $atabla_3 = array();
                    if ($fila_3 = fetchAssoc($consulta_3)) { $atabla_3 = $fila_3; }
                    $tRating = 0;
                    if ($atabla_3['count_stars'] <> 0) 
                    {
                        $tRating = $atabla_3['total_stars'] / $atabla_3['count_stars'];
                        $tRating = number_format($tRating, 1);
                    }
                ?>
                <div class="c-opacity">
                    <?php
                        for ($ifor = 1; $ifor <= 50; $ifor++)
                        {
                            $wifor = ($ifor * 0.1);
                            $wifor = number_format($wifor, 1);
                            ?>
                            <input class="star-big {split:10} required" type="radio" name="test-4-rating-3" value="<?= $wifor ?>" disabled="disabled"  <?php if ($tRating == $wifor): ?> checked="checked" <?php endif; ?>/>
                            <?php
                        } 
                    ?>
                    <span style="font-size:18px; font-weight:bold;"><?= $tRating ?></span>
                </div>
                <div class="c-ver-rat">
                    <strong><?= $atabla_3['count_stars'] ?> <?=isset($dataGlobal['verified_ratings']) ? $dataGlobal['verified_ratings'] : 'Verified Ratings'?></strong>
                </div>
            </div>
            <div cms-cols="col c-5 cs-5 cx-12" >
                <div class="ca-write">                    
                    <a href="<?=SERVER.$page?>" class="btn-cms btn-yellow ">
                        <span class="glyphicon glyphicon-pencil"></span> <?=isset($dataGlobal['write_review']) ? $dataGlobal['write_review'] : 'Write a Review'?>
                    </a>
                </div>
                <div class="ca-hea-msj"><?= $atabla_opt['header_review_message']; ?> </div>
            </div>
        </div>
        <div class="">  
            <?php
                $t1 = "rating_reviews";
                $t2 = "rating_reviews_avatar";
                $sql = " select  $t1.*, $t2.imagen as rat_rev_avat_imagen from  $t1,$t2 where $t1.avatar=$t2.idx and  $t1.comment_status_id='2' order by $t1.fecreg desc";
                $consulta = ejecutar($sql);
                $num_reg = numRows($consulta);
                $pages = new Paginator;
                $pages->items_total = $num_reg;
                $pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
                $pages->paginate($atabla_opt['num_reg_page']); //cuantos debe de mostrar por pagina
                $item = 0;
                $consulta = ejecutar($sql . $pages->limit);
                $num_reg = numRows($consulta);
                if ($num_reg == 0) 
                {
                    ?>
                    <div align="center" style="color:#F00"><?=isset($dataGlobal['label_no_label']) ? $dataGlobal['label_no_label'] : 'No Record'?></div>
                    <?php
                }
                else 
                {
                    while ($fila = fetchArray($consulta)) 
                    {
                        $item++; ?>
                        <hr style="border-top-width: 0px; border-bottom-width: 1px; border-style: none none solid; border-bottom: 1px solid #CCC; margin: 4px 0px;">
                        <div class="row">
                            <div class="col-md-1">
                                <div align="center">
                                    <img src="<?=SERVER?>imgcms/write-review/avatar/<?= $fila['rat_rev_avat_imagen'] ?>" class="img-circle picture-wri-xy" title="<?= $fila["nombres"] ?> <?= $fila["apellidos"] ?>" alt="avatar"/>
                                </div>  
                            </div>
                            <div  class="col-md-11" >
                                <div class="row">
                                    <div cms-cols="col c-12 cs-12 cx-12">
                                        <div class="stars-left-name">
                                            <strong>
                                                <?= $fila["nombres"] ?> 
                                                <?= $fila["apellidos"] ?>
                                                <!--?= $fila["city"] ?>, 
                                                <!--?= strtoupper($fila["state"]) ?-->
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div  cms-cols="col c-12 cs-12 cx-12">
                                        <div class="c-opacity" >
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '0.5'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '1.0'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '1.5'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '2.0'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '2.5'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '3.0'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '3.5'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '4.0'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '4.5'): ?> checked="checked" <?php endif; ?> />
                                            <input class="star {split:2}" type="radio" name="<?= $fila["idx"] ?>" disabled="disabled" <?php if ($fila["stars"] == '5.0'): ?> checked="checked" <?php endif; ?> />
                                            <strong><!--?php echo number_format($fila["stars"], 1); ?-->
                                            
                                                                                     <?php 
                                            if ($atabla_opt['show_date'] == 1)
                                            {?>
                                                <div class="t-text-date stars-left">
                                                    - <?= strftime('%B %d, %Y', strtotime($fila['fecreg'])) ?>
                                                </div>
                                            <?php 
                                            }
                                        ?>
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!--div cms-cols="col c-12 cs-12 cx-12">
                                        <!--?php if (!empty($fila["project"])) { ?>
                                            <div class="title-project-rr">
                                                <!--?= $fila["project"] ?>
                                            </div> 
                                        <!--?php } ?>
                                    </div-->
                                    <div cms-cols="col c-12 cs-12 cx-12">
                                        <div class="desc-rating">
                                            <?= nl2br($fila["descrip"]) ?>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                    if ($atabla_opt['counter_show'] == 1)
                                    { ?> 
                                        <div class="row">
                                            <div cms-cols="col c-12 cs-12 cx-12">
                                                <div class="sec-btns-yes-no">
                                                    <span style="font-size:12px; font-weight:bold;">
                                                        <?=isset($dataGlobal['review_helpful']) ? $dataGlobal['review_helpful'] : 'Was this review helpful?'?>
                                                    </span>
                                                    <button type="button" name="button_1" class="btnLike btn-cms btn-cms-small" data-id="<?= $fila["idx"] ?>" id="btnLike_<?= $fila["idx"] ?>" style="cursor:pointer; font-size:15px;"/>
                                                        <span id="count_like_<?= $fila["idx"] ?>">
                                                            <?= $fila["count_like"] ?>
                                                        </span>
                                                        <i class="fa fa-thumbs-o-up"></i>
                                                    </button>
                                                    <button type="button" name="button_2" class="btnUnlike btn-cms btn-red btn-cms-small" data-id="<?= $fila["idx"] ?>" id="btnUnlike_<?= $fila["idx"] ?>" style="cursor:pointer; font-size:15px;"/>
                                                        <span id="count_unlike_<?= $fila["idx"] ?>">
                                                            <?= $fila["count_unlike"] ?>
                                                        </span> 
                                                        <i class="fa fa-thumbs-o-down"></i>
                                                    </button>
                                                    <span id="notificationLike_<?= $fila["idx"] ?>" style="margin-bottom:5px; margin-top:5px;"></span>
                                                </div>
                                                <div style=" margin: 10px;">
                                                    <?php
                                                        $sql_idx_gallery = "SELECT idx_gallery FROM rating_reviews WHERE idx='".$fila['idx']."' ";
                                                        $query_idx_gallery = ejecutar($sql_idx_gallery);
                                                        $totalRows_u1 = numRows($query_idx_gallery );
                                                        if($totalRows_u1 > 0){
                                                            $idx_gallery = fetchAssoc($query_idx_gallery);
                                                            $gallery=$idx_gallery['idx_gallery'];
                                                            $select_sql="SELECT a.nomcar,a.slug, b.imagen FROM galerias_lista AS a INNER JOIN detgaleriaslista AS b ON a.idx=b.idgalerias WHERE a.idx='".$gallery."'  order by a.idx ASC limit 0, 5";
                                                            $query_select=ejecutar($select_sql);
                                                            if(numRows($query_select) > 0)
                                                            {
                                                                $imagenes='';
                                                                while($img=fetchAssoc($query_select))
                                                                { 
                                                                    $wurlGall = SERVER.'gallerys/'.$img["slug"];
                                                                    $ruta="imgcms/galeria/".$img['nomcar']."/small/".$img['imagen'];  
                                                                    $imagenes.='<a href="'.$wurlGall.'"><img src="'.$ruta.'" style="width:50px;margin-right:5px;" /></a>';
                                                                }
                                                                echo $imagenes;
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                    }
                                ?>
                            </div>
                        </div>
                        <?php
                    }//fin del while
                }//fin del else
            ?> 
            <?php if ($pages->num_pages > 1) { ?>
                <div align="center" style="padding:8px;">
                    <?php echo $pages->display_pages(); ?>                        
                </div>
            <?php } ?>
        </div>
    </div>
</div>
