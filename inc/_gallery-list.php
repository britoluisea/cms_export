<?php
include('paginator.class.php');
$sql_pag = "SELECT * FROM galerias_lista_options where idx = '1' ";
$res_pag = ejecutar($sql_pag);
if ($col_pag = fetchArray($res_pag)) {
    //////////Inicio de calculo
    $numregpage = $col_pag['numregweb'];
    $back_col = $col_pag['back_col'];
    $text_col = $col_pag['text_col'];
    $width = $col_pag['width'];
    $height = $col_pag['height'];
}
?>
<link rel="stylesheet" href="_files/gallery/css/style.css">
<?php
$gp = 1;
$t = "galerias_lista";
$sql = "select * from $t where activo = 1 order by item desc ";
$consulta = ejecutar($sql);
$num_reg = numRows($consulta);
$pages = new Paginator;
$pages->items_total = $num_reg;
$pages->mid_range = 10; // Number of pages to display. Must be odd and > 3
//$pages->paginate($num_reg_page_cms);  //cuantos debe de mostrar por pagina
$pages->paginate($numregpage);
$consulta = ejecutar($sql . $pages->limit);
?>
<?php
if ($num_reg == 0) {
    ?>
    <div align="center" style="color:#F00">No Record</div>
    <?php
} else {
    ?>
    <?php while ($fila = fetchArray($consulta)) 
    { 
        $wurlGall = SERVER;
        $wurlGall .= isset($dataGlobal['url_gallerys']) ? $dataGlobal['url_gallerys'] : 'gallerys';
        $wurlGall .= '/'.$fila["slug"];
        $sql_fot = "SELECT idx FROM detgaleriaslista where idgalerias = '" . $fila["idx"] . "'";
        $res_fot = ejecutar($sql_fot);
        $numPhotos = numRows($res_fot);
        $src = SERVER."imgcms/galeria/".$fila['nomcar']."/thumbs/".$fila['imagen'];
        if(is_file($src)){         
            list($widthx, $heightx) = getimagesize($src);
            if($widthx > $heightx){
            $width = 200;
            $height = 150;
            }   
        }else{
        $width = 150;
        $height = 200;
        }
        ?>
        <div class="col-sm-4" style="margin-bottom:20px">
            <div class="panel panel-default text-center">
                <div class="panel-heading" style="overflow:hidden; height:196px;padding:0px;display: flex;flex-direction: column;justify-content: center;">
                    <a href="<?= $wurlGall ?>">
                        <?php if ($fila["imagen"] == ""): ?>
                            <img src="img/img_blanco.png"/>
                        <?php else : ?>
                            <img src="<?=$src?>"  style="width:100%;" alt="<?= $fila['nombre'] ?>"/>
                        <?php endif; ?>
                    </a>
                </div>
                <div style="background-color:<?= $back_col ?>;height:40px;overflow: hidden;display: flex;justify-content: center;flex-direction: column;">
                    <a style="color: <?= $text_col ?>" href="<?= $wurlGall ?>"><?= $fila['nombre'] ?> (<?= $numPhotos ?>)</a>
                </div>
            </div>
        </div>    
    <?php } ?>                   
    <?php if ($pages->num_pages > 1): ?>       
        <div align="center" style="padding:8px;">
            <?php echo $pages->display_pages(); ?> <?php //echo $pages->display_jump_menu() ?> <?php //echo $pages->display_items_per_page() ?> 
        </div> 
    <?php endif; ?>
    <?php
}
?>