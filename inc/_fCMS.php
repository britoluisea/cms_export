<?php
class fCMS{
    public  function obtener_menu($idx_menu, $class_li, $attr_li, $class_A, $attr_A, $class_li_sub, $attr_li_sub, $class_A_sub, $attr_A_sub,  $icon_A_sub, $html_A_before, $html_A_after, $class_li_ul, $attr_li_ul, $class_li_ul_li, $attr_li_ul_li, $class_li_ul_li_a, $attr_li_ul_li_a, $html_li_ul_li_a_before, $html_li_ul_li_a_after)
        {
            GLOBAL $url_request;
            $idx_menu = isset($idx_menu) ? $idx_menu : '';
            $class_li = isset($class_li) ? $class_li : '';
            $attr_li = isset($attr_li) ? $attr_li : '';
            $class_A = isset($class_A) ? $class_A : '';
            $attr_A = isset($attr_A) ? $attr_A : '';
            $class_li_sub = isset($class_li_sub) ? $class_li_sub : '';
            $attr_li_sub = isset($attr_li_sub) ? $attr_li_sub : '';
            $class_A_sub = isset($class_A_sub) ? $class_A_sub : '';
            $attr_A_sub = isset($attr_A_sub) ? $attr_A_sub : '';
            $icon_A_sub = isset($icon_A_sub) ? $icon_A_sub : '';
            $html_A_before = isset($html_A_before) ? $html_A_before : '';
            $html_A_after = isset($html_A_after) ? $html_A_after : '';
            $class_li_ul = isset($class_li_ul) ? $class_li_ul : '';
            $attr_li_ul = isset($attr_li_ul) ? $attr_li_ul : '';
            $class_li_ul_li = isset($class_li_ul_li) ? $class_li_ul_li : '';
            $attr_li_ul_li = isset($attr_li_ul_li) ? $attr_li_ul_li : '';
            $class_li_ul_li_a = isset($class_li_ul_li_a) ? $class_li_ul_li_a : '';
            $attr_li_ul_li_a = isset($attr_li_ul_li_a) ? $attr_li_ul_li_a : '';
            $html_li_ul_li_a_before = isset($html_li_ul_li_a_before) ? $html_li_ul_li_a_before : '';
            $html_li_ul_li_a_after = isset($html_li_ul_li_a_after) ? $html_li_ul_li_a_after : '';
            $s="SELECT id, name, url FROM menu_items WHERE idx_menu='".$idx_menu."' AND nivel=0 ORDER BY item ASC";
            if($e = ejecutar($s))
            {
                $cont=0;
                while($f = fetchAssoc($e))
                {
                    $active='';
                    $ico='';
                    if($url_request['request_url']==$f['url'])
                    {
                        $active='active';
                    }
                    $_s = "SELECT name, url FROM menu_items WHERE nivel ='".$f['id']."' " ;
                    $_e = ejecutar($_s);
                    $_n = numRows($_e);
                    if($_n > 0){
                        $ico = $icon_A_sub;
                        $classLI = $class_li_sub;
                        $attrLI = $attr_li_sub;
                        $classA = $class_A_sub;
                        $attrA = $attr_A_sub;
                    }
                    else
                    {
                        $classLI = $class_li;
                        $attrLI = $attr_li;
                        $classA = $class_A;
                        $attrA = $attr_A;
                    }
                    $url_menu= SERVER.$f['url'];
                    if($f['url']=='#'){$attrA.=' data-nolink="no" ';}
                    echo'<li class="'.$active.' '.$classLI.' " '.$attrLI.' >';
                    echo'   <a href="'.$url_menu.'" class="'.$classA.'" '.$attrA.' >'.$html_A_before.$f['name'].' '.$ico.$html_A_after.'</a>';
                    if($_n > 0)
                    {
                        echo'<ul class="'.$class_li_ul.'" '.$attr_li_ul.' >';
                        while($_f = fetchAssoc($_e)){
                            echo'<li class="'.$class_li_ul_li.'" '.$attr_li_ul_li.'>';
                            echo'<a href="'.SERVER.$_f['url'].'" class="'.$class_li_ul_li_a.'" '.$attr_li_ul_li_a.' >'.$html_li_ul_li_a_before.$_f['name'].$html_li_ul_li_a_after.'</a></li>';
                        }
                        echo'</ul>';
                    }
                    echo'</li>';
                    $cont++;
                }
            }
        }
    public  function obtener_menu_custom($idx_menu, $class_li, $attr_li, $class_A, $attr_A, $class_li_sub, $attr_li_sub, $class_A_sub, $attr_A_sub,  $icon_A_sub, $html_A_before, $html_A_after, $html_li_ul_before, $html_li_ul_after, $class_li_ul, $attr_li_ul, $class_li_ul_li, $attr_li_ul_li, $class_li_ul_li_a, $attr_li_ul_li_a, $html_li_ul_li_a_before, $html_li_ul_li_a_after)
        {
            GLOBAL $url_request;
            $idx_menu = isset($idx_menu) ? $idx_menu : '';
            $class_li = isset($class_li) ? $class_li : '';
            $attr_li = isset($attr_li) ? $attr_li : '';
            $class_A = isset($class_A) ? $class_A : '';
            $attr_A = isset($attr_A) ? $attr_A : '';
            $class_li_sub = isset($class_li_sub) ? $class_li_sub : '';
            $attr_li_sub = isset($attr_li_sub) ? $attr_li_sub : '';
            $class_A_sub = isset($class_A_sub) ? $class_A_sub : '';
            $attr_A_sub = isset($attr_A_sub) ? $attr_A_sub : '';
            $icon_A_sub = isset($icon_A_sub) ? $icon_A_sub : '';
            $html_A_before = isset($html_A_before) ? $html_A_before : '';
            $html_A_after = isset($html_A_after) ? $html_A_after : '';
            $class_li_ul = isset($class_li_ul) ? $class_li_ul : '';
            $attr_li_ul = isset($attr_li_ul) ? $attr_li_ul : '';
            $class_li_ul_li = isset($class_li_ul_li) ? $class_li_ul_li : '';
            $attr_li_ul_li = isset($attr_li_ul_li) ? $attr_li_ul_li : '';
            $class_li_ul_li_a = isset($class_li_ul_li_a) ? $class_li_ul_li_a : '';
            $attr_li_ul_li_a = isset($attr_li_ul_li_a) ? $attr_li_ul_li_a : '';
            $html_li_ul_li_a_before = isset($html_li_ul_li_a_before) ? $html_li_ul_li_a_before : '';
            $html_li_ul_li_a_after = isset($html_li_ul_li_a_after) ? $html_li_ul_li_a_after : '';
            $s="SELECT id, name, url FROM menu_items WHERE idx_menu='".$idx_menu."' AND nivel=0 ORDER BY item ASC";
            if($e = ejecutar($s))
            {
                $cont=0;
                while($f = fetchAssoc($e))
                {
                    $active='';
                    $ico='';
                    if($url_request['request_url']==$f['url'])
                    {
                        $active='active';
                    }
                    $_s = "SELECT name, url FROM menu_items WHERE nivel ='".$f['id']."' " ;
                    $_e = ejecutar($_s);
                    $_n = numRows($_e);
                    if($_n > 0){
                        $ico = $icon_A_sub;
                        $classLI = $class_li_sub;
                        $attrLI = $attr_li_sub;
                        $classA = $class_A_sub;
                        $attrA = $attr_A_sub;
                    }
                    else
                    {
                        $classLI = $class_li;
                        $attrLI = $attr_li;
                        $classA = $class_A;
                        $attrA = $attr_A;
                    }
                    $url_menu= SERVER.$f['url'];
                    if($f['url']=='#'){$attrA.=' data-nolink="no" ';}
                    echo'<li class="'.$active.' '.$classLI.' " '.$attrLI.' >';
                    echo'   <a href="'.$url_menu.'" class="'.$classA.'" '.$attrA.' >'.$html_A_before.$f['name'].' '.$ico.$html_A_after.'</a>';
                    if($_n > 0)
                    {
                        echo $html_li_ul_before.'<ul class="'.$class_li_ul.'" '.$attr_li_ul.' >';
                        while($_f = fetchAssoc($_e)){
                            echo'<li class="'.$class_li_ul_li.'" '.$attr_li_ul_li.'>';
                            echo'<a href="'.SERVER.$_f['url'].'" class="'.$class_li_ul_li_a.'" '.$attr_li_ul_li_a.' >'.$html_li_ul_li_a_before.$_f['name'].$html_li_ul_li_a_after.'</a></li>';
                        }
                        echo'</ul>'.$html_li_ul_after;
                    }
                    echo'</li>';
                    $cont++;
                }
            }
        }
}