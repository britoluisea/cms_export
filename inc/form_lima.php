<?php
$sql = "SELECT * FROM formularios where idx = '1' ";
$res = ejecutar($sql);
$k = fetchArray($res);
$validRecaptcha = $k['recaptcha'];
$key_website = $k['key_website'];
$razon_social='';
$name='';
$telephone='';
$email='';
$comments='';
if(isset($_POST['autofilling'])){
$razon_social=isset($_POST['razon_social']) ? $_POST['razon_social'] : '';
$name=isset($_POST['name']) ? $_POST['name'] : '';
$telephone=isset($_POST['telephone']) ? $_POST['telephone'] : '';
$email=isset($_POST['email']) ? $_POST['email'] : '';
$comments=isset($_POST['comments']) ? $_POST['comments'] : '';
}
if(isset($_SESSION['msj_form']) && $_SESSION['msj_form']==1)
{ ?>
	<div class="msj-response">
		<strong>
	        ¡Gracias por contactarnos!  	
		</strong>
	</div>
	<br><br>
	<?php 
	unset($_SESSION['msj_form']);
} 
?>
<form action="<?=SERVER_P?>form/send/form_lima.php" method="post">
	<div cms-cols="col c50 cx100">
		<input type="text" autofocus placeholder="Raz&oacute;n Social (si Ud. es una empresa)" name="razon_social" value="<?=$razon_social?>"  >
		
	</div>	
	<div cms-cols="col c50 cx100">
		<input type="text" placeholder="Nombre del Contacto (su nombre)" name="name" value="<?=isset($_SESSION['entries']['name']) ? $_SESSION['entries']['name'] : $name?>" required>
		<?=isset($_SESSION['errors']['name']) ? '<div class="error">'.$_SESSION['errors']['name'].'</div>' : '' ?>
	</div>	
	<div cms-cols="col c50 cx100">
		<input type="text" placeholder="Correo Electr&oacute;nico" name="email" value="<?=isset($_SESSION['entries']['email']) ? $_SESSION['entries']['email'] : $email?>" required>
	</div>	
	<div cms-cols="col c50 cx100">
		<input type="text" placeholder="Su Tel&eacute;fono" name="telephone" value="<?=isset($_SESSION['entries']['telephone']) ? $_SESSION['entries']['telephone'] : $telephone?>" required>
		<?=isset($_SESSION['errors']['telephone']) ? '<div class="error">'.$_SESSION['errors']['telephone'].'</div>' : '' ?>
	</div>	
	<div cms-cols="col c100 cx100">
		<textarea placeholder="Haga sus comentarios aqu&iacute;..." name="comments" required><?=isset($_SESSION['entries']['comments']) ? $_SESSION['entries']['comments'] : $comments?></textarea>
		<?=isset($_SESSION['errors']['comments']) ? '<div class="error">'.$_SESSION['errors']['comments'].'</div>' : '' ?>
	</div>
                <?php 
            if($validRecaptcha==1 && $key_website!='')
            { ?>
                <div cms-cols="col c100 cx100">
                    <div id="recaptcha" class="g-recaptcha"></div>
                    <div class="error">
                        <?=isset($_SESSION['errors']['g-recaptcha-response']) ? $_SESSION['errors']['g-recaptcha-response'] : '' ?>
                    </div>
                </div>
                <?php 
            }
            else 
            {?>
                <div cms-cols="col c100 cx100">
                    <label><?=isset($dataGlobal['input_captcha']) ? $dataGlobal['input_captcha'] : 'Add the result of the sum'?> <span class="asterisco">*</span> </label>
                </div>
                <div cms-cols="col c20 cx100" >
                    <img src="<?=SERVER_P?>form/captcha/captcha.php" alt="captcha" class="captcha-cms" />
                </div>
                <div cms-cols="col c80 cx100">
                    <input name="g-recaptcha-response" type="text" id="captcha" class="form-captcha">
                    <div class="error">
                        <?=isset($_SESSION['errors']['g-recaptcha-response']) ? $_SESSION['errors']['g-recaptcha-response'] : '' ?>
                    </div>
                </div>
                <?php
            } 
        ?>
	<div cms-cols="col c100 cx100">
        <input type="hidden"  name="uri" value="<?=(isset($preview) && $preview==1) ? 'preview/': ''?><?=$url_request['request_url']?>">
		<button type="submit" id="submit" class="btn-cms">Enviar Solicitud</button>
	</div>
</form>
<!-- No Borrar-->
<?php unset($_SESSION['errors']) ?>
<?php unset($_SESSION['entries']) ?>
<!-- No Borrar-->
<?php if($validRecaptcha==1 && $key_website!=''){ ?>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('recaptcha', {
                'sitekey' : '<?=$key_website?>'
            });
      };
     </script>
     <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
    </script>
<?php } ?>