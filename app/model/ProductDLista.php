<?php
class App_Model_ProductDLista {
    protected $_name = 'project_lista';
    private $contenido = array();
	private $contenidoPag = array();
	public function getListar($idcategorias)
	{	
		$sql="SELECT 
		t1.*,
		t2.nombre as cat_nombre
		FROM 
		$this->_name as t1
		INNER JOIN project_category AS t2 ON t2.idx = t1.idcategorias
		where
		t1.idcategorias = '$idcategorias' and
		t1.activo='1' 
		order by t1.item desc
		";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: '.$sql);
			exit();
		}
		else
		{	
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
	}
	public function getListarPagination($idcategorias, $limit)
	{	
		$sql="SELECT 
		t1.*,
		t2.nombre as cat_nombre,
		t3.nombre as status_nombre
		FROM 
		$this->_name as t1
		INNER JOIN project_category AS t2 ON t2.idx = t1.idcategorias
		INNER JOIN project_status AS t3 ON t3.idx = t1.projectstatus
		where
		t1.idcategorias = '$idcategorias' and
		t1.activo='1' 
		order by t1.item desc
		".$limit;
		//echo $sql.'<br>';
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: '.$sql);
			exit();
		}
		else
		{	
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenidoPag[] = $fila;
				}
		    return $this->contenidoPag;	
		}	
	}
	public function getById($id)
	{	
		$sql="SELECT 
		t1.*,
		t3.nombre as status_nombre
		FROM 
		$this->_name as t1
		INNER JOIN project_status AS t3 ON t3.idx = t1.projectstatus
		where
		t1.idx = '$id' and
		t1.activo='1'
		";
		$consulta=ejecutar($sql);
		//echo $sql;
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: '.$sql);
			exit();
		}
		else
		{	
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenido = $fila;
				}
		    return $this->contenido;	
		}	
	}
}
?>