<?php
class App_Model_ProductDListaDet {
    protected $_name = 'project_detalle';
    private $contenido = array();
	private $contenidoPag = array();
	public function getCount($idgalerias)
	{	
		$sql="SELECT 
		count(*) as count
		FROM 
		$this->_name as t1
		where
		t1.idgalerias = '$idgalerias' and
		t1.activo='1'
		";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: '.$sql);
			exit();
		}
		else
		{	
			//Si la consulta es exitosa, el método regresa $resultado.
			$this->contenido="";
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenido = $fila;
				}
		    return $this->contenido;	
		}	
	}
	public function getListar($id, $ordenes)
	{	
		if($ordenes == 'A'){
			$ordenes = " order by nombre2 asc";			
		}
		elseif($ordenes == 'D'){
			$ordenes = " order by nombre2 desc";
		}
		elseif($ordenes == 'C'){
			$ordenes = " order by item desc";
		}	
		else{$ordenes='';}	
		$sql="SELECT *, CONCAT(REPEAT('0', 70-LENGTH(nombre)), nombre) as nombre2 FROM project_detalle where idgalerias = '$id' $ordenes ";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: '.$sql);
			exit();
		}
		else
		{	
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
	}
}
?>