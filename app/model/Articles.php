<?php
class App_Model_Articles
{
	protected $_name = 'articles';
	
	
	private $contenidoListar= array();
	private $contenidoListarPag= array();
	private $contenido;
	private $contenidoListarRecent= array();
	public function getListar($slug)
	{
		$sql = "SELECT * FROM articles_category where slug='$slug' and activo='1' ";
		$res=ejecutar($sql);
		if ($col=fetchAssoc($res))
		{
			$id=$col['idx'];
		}
		
	
		$sql="SELECT * FROM $this->_name where idx_articles_category='$id' and activo ='1' order by fecreg desc";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
				throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
				exit();
			}
			else
			{	
				while($fila=fetchAssoc($consulta))
				{ 
					$this->contenidoListar[] = $fila;
				}
			return $this->contenidoListar;	
		}
		

	}
	public function getListarPagination($id)
	{
		$sql="SELECT * FROM $this->_name where idx='$id' and activo ='1' order by fecreg desc ";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{	
			if($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListarPag[] = $fila;
			}
			return $this->contenidoListarPag;
		}	
	}
	public function getListarFiltro($slug,$limit)
	{		
		$sql = "SELECT * FROM articles_category where slug='$slug' and activo='1'";
		$res=ejecutar($sql);
		if ($col=fetchAssoc($res))
		{
			$id=$col['idx'];
		}
		
		$sql="SELECT * FROM $this->_name where idx_articles_category='$id' and activo='1' order by fecreg desc  limit 0,$limit";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{	
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;
		}
		
	}
	public function getById($slug)
	{	
		$sql="SELECT *	FROM $this->_name where
		slug = '$slug' and
		activo='1'
		limit 0,1";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			//Si la consulta es exitosa, el método regresa $resultado.
			if($fila=fetchAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
	} 
	public function getByIdUsuario($slug)
	{	
		$t1="articles";
		$t2="usuarios";
		$sql="SELECT 
		$t1.*,
		$t2.idx as usu_idx, 
		$t2.username as usu_username,
		$t2.nombres as usu_nombres,
		$t2.apellidos as usu_apellidos,
		$t2.userlevel as usu_userlevel,
		$t2.slug as usu_slug
		FROM
		$t1,$t2
		where
		$t1.slug = '$slug' and
		$t1.activo='1' and
		$t1.idx_usuario = $t2.idx
		limit 0,1";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			//Si la consulta es exitosa, el método regresa $resultado.
			if($fila=fetchAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
	} 
	public function getListarTagArticles($id)
	{

		$sql="
		SELECT articles.*,
		articles_detail.idx_articles as art_det_idx_articles,
		articles_detail.idx_articles_tag as art_det_idx_articles_tag,
		articles_tag.nombre as tag_nombre,
		articles_tag.activo as tag_activo,
		usuarios.idx as usu_idx, 
		usuarios.username as usu_username,
		usuarios.nombres as usu_nombres,
		usuarios.apellidos as usu_apellidos,
		usuarios.userlevel as usu_userlevel
		FROM
		articles,articles_detail,articles_tag,usuarios
		where
		articles.idx = articles_detail.idx_articles and
		articles_detail.idx_articles_tag = '$id' and
		articles_tag.idx = articles_detail.idx_articles_tag and
		articles_tag.activo='1' and
		articles.idx_usuario = usuarios.idx
		order by
		articles.fecreg desc
		";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;	
		}
	
	}

}
?>