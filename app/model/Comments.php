<?php
class App_Model_Comments
{
	protected $_name = 'comments';
	
	
	private $contenido= array();
	private $contenidoReply= array();
		
	public function delete($id)
	{
			$sql_0 = "delete from usuarios_verify where idx = '$id'";
			if (!$res_0 = ejecutar($sql_0)) {
				echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
				exit();
			}	
			
	} 
	public function deleteUserId($id)
	{
			$sql_0 = "delete from usuarios_verify where user_id = '$id'";
			if (!$res_0 = ejecutar($sql_0)) {
				echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
				exit();
			}	
			
	}
	public function getListar($id)
	{
		$t1="comments";
		$t3="usuarios";
		$sql="select 
		$t1.*,
		$t3.idx as usu_idx, 
		$t3.username as usu_username,
		$t3.nombres as usu_nombres,
		$t3.apellidos as usu_apellidos,
		$t3.userlevel as usu_userlevel,
		$t3.slug as usu_slug,
		$t3.imagen as usu_imagen,
		$t3.avatar as usu_avatar 
		from $t1,$t3
		where
		$t1.comment_status_id='2' and
		$t1.article_id='".$id."' and
		$t1.comment_parent='0' and	
		$t1.user_id=$t3.idx
		order by $t1.fecreg desc";	
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
			
	} 
	
	public function getListarReply($id)
	{
		$this->contenidoReply=array();
		$t10="comments";
		$t11="usuarios";
		$sql="select 
		$t10.*,
		$t11.idx as usu_idx, 
		$t11.username as usu_par_username,
		$t11.nombres as usu_par_nombres,
		$t11.apellidos as usu_par_apellidos,
		$t11.userlevel as usu_par_userlevel,
		$t11.slug as usu_par_slug,
		$t11.imagen as usu_par_imagen,
		$t11.avatar as usu_par_avatar 
		from $t10,$t11
		where
		$t10.comment_status_id='2' and
		$t10.comment_parent = '".$id."' and
		$t10.user_id=$t11.idx
		order by $t10.fecreg";	
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			//Si la consulta es exitosa, el método regresa $resultado.
			while($fila=fetchAssoc($consulta))
				{ 
					$this->contenidoReply[] = $fila;
				}
				
		 		return $this->contenidoReply;
		}	
	
			
	} 
}
?>