<?php
class App_Model_NewsTag
{
	protected $_name = 'news_tag';
	
	
	private $contenidoListar= array();
	private $contenido= array();	
	public function getListar()
	{
		$sql="SELECT * FROM $this->_name where activo='1' order by item desc";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;	
		}
	
	}

	
	
	public function getById($id)
	{
		$sql="SELECT * FROM $this->_name where idx = '$id'";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			if($fila=fetchAssoc($consulta))
			{ 
				$this->contenido[] = $fila;
			}
			return $this->contenido;
		}	
	} 
	
	public function getByIdSlug($slug)
	{
		$sql="SELECT * FROM $this->_name where slug = '$slug' and activo='1' limit 0,1";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			if($fila=fetchAssoc($consulta))
			{ 
				$this->contenido[] = $fila;
			}
			return $this->contenido;
		}
		
	} 

}
?>