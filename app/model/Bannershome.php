<?php
class App_Model_Bannershome {

    protected $_name = 'bannershome';
    private $contenidoInner = array();
	private $contenidoListar = array();
	
	public function getListar()
	{
			
		$sql="SELECT * FROM $this->_name where activo ='1' and page=0 order by item asc";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
				throw new Exception('No Funciona la Consulta. Error App_Model_Bannershome');
				exit();
			}
			else
			{	
				while($fila=fetchAssoc($consulta))
				{ 
					$this->contenidoListar[] = $fila;
				}
			return $this->contenidoListar;	
		}
		
	}	
	public function getInnerHeader($idx)
	{
			
		$sql="SELECT * FROM $this->_name where activo ='1' and page = '".$idx."' ";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
				throw new Exception('No Funciona la Consulta. Error App_Model_Bannershome');
				exit();
		}
		else
		{	
			$this->contenidoInner['nro'] = numRows($consulta);
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoInner['contenido'] = $fila;
			}
			return $this->contenidoInner;	
		}
		
	}	
}
?>