<?php
require(dirname(__FILE__)."/../../p/plugins/phpmailer/class.phpmailer.php");
require(dirname(__FILE__)."/../../p/plugins/phpmailer/class.smtp.php");

class App_Model_RatingReviews {

    protected $_name = 'rating_reviews';
    private $usuario = array();
    private $contenido = array();
    private $contenidoListar = array();

    public function getListar() {
        $sql = "SELECT 
		t1.*,
		t2.imagen as avatar_imagen
		FROM 
		$this->_name as t1
		INNER JOIN rating_reviews_avatar AS t2 ON t2.idx = t1.avatar
		where
		t1.comment_status_id='2' 
		order by t1.fecreg desc
		";

        $consulta = ejecutar($sql);
        $num_reg = numRows($consulta);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: getListar' );            
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            while ($fila = fetchAssoc($consulta)) {
                $this->contenidoListar[] = $fila;
            }
            return $this->contenidoListar;
        }
    }

    public function add(array $data) {
        /* echo "<pre>";
          print_r($data);
          echo "</pre>";
          echo $id;
        */
        $fecreg = date("Y-m-d H:i:s");
        $wips = getenv('REMOTE_ADDR');
        $nombres = addslashes(trim($data['nombres']));
        $apellidos = addslashes(trim($data['apellidos']));
        $username = trim($data['username']);
        $project = trim($data['project']);
        $city = trim($data['city']);
        $state = trim($data['state']);
        $telephone = trim($data['telephone']);
        $genero = trim($data['genero']);
        $descrip = addslashes(trim($data['descrip']));
        $stars = trim($data['stars']);
        $avatar = $data['avatar'];

        $sql_0 = "insert into rating_reviews (nombres, apellidos, username, project, city, state, telephone, genero, descrip, stars,  fecreg, comment_status_id, comment_status_id_salvar, avatar, ips )
							  values ('$nombres', '$apellidos', '$username', '$project', '$city', '$state', '$telephone', '$genero', '$descrip', '$stars', '$fecreg', '1', '1', '$avatar', '$wips')";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;         
        }

        if ($_SERVER['HTTP_HOST'] <> 'localhost' && IP <> IP_VHOST) {
			$idArt = obtener_idx($sql_0);
            $sql = "select email from rating_reviews_options where idx = '1'  ";
            $consulta = ejecutar($sql);
            $atabla = array();
            if ($fila = fetchAssoc($consulta)) {
                $atabla['en'] = $fila;
            }

            if (!empty($atabla['en']['email'])) {
                
                $t1 = "rating_reviews";
                $sql = "
			select 
			rating_reviews.*
			from 
			$t1
			where
			$t1.idx = '" . $idArt . "' 
			order by rating_reviews.fecreg desc limit 0,1";
                $consulta = ejecutar($sql);
                $atabla_comm = array();
                if ($fila_comm = fetchAssoc($consulta)) {
                    $atabla_comm[0] = $fila_comm;
                }

                $sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
                $consulta_company = ejecutar($sql_company);
                $atabla_company = array();
                if ($fila_company = fetchAssoc($consulta_company)) {
                    $atabla_company['en'] = $fila_company;
                }
                //Especificamos los datos y configuración del servidor
                $sqlPen = "select * from rating_reviews where comment_status_id='1'"; //Devuelve todos los Idiomas
                $consultaPen = ejecutar($sqlPen);
                $numPen = numRows($consultaPen);

                ob_start(); //Abrimos el buffer			
                include (dirname(__FILE__)."/../../p/plugins/phpmaileremail/rating-reviews-new.php");
                $email_text = ob_get_contents(); //Vaciamos el buffer
                ob_end_clean();

                $mail = new PHPMailer();
                $mail->From = $atabla_comm[0]['username'];
                $mail->FromName = $atabla_comm[0]['nombres'] . ' ' . $atabla_comm[0]['apellidos'];
                $mail->Subject = 'Please moderate: Review "' . $atabla_comm[0]['project'] . '"?';
                $mail->AltBody = "";
                $mail->MsgHTML($email_text);
                //$mail->AddAttachment("adjunto.txt");
                //$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
                $mail->AddAddress($atabla['en']['email'], $atabla_company['en']["compania"]);
                $mail->IsHTML(true);
                $mail->CharSet = 'UTF-8';
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;                    
                } else {
                    //echo "Message sent!";
                }
            }
        }

        $_SESSION['data_saved'] = isset($dataGlobal['response_write_review']) ? $dataGlobal['response_write_review'] : "Your review has been sent and will be publishing within moments, appreciate your patience.";       
    }

    public function getListarLimit($lim) {
        $lim = "limit 0," . $lim;
        $sql = "SELECT 
		t1.*,
		t2.imagen as avatar_imagen
		FROM 
		$this->_name as t1
		INNER JOIN rating_reviews_avatar AS t2 ON t2.idx = t1.avatar
		where
		t1.comment_status_id='2' 
		order by t1.fecreg desc
		$lim
		";

        $consulta = ejecutar($sql);
        $num_reg = numRows($consulta);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: getListarLimit' );            
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            while ($fila = fetchAssoc($consulta)) {
                $this->contenidoListar[] = $fila;
            }
            return $this->contenidoListar;
        }
    }

}

?>