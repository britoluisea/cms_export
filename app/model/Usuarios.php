<?php
require_once("UsuariosVerify.php");
require_once 'p/system/phpthumb-latest/ThumbLib.inc.php';
require_once("app_email/functions/func_email.php");
require_once("app_email/classes/mime.php");
require_once("app_email/classes/email.php");

class App_Model_Usuarios {

    protected $_name = 'usuarios';
    private $usuario = array();
    private $contenido = array();
    private $contenidoVerify = array();
    private $contenidoByIdVerifyActive = array();

    public function urls_amigables($url) {
        //$url = strtolower($url);
        $url = mb_convert_case($url, MB_CASE_LOWER, "UTF-8");
        $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $repl = array('a', 'e', 'i', 'o', 'u', 'n');
        $url = str_replace($find, $repl, $url);
        $find = array(' ', '&', '\r\n', '\n', '+');
        $url = str_replace($find, '-', $url);
        $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
        $repl = array('', '-', '');
        $url = preg_replace($find, $repl, $url);
        return $url;
    }

    public function subir_imagen_tmp($name_subir, $temp) {
        $name_subir = "img_" . rand(100, 999) . $name_subir;
        ini_set("memory_limit", "300M");
        set_time_limit(0);
        copy($temp, "tmp/$name_subir");
        return $name_subir;
    }

    public function adaptiveResizeSmall($ruta_imagen_tmp, $wruta_save, $wancho, $walto, $wanchosmall, $waltosmall) {
        ini_set("memory_limit", "300M");
        set_time_limit(0);
        $datos = getimagesize("tmp/" . $ruta_imagen_tmp);
        $ext = "jpg";
        if ($datos[2] == 1) {
            $ext = "gif";
        }
        if ($datos[2] == 2) {
            $ext = "jpg";
        }
        if ($datos[2] == 3) {
            $ext = "png";
        }
        $nombre_foto2 = "img_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999) . "_" . rand(100, 999);

        $thumb = PhpThumbFactory::create("tmp/" . $ruta_imagen_tmp);
        $thumb->adaptiveResize($wancho, $walto);
        $thumb->save($wruta_save . "/" . $nombre_foto2 . "." . $ext, $ext);

        //small	
        $thumb = PhpThumbFactory::create("tmp/" . $ruta_imagen_tmp);
        $thumb->adaptiveResize($wanchosmall, $waltosmall);
        $thumb->save($wruta_save . '/small/' . $nombre_foto2 . "." . $ext, $ext);

        return $nombre_foto2 . "." . $ext;
    }

    public function login(array $data,$opc=null) {
	/*	echo "--->".$opc;
       echo "<pre>";
          print_r($data);
          echo "</pre>"; 
		  exit();*/
        $_SESSION['user']['login'] = false;
        $usuarios = addslashes($data['username']);
        $claves = md5(md5(addslashes($data['pwd'])));
        $sql = "SELECT * FROM usuarios where activo='1' and username = '$usuarios' and claves = '$claves'  limit 0,1";
        $consulta = ejecutar($sql);
        if ($fila = fetchAssoc($consulta)) {
            //$this->usuario[]=$fila;


            if ($fila["verif_email"] == 1) {
                unset($_SESSION['user']);
                $_SESSION['user']['login'] = true;
                $_SESSION['user']['idx'] = $fila['idx'];
                $_SESSION['user']['username'] = $fila['username'];
                $_SESSION['user']['nombres'] = $fila['nombres'];
                $_SESSION['user']['apellidos'] = $fila['apellidos'];
                $_SESSION['user']['imagen'] = $fila['imagen'];
				$_SESSION['user']['id_estado'] = $fila['id_estado'];
				if($opc==1)
				{
				?>
                	<script>
					 //self.parent.location.reload();
					 //self.parent.tb_remove(true);
					 self.parent.tb_remove();
        			 self.parent.llamar();
				  </script>
                <?php
				}
				else
				{
                	header("Location: my-account.php");
				}
				exit();
            } else {
                unset($_SESSION['user']);
                $_SESSION['user']['idx_code'] = $fila['idx'];
                $_SESSION['user']['username_code'] = $fila['username'];
                $_SESSION['user']['nombres_code'] = $fila['nombres'];
                $_SESSION['user']['apellidos_code'] = $fila['apellidos'];
				if($opc==1)
				{
					header("Location: my-account-active-account-p.php");
				}
				else
				{
                	header("Location: my-account-active-account.php");
				}
 
                exit();
            }
        } else {
            $_SESSION['user']['login'] = false;
            $_SESSION['user']['error'] = "Invalid username or password.";
			if($opc==1)
			{
				header("Location: login-p.php");
			}
			else
			{
				header("Location: login.php");
			}
            exit();
        }
    }

    public function add(array $data,$opc=null) {
       /* echo "<pre>";
          print_r($data);
          echo "</pre>"; 
		  exit();*/
        $urlDomain = str_replace('www.', '', $_SERVER['SERVER_NAME']);
        $username = $data['username'];
        $pwd = $data['pwd'];
        $nombres = $data['nombres'];
        $apellidos = $data['apellidos'];
        $genero = $data['genero'];
        $anio = $data['anio'];

        $username = trim(strtolower($username));
        $wusername = explode('@', $username);
        $slug = $this->urls_amigables($wusername[0]);

        $fecreg = date("Y-m-d H:i:s");
        $claves = md5(md5($pwd));
        $wips = getenv('REMOTE_ADDR');
		$avatar = $data['avatar'];
		
		$telefono = $data['telefono'];
		$direccion = $data['direccion'];
		$ciudad = $data['ciudad'];
		$id_estado = $data['id_estado'];
		$zipcode = $data['zipcode'];
		
         $sql_0 = "insert into usuarios (
		 idxamusuario, username, claves,nombres,apellidos,genero, anio, userlevel , fecreg, slug, verif_email, telefono, direccion, ciudad, id_estado, zipcode, ips, avatar, activo)
		 values 
		 ('1','$username','$claves','$nombres','$apellidos','$genero','$anio','3','$fecreg','$slug', '0', '$telefono', '$direccion', '$ciudad', '$id_estado', '$zipcode', '$wips', '$avatar', '1')";
        if (!$res_0 = ejecutar($sql_0)) {
            //echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            $_SESSION['user']['error'] = "This username or email already exist!. Please enter a different one or contact your Administrator for assistance.";
            header("Location: register.php");
            exit();
        }
        $widx = obtener_idx($sql_0);
        $token = rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(10, 99);
        $wips = getenv('REMOTE_ADDR');
        $sql = "insert into usuarios_verify   (idxamusuario, user_id, token, ips, fecreg)
								   values  ('1', '$widx', '$token', '$wips', '$fecreg')";
        if (!$res = ejecutar($sql)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            exit();
        }
        if ($_SERVER['HTTP_HOST'] <> 'localhost') {
            define('EMAIL_TRANSPORT', 'sendmail'); // 'sendmail' or 'smtp'
            define('EMAIL_LINEFEED', 'LF'); // 'LF' OR 'CRLF'
            define('EMAIL_USE_HTML', 'true'); //'true' OR 'false'
            //define('ENTRY_EMAIL_ADDRESS_CHECK', 'false'); // REVISAR
            define('SEND_EMAILS', 'true'); //'true' or 'false'
            // charset for web pages and emails
            //define('CHARSET', 'iso-8859-1');
            define('CHARSET', 'utf-8');
            // email classes
            //*****************************************************************************************************************
            $sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = ejecutar($sql_company);
            $atabla_company = array();
            while ($fila_company = fetchAssoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }

            ob_start(); //Abrimos el buffer			
            include ("app_email/register-email.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
            $wnombreasunto = "Confirm Registration";
            $wemp_compania = $atabla_company['en']["compania"];
            $wemp_email = $atabla_company['en']["username"];
            $email_text_subject = $wemp_compania . " - " . $wnombreasunto;
            $store_owner = ''; // COLOCAR SIEMPRE BLANCO
            //$store_owner_email_address = 'Alex Marcial Photo<demodemo92@yahoo.com>';
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            $to_name = $nombres . ' ' . $apellidos;
            $wemail = $username;
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            //tep_mail("Omar calderón", "calderon_edsel@hotmail.com", EMAIL_TEXT_SUBJECT, "Bien enviastes tu mensaje", STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            tep_mail($to_name, $wemail, $email_text_subject, $email_text, $store_owner, $store_owner_email_address);

            ob_start(); //Abrimos el buffer			
            include ("app_email/register-new-user-email.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
            $wnombreasunto = "New User Registration";
            $wemp_compania = $atabla_company['en']["compania"];
            $wemp_email = $atabla_company['en']["username"];
            $email_text_subject = $wemp_compania . " - " . $wnombreasunto;
            $store_owner = ''; // COLOCAR SIEMPRE BLANCO
            //$store_owner_email_address = 'Alex Marcial Photo<demodemo92@yahoo.com>';
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            $to_name = $atabla_company['en']["nombres"] . ' ' . $atabla_company['en']["apellidos"];
            $wemail = $atabla_company['en']["username"];
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            //tep_mail("Omar calderón", "calderon_edsel@hotmail.com", EMAIL_TEXT_SUBJECT, "Bien enviastes tu mensaje", STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            tep_mail($to_name, $wemail, $email_text_subject, $email_text, $store_owner, $store_owner_email_address);
        }
        $_SESSION['user']['saved'] = "Do not forget to check your email, we've sent you a link to activate your account.";
		   if($opc==1)
			{
				header("Location: login-p.php");
			}
			else
			{
				header("Location: login.php");
			}
        exit();

        /* $_SESSION['user']['login']			= true;
          $_SESSION['user']['idx'] = $widx;
          $_SESSION['user']['username'] = $username;
          $_SESSION['user']['nombres'] = $nombres;
          $_SESSION['user']['apellidos'] = $apellidos; */
    }

    public function getById() {
        $idxUser = isset($_SESSION['user']['idx']) ? $_SESSION['user']['idx'] : '';
        $sql = "SELECT *	FROM $this->_name where
		idx = '" . $idxUser . "' and
		activo='1'
		limit 0,1";
        $consulta = ejecutar($sql);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: ' );
            exit();
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            if ($fila = fetchAssoc($consulta)) {
                $this->contenido = $fila;
            }
            return $this->contenido;
        }
    }
	
	  public function getByIdUbigeo() {
		$sql="
		select t1.*,
		t2.nombre as ubigeoestadousa_nombre,
		t2.codigo as ubigeoestadousa_codigo
		from
		$this->_name AS t1
		LEFT JOIN ubigeoestadousa AS t2 ON t2.codigo = t1.id_estado
		where
		t1.idx = '" . $_SESSION['user']['idx'] . "' and
		t1.activo='1'
		limit 0,1";	
        $consulta = ejecutar($sql);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: ' );
            exit();
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            if ($fila = fetchAssoc($consulta)) {
                $this->contenido = $fila;
            }
            return $this->contenido;
        }
    }

    public function edit(array $data, array $file) {
	/*	 echo "<pre>";
          print_r($data);
		  print_r($file);
          echo "</pre>"; 
		  exit();*/
        $row = $this->getById();
        $nombre_foto = $row["imagen"];
        $temp = $file["tmp_name"];
        if ($temp <> "") {
            $wruta_save = "imgcms/users";
            $nombre_img = $file["name"];
            $wancho = 600;
            $walto = 450;
            $ruta_imagen_tmp = $this->subir_imagen_tmp($nombre_img, $temp);

            @unlink($wruta_save . "/" . $row["imagen"]);
            @unlink($wruta_save . "/small/" . $row["imagen"]);

            $wanchosmall = 52;
            $waltosmall = 52;
            $nombre_foto = $this->adaptiveResizeSmall($ruta_imagen_tmp, $wruta_save, $wancho, $walto, $wanchosmall, $waltosmall);
            @unlink("tmp/" . $ruta_imagen_tmp);
        }
        $username = $row['username']; //no editar
        $nombres = $data['nombres'];
        $apellidos = $data['apellidos'];
        $genero = $data['genero'];
        $anio = $data['anio'];
        $avatar = $data['avatar'];
        if ($genero == 1):
            $wname_genero = "Male";
        else:
            $wname_genero = "Female";
        endif;
		
		$telefono = $data['telefono'];
		$direccion = $data['direccion'];
		$ciudad = $data['ciudad'];
		$id_estado = $data['id_estado'];
		$zipcode = $data['zipcode'];
        //$username = trim(strtolower($username));
        //$wusername=explode('@',$username);
        //$slug = $this->urls_amigables($wusername[0]);

        $fecreg = date("Y-m-d H:i:s");
        $sql_0 = "update usuarios set  
			nombres = '$nombres',
			apellidos = '$apellidos',
			genero = '$genero',
			anio = '$anio',
			fecmod = '$fecreg',
			imagen = '$nombre_foto',
			avatar = '$avatar',
			telefono = '$telefono',
			direccion = '$direccion',
			ciudad = '$ciudad',
			id_estado = '$id_estado',
			zipcode = '$zipcode'
		 	where
		 	idx = '" . $_SESSION['user']['idx'] . "'  ";
		
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            $_SESSION['user']['error'] = "Error: User Exist.";
            header("Location: my-account-edit.php");
            exit();
        }
		$row = $this->getById();
		
        $_SESSION['user']['username'] = $username;
        $_SESSION['user']['nombres'] = $row['nombres'];
        $_SESSION['user']['apellidos'] = $row['apellidos'];
        $_SESSION['user']['imagen'] = $nombre_foto;
		$_SESSION['user']['id_estado'] = $row['id_estado'];
        $_SESSION['user']['saved'] = "Data Saved.";

        if ($_SERVER['HTTP_HOST'] <> 'localhost') {
            define('EMAIL_TRANSPORT', 'sendmail'); // 'sendmail' or 'smtp'
            define('EMAIL_LINEFEED', 'LF'); // 'LF' OR 'CRLF'
            define('EMAIL_USE_HTML', 'true'); //'true' OR 'false'
            //define('ENTRY_EMAIL_ADDRESS_CHECK', 'false'); // REVISAR
            define('SEND_EMAILS', 'true'); //'true' or 'false'
            // charset for web pages and emails
            //define('CHARSET', 'iso-8859-1');
            define('CHARSET', 'utf-8');
            // email classes
            //*****************************************************************************************************************
            $sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
            $consulta_company = ejecutar($sql_company);
            $atabla_company = array();
            while ($fila_company = fetchAssoc($consulta_company)) {
                $atabla_company['en'] = $fila_company;
            }

            ob_start(); //Abrimos el buffer			
            include ("app_email/register-edit.php");
            $email_text = ob_get_contents(); //Vaciamos el buffer
            ob_end_clean();
            $wnombreasunto = "Account Edit";
            $wemp_compania = $atabla_company['en']["compania"];
            $wemp_email = $atabla_company['en']["username"];
            $email_text_subject = $wemp_compania . " - " . $wnombreasunto;
            $store_owner = ''; // COLOCAR SIEMPRE BLANCO
            //$store_owner_email_address = 'Alex Marcial Photo<demodemo92@yahoo.com>';
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            $to_name = $atabla_company['en']["nombres"] . ' ' . $atabla_company['en']["apellidos"];
            $wemail = $atabla_company['en']["username"];
            $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
            //tep_mail("Omar calderón", "calderon_edsel@hotmail.com", EMAIL_TEXT_SUBJECT, "Bien enviastes tu mensaje", STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            tep_mail($to_name, $wemail, $email_text_subject, $email_text, $store_owner, $store_owner_email_address);
        }
        header("Location: my-account-edit.php");
        exit();
    }

    public function changePass(array $data) {
        /* echo "<pre>";
          print_r($data);
          echo "</pre>";
          exit(); */
        $pwd1 = md5(md5($data['pwd1']));

        $sql = "select * from usuarios where idx='" . $_SESSION['user']['idx'] . "' limit 0,1";
        $consulta = ejecutar($sql);
        if ($fila = fetchAssoc($consulta)) {
            $pwd_ant = $fila["claves"];
        }
        if ($pwd1 == $pwd_ant) {
            $pwd2 = md5(md5($data['pwd2']));
            $sql_upd = "update usuarios set claves='$pwd2' where idx='" . $_SESSION['user']['idx'] . "' ";
            $consulta_upd = ejecutar($sql_upd);
            $_SESSION['user']['saved'] = "Data Saved.";

            header("location: my-account-change-password.php");
            exit();
        } else {
            $_SESSION['user']['error'] = "Error Old Password.";
            header("location: my-account-change-password.php");
            exit();
        }
    }

    public function getByIdVerify($id) {
        $sql = "SELECT *	FROM $this->_name where
		idx = '$id' and
		verif_email='0'
		limit 0,1";
        $consulta = ejecutar($sql);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: ' );
            exit();
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            if ($fila = fetchAssoc($consulta)) {
                $this->contenidoVerify[] = $fila;
            }
            return $this->contenidoVerify;
        }
    }

    public function getVerifyActive($id) {
        $sql_0 = "update usuarios set
			verif_email = '1',
			activo = '1'
		 	where
		 	idx = '$id'  ";
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            exit();
        }
    }

    public function getByIdVerifyActive($id) {
        $sql = "SELECT *	FROM $this->_name where
		idx = '$id' and
		activo='1'
		limit 0,1";
        $consulta = ejecutar($sql);
        if (!$consulta) {
            throw new Exception('No Funciona la Consulta. El Error es el siguiente: ' );
            exit();
        } else {
            //Si la consulta es exitosa, el método regresa $resultado.
            if ($fila = fetchAssoc($consulta)) {
                $this->contenidoByIdVerifyActive = $fila;
            }
            return $this->contenidoByIdVerifyActive;
        }
    }

    public function enviarCorreoActivacion($opc=null) {
        if ($_SESSION['user']['idx_code'] <> "" and $_SESSION['user']['username_code'] <> "") {
            $urlDomain = str_replace('www.', '', $_SERVER['SERVER_NAME']);
            $nombres = $_SESSION['user']['nombres_code'];
            $apellidos = $_SESSION['user']['apellidos_code'];
            $mUsuariosVerify = new App_Model_UsuariosVerify();
            $mUsuariosVerify->deleteUserId($_SESSION['user']['idx_code']);

            $fecreg = date("Y-m-d H:i:s");
            $widx = $_SESSION['user']['idx_code'];
            $token = rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(10, 99);
            $wips = getenv('REMOTE_ADDR');
            $sql = "insert into usuarios_verify   (idxamusuario, user_id, token, ips, fecreg)
									   values  ('1', '$widx', '$token', '$wips', '$fecreg')";
            if (!$res = ejecutar($sql)) {
                echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
                exit();
            }
            if ($_SERVER['HTTP_HOST'] <> 'localhost') {
                define('EMAIL_TRANSPORT', 'sendmail'); // 'sendmail' or 'smtp'
                define('EMAIL_LINEFEED', 'LF'); // 'LF' OR 'CRLF'
                define('EMAIL_USE_HTML', 'true'); //'true' OR 'false'
                //define('ENTRY_EMAIL_ADDRESS_CHECK', 'false'); // REVISAR
                define('SEND_EMAILS', 'true'); //'true' or 'false'
                // charset for web pages and emails
                //define('CHARSET', 'iso-8859-1');
                define('CHARSET', 'utf-8');
                // email classes
                //*****************************************************************************************************************
                $sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
                $consulta_company = ejecutar($sql_company);
                $atabla_company = array();
                while ($fila_company = fetchAssoc($consulta_company)) {
                    $atabla_company['en'] = $fila_company;
                }

                ob_start(); //Abrimos el buffer			
                include ("app_email/register-email.php");
                $email_text = ob_get_contents(); //Vaciamos el buffer
                ob_end_clean();
                $wnombreasunto = "Confirm Registration";
                $wemp_compania = $atabla_company['en']["compania"];
                $wemp_email = $atabla_company['en']["username"];
                $email_text_subject = $wemp_compania . " - " . $wnombreasunto;
                $store_owner = ''; // COLOCAR SIEMPRE BLANCO
                //$store_owner_email_address = 'Alex Marcial Photo<demodemo92@yahoo.com>';
                $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
                $to_name = $nombres . ' ' . $apellidos;
                $wemail = $_SESSION['user']['username_code'];
                $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
                //tep_mail("Omar calderón", "calderon_edsel@hotmail.com", EMAIL_TEXT_SUBJECT, "Bien enviastes tu mensaje", STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
                tep_mail($to_name, $wemail, $email_text_subject, $email_text, $store_owner, $store_owner_email_address);
            }
            unset($_SESSION['user']);
            $_SESSION['user']['saved'] = "Do not forget to check your email, we've sent you a link to activate your account.";
			if($opc==1)
			{
				header("Location: login-p.php");
			}
			else
			{
            	header("Location: login.php");
			}
            exit();
        } else {
            $_SESSION['user']['error_correo'] = "Failed to send activation code.";
            header("Location: login.php");
            exit();
        }
    }

    public function forgotPassword(array $data,$opc=null) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit();
        $_SESSION['correo_enviado'] = 'NO';
        $usuarios = addslashes($data['username']);
        $sql3 = "SELECT * FROM usuarios where activo=1 and username = '$usuarios'" . " limit 0,1";
        $res3 = ejecutar($sql3, Conectar::conex());
        if ($col3 = fetchAssoc($res3)) {

            $sql4 = "delete from usuariosreset where username = '$usuarios' ";
            $res4 = ejecutar($sql4, Conectar::conex());
            $token = rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999);

            $sql4 = "insert into usuariosreset (username , token    , fecreg , fecmod , activo) values ( '$usuarios' , '$token', now(), now(), 1)";
            $res4 = ejecutar($sql4, Conectar::conex());


            $sql_1 = "select * from usuarioscompany where idx = '999' "; //Devuelve todos los Idiomas
            $consulta_1 =ejecutar($sql_1);
            $atabla = array();
            while ($fila_1 = fetchAssoc($consulta_1)) {
                $atabla['en'] = $fila_1;
            }

            if ($_SERVER['HTTP_HOST'] <> 'localhost') {
                define('EMAIL_TRANSPORT', 'sendmail'); // 'sendmail' or 'smtp'
                define('EMAIL_LINEFEED', 'LF'); // 'LF' OR 'CRLF'
                define('EMAIL_USE_HTML', 'true'); //'true' OR 'false'
                //define('ENTRY_EMAIL_ADDRESS_CHECK', 'false'); // REVISAR
                define('SEND_EMAILS', 'true'); //'true' or 'false'
                // charset for web pages and emails
                //define('CHARSET', 'iso-8859-1');
                define('CHARSET', 'utf-8');
                // email classes
                //*****************************************************************************************************************
                $sql_company = "select * from usuarios where userlevel = '999'  "; //Devuelve todos los Idiomas
                $consulta_company = ejecutar($sql_company);
                $atabla_company = array();
                while ($fila_company = fetchAssoc($consulta_company)) {
                    $atabla_company['en'] = $fila_company;
                }
                ob_start(); //Abrimos el buffer
                ?>
                <div>Please click on the url below to create a new password.<br /> <br /><div><a href="http://<?= $_SERVER['HTTP_HOST'] ?>/forgot-password.php?token=<?= $token ?>">http://<?= $_SERVER['HTTP_HOST'] ?>/forgot-password.php?token=<?= $token ?></a></div>
				<?php
                $email_text = ob_get_contents(); //Vaciamos el buffer
                ob_end_clean();
                $wnombreasunto = "Forgot Password";
                $wemp_compania = $atabla_company['en']["compania"];
                $wemp_email = $atabla_company['en']["username"];
                $email_text_subject = $wemp_compania . " - " . $wnombreasunto;
                $store_owner = ''; // COLOCAR SIEMPRE BLANCO
                //$store_owner_email_address = 'Alex Marcial Photo<demodemo92@yahoo.com>';
                $store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
                $to_name = $col3['nombres'] . ' ' . $col3['apellidos'];
                $wemail = $usuarios;
                //$store_owner_email_address = $wemp_compania . '<' . $wemp_email . '>';
                //tep_mail("Omar calderón", "calderon_edsel@hotmail.com", EMAIL_TEXT_SUBJECT, "Bien enviastes tu mensaje", STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
                tep_mail($to_name, $wemail, $email_text_subject, $email_text, $store_owner, $store_owner_email_address);
            }
            $_SESSION['correo_enviado'] = 'SI';
			if($opc==1)
			{
				header("Location: forgot-p.php");
			}
			else
			{
            	header("Location: forgot.php");
			}
            exit();
        } else {
            //echo $_SESSION['correo_enviado'];
            //$_SESSION['error_array'] = "Invalid username or password.";
            if($opc==1)
			{
				header("Location: forgot-p.php");
			}
			else
			{
            	header("Location: forgot.php");
			}
            exit();
        }
    }
	public function forgotPasswordChange(array $data,$id,$username)
	{
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		echo $id;
		exit();*/
		$wclaves = md5(md5($data['pwd2']));
		$sql_0 = "update usuarios set claves = '$wclaves' where idx = '$id'";
		if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            exit();
        }
		
		$sql_0 = "delete from usuariosreset where username = '$username'";
		if (!$res_0 = ejecutar($sql_0)) {
			echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
			exit();
		}
						
		$_SESSION['user']['saved'] = "Congratulations your password has been successfully updated.";
        header("Location: login.php");
		exit();
	}
	public function editShippingAddress(array $data) {
	 	/*  echo "<pre>";
          	print_r($data);
          echo "</pre>"; 
		  exit();
*/		
		
		$username = $row['username']; //no editar
        $nombres = $data['nombres'];
        $apellidos = $data['apellidos'];
      
		$telefono = $data['telefono'];
		$direccion = $data['direccion'];
		$ciudad = $data['ciudad'];
		$id_estado = $data['id_estado'];
		$zipcode = $data['zipcode'];
        //$username = trim(strtolower($username));
        //$wusername=explode('@',$username);
        //$slug = $this->urls_amigables($wusername[0]);

        $fecreg = date("Y-m-d H:i:s");
        $sql_0 = "update usuarios set  
			nombres = '$nombres',
			apellidos = '$apellidos',
			telefono = '$telefono',
			direccion = '$direccion',
			ciudad = '$ciudad',
			id_estado = '$id_estado',
			zipcode = '$zipcode'
		 	where
		 	idx = '" . $_SESSION['user']['idx'] . "'  ";
		
        if (!$res_0 = ejecutar($sql_0)) {
            echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
            $_SESSION['user']['error'] = "Error: User Exist.";
            header("Location: my-account-edit.php");
            exit();
        }
		$row = $this->getById();
	
        $_SESSION['user']['nombres'] = $row['nombres'];
        $_SESSION['user']['apellidos'] = $row['apellidos'];
		$_SESSION['user']['id_estado'] = $row['id_estado'];
   
        header("Location: store-confirm.php");
        exit();
    }
	
}
?>