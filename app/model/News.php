<?php
class App_Model_News
{
	protected $_name = 'news';
	
	
	private $contenidoListar= array();
	private $contenidoListarPag= array();
	private $contenido;
	
	public function getListar($slug)
	{
		$sql = "SELECT * FROM news_category where slug='$slug' and activo='1' ";
		$res=ejecutar($sql);
		if ($col=fecthAssoc($res))
		{
			$id=$col['idx'];
		}
		
	
		$sql="SELECT * FROM $this->_name where idx_articles_category='$id' and activo ='1' order by fecreg desc";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
				throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
				exit();
			}
			else
			{	
				while($fila=fecthAssoc($consulta))
				{ 
					$this->contenidoListar[] = $fila;
				}
			return $this->contenidoListar;	
		}
		

	}
	public function getListarPagination($slug, $limit)
	{
		$sql = "SELECT * FROM news_category where slug='$slug' and activo='1'";
		$res=ejecutar($sql);
		if ($col=fecthAssoc($res))
		{
			$id=$col['idx'];
			$sort=$col['sort'];
		}
		$wsort="desc";	
		if($sort==1)
		{
			$wsort="desc";
		}
		else
		{
			$wsort="asc";
		}
		$sql="SELECT * FROM $this->_name where idx_articles_category='$id' and activo ='1' order by fecreg $wsort ".$limit;
		
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{	
			while($fila=fecthAssoc($consulta))
			{ 
				$this->contenidoListarPag[] = $fila;
			}
			return $this->contenidoListarPag;
		}	
	}
	public function getListarFiltro($slug,$limit)
	{		
		$sql = "SELECT * FROM news_category where slug='$slug' and activo='1'";
		$res=ejecutar($sql);
		if ($col=fecthAssoc($res))
		{
			$id=$col['idx'];
		}
		
		$sql="SELECT * FROM $this->_name where idx_articles_category='$id' and activo='1' order by fecreg desc  limit 0,$limit";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{	
			while($fila=fecthAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;
		}
		
	}
	public function getById($slug)
	{	
		$sql="SELECT 
		$this->_name.*,
		news_category.nombre as cat_nombre,
		news_category.slug as cat_slug,
		news_category.descrip as cat_descrip,
		news_category.descrip_top as cat_descrip_top,
		news_category.descrip_bottom as cat_descrip_bottom,
		news_category.show_date as cat_show_date
		FROM
		$this->_name,
		news_category
		where
		news.idx_articles_category = news_category.idx and
		news.slug = '$slug' and
		news.activo='1' and
		news_category.activo='1'
		limit 0,1";
		$consulta=ejecutar($sql);
		$num_reg=numRows($consulta);
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			//Si la consulta es exitosa, el método regresa $resultado.
			if($fila=fecthAssoc($consulta))
				{ 
					$this->contenido[] = $fila;
				}
		    return $this->contenido;	
		}	
	} 
	public function getListarTagArticles($id)
	{

		$sql="
		SELECT news.*,
		news_detail.idx_articles as art_det_idx_articles,
		news_detail.idx_articles_tag as art_det_idx_articles_tag,
		news_tag.nombre as tag_nombre,
		news_tag.activo as tag_activo,
		news_category.idx as news_category_idx,
		news_category.show_date as news_category_show_date
		FROM
		news,news_detail,news_tag,news_category
		where
		news.idx = news_detail.idx_articles and
		news_detail.idx_articles_tag = '$id' and
		news_tag.idx = news_detail.idx_articles_tag and
		news_category.idx = news.idx_articles_category and
		news_tag.activo='1'
		order by
		news.fecreg desc
		";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fecthAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;	
		}
	
	}
	public function getListarHome($limit)
	{	
			$sql="
			SELECT news.*, news_category.nombre as cat_nombre, news_category.slug as cat_slug FROM
			news,news_category
			where
			news.idx_articles_category = news_category.idx and
			news.activo ='1' and
			news_category.activo ='1'
			order by 
			news.fecreg desc limit 0,$limit";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
				throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
				exit();
			}
			else
			{	
				while($fila=fecthAssoc($consulta))
				{ 
					$this->contenidoListar[] = $fila;
				}
			return $this->contenidoListar;	
		}
		

	}
}
?>