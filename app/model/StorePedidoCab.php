<?php
require_once("Usuarios.php");
require("p/plugins/phpmailer/class.phpmailer.php");
require("p/plugins/phpmailer/class.smtp.php");
define(_LGPANEL, "en");
define(_PRODUCTOMONEDASIMBOLODE, "$");

class App_Model_StorePedidoCab {

    protected $_name = 'store_pedido_cab';
    private $contenido = array();

    public function number_pad($number) {
        return str_pad((int) $number, 5, "0", STR_PAD_LEFT);
    }

    public function enviar_email($from, $fromName, $subject, $msgHtml, $addAddress, $addAddressComp) {
        $mail = new PHPMailer();
        $mail->From = $from;
        $mail->FromName = $fromName;
        $mail->Subject = $subject;
        $mail->AltBody = "";
        $mail->MsgHTML($msgHtml);
        //$mail->AddAttachment("adjunto.txt");
        //$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
        $mail->AddAddress($addAddress, $addAddressComp);
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';

        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            exit();
        } else {
            //echo "Message sent!";
        }
    }

    public function add(array $data) {
		/*header("Location: store-checkout.php");
		exit();*/
        if (!empty($_SESSION["carrito"])) {
           /* echo "<pre>";
            print_r($data);
            print_r($_SESSION["carrito"]);
            echo "</pre>";*/
           // exit();
            $wnumpedido = 0;
            $mes = date("m");
            $anio = date("Y");
            //$wmespro=$anio.$mes;
            $wmespro = ''; // ya no va nada porque se requiere que solo valla el numero
            //$sql="select * FROM menunumera where mespro='$wmespro' and idxamusuario = '$wi'";
            $sql = "select * FROM store_pedido_numera where idxamusuario = '1'";

            $consul = ejecutar($sql);
            $numreg = numRows($consul);
            if ($numreg == 0) {
                $sql = "insert into store_pedido_numera (idxamusuario,mespro,numpedido) value ('1','$wmespro',0)";
                $consul = ejecutar($sql);
            }

            //echo $numreg;
            //$sql="update menunumera set numpedido=numpedido+1 where mespro='$wmespro' and idxamusuario = '$wi'";
            $sql = "update store_pedido_numera set numpedido=numpedido+1 where idxamusuario = '1'";
            $consul = ejecutar($sql);
            ;
            //$sql="select * FROM menunumera where mespro='$wmespro' and idxamusuario = '$wi' ";
            $sql = "select * FROM store_pedido_numera where idxamusuario = '1' ";
            $consul = ejecutar($sql);
            if ($col = fetchArray($consul)) {
                $numpedido = $col["numpedido"];
            }

            $wfecha = date("Y-m-d H:i:s");
            $wip = $_SERVER["REMOTE_ADDR"];
            $numpedido = $this->number_pad($numpedido);
            // exit();
			
			$mUsuarios = new App_Model_Usuarios();
            $rowUsuarios=$mUsuarios->getById();
			
            $totalItems = 0;
            $subTotal = 0;
            $taxTotal = 0;
            $total = 0;

            $totalpeso1 = 0;
            $totalpeso2 = 0;
            foreach ($_SESSION["carrito"] as $item) :
                $taxItem = 0;
                if ($item["chk_tax"] == 1) {
                    $sql = "select * from store_tax where activo='1' and codigoestadousa = '" . $rowUsuarios['id_estado'] . "' order by item desc";
                    $consulta = ejecutar($sql);
                    if ($fila = fetchArray($consulta)) {
                        //  echo "Tab DB=>" . $fila['tax'];
                        //echo "<br>";
                        $totalPrecioCantid = $item["precio"] * $item["cantid"];
                        //echo "<br>";
                        $taxItem = ($totalPrecioCantid * $fila['tax']) / 100;
                    }
                }
                if ($item["peso_medida"] == 1) {
                    $totalpeso1 = $totalpeso1 + ($item["peso"] * $item["cantid"] );
                }
                if ($item["peso_medida"] == 2) {
                    $totalpeso2 = $totalpeso2 + ($item["peso"] * $item["cantid"] );
                }
                $totalItems = $totalItems + $item['cantid'];
                $subTotal = $subTotal + ($item["precio"] * $item["cantid"]);
                $taxTotal = $taxTotal + $taxItem;
            endforeach;

            $sqlTipEnv = "select * from store_tipo_envio as t1 where activo = '1' and idx = '" . $data['metodo_envio'] . "'   order by item DESC";
            $conTipEnv = ejecutar($sqltipEnv);
            $atipo_envio = array();
            if ($filaTipEnv = fetchArray($conTipEnv)) {
                $tipo_envio_idx = $filaTipEnv['idx'];
                $tipo_envio_nombre = $filaTipEnv['nombre'];
                //Weight  
                if ($filaTipEnv["opcion"] == 1) {
                    //lb
                    if ($filaTipEnv["pes_med"] == 1) {
                        //lb =kg * 2.2046
                        $x1 = $totalpeso1;
                        $x2 = $totalpeso2 * 2.204;
                        $wsubtotal_costo = $x1 + $x2;
                    }
                    //kg
                    if ($filaTipEnv["pes_med"] == 2) {
                        //kg =lb / 2.2046
                        $x1 = $totalpeso1 / 2.204;
                        $x2 = $totalpeso2;
                        $wsubtotal_costo = $x1 + $x2;
                    }
                }
                //Price
                if ($filaTipEnv["opcion"] == 2) {
                    $wsubtotal_costo = $subTotal;
                }
                //Quantity
                if ($filaTipEnv["opcion"] == 3) {
                    $wsubtotal_costo = $totalItems;
                }

                $costo_envio = explode("\n", $filaTipEnv["cos_env"]);
                $costo = 0.00;
                for ($ifor2 = 0; $ifor2 < count($costo_envio); $ifor2++) {
                    /* echo $ifor2;
                      echo "<br>"; */
                    $wcosto_envio = explode(":", $costo_envio[$ifor2]);
                    /* echo "<pre>";
                      print_r($wcosto_envio);
                      echo "</pre>"; */
                    if ($wsubtotal_costo <= $wcosto_envio[0]) {
                        $costo = $wcosto_envio[1] + $filaTipEnv["env_adi"];
                        break;
                    }
                }
            }
            $total = $subTotal + $taxTotal + $costo;

            $sql = "select * from store_pago as t1 where lg='en' and activo = '1' and idx = '" . $data['payment'] . "' limit 0,1";
            $con = ejecutar($sql);
            if ($fila = fetchArray($con)) {
                $pago_idx = $fila['idx'];
                $pago_nombre = $fila['nombre'];
            }

           
		
		
            $sql = "insert into store_pedido_cab 
(idxamusuario, numpedido, idx_status, idx_status_shipping,  total_items,   subtotal,      tax,     shipping,    total,  tipo_envio_idx,  tipo_envio_nombre,      pago_idx,    pago_nombre,  cliente_id, cliente_username, cliente_nombres, 												cliente_apellidos, cliente_direccion, cliente_ciudad, cliente_id_estado, cliente_zipcode, cliente_telefono, ips, orden_realizado, fec_emi, activo) 
value ('1', '$numpedido', '1', '1',     '$totalItems', '$subTotal', '$taxTotal', '$costo', '$total', '$tipo_envio_idx', '$tipo_envio_nombre', '$pago_idx', '$pago_nombre',  '".$rowUsuarios['idx']."', '".$rowUsuarios['username']."', '".$rowUsuarios['nombres']."', '".$rowUsuarios['apellidos']."', '".$rowUsuarios['direccion']."', '".$rowUsuarios['ciudad']."', '".$rowUsuarios['id_estado']."', '".$rowUsuarios['zipcode']."', '".$rowUsuarios['telefono']."',  '$wip', 'WEB', '$wfecha', '1')";

            if (!$consul = ejecutar($sql)) {
                echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
                exit();
            }
            
            $witem = 0;
            foreach ($_SESSION["carrito"] as $item) :
                $witem++;
                $sql_0 = "insert into 
store_pedido_det (idxamusuario,numpedido,item,producto_idx,producto_nombre,producto_codigo,producto_imagen,color_idx,color_nombre,size_idx,size_nombre,cantidad,precio,precio_desc,precio_ant, peso, peso_medida, chk_tax)
values 
('1','$numpedido','$witem','" . $item['id'] . "', '" . $item['nombre'] . "', '" . $item['codigo'] . "', '" . $item['imagen'] . "', '" . $item['color_idx'] . "', '" . $item['color_nombre'] . "', '" . $item['size_idx'] . "', '" . $item['size_nombre'] . "', '" . $item['cantid'] . "', '" . $item['precio'] . "', '" . $item['precio_descuento'] . "', '" . $item['precio_anterior'] . "', '" . $item['peso'] . "', '" . $item['peso_medida'] . "', '" . $item['chk_tax'] . "' )";
                if (!$res_0 = ejecutar($sql)) {
                    echo "$sql_0 <br>Error al insertar y/o modificar tabla " ;
                    exit();
                }
            endforeach;
			
			$_SESSION["totalItems"]=$totalItems;
			$_SESSION["total"]=$total;
            $_SESSION['numpedido'] = $numpedido;
			$_SESSION['pago_idx'] = $pago_idx;
            //exit();
            if ($_SERVER['HTTP_HOST'] <> 'localhost') {

                $sql_company = "select * from usuarios where userlevel = '999'  ";
                $consulta_company = ejecutar($sql_company);
                $atabla_company = array();
                while ($fila_company = fetchAssoc($consulta_company)) {
                    $atabla_company['en'] = $fila_company;
                }

                ob_start(); //Abrimos el buffer			
                include ("_files/store/mail/pedido.php");
                $email_text = ob_get_contents(); //Vaciamos el buffer
                ob_end_clean();


                $wSubject = 'Your order has been placed';
//Cliente
                $this->enviar_email($atabla_company['en']["username"], $atabla_company['en']["compania"], $wSubject, $email_text, $rowUsuarios['username'], $rowUsuarios['nombres'] . ' ' . $rowUsuarios['apellidos']);

//Dueño de la cuenta
                $this->enviar_email($rowUsuarios['username'], $rowUsuarios['nombres'] . ' ' . $rowUsuarios['apellidos'], $wSubject, $email_text, $atabla_company['en']["username"], $atabla_company['en']["compania"]);

//Store / Order Notification
                $sql_1 = "select * from store_notification_order where idx='1' ";
                $consulta_1 = efecutar($sql_1);
                if ($fila_1 = fetchArray($consulta_1)) {
                    for ($ifor = 1; $ifor <= 3; $ifor++) {
                        $wusername = $fila_1['email_' . $ifor];
                        if (!empty($wusername)) {
                            $this->enviar_email($username, $nombres . ' ' . $apellidos, $wSubject, $email_text, $wusername, $atabla_company['en']["compania"]);
                        }
                    }
                }

                /* $mail = new PHPMailer();
                  $mail->From = $username;
                  $mail->FromName = $nombres.' '.$apellidos;
                  $mail->Subject = 'Your order has been placed�?�?';
                  $mail->AltBody = "";
                  $mail->MsgHTML($email_text);
                  //$mail->AddAttachment("adjunto.txt");
                  //$mail->AddAddress("calderon_edsel@hotmail.com", "Usuario Prueba");
                  $mail->AddAddress($atabla_company['en']["username"], $atabla_company['en']["compania"]);
                  $mail->IsHTML(true);
                  $mail->CharSet = 'UTF-8'; */

//Cliente

                /* 	if (!$mail->send()) {
                  echo "Mailer Error: " . $mail->ErrorInfo;
                  exit();
                  } else {
                  //echo "Message sent!";
                  } */
            }
			
			
            unset($_SESSION["carrito"]);
			unset($_SESSION["shippingId"]);
			unset($_SESSION["taxTotal"]);
			unset($_SESSION["shippingTotal"]);
			unset($_SESSION["subTotal"]);
			
			
			
            header("Location: store-enjoy.php");
            exit();
        } else {
            header("Location: store-catalog.php");
            exit();
        }
    }

}
?>