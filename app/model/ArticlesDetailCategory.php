<?php
class App_Model_ArticlesDetailCategory
{
	protected $_name = 'articles_detail_category';
	
	
	private $contenidoListar= array();
	private $contenido= array();	
	public function getListar()
	{
		$sql="SELECT * FROM $this->_name where activo='1' order by item desc";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;	
		}
	
	}

	public function getListarPostedIn($id)
	{
		$sql="
		SELECT $this->_name.*, articles_category.nombre as tag_nombre, articles_category.slug as tag_slug 
		FROM
		$this->_name,articles_category 
		where 
		$this->_name.idx_articles_category=articles_category.idx and
		$this->_name.idx_articles='$id' and
		articles_category.activo='1'
		order by 
		articles_category.item desc
		";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenidoListar[] = $fila;
			}
			return $this->contenidoListar;	
		}
	
	}
	
	
	
	public function getById($id)
	{
		$sql="SELECT * FROM $this->_name where idx = '$id'";
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			if($fila=fetchAssoc($consulta))
			{ 
				$this->contenido[] = $fila;
			}
			return $this->contenido;
		}	
	} 
	
		
	public function getByIdCategory($id, $limit)
	{
		$sql="SELECT * FROM $this->_name where idx_articles_category = '$id'".$limit;
		$consulta=ejecutar($sql);
		if(!$consulta)
		{
			throw new Exception('No Funciona la Consulta. El Error es el siguiente: ');
			exit();
		}
		else
		{
			while($fila=fetchAssoc($consulta))
			{ 
				$this->contenido[] = $fila;
			}
			return $this->contenido;
		}	
	} 

}
?>