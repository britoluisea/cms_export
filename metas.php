<?php
//clases
require_once ("app/model/Informativo.php"); 
require_once ("app/model/Webmarketing.php");
$mInformativo = new App_Model_Informativo();
$mWebmarketing = new App_Model_Webmarketing();
$urlCanonical = SERVER;
//validar favicon
$favicon="imgcms/system/favicon.ico";
if(!is_file($favicon))
{
	echo '<link rel="icon" href="'.SERVER.'imgcms/system/icon-cms.png" type="image/png" sizes="16x16">';
}
else
{
	echo '<link rel="icon" href="'.SERVER.$favicon.'" type="image/x-ico" sizes="16x16">';
}
echo "\n";
	if($url_request['request_file'] == 'index.php')
	{
		$sql_cons=" SELECT t1.id, t1.title, t1.keywords, t1.description FROM informativo as t1 INNER JOIN meta_page  as t2 ON  t1.id=t2.id_page  INNER JOIN router AS t3 ON t1.idx_router=t3.idx where t3.file='".$url_request['request_file']."' and t2.active=1 ";
		$query_cons= ejecutar($sql_cons);
		if($num_cons=numRows($query_cons) > 0)
		{
			$custom=fetchAssoc($query_cons);
			$id = $custom['id'];
				echo '<title>'.$custom['title'].'</title>';
				echo "\n";
				echo '<meta property="og:type" content="website" />';
				echo "\n";
				echo '<meta name="keywords" content="'.$custom['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$custom['description'].'">';
				echo "\n";
	     		echo '<meta property="og:description" content="'.$custom['description'].'" /> ';
				echo "\n";
			$sql_cons=" select img from img_cover_page where id_page='".$id."' ";
			$query_cons= ejecutar($sql_cons);
			if($num_cons=numRows($query_cons) > 0)
			{
				$img=fetchAssoc($query_cons);
				$img_cover=$img['img'];
				if(!empty($img_cover)){
					echo '<meta property="og:image" content="'.SERVER.$img_cover.'"/>';
					echo "\n";
				}
			}
	     		echo '<meta property="og:url" content="'.SERVER.$url_request['request_url'] .'" />';
			echo "\n";	
				$share_fb='
					    <div id="fb-root"></div>
					    <script>(function(d, s, id) {
					      var js, fjs = d.getElementsByTagName(s)[0];
					      if (d.getElementById(id)) return;
					      js = d.createElement(s); js.id = id;
					      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
					      fjs.parentNode.insertBefore(js, fjs);
					    }(document, "script", "facebook-jssdk"));</script>
					                        <div class="gallery-social">             
					                            <div style="z-index:1000;" class="fb-like" data-href="'.SERVER.$url_request['request_url'].'" data-send="true" data-layout="button_count" data-width="50" data-show-faces="true"></div>
					                        </div>
				';
		}
		else
		{
			$rowInf=$mInformativo->getByIdUrl($url_request['id_router']);
			$numInf=count($rowInf);
			if($numInf==0){
					$w_usamarketing	= 0;
				}
				else{
					$w_usamarketing	= $rowInf[0]['usamarketing'];
				}
			if ($w_usamarketing == 1)
			{
				echo '<title>'.$rowInf[0]['title'].'</title>';
				echo "\n";
				echo '<meta name="keywords" content="'.$rowInf[0]['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$rowInf[0]['description'].'">';
				echo "\n";
			}
			if ($w_usamarketing == 5 or $w_usamarketing == 0 )
			{	
				$rowWebMark=$mWebmarketing->getById(1);
				echo '<title>'.$rowWebMark[0]['title'].'</title>';
				echo "\n";
				echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
				echo "\n";
			}
		}
		$urlCanonical = SERVER.$url_request['request_url'];
	}
	elseif($url_request['id_router'] > 0 && $url_request['request_access']==0)
	{
		$sql_cons=" SELECT t1.id, t1.title, t1.keywords, t1.description FROM informativo as t1 INNER JOIN meta_page  as t2 ON  t1.id=t2.id_page where t1.id='".$url_request['id_router']."' and t2.active=1 ";
		//echo$sql_cons; exit;
		$query_cons= ejecutar($sql_cons);
		if($num_cons=numRows($query_cons) > 0)
		{
			$custom=fetchAssoc($query_cons);
			$id = $custom['id'];
				echo '<title>'.$custom['title'].'</title>';
				echo "\n";
				echo '<meta property="og:type" content="website" />';
				echo "\n";
				echo '<meta name="keywords" content="'.$custom['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$custom['description'].'">';
				echo "\n";
	     		echo '<meta property="og:description" content="'.$custom['description'].'" /> ';
				echo "\n";
			$sql_cons=" select img from img_cover_page where id_page='".$id."' ";
			$query_cons= ejecutar($sql_cons);
			if($num_cons=numRows($query_cons) > 0)
			{
				$img=fetchAssoc($query_cons);
				$img_cover=$img['img'];
				if(!empty($img_cover)){
					echo '<meta property="og:image" content="'.SERVER.$img_cover.'"/>';
					echo "\n";
				}
			}
	     	echo '<meta property="og:url" content="'.SERVER.$url_request['request_url'] .'" />';
			echo "\n";	
				$share_fb='
					    <div id="fb-root"></div>
					    <script>(function(d, s, id) {
					      var js, fjs = d.getElementsByTagName(s)[0];
					      if (d.getElementById(id)) return;
					      js = d.createElement(s); js.id = id;
					      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
					      fjs.parentNode.insertBefore(js, fjs);
					    }(document, "script", "facebook-jssdk"));</script>
					                        <div class="gallery-social">             
					                            <div style="z-index:1000;" class="fb-like" data-href="'.SERVER.$url_request['request_url'].'" data-send="true" data-layout="button_count" data-width="50" data-show-faces="true"></div>
					                        </div>
				';
		}
		else
		{
			$rowInf=$mInformativo->getByIdUrl($url_request['id_router']);
			$numInf=count($rowInf);
			if($numInf==0){
					$w_usamarketing	= 0;
				}
				else{
					$w_usamarketing	= $rowInf[0]['usamarketing'];
				}
			if ($w_usamarketing == 1)
			{
				echo '<title>'.$rowInf[0]['title'].'</title>';
				echo "\n";
				echo '<meta name="keywords" content="'.$rowInf[0]['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$rowInf[0]['description'].'">';
				echo "\n";
			}
			if ($w_usamarketing == 5 or $w_usamarketing == 0 )
			{	
				$rowWebMark=$mWebmarketing->getById(1);
				echo '<title>'.$rowWebMark[0]['title'].'</title>';
				echo "\n";
				echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
				echo "\n";
				echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
				echo "\n";
			}
		}
		$urlCanonical = SERVER.$url_request['request_url'];
	}
	elseif($url_request['request_access']==1)
	{
		$urlCanonical = SERVER.$url_request['request_url'];	
		if($uriRule['count'] >= 0)
		{
			if($uriRule['count'] ==2 && ($uriRule['sub'][0]=='gallerys' || $uriRule['sub'][0]=='galerias'))
			{
				$sql = "SELECT * FROM galerias_lista where slug = '".$uriRule['sub'][1]."' limit 0,1";
				$res=ejecutar($sql);
				if ($colMetas=fetchArray($res))
				{	
					$title_old='';
					$description_old='';
					if ($colMetas['usamarketing'] == 1)
					{
						echo '<title>'.$colMetas['title'].'</title>';
						echo "\n";
						echo '<meta name="keywords" content="'.$colMetas['keywords'].'">';
						echo "\n";
						echo '<meta name="description" content="'.$colMetas['description'].'">';
						echo "\n";
						$title_old=$colMetas['title'];
						$description_old=$colMetas['description'];
					}
					else
					{
						$rowWebMark=$mWebmarketing->getById(1);
						echo '<title>'.$rowWebMark[0]['title'].'</title>';
						echo "\n";
						echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
						echo "\n";
						echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
						echo "\n";
						$title_old=$rowWebMark[0]['title'];
						$description_old=$rowWebMark[0]['description'];
					}
					echo '<meta property="og:image" content="'.SERVER.'imgcms/galeria/'.$colMetas['nomcar'].'/thumbs/'.$colMetas['imagen'].'">';
					echo "\n";
					echo '<meta property="og:type" content="website" />';
					echo "\n";
					echo '<meta property="og:url" content="'.$urlCanonical.'">';
					echo "\n";
					echo '<meta property="og:title"   content="'.$title_old.'">';
					echo "\n";
					echo '<meta property="og:description" content="'.$description_old.'" /> ';
					echo "\n";
				}
			}
			elseif($uriRule['count'] >=2 && ($uriRule['sub'][0]=='projects' || $uriRule['sub'][0]=='proyectos'))
			{
				if($uriRule['count'] ==2)
				{
					$sql = "SELECT * FROM project_category where slug = '".$uriRule['sub'][1]."' ";
					$res=ejecutar($sql);
					if ($colMetas=fetchArray($res))
					{	
						$title_old='';
						$description_old='';
						if ($colMetas['usamarketing'] == 1)
						{
							echo '<title>'.$colMetas['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$colMetas['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$colMetas['description'].'">';
							echo "\n";
							$title_old=$colMetas['title'];
							$description_old=$colMetas['description'];
						}
						else
						{
							$rowWebMark=$mWebmarketing->getById(1);
							echo '<title>'.$rowWebMark[0]['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
							echo "\n";
							$title_old=$rowWebMark[0]['title'];
							$description_old=$rowWebMark[0]['description'];
						}
						echo '<meta property="og:image" content="'.SERVER.'imgcms/product_d/'.$colMetas['nomcar'].'/thumbs/'.$colMetas['imagen'].'">';
						echo "\n";
						echo '<meta property="og:type" content="website" />';
						echo "\n";
						echo '<meta property="og:url" content="'.$urlCanonical.'">';
						echo "\n";
						echo '<meta property="og:title"   content="'.$title_old.'">';
						echo "\n";
						echo '<meta property="og:description" content="'.$description_old.'" /> ';
						echo "\n";
					}

				}
				elseif($uriRule['count'] ==3)
				{
					$sql = "SELECT * FROM project_lista where slug = '".$uriRule['sub'][2]."' ";
					$res=ejecutar($sql);
					if ($colMetas=fetchArray($res))
					{	
						$title_old='';
						$description_old='';
						if ($colMetas['usamarketing'] == 1)
						{
							echo '<title>'.$colMetas['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$colMetas['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$colMetas['description'].'">';
							echo "\n";
							$title_old=$colMetas['title'];
							$description_old=$colMetas['description'];
						}
						else
						{
							$rowWebMark=$mWebmarketing->getById(1);
							echo '<title>'.$rowWebMark[0]['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
							echo "\n";
							$title_old=$rowWebMark[0]['title'];
							$description_old=$rowWebMark[0]['description'];
						}
						echo '<meta property="og:image" content="'.SERVER.'imgcms/product_d/'.$colMetas['nomcar'].'/thumbs/'.$colMetas['imagen'].'">';
						echo "\n";
						echo '<meta property="og:type" content="website" />';
						echo "\n";
						echo '<meta property="og:url" content="'.$urlCanonical.'">';
						echo "\n";
						echo '<meta property="og:title"   content="'.$title_old.'">';
						echo "\n";
						echo '<meta property="og:description" content="'.$description_old.'" /> ';
						echo "\n";
					}
				}
			}
			elseif($uriRule['sub'][0]=='blog')
			{
				if($uriRule['count'] ==2)
				{
					$sql = "SELECT * FROM articles where slug = '".$uriRule['sub'][1]."' ";
					$res=ejecutar($sql);
					if ($colMetas=fetchArray($res))
					{	
						echo '<title>'.$colMetas['nombre'].'</title>';
						echo "\n";
						echo '<meta name="description" content="'.$colMetas['descrip_share'].'">';
						echo "\n";						
						echo '<meta property="og:image" content="'.SERVER.'imgcms/articles/thumbs/'.$colMetas['imagen'].'">';
						echo "\n";
						echo '<meta property="og:type" content="article" />';
						echo "\n";
						echo '<meta property="og:url" content="'.$urlCanonical.'">';
						echo "\n";
						echo '<meta property="og:title"   content="'.$colMetas['nombre'].'">';
						echo "\n";
						echo '<meta property="og:description" content="'.$colMetas['descrip_share'].'" /> ';
						echo "\n";
					}
				}
				elseif($uriRule['count'] > 2)
				{
					$rowInf=$mInformativo->getByIdUrl($url_request['id_router']);
						$numInf=count($rowInf);
						if($numInf==0){
								$w_usamarketing	= 0;
							}
							else{
								$w_usamarketing	= $rowInf[0]['usamarketing'];
							}
						if ($w_usamarketing == 1)
						{
							echo '<title>'.$rowInf[0]['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$rowInf[0]['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$rowInf[0]['description'].'">';
							echo "\n";
						}
						if ($w_usamarketing == 5 or $w_usamarketing == 0 )
						{	
							$rowWebMark=$mWebmarketing->getById(1);
							echo '<title>'.$rowWebMark[0]['title'].'</title>';
							echo "\n";
							echo '<meta name="keywords" content="'.$rowWebMark[0]['keywords'].'">';
							echo "\n";
							echo '<meta name="description" content="'.$rowWebMark[0]['description'].'">';
							echo "\n";
						}
				}
			}
		}
	}
	$is_preview = (!isset($preview)) ? false : (($preview!=1) ? false : true );
	if($wactivowebsite == 2 && !isset($_SESSION['value_admin_idx']) && !$is_preview )
	{ ?>
			<style>body{opacity: 0;}</style>
		<?php
	}
	elseif($wactivowebsite == 0 && !isset($_SESSION['value_admin_idx']))
	{?>
		<style>body{opacity: 0;}</style>
		<?php
	}
	//echo'<pre>'; print_r($url_request); echo'</pre>';
	if($url_request['request_file']=='error.php')
	{
		echo'<meta name = "robots" content = "noindex, nofollow">';
		echo "\n";
	}
	else
	{
		if(isset($preview) && $preview==1){echo'<meta name = "robots" content = "noindex, nofollow">';}
		else{echo'<meta name = "robots" content = "index, follow">';}
		echo "\n";
	}
	$tag_location  =isset($dataGlobal['lang']) ? $dataGlobal['lang'] : 'en';
	$tag_location .='_'.strtoupper($tag_location);
	echo '<meta property="og:locale" content="'.$tag_location.'" />';
	echo "\n";
?>
<Link rel="canonical" href="<?=$urlCanonical?>">
<?php	if (isset($_SESSION['var_sec_admin']))	{?>
<link rel="stylesheet" href="<?=CSS_P?>editContent.css">
<?php	}?>
<link rel="stylesheet" href="<?=CSS_P?>website.css">
<?php include("inc/insert_head_css_js.php"); ?>